import multiprocessing
import os
import pickle
import sys
import warnings

from datetime import datetime
from unittest.mock import Mock

import numpy
import pandas
import pytest
import pyqtgraph
import xarray
from TROPOMIQt import data_utils
from TROPOMIQt import h5pyimport


def test_gen_dist_labels():
    # Anchorage and Fairbanks
    selection = ((-6668070.894364323, 9527059.557042453),
                 (-6424960.258420893, 8638587.965829486))
    labels = data_utils.gen_dist_labels(selection)
    # Should have one label
    assert len(labels) == 1

    dist_label = labels[0][0].textItem.toPlainText()
    expected_dist = '418km'
    assert dist_label == expected_dist

    label_loc = labels[0][0].pos()
    expected_loc = pyqtgraph.Point(-6546515.576393, 9082823.761436)
    assert label_loc == expected_loc


def test_calc_selected_area():
    # Big Island of Hawaii, roughly
    selection = [(-7214426.9632832995, 2180335.6057523596),
                 (-7258586.51214896, 2234256.7391041126),
                 (-7332030.814472899, 2270049.2155531207),
                 (-7336214.350681224, 2262146.98049295),
                 (-7327847.278264573, 2240764.462094841),
                 (-7338073.700107147, 2223100.6425485774),
                 (-7353878.1702274885, 2209155.5218541585),
                 (-7341327.561602511, 2177081.744256995),
                 (-7334819.8386117825, 2158488.24999777),
                 (-7338538.537463628, 2132922.1953913355),
                 (-7333890.163898821, 2123160.610905242),
                 (-7310648.29607479, 2108750.652854343),
                 (-7293914.151241487, 2134316.7074607774),
                 (-7265559.072496168, 2150586.014937599),
                 (-7257192.000079517, 2148726.665511677),
                 (-7238133.668463811, 2157093.737928328),
                 (-7216751.150065702, 2173827.8827616307)]
    area = data_utils.calc_selected_area(selection)
    # Google says 10,432 sq km. Selection is not perfect, obviously, but we are
    # close enough to trust this result.
    assert area == 10901


def test_calc_alt_mass_du_at_1km(test_file_path):
    alt = 1
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        data = xarray.open_dataset(os.path.join(test_file_path, 'loaded_data1.nc'))
    densities = data[f"SO2_number_density_{alt}km"]
    calc_results =\
        pandas.read_pickle(os.path.join(test_file_path, 'calc_results.pickle'))

    row = calc_results.loc[alt]
    with open(os.path.join(test_file_path, 'selection_mask.pickle'), 'rb') as file:
        selection_mask = pickle.load(file)

    pixel_areas = data['area'][selection_mask]

    expected_all_du = densities * 2241.15
    expected_alt_du = expected_all_du[selection_mask]
    expected_mass = 0.3516

    mass, du_val, all_du = data_utils.calc_alt_mass_du(row, densities, pixel_areas,
                                                       selection_mask)

    assert mass == expected_mass
    assert (du_val == expected_alt_du).all()
    assert (all_du == expected_all_du).all()


def test_calc_alt_mass_du_at_interpolated_3km(test_file_path):
    alt = 3
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        data = xarray.open_dataset(os.path.join(test_file_path, 'loaded_data1.nc'))
    densities = None
    pixel_areas = data['area']
    calc_results =\
        pandas.read_pickle(os.path.join(test_file_path, 'calc_results.pickle'))

    row = calc_results.loc[alt]
    with open(os.path.join(test_file_path, 'selection_mask.pickle'), 'rb') as file:
        selection_mask = pickle.load(file)

    expected_all_du = row['int_du']
    expected_alt_du = expected_all_du[selection_mask]
    expected_mass = 0.2646

    mass, du_val, all_du = data_utils.calc_alt_mass_du(row, densities, pixel_areas,
                                                       selection_mask)

    assert mass == expected_mass
    assert (du_val == expected_alt_du).all()
    assert all_du is expected_all_du


def test_load_file(test_file_path):
    file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file_path = os.path.join(test_file_path, file_name)
    
    # Test good load
    result = data_utils.load_file(file_path, "", "")
    assert result[0] is not None
    assert result[1] is None

    # Test bad file path
    result = data_utils.load_file(file_name, "", "")
    assert result[0] is None
    assert result[1] is not None

    # Test no data received, and callback
    cb = Mock()
    result = data_utils.load_file(file_path, "latitude>89", "", callback = cb)
    assert result[0] is None
    assert result[1] == "No data loaded. Check your filters and data files"
    assert cb.called


def test_flatten_data(binned_data):
    data = binned_data
    
    dims = dict(data.sizes)
    flat_data = data_utils.flatten_data(data)
    flat_dims = dict(flat_data.sizes)
    assert len(flat_dims) == 2
    assert flat_dims['time'] == dims['x'] * dims['y']

    data = binned_data.sel(file = 0)
    dims = dict(data.sizes)
    flat_data = data_utils.flatten_data(data)
    flat_dims = dict(flat_data.sizes)
    assert len(flat_dims) == 2
    assert flat_dims['time'] == dims['x'] * dims['y']


def test_parse_file_range():
    omps_file = "OMPS-NPP_NMSO2-PCA-L2_v1.1_2021m0101t005626_o00001_2021m0101t011113.h5"
    SO2_file = "SO2_202001011349_202001011456_other-stuffpytest .nc"

    # Test omps parsing
    df, dt = data_utils.parse_file_range(omps_file)
    omps_date = datetime(2021, 1, 1, 0, 56, 26)
    assert df == omps_date
    assert dt == omps_date

    # Test other parsing
    df, dt = data_utils.parse_file_range(SO2_file)
    so2_from = datetime(2020, 1, 1, 13, 49)
    so2_to = datetime(2020, 1, 1, 14, 56)
    assert df == so2_from
    assert dt == so2_to


def test_calc_other_values(generic_test_object, test_file_path):
    row = generic_test_object._calc_results.loc[3]
    total_mass = generic_test_object._calc_results.loc[3]['selected_mass']
    all_du = row.int_du

    with open(os.path.join(test_file_path, 'selection_mask.pickle'), 'rb') as file:
        selection_mask = pickle.load(file)

    alt_du_val = all_du[selection_mask]
    ret_dict = data_utils.calc_other_values(row, total_mass, all_du, alt_du_val)
    # since this is an interpolated altitude, we should get em_rate in
    # addition to the standard
    assert 'em_rate' in ret_dict
    expected = {'sel_mean_du': 2.32,
                'sel_max_du': 9.6,
                'sel_mean_perc': 99.0,
                'sel_max_perc': 100.0,
                'em_rate': 1587.6}
    assert ret_dict == expected
