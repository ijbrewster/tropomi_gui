from unittest.mock import Mock

import pyqtgraph

from PySide6.QtCore import QPoint

from TROPOMIQt import PlotGraphicsView


def test_init():
    pgv = PlotGraphicsView.PlotGraphicsView()
    assert pgv.clickTimer.isSingleShot()


def test_setMode():
    pgv = PlotGraphicsView.PlotGraphicsView()
    assert pgv.mode is PlotGraphicsView.Mode.PAN
    assert pgv._roi._locked is False

    pgv.setMode(PlotGraphicsView.Mode.WIND)
    assert pgv.mode is PlotGraphicsView.Mode.WIND
    assert pgv._roi._locked is True
    assert pgv.isInteractive() is True

    pgv.setMode(PlotGraphicsView.Mode.SELECT)
    assert pgv.mode is PlotGraphicsView.Mode.SELECT
    assert pgv._roi._locked is False
    assert pgv.isInteractive() is False


def test_mouse_enter_leave():
    pgv = PlotGraphicsView.PlotGraphicsView()
    assert pgv.mouseIn is False
    pgv.enterEvent(42)
    assert pgv.mouseIn is True
    pgv.leaveEvent(42)
    assert pgv.mouseIn is False


def test_mouse_click_event():
    pgv = PlotGraphicsView.PlotGraphicsView()
    windsSelected = Mock()
    pgv.windsSelected = windsSelected
    pgv.setMode(PlotGraphicsView.Mode.WIND)
    clickPoint = Mock()
    clickPoint.x.return_value = 42
    clickPoint.y.return_value = 16
    pgv.mouseClickEvent()
    assert windsSelected.emit.called

    fws = Mock()
    pgv.forwardWindsSelected = fws
    pgv.setMode(PlotGraphicsView.Mode.WINDF)
    pgv.mouseClickEvent()
    assert fws.emit.called

    pgv.setMode(PlotGraphicsView.Mode.MEASURE)
    pgv.mouseClickEvent(clickPoint)
    assert clickPoint.x.called
    assert clickPoint.y.called
    assert len(pgv._measure_sel) == 1

    clickPoint.reset_mock()
    clickPoint.x.return_value = 16
    clickPoint.y.return_value = 42

    pgv._clickPoint = clickPoint
    pgv.mouseClickEvent()
    assert clickPoint.x.called
    assert clickPoint.y.called
    assert len(pgv._measure_sel) == 2

    pgv.setMode(PlotGraphicsView.Mode.SELECT)
    pgv.mouseClickEvent()
    assert len(pgv._selection) == 1
    assert pgv._selection == [(16, 42)]


def test_select_region():
    pgv = PlotGraphicsView.PlotGraphicsView()
    ret = pgv.get_select_region()
    test_region = {'pos': (42.0, 16.0), 'size': (1.0, 1.0),
                   'angle': 0.0, 'closed': False, 'points': []}

    assert ret != test_region
    pgv.set_select_region(test_region)
    ret = pgv.get_select_region()
    assert ret == test_region

    pgv.setMode(PlotGraphicsView.Mode.MEASURE)
    ret = pgv.get_select_region()
    assert ret != test_region
    pgv.set_select_region(test_region)
    ret = pgv.get_select_region()
    assert ret == test_region


def test_region_changed_final():
    pgv = PlotGraphicsView.PlotGraphicsView()
    areaSelected = Mock()
    pgv.areaSelected = areaSelected
    pgv._region_changed_final()
    assert areaSelected.emit.called
    areaSelected.emit.reset_mock()
    pgv.setMode(PlotGraphicsView.Mode.MEASURE)
    pgv._region_changed_final()
    assert areaSelected.emit.called


def test_get_noise_roi():
    pgv = PlotGraphicsView.PlotGraphicsView()
    ret = pgv.get_noise_roi()
    expected = ([], 0, 0)
    assert ret == expected


def test_doubleClickEvent(monkeypatch):
    super_double = Mock()
    event = Mock()
    winds_cleared = Mock()
    selection_cleared = Mock()
    measure_cleared = Mock()
    zoom_data = Mock()
    monkeypatch.setattr(pyqtgraph.PlotWidget, "mouseDoubleClickEvent", super_double)
    pgv = PlotGraphicsView.PlotGraphicsView()
    pgv._selection = [(1, 2), (3, 4)]
    pgv.measureCleared = measure_cleared
    pgv.windsCleared = winds_cleared
    pgv.zoomData = zoom_data
    pgv.selectionCleared = selection_cleared
    pgv.setMode(PlotGraphicsView.Mode.ZOOM)
    pgv._on_mouseDoubleClickEvent(event)

    assert zoom_data.emit.called
    assert selection_cleared.emit.called
    assert not event.accept.called
    assert super_double.called

    selection_cleared.emit.reset_mock()
    super_double.reset_mock()

    pgv.setMode(PlotGraphicsView.Mode.MEASURE)
    pgv._on_mouseDoubleClickEvent(event)

    assert measure_cleared.emit.called
    assert event.accept.called
    assert not super_double.called

    measure_cleared.emit.reset_mock()
    event.accept.reset_mock()
    winds_cleared.reset_mock()

    pgv.setMode(PlotGraphicsView.Mode.WIND)
    pgv._on_mouseDoubleClickEvent(event)

    assert winds_cleared.emit.called
    assert event.accept.called
    assert super_double.called


def test_mouseReleaseEvent(monkeypatch):
    event = Mock()
    release_point = QPoint(42, 42)
    event.pos.return_value = release_point
    event.localPos.return_value = release_point

    item1 = Mock()
    item2 = Mock()
    item2_curve = Mock()
    item2.curve = item2_curve
    items = Mock(return_value = [item1, item2_curve])

    main_window = Mock()
    main_window.get_wind_plots.return_value = [item2]

    click_timer = Mock()
    click_timer.isActive.return_value = False

    super_release = Mock()

    monkeypatch.setattr(pyqtgraph.PlotWidget, "mouseReleaseEvent", super_release)
    pgv = PlotGraphicsView.PlotGraphicsView()
    
    mock_parent = Mock(return_value = main_window)
    pgv.window = mock_parent
    pgv.items = items
    pgv.clickTimer = click_timer

    pgv.mouseReleaseEvent(event)
    assert super_release.called

    super_release.reset_mock()
    items.return_value = [item1]
    pgv.mouseReleaseEvent(event)
    assert super_release.called

    pgv.setMode(PlotGraphicsView.Mode.MEASURE)
    pgv.mouseReleaseEvent(event)

    assert click_timer.start.called
    assert not click_timer.stop.called
    click_timer.isActive.return_value = True  # make this the second click
    pgv.mouseReleaseEvent(event)

    assert click_timer.stop.called

    click_timer.stop.reset_mock()
    pgv._clickPoint = None
    pgv.mouseReleaseEvent(event)
    assert click_timer.stop.called

    # Test if the mouse has moved significantly
    click_event = Mock()
    pgv.mouseClickEvent = click_event
    click_timer.stop.release_mock()
    pgv._clickPoint = QPoint(242, 242)
    pgv.mouseReleaseEvent(event)

    assert click_event.called


def test_mouseMoveEvent(monkeypatch):
    super_move = Mock()
    hover_timer = Mock()
    event = Mock()
    mouse_moved = Mock()
    monkeypatch.setattr(pyqtgraph.PlotWidget, "mouseMoveEvent", super_move)
    pgv = PlotGraphicsView.PlotGraphicsView()
    pgv.hoverTimer = hover_timer
    pgv.mouseMoved = mouse_moved

    pgv.mouseMoveEvent(event)
    hover_timer.start.assert_called_with(300)
    assert event.ignore.called
    assert super_move.called
    assert mouse_moved.emit.called


def test_mousePressEvent(monkeypatch):
    super_press = Mock()
    event = Mock()
    monkeypatch.setattr(pyqtgraph.PlotWidget, "mousePressEvent", super_press)
    pgv = PlotGraphicsView.PlotGraphicsView()
    pgv.setMode(PlotGraphicsView.Mode.SELECT)

    assert pgv.isInteractive() == False
    assert pgv.mousePressEvent(event)
    assert event.accept.called
    assert not super_press.called

    event.accept.reset_mock()
    pgv.setMode(PlotGraphicsView.Mode.PAN)

    pgv.mousePressEvent(event)
    assert super_press.called
    assert not event.accept.called


def test_mouseHoverEvent():
    mouse_hover = Mock()
    mouse_pos = QPoint(42, 42)
    map_from_global = Mock(return_value = mouse_pos)
    pgv = PlotGraphicsView.PlotGraphicsView()
    pgv.mapFromGlobal = map_from_global
    pgv.mouseHover = mouse_hover

    pos = pgv.getPlotItem().getViewBox().mapSceneToView(mouse_pos)
    pgv.mouseIn = True
    pgv.mouseHoverEvent()

    mouse_hover.emit.assert_called_with(pos)
