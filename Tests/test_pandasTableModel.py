from unittest.mock import Mock, call
import pandas
import numpy

import pytest
from PySide6.QtCore import Qt

from TROPOMIQt import pandasTableModel


@pytest.fixture
def data_table():
    fake_test_data = {
        'selected_mass': [42, 142, 242],
        'alt': [1, 7, 15],
        'sel_mean_du': [.1, 1, numpy.nan],
        'noise_mass': [numpy.nan, 42, 142],
        'adj_mass': [0, 0, 0],
    }

    df = pandas.DataFrame.from_dict(fake_test_data)
    df.set_index('alt', inplace = True)
    return df


@pytest.fixture
def tablemodel():
    model = pandasTableModel.PandasTableModel()
    return model


@pytest.fixture
def popmodel(data_table):
    model = pandasTableModel.PandasTableModel(data_table)
    return model


def test_rowCount(popmodel, data_table):
    assert popmodel.rowCount() == len(data_table)


def test_rowCount_nodata(tablemodel):
    tablemodel._df = None
    assert tablemodel.rowCount() == 0


def test_columnCount_nodata(tablemodel):
    tablemodel._df = None
    assert tablemodel.columnCount() == 0


def test_refreshView(monkeypatch, popmodel, data_table):
    sort = Mock()
    monkeypatch.setattr(popmodel._df, "sort_index", sort)

    data_change = Mock()
    header_data_changed = Mock()

    popmodel.dataChanged = data_change
    popmodel.headerDataChanged = header_data_changed

    popmodel.refreshView()
    assert sort.called
    assert data_change.emit.called
    header_data_changed.emit.assert_has_calls([call(Qt.Horizontal, 0, len(data_table.columns)),
                                               call(Qt.Vertical, 0, len(data_table))])

    header_data_changed.emit.reset_mock()
    popmodel.refreshView()

    header_data_changed.emit.assert_not_called()


def test_set_data_frame(data_table, tablemodel):
    assert tablemodel._df.empty
    layoutchanged = Mock()
    layoutabout_to_be_changed = Mock()
    tablemodel.layoutAboutToBeChanged = layoutabout_to_be_changed
    tablemodel.layoutChanged = layoutchanged
    tablemodel.setDataframe(data_table)
    assert tablemodel._df is data_table
    assert layoutchanged.emit.called
    assert layoutabout_to_be_changed.emit.called


def test_data(popmodel, data_table):
    mean_du_col = data_table.columns.get_loc('sel_mean_du')
    adj_mass_col = data_table.columns.get_loc('adj_mass')
    index = popmodel.index(1, mean_du_col)

    # All data is returned as a string for display purposes
    expected_data = str(data_table.iloc[1, 1])

    # Test some role other than display
    data = popmodel.data(index, Qt.EditRole)
    assert data is None

    # Test random data
    data = popmodel.data(index)
    assert data == expected_data

    # Test NaN data
    index = popmodel.index(2, mean_du_col)
    data = popmodel.data(index)
    assert data == ''

    # Noise should be nan for row 0, and valid for other rows
    index = popmodel.index(0, adj_mass_col)
    expected = str(data_table.iloc[0]['selected_mass'])
    data = popmodel.data(index)
    assert data == expected

    index = popmodel.index(1, adj_mass_col)
    expected = str(data_table.iloc[1]['selected_mass'] - data_table.iloc[1]['noise_mass'])
    data = popmodel.data(index)
    assert data == expected

    # Drop the noise column. No noise should just result in selected
    popmodel._df.drop(columns = 'noise_mass', inplace = True)
    expected = str(data_table.iloc[2]['selected_mass'])
    index = popmodel.index(2, adj_mass_col)
    data = popmodel.data(index)
    assert data == expected


def test_headerData(popmodel, data_table):
    col_name = data_table.columns[1]
    col_name = pandasTableModel.HEADER_MAP[col_name]
    row_index = str(data_table.index[1])

    # Try non-display role
    data = popmodel.headerData(1, Qt.Horizontal, Qt.EditRole)
    assert data is None

    data = popmodel.headerData(1, Qt.Horizontal)
    assert data == col_name

    data = popmodel.headerData(1, Qt.Vertical)
    assert data == row_index

    # Try a bad vertical index
    data = popmodel.headerData(42, Qt.Vertical)
    assert data is None
