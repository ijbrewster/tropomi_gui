from unittest.mock import Mock

import pyproj
import pytest

from TROPOMIQt.LatLonAxisItem import LatLonAxisItem
from TROPOMIQt import utils


def test_tickStrings():
    axis_item = LatLonAxisItem("bottom")

    transformer = pyproj.Transformer.from_proj(utils.lat_lon_proj,
                                               utils.mercator_proj)

    lon_value = -140
    lat_value = 40

    merc_x, merc_y = transformer.transform(lat_value, lon_value)
    strs = axis_item.tickStrings([merc_x], 1, 1)  # scale and spacing values are not used
    assert strs[0] == str(lon_value)

    axis_item = LatLonAxisItem("left")
    strs = axis_item.tickStrings([merc_y], 1, 1)
    assert strs[0] == str(lat_value)


def test_tickValues():
    transformer = pyproj.Transformer.from_proj(utils.lat_lon_proj,
                                               utils.mercator_proj)

    min_lon = -180
    max_lon = -140
    min_lat = 50
    max_lat = 70

    x_range, y_range = transformer.transform([min_lat, max_lat], [min_lon, max_lon])
    axis_item = LatLonAxisItem("left")
    lat_spacing = [(10, 0),
                   (2, 0),
                   (1, 0)
                   ]
    axis_item.setTickSpacing(levels = lat_spacing)
    size = 512
    ticks = axis_item.tickValues(y_range[0], y_range[1], size)
    ticks2 = axis_item.tickValues(y_range[1], y_range[0], size)
    # given the above parameters, this is the expected output.
    expected_ticks = [(10, [6413524.59416364, 8362698.548500749, 11028513.630920077]),
                      (2, [6766432.490645153, 7135562.567523375, 7522963.241265123,
                           7931049.576003141, 8821377.203064468, 9311318.355852628,
                           9837766.745029796, 10407332.515149962]),
                      (1, [6588066.570105612, 6948849.384826752, 7326837.715045549,
                           7724253.0281235725, 8143727.653997744, 8588415.031895077,
                           9062139.39009366, 9569603.113292381, 10116680.739456367,
                           10710847.024360336])]

    # We should get the same answer regardless of which order we pass the min/max
    assert ticks == ticks2

    for idx, item in enumerate(expected_ticks):
        level, expected_values = item
        act_level, act_values = ticks[idx]
        assert act_level == level
        assert act_values == pytest.approx(expected_values)

    # Test longitude
    axis_item = LatLonAxisItem("bottom")
    ticks = axis_item.tickValues(x_range[0], x_range[1], size)
    expected = [(2000000.0, [-10000000.0, -8000000.0, -6000000.0]),
                (1000000.0, [-9000000.0, -7000000.0])]
    assert ticks == expected
    axis_item.setTickSpacing(levels = [])
    ticks = axis_item.tickValues(x_range[0], x_range[1], size)
