from unittest.mock import Mock

import pytest
from PySide6.QtGui import QColor
from PySide6.QtWidgets import QFrame
from PySide6.QtSvgWidgets import QSvgWidget

from TROPOMIQt import svgbutton


@pytest.fixture
def button():
    button = svgbutton.QSvgButton()
    return button


def test_setActiveColor(button):
    color = QColor(42, 42, 42)
    button.setActiveColor(color)
    assert button._active_color is color
    assert button._active_color is not button._hover_color

    button._active_color = None
    button.setActiveColor(color, isHover = True)
    assert button._active_color is color
    assert button._hover_color is color


def test_set_frame_and_inactive(button):
    fake_frame = Mock()
    load_colored = Mock()
    button._load_colored = load_colored

    button.setFrame(fake_frame)
    assert button._frame is fake_frame

    button.setActive(False)

    assert button._isActive is False
    load_colored.assert_called_once_with(button._fill_color)
    fake_frame.setFrameShadow.assert_called_with(QFrame.Raised)


def test_enter_event(monkeypatch, button):
    super_enter = Mock()
    hover_color = QColor(42, 42, 42)
    load_colored = Mock()

    monkeypatch.setattr(QSvgWidget, "enterEvent", super_enter)
    button._load_colored = load_colored

    button.enterEvent(Mock())

    super_enter.assert_called()
    load_colored.assert_not_called()

    button._hover_color = hover_color
    super_enter.reset_mock()

    button.enterEvent(Mock())

    super_enter.assert_called_once()
    load_colored.assert_called_once_with(hover_color)


def test_leave_event(monkeypatch, button):
    super_leave = Mock()
    load_colored = Mock()
    hover_color = QColor(42, 42, 42)
    fill_color = QColor(142, 142, 142)
    active_color = QColor(242, 242, 242)

    monkeypatch.setattr(QSvgWidget, "leaveEvent", super_leave)
    button._load_colored = load_colored

    button.leaveEvent(Mock())

    super_leave.assert_called()
    load_colored.assert_not_called()

    super_leave.reset_mock()
    button._hover_color = hover_color
    button._fill_color = fill_color

    button.leaveEvent(Mock())

    load_colored.assert_called_once_with(fill_color)
    super_leave.assert_called_once()

    super_leave.reset_mock()
    load_colored.reset_mock()
    button._isActive = True
    button._active_color = active_color

    button.leaveEvent(Mock())
    load_colored.assert_called_once_with(active_color)
    super_leave.assert_called_once()


def test_mouseReleaseEvent(monkeypatch, button):
    super_release = Mock()
    clicked = Mock()

    monkeypatch.setattr(QSvgWidget, "mouseReleaseEvent", super_release)
    button.clicked = clicked

    button.mouseReleaseEvent(42)

    super_release.assert_called_once()
    clicked.emit.assert_called_once()
