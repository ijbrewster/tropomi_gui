from unittest.mock import Mock

import numpy
import pytest

from TROPOMIQt import load_thread


@pytest.fixture
def fake_test_data():
    data = {
        'latitude': numpy.asarray([42, 43, 44, 45, 46]),
        'longitude': numpy.asarray([-178, -179, -180, 179, 178]),
        'cloud_fraction': numpy.asarray([.10, .20, .30, .40, .50]),
        'SO2_column_number_density': numpy.asarray([.001, .01, .1, 1, numpy.nan]),
        'SO2_column_number_density_validity': numpy.asarray([100, 80, 60, 40, 0]),
        'sensor_zenith_angle': numpy.asarray([0, 20, 40, 60, 80]),
        'solar_zenith_angle': numpy.asarray([10, 30, 50, 70, 90]),
    }
    return data


@pytest.fixture
def empty_filter():
    filt = {
        'lng_from': None,
        'lng_to': None,
        'lat_from': None,
        'lat_to': None,
        'cloud_fraction': None,
        'density': None,
        'validity': None,
        'sensor_zenith': None,
        'solar_zenith': None,
    }
    return filt


def test_generate_path():
    path = [(2, 4), (4, 4), (4, 2), (2, 2)]
    qPainterpath = load_thread._generate_path(path)
    bounding_rect = qPainterpath.boundingRect()
    assert bounding_rect.bottom() == 4
    assert bounding_rect.left() == 2
    assert bounding_rect.right() == 4
    assert bounding_rect.top() == 2


def test_build_no_filter(fake_test_data, empty_filter):
    # Test with no filters
    # Last is false, because we always remove nan density
    expected = [True, True, True, True, False]
    gen_filt, err = load_thread._build_filter(fake_test_data, empty_filter, False)
    assert err is None
    assert (gen_filt == expected).all()


def test_build_lon_filter(fake_test_data, empty_filter):
    # Test with positive lng_from
    empty_filter['lng_from'] = 179
    empty_filter['lng_to'] = -179
    expected = [False, True, True, True, False]
    gen_filt, err = load_thread._build_filter(fake_test_data, empty_filter, False)
    assert err is None
    assert (gen_filt == expected).all()

    # Test all in same hemesphere
    empty_filter['lng_from'] = -180
    empty_filter['lng_to'] = -178
    expected = [True, True, True, False, False]
    gen_filt, err = load_thread._build_filter(fake_test_data, empty_filter, False)
    assert err is None
    assert (gen_filt == expected).all()


def test_build_lat_filter(fake_test_data, empty_filter):
    # Filter on latitude only
    empty_filter['lat_from'] = 43
    empty_filter['lat_to'] = 45
    expected = [False, True, True, True, False]
    gen_filt, err = load_thread._build_filter(fake_test_data, empty_filter, False)
    assert err is None
    assert (gen_filt == expected).all()


def test_build_cloud_filter(fake_test_data, empty_filter):
    # Filter on cloud fraction
    empty_filter['cloud_fraction'] = 30
    expected = [True, True, False, False, False]
    gen_filt, err = load_thread._build_filter(fake_test_data, empty_filter, False)
    assert err is None
    assert (gen_filt == expected).all()


def test_build_du_filter(fake_test_data, empty_filter):
    # Filter on density at least DU
    empty_filter['density'] = 22.5
    expected = [False, False, True, True, False]
    gen_filt, err = load_thread._build_filter(fake_test_data, empty_filter, False)
    assert err is None
    assert (gen_filt == expected).all()


def test_build_validity_filter(fake_test_data, empty_filter):
    # Filter on validity at least 41
    empty_filter['validity'] = 41
    expected = [True, True, True, False, False]
    gen_filt, err = load_thread._build_filter(fake_test_data, empty_filter, False)
    assert err is None
    assert (gen_filt == expected).all()

    # And validty filter error
    del fake_test_data['SO2_column_number_density_validity']
    expected = [True, True, True, True, False]
    gen_filt, err = load_thread._build_filter(fake_test_data, empty_filter, False)
    assert err == 'Validity'
    assert (gen_filt == expected).all()


def test_build_sensor_zenith_filter(fake_test_data, empty_filter):
    # Filter on sensor zenith
    empty_filter['sensor_zenith'] = 40
    expected = [True, True, False, False, False]
    gen_filt, err = load_thread._build_filter(fake_test_data, empty_filter, False)
    assert err is None
    assert (gen_filt == expected).all()

    # And validty filter error
    del fake_test_data['sensor_zenith_angle']
    expected = [True, True, True, True, False]
    gen_filt, err = load_thread._build_filter(fake_test_data, empty_filter, False)
    assert err == 'Sensor Zenith'
    assert (gen_filt == expected).all()


def test_build_solar_zenith_filter(fake_test_data, empty_filter):
    # Filter on sensor zenith
    empty_filter['solar_zenith'] = 50
    expected = [True, True, False, False, False]
    gen_filt, err = load_thread._build_filter(fake_test_data, empty_filter, False)
    assert err is None
    assert (gen_filt == expected).all()

    # And validty filter error
    del fake_test_data['solar_zenith_angle']
    expected = [True, True, True, True, False]
    gen_filt, err = load_thread._build_filter(fake_test_data, empty_filter, False)
    assert err == 'Solar Zenith'
    assert (gen_filt == expected).all()


def test_processor_thread_run(raw_data, loaded_data, empty_filter):
    thread = load_thread.ProcessorThread(raw_data)
    assert thread.so2data is raw_data  # kinda pointless, but this *is* a test, so...

    show_msg = Mock()
    parent = Mock()
    parent().get_filters.return_value = empty_filter
    parent().is_stack.return_value = False

    thread.parent = parent
    thread.show_msg = Mock()

    thread.run()

    # Make sure the loaded data is what we expect
    assert thread.so2data == loaded_data

    # And that no messages have been attempted to be displayed
    assert not show_msg.emit.called


def test_processor_thread_run_with_error(raw_data, loaded_data, empty_filter):
    thread = load_thread.ProcessorThread(raw_data)

    # Remove the validity column
    del raw_data['SO2_column_number_density_validity']
    empty_filter['validity'] = 50  # Try to filter on a column that (now) doesn't exist

    show_msg = Mock()
    parent = Mock()
    parent().get_filters.return_value = empty_filter
    parent().is_stack.return_value = False

    thread.parent = parent
    thread.show_msg = show_msg

    thread.run()

    # Make sure the loaded data is what we expect
    assert thread.so2data == loaded_data

    # And that no messages have been attempted to be displayed
    assert show_msg.emit.called


def test_processor_thread_filter_all(raw_data, loaded_data, empty_filter):
    thread = load_thread.ProcessorThread(raw_data)

    # Apply a rediculous filter that removes all data
    empty_filter['validity'] = 101

    no_data = Mock()
    parent = Mock()
    parent().get_filters.return_value = empty_filter
    parent().is_stack.return_value = False

    thread.parent = parent
    thread.no_data = no_data

    thread.run()

    # Make sure the loaded data is what we expect
    assert thread.so2data == raw_data

    # And that no messages have been attempted to be displayed
    assert no_data.emit.called


def test_processor_thread_bad_data(raw_data, loaded_data, empty_filter):
    # Mess up the pixel bounds
    raw_data['latitude_bounds'] = (('y', 'x', 'space'), numpy.empty((0, 0, 4), dtype = float))
    raw_data['longitude_bounds'] = (('y', 'x', 'space'), numpy.empty((0, 0, 4), dtype = float))

    thread = load_thread.ProcessorThread(raw_data)

    no_data = Mock()
    parent = Mock()
    parent().get_filters.return_value = empty_filter
    parent().is_stack.return_value = False

    thread.parent = parent
    thread.no_data = no_data

    thread.run()

    # And that no messages have been attempted to be displayed
    assert no_data.emit.called


def test_processor_thread_no_cloud(raw_data, loaded_data, empty_filter):
    # Remove the cloud data
    raw_data = raw_data.drop_vars("cloud_fraction")

    thread = load_thread.ProcessorThread(raw_data)

    assert 'cloud_fraction' not in thread.so2data
    assert 'cloud' not in thread.so2data

    no_data = Mock()
    parent = Mock()
    parent().get_filters.return_value = empty_filter
    parent().is_stack.return_value = False

    thread.parent = parent
    thread.no_data = no_data

    thread.run()

    # Cloud should be added to the data even though it didn't exist
    assert 'cloud' in thread.so2data

    # And it should all be nan
    assert numpy.count_nonzero(~numpy.isnan(thread.so2data.cloud.data)) == 0


