from unittest.mock import Mock

import pytest

from PySide6.QtWidgets import QFileDialog

from TROPOMIQt import preferences


@pytest.fixture
def prefswin():
    win = preferences.PrefsWin()
    return win


def test_save(prefswin):
    settings = Mock()
    prefswin.settings = settings

    # Set some values so we can check
    prefswin._ui.leSavePath.setText("/test/save/path")
    prefswin._ui.leFileName.setText("TestFile")
    prefswin._ui.sbWidth.setValue(42)
    prefswin._ui.sbHeight.setValue(42)

    prefswin._savePreferences()

    settings.beginGroup.assert_called_with("preferences")
    settings.setValue.assert_any_call('path', '/test/save/path')
    settings.setValue.assert_any_call('resultFile', 'TestFile')
    settings.setValue.assert_any_call('imgWidth', 42)
    settings.setValue.assert_any_call('imgHeight', 42)
    settings.endGroup.assert_called()


def test_get_save_path(monkeypatch, prefswin):
    get_directory = Mock(return_value = None)
    monkeypatch.setattr(QFileDialog, "getExistingDirectory", get_directory)

    # set some random value to check against
    prefswin._ui.leSavePath.setText("/some/random/path")

    # test when user cancels
    prefswin._get_save_path()

    assert prefswin._ui.leSavePath.text() == "/some/random/path"

    get_directory.return_value = '/a/user/selected/path'
    prefswin._get_save_path()

    assert prefswin._ui.leSavePath.text() == '/a/user/selected/path'


def test_image_size_changed(prefswin):
    prefswin._imageSizeChanged(42)  # bad index
    assert prefswin._ui.sbWidth.isEnabled()
    assert prefswin._ui.sbHeight.isEnabled()

    prefswin._imageSizeChanged(1)  # good index (11,8)
    assert not prefswin._ui.sbWidth.isEnabled()
    assert not prefswin._ui.sbHeight.isEnabled()
    assert prefswin._ui.sbHeight.value() == 6
    assert prefswin._ui.sbWidth.value() == 9
