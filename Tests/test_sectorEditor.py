from unittest.mock import Mock

from TROPOMIQt import SectorEditor

from PySide6.QtWidgets import QInputDialog


def test_currentItemChanged():
    se = SectorEditor.SectorEditor(None)
    assert se._ui.twSectors.topLevelItemCount() == 2
    current_item = Mock()
    current_item.data.return_value = True

    se.on_twSectors_currentItemChanged(current_item, None)
    assert se._ui.bRemoveSector.isEnabled() == True
    assert se._ui.bEdit.isEnabled() == True


def test_removeSector():
    se = SectorEditor.SectorEditor(None)
    sectors_changed = Mock()
    twSectors = Mock()
    item = Mock()
    twSectors.currentItem.return_value = item

    item.data.return_value = False
    se._ui.twSectors = twSectors
    se.sectors_changed = sectors_changed

    se.on_bRemoveSector_clicked()
    assert not sectors_changed.emit.called

    item.data.return_value = True
    se.on_bRemoveSector_clicked()
    assert sectors_changed.emit.called


def test_edit(monkeypatch):
    id_getText = Mock(return_value = ("bob", False))
    monkeypatch.setattr(QInputDialog, "getText", id_getText)

    se = SectorEditor.SectorEditor(None)
    sectors_changed = Mock()
    twSectors = Mock()
    item = Mock()
    item.text.return_value = "Mr. Smith"
    twSectors.currentItem.return_value = item

    item.data.return_value = False
    se._ui.twSectors = twSectors
    se.sectors_changed = sectors_changed

    se.on_bEdit_clicked()
    assert not sectors_changed.emit.called

    # Ok, it behaves properly when the item is not editable, let's see what happens when it is
    item.data.return_value = True

    # but we'll cancel the new name dialog
    se.on_bEdit_clicked()

    assert not sectors_changed.emit.called

    # Ok, lets do it for real this time
    id_getText.return_value = ("bob", True)
    load_views = Mock(return_value = [{'name': 'Mr. Smith', }])
    monkeypatch.setattr(SectorEditor, "load_user_views", load_views)
    monkeypatch.setattr(SectorEditor, "save_user_views", Mock())
    se.on_bEdit_clicked()

    assert sectors_changed.emit.called
    item.setText.assert_called_with(0, 'bob')


def test_done_button():
    accept = Mock()
    se = SectorEditor.SectorEditor(None)
    se.accept = accept
    se.on_bDone_clicked()
    assert accept.called
