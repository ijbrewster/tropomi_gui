from unittest.mock import Mock

import numpy

from TROPOMIQt.file_formats import iasi
iasi._file_def = iasi.DEF


def test_iasi_process_with_so2_col():
    file = Mock()
    file._file_data = {
        'SO2_VCD': numpy.array(
            [
                [0, 1, 2, 94128.3, 4, 5, 6, 7, 8],
                [0, 1, 2, 94128.3, 4, 5, 6, 7, 8],
                [0, 1, 2, 94128.3, 4, 5, 6, 7, 8],
                [0, 1, 2, 94128.3, 4, 5, 6, 7, 8],
                [0, 1, 2, 94128.3, 4, 5, 6, 7, 8]
            ]
        ),
    }

    options = {'so2_column': '19km'}
    iasi._process_iasi_data(file, options)
    numpy.testing.assert_array_equal(file._file_data['SO2_column_number_density'],
                                     [42, 42, 42, 42, 42])
    assert 'SO2_VCD' not in file._file_data


def test_iasi_process_without_so2_col():
    file = Mock()
    file._file_data = {
        'SO2_VCD': numpy.array(
            [
                [94128.3, 1, 2, 3, 4, 5, 6, 7, 8],
                [94128.3, 1, 2, 3, 4, 5, 6, 7, 8],
                [94128.3, 1, 2, 3, 4, 5, 6, 7, 8],
                [94128.3, 1, 2, 3, 4, 5, 6, 7, 8],
                [94128.3, 1, 2, 3, 4, 5, 6, 7, 8]
            ]
        ),
    }

    options = {}
    iasi._process_iasi_data(file, options)
    numpy.testing.assert_array_equal(file._file_data['SO2_column_number_density'],
                                     [42, 42, 42, 42, 42])
    assert 'SO2_VCD' not in file._file_data
