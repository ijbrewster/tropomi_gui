import random

import numpy

from TROPOMIQt.file_formats import viirs


def test_viirs_pointtime():
    test_filename = 'V2021362205348.SO2AI_JPSS-1.h5'
    timestamp = viirs._viirs_pointtime(test_filename)
    expected = 1640724828.0
    assert timestamp == expected


def test_viirs_validity():
    # VIIRS doesn't have a validty, so this should always return 1
    assert viirs._viirs_validity(random.randint(-1000, 1000)) == 1


def test_viirs_density():
    values = numpy.array([25, 50, 75, 100, 125, 150])
    expected = (values * .2) / 2241.15
    expected[-1] = expected[-2] = expected[-3]
    result = viirs._viirs_density(values)

    assert result[-1] == result[-2] == result[-3] == 0.008923989915891394
    assert (result == expected).all()


def test_get_new_row():
    test_data = numpy.array(
        [
            [5, 10, 15, 20, numpy.nan, 25, 30],
            [10, 20, 30, numpy.nan, 40, 50, 60],
            [numpy.nan, numpy.nan, numpy.nan, numpy.nan,
             numpy.nan, numpy.nan, numpy.nan]
        ]
    )

    alt_data = numpy.roll(test_data, (1, 0), axis = (0, 1))

    result = viirs._get_new_row(test_data, alt_data, 1)
    assert result[0] == 10
    assert result[3] == 20
    assert numpy.isnan(result).any() == False


def test_make_bounds():
    test_longitude = numpy.asarray([
        [-999, -999, -999, -999, -999, -999, -999, -999, -999, -999],
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
    ], dtype = float)

    expected_lon_corners = numpy.array(
        [
            [[numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan]],

            [[-167.5, 177.5, 177.5, -167.5],
             [150., 162.5, -170., 177.5],
                [-147.5, -130., -152.5, -170.],
                [-150., -157.5, -160., -152.5],
                [-162.5, -170., -167.5, -160.],
                [-167.5, -172.5, -172.5, -167.5],
                [-170., -177.5, -180., -172.5],
                [-100., 170., 90., 180.],
                [-77.5, -80., 87.5, 90.],
                [87.5, -107.5, -107.5, 87.5]],

            [[-167.5, 177.5, 177.5, -167.5],
             [177.5, -170., -170., 177.5],
                [-170., -152.5, -152.5, -170.],
                [-152.5, -160., -160., -152.5],
                [-160., -167.5, -167.5, -160.],
                [-167.5, -172.5, -172.5, -167.5],
                [-172.5, -180., -180., -172.5],
                [180., 90., 90., 180.],
                [90., 87.5, 87.5, 90.],
                [87.5, -107.5, -107.5, 87.5]],

            [[-167.5, 177.5, 177.5, -167.5],
             [177.5, -170., 162.5, 150.],
                [-170., -152.5, -130., -147.5],
                [-152.5, -160., -157.5, -150.],
                [-160., -167.5, -170., -162.5],
                [-167.5, -172.5, -172.5, -167.5],
                [-172.5, -180., -177.5, -170.],
                [180., 90., 170., -100.],
                [90., 87.5, -80., -77.5],
                [87.5, -107.5, -107.5, 87.5]]])

    result = viirs._make_bounds(test_longitude)
    numpy.testing.assert_equal(result, expected_lon_corners)


def test_make_bounds_all_nan():
    test_longitude = numpy.asarray([
        [-999, -999, -999, -999, -999, -999, -999, -999, -999, -999],
        [-999, -999, -999, -999, -999, -999, -999, -999, -999, -999],
        [-999, -999, -999, -999, -999, -999, -999, -999, -999, -999],
        [-999, -999, -999, -999, -999, -999, -999, -999, -999, -999],
    ], dtype = float)

    expected_lon_corners = numpy.array(
        [
            [[numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan]],

            [[numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan]],

            [[numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan]],

            [[numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan]]
        ])

    result = viirs._make_bounds(test_longitude)
    numpy.testing.assert_equal(result, expected_lon_corners)


def test_make_nan():
    #Just a simple function to return an array of the same shape, full of NAN values
    test_array = numpy.random.rand(15, 32)
    result = viirs._make_nan(test_array)
    assert numpy.isnan(result).all()
    assert result.shape == (15, 32)


def test_make_corners_no_fill():
    test_longitude = numpy.asarray([
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
    ])

    expected_lon_corners = numpy.array(
        [
            [[-167.5, 177.5, 177.5, -167.5],
             [150., 162.5, -170., 177.5],
                [-147.5, -130., -152.5, -170.],
                [-150., -157.5, -160., -152.5],
                [-162.5, -170., -167.5, -160.],
                [-167.5, -172.5, -172.5, -167.5],
                [-170., -177.5, -180., -172.5],
                [-100., 170., 90., 180.],
                [-77.5, -80., 87.5, 90.],
                [87.5, -107.5, -107.5, 87.5]],

            [[-167.5, 177.5, 177.5, -167.5],
             [177.5, -170., -170., 177.5],
             [-170., -152.5, -152.5, -170.],
             [-152.5, -160., -160., -152.5],
             [-160., -167.5, -167.5, -160.],
             [-167.5, -172.5, -172.5, -167.5],
             [-172.5, -180., -180., -172.5],
             [180., 90., 90., 180.],
             [90., 87.5, 87.5, 90.],
             [87.5, -107.5, -107.5, 87.5]],

            [[-167.5, 177.5, 177.5, -167.5],
             [177.5, -170., -170., 177.5],
             [-170., -152.5, -152.5, -170.],
             [-152.5, -160., -160., -152.5],
             [-160., -167.5, -167.5, -160.],
             [-167.5, -172.5, -172.5, -167.5],
             [-172.5, -180., -180., -172.5],
             [180., 90., 90., 180.],
             [90., 87.5, 87.5, 90.],
             [87.5, -107.5, -107.5, 87.5]],

            [[-167.5, 177.5, 177.5, -167.5],
             [177.5, -170., 162.5, 150.],
             [-170., -152.5, -130., -147.5],
             [-152.5, -160., -157.5, -150.],
             [-160., -167.5, -170., -162.5],
             [-167.5, -172.5, -172.5, -167.5],
             [-172.5, -180., -177.5, -170.],
             [180., 90., 170., -100.],
             [90., 87.5, -80., -77.5],
             [87.5, -107.5, -107.5, 87.5]]
        ])

    result = viirs._make_bounds(test_longitude)
    numpy.testing.assert_equal(result, expected_lon_corners)
