"""test_utils.py - Tests for various functions in the utils files"""
import os
import pickle
from unittest.mock import Mock

import numpy
import pytest
import requests
from PySide6.QtWidgets import QMessageBox
from PySide6.QtCore import QStandardPaths

from TROPOMIQt import utils, __version__


def test_check_for_update_non_interactive(monkeypatch):
    infobox = Mock()
    monkeypatch.setattr(QMessageBox, 'information', infobox)
    monkeypatch.setattr(QMessageBox, 'critical', infobox)

    mock_dialog = Mock()
    mock_dialog_class = Mock(return_value = mock_dialog)

    monkeypatch.setattr(utils, "QDialog", mock_dialog_class)
    monkeypatch.setattr(utils, "QVBoxLayout", Mock())

    requests_get = Mock()
    bad_response = Mock()
    bad_response.status_code = 400  # Anything other than 200
    bad_response.text = "Test Result"
    requests_get.return_value = bad_response

    monkeypatch.setattr(requests, "get", requests_get)

    # Run test with bad response code
    utils.check_for_update(False)

    good_response = Mock()

    response_json = Mock(return_value = {'version': __version__,
                                         'update': False, })
    good_response.json = response_json
    good_response.status_code = 200

    requests_get.return_value = good_response

    # Run test with no update
    utils.check_for_update(False)

    mock_dialog.assert_not_called()

    response_json.return_value = {'version': __version__,
                                  'update': True,
                                  'notes': 'These are notes',
                                  'link': 'This is a link', }

    # Run test with update

    utils.check_for_update(False)
    mock_dialog.open.assert_called()


def test_check_for_update_interactive(monkeypatch):
    infobox = Mock()
    monkeypatch.setattr(QMessageBox, 'information', infobox)
    monkeypatch.setattr(QMessageBox, 'critical', infobox)

    requests_get = Mock()
    bad_response = Mock()
    bad_response.status_code = 400  # Anything other than 200
    bad_response.text = "Test Result"
    requests_get.return_value = bad_response

    monkeypatch.setattr(requests, "get", requests_get)

    # Run test
    utils.check_for_update(True)

    requests_get.assert_called()
    infobox.assert_called_once_with(None, "Unable to check",
                                    "An error occured while checking for updates\nTest Result")

    good_response = Mock()
    no_update_response = {'version': __version__,
                          'update': False, }
    response_json = Mock(return_value = no_update_response)
    good_response.json = response_json
    good_response.status_code = 200

    requests_get.return_value = good_response

    requests_get.reset_mock()
    infobox.reset_mock()

    # Run test
    utils.check_for_update(True)

    infobox.assert_called()
    infobox.assert_called_once_with(None, "No updates available",
                                    f"You have the most recent version ({__version__})")


def test_on_set_text_triggered():
    item = Mock()
    value = "Test Value"
    utils.on_setText_triggered(item, value)
    item.setText.assert_called_with(value)


def test_load_user_views(monkeypatch, test_file_path):
    mkdirs = Mock()
    monkeypatch.setattr(os, "makedirs", mkdirs)

    test_location = "/some/test/location"
    test_writable_location = Mock(return_value = test_location)
    monkeypatch.setattr(QStandardPaths, "writableLocation",
                        test_writable_location)

    res = utils.load_user_views()

    mkdirs.assert_called_once_with(test_location, exist_ok = True)

    # No such file, should return an empty list
    assert res == []

    test_writable_location.return_value = test_file_path
    mkdirs.reset_mock()

    # Test finding file
    res = utils.load_user_views()
    mkdirs.assert_called_once_with(test_file_path, exist_ok = True)

    # Found file, should return a non-empty list
    assert len(res) == 3

    test_views = [[47, -150, 1, 'Test'], ]
    loads = Mock(return_value = test_views)
    save = Mock()
    monkeypatch.setattr(pickle, "loads", loads)
    monkeypatch.setattr(utils, "save_user_views", save)

    res = utils.load_user_views()
    assert save.called
    expected = [{'name': 'Test',
                 'latFrom': 41.855,
                 'latTo': 51.691,
                 'longFrom': -158.983,
                 'longTo': -141.017,
                 'ul_lon': -159.9168853452995,
                 'ur_lon': -140.0831146547005,
                 'll_lon': -158.25349660088776,
                 'lr_lon': -141.74650339911224}]
    assert res == expected


def test_calc_sector_bounds():
    test_sector = {'longFrom': -140,
                   'longTo': -160,
                   'latFrom': 46,
                   'latTo': 48, }

    expected_result = {'longFrom': -140,
                       'longTo': -160,
                       'latFrom': 46,
                       'latTo': 48,
                       'ul_lon': -151.86809568733077,
                       'ur_lon': -148.13190431266923,
                       'll_lon': -151.79944567453217,
                       'lr_lon': -148.20055432546783}

    new_sector = utils.calc_sector_bounds(test_sector)
    assert new_sector == expected_result

    test_sector = {'longFrom': -190,
                   'longTo': -220,
                   'latFrom': 42,
                   'latTo': 60, }
    expected = {'longFrom': -190,
                'longTo': -220,
                'latFrom': 42,
                'latTo': 60,
                'ul_lon': 132.5,
                'ur_lon': 177.5,
                'll_lon': 139.8616317,
                'lr_lon': 170.1383682}

    new_sector = utils.calc_sector_bounds(test_sector)
    assert new_sector == pytest.approx(expected)


def test_sector_sort_key(monkeypatch):
    test_sector = {'longFrom': -140,
                   'longTo': -160,
                   'latFrom': 46,
                   'latTo': 48,
                   'ul_lon': -151.86809568733077,
                   'ur_lon': -148.13190431266923,
                   'll_lon': -151.79944567453217,
                   'lr_lon': -148.20055432546783,
                   'name': 'Test1', }

    expected_key = test_sector['ul_lon']
    key = utils.sector_sort_key(test_sector)
    assert key == expected_key

    # Test without all parameters
    calc_sector_bounds = Mock(return_value = test_sector)
    monkeypatch.setattr(utils, "calc_sector_bounds", calc_sector_bounds)
    incomplete_sector = test_sector.copy()
    del incomplete_sector['ul_lon']
    key = utils.sector_sort_key(incomplete_sector)
    calc_sector_bounds.assert_called_once_with(incomplete_sector)
    assert key == expected_key

    # Test with "large" sector
    test_sector['name'] = 'Full Arc'
    key = utils.sector_sort_key(test_sector)
    assert key == expected_key - 3000


def test_convert_view_format():
    test_view = [47, -150, 1, 'Test']
    expected_result = {'name': 'Test',
                       'latFrom': 41.855,
                       'latTo': 51.691,
                       'longFrom': -158.983,
                       'longTo': -141.017, }
    res = utils._convert_view_format(test_view)
    assert res == expected_result


def test_save_user_views(monkeypatch, test_file_path):
    test_writable_location = Mock(return_value = test_file_path)
    monkeypatch.setattr(QStandardPaths, "writableLocation",
                        test_writable_location)

    res = utils.load_user_views()

    test_writable_location.return_value = '/tmp'

    utils.save_user_views(res)

    # Reload it from /tmp and make sure it matches
    test = utils.load_user_views()
    assert test == res


def test_data_cache():
    with pytest.raises(ValueError):
        utils.DataCache("bob")  # No value attribute, should raise

    data_key = Mock()
    data_key.value.return_value = "one"
    test_cache = utils.DataCache(data_key)

    # Test set_data
    test_cache.set_data("Test Data One")
    test_data_two = "Test Data Two"
    test_cache.set_data(test_data_two)
    assert test_cache._my_data['one'] == test_data_two

    assert test_cache.data() == test_data_two

    data_key.value.return_value = "two"
    test_cache.set_data("Data for key two")
    assert "two" in test_cache._my_data
    test_cache.clear()
    assert "one" in test_cache._my_data
    assert "two" not in test_cache._my_data
    test_cache.clear_all()
    assert test_cache._my_data == {}
    with pytest.raises(AttributeError):
        test_cache.some_attr

    assert not test_cache
    test_cache.set_data(test_data_two)
    assert test_cache
    assert "two" in test_cache
    data = Mock()
    data.some_attr = test_data_two
    test_cache.set_data(data)
    assert test_cache.some_attr == test_data_two


def test_haversine_np():
    # From lyon to paris, and lyon to new york, examples from internet
    lons1 = numpy.asarray([4.8422, 4.8422])
    lats1 = numpy.asarray([45.7597, 45.7597])

    lons2 = numpy.asarray([2.3508, -74.2351462])
    lats2 = numpy.asarray([48.8567, 40.7033962])

    res = utils.haversine_np(lons1, lats1,
                             lons2, lats2)

    assert res == pytest.approx([391.97046653187857, 6159.5581919307915])
