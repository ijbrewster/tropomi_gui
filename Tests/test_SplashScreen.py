from unittest.mock import Mock
from TROPOMIQt import SplashScreen


def test_splash_screen():
    ss = SplashScreen.SO2SpashScreen()
    painter = Mock()
    ss.drawContents(painter)

    assert painter.setPen.called
    assert painter.setBrush.called
    assert painter.drawText.called
