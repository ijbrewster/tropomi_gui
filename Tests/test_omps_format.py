from datetime import datetime, timezone

import numpy
import pytest

from TROPOMIQt.file_formats import omps


def test_omps_pointtime():
    test_data = ['', '2021-03-16T11:45:42.1234Z']
    expected = [None, datetime(2021, 3, 16, 11, 45, 42, 123400, timezone.utc).timestamp()]
    result = omps._omps_pointtime(test_data)
    assert (result == expected).all()


def test_omps_validity():
    """Kind of a stupid function to test, but it gets us one step closer to 100% coverage"""
    arg = .58
    expected = 42
    result = omps._omps_validity(arg)

    # thank you floating point errors for requiring the "approx" function here
    assert result == pytest.approx(expected)


def test_omps_density():
    """convert OMPS DU values to density to match other file formats that provide density"""
    DU = 2
    density = .0008924
    result = omps._omps_density(DU)
    assert result == pytest.approx(density, rel = 1e7)


def test_make_bounds():
    test_longitude = numpy.asarray([
        [-999, -999, -999, -999, -999, -999, -999, -999, -999, -999],
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
    ], dtype = float)

    expected_lon_corners = numpy.array(
        [
            [[numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
                [numpy.nan, numpy.nan, numpy.nan, numpy.nan]],

            [[-167.5, 177.5, 177.5, -167.5],
             [150., 162.5, -170., 177.5],
                [-147.5, -130., -152.5, -170.],
                [-150., -157.5, -160., -152.5],
                [-162.5, -170., -167.5, -160.],
                [-167.5, -172.5, -172.5, -167.5],
                [-170., -177.5, -180., -172.5],
                [-100., 170., 90., 180.],
                [-77.5, -80., 87.5, 90.],
                [87.5, -107.5, -107.5, 87.5]],

            [[-167.5, 177.5, 177.5, -167.5],
             [177.5, -170., -170., 177.5],
                [-170., -152.5, -152.5, -170.],
                [-152.5, -160., -160., -152.5],
                [-160., -167.5, -167.5, -160.],
                [-167.5, -172.5, -172.5, -167.5],
                [-172.5, -180., -180., -172.5],
                [180., 90., 90., 180.],
                [90., 87.5, 87.5, 90.],
                [87.5, -107.5, -107.5, 87.5]],

            [[-167.5, 177.5, 177.5, -167.5],
             [177.5, -170., 162.5, 150.],
                [-170., -152.5, -130., -147.5],
                [-152.5, -160., -157.5, -150.],
                [-160., -167.5, -170., -162.5],
                [-167.5, -172.5, -172.5, -167.5],
                [-172.5, -180., -177.5, -170.],
                [180., 90., 170., -100.],
                [90., 87.5, -80., -77.5],
                [87.5, -107.5, -107.5, 87.5]]])

    result = omps._make_bounds(test_longitude)
    numpy.testing.assert_equal(result, expected_lon_corners)


def test_make_bounds_all_good():
    test_longitude = numpy.asarray([
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
        [-175, -190, -150, -155, -165, -170, -175, 175, 365, 170],
    ], dtype = float)

    expected_lon_corners = numpy.array(
        [
            [[-167.5, 177.5, 177.5, -167.5],
             [150., 162.5, -170., 177.5],
             [-147.5, -130., -152.5, -170.],
             [-150., -157.5, -160., -152.5],
             [-162.5, -170., -167.5, -160.],
             [-167.5, -172.5, -172.5, -167.5],
             [-170., -177.5, -180., -172.5],
             [-100., 170., 90., 180.],
             [-77.5, -80., 87.5, 90.],
             [87.5, -107.5, -107.5, 87.5]],

            [[-167.5, 177.5, 177.5, -167.5],
             [177.5, -170., -170., 177.5],
             [-170., -152.5, -152.5, -170.],
             [-152.5, -160., -160., -152.5],
             [-160., -167.5, -167.5, -160.],
             [-167.5, -172.5, -172.5, -167.5],
             [-172.5, -180., -180., -172.5],
             [180., 90., 90., 180.],
             [90., 87.5, 87.5, 90.],
             [87.5, -107.5, -107.5, 87.5]],

            [[-167.5, 177.5, 177.5, -167.5],
             [177.5, -170., -170., 177.5],
             [-170., -152.5, -152.5, -170.],
             [-152.5, -160., -160., -152.5],
             [-160., -167.5, -167.5, -160.],
             [-167.5, -172.5, -172.5, -167.5],
             [-172.5, -180., -180., -172.5],
             [180., 90., 90., 180.],
             [90., 87.5, 87.5, 90.],
             [87.5, -107.5, -107.5, 87.5]],

            [[-167.5, 177.5, 177.5, -167.5],
             [177.5, -170., 162.5, 150.],
             [-170., -152.5, -130., -147.5],
             [-152.5, -160., -157.5, -150.],
             [-160., -167.5, -170., -162.5],
             [-167.5, -172.5, -172.5, -167.5],
             [-172.5, -180., -177.5, -170.],
             [180., 90., 170., -100.],
             [90., 87.5, -80., -77.5],
             [87.5, -107.5, -107.5, 87.5]]])

    result = omps._make_bounds(test_longitude)
    numpy.testing.assert_equal(result, expected_lon_corners)


def test_make_bounds_all_nan():
    test_longitude = numpy.asarray([
        [-999, -999, -999, -999, -999, -999, -999, -999, -999, -999],
        [-999, -999, -999, -999, -999, -999, -999, -999, -999, -999],
        [-999, -999, -999, -999, -999, -999, -999, -999, -999, -999],
        [-999, -999, -999, -999, -999, -999, -999, -999, -999, -999],
    ], dtype = float)

    expected_lon_corners = numpy.array(
        [
            [[numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan]],

            [[numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan]],

            [[numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan]],

            [[numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan],
             [numpy.nan, numpy.nan, numpy.nan, numpy.nan]]
        ])

    result = omps._make_bounds(test_longitude)
    numpy.testing.assert_equal(result, expected_lon_corners)
