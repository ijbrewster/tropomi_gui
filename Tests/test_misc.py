import sys
import importlib
import os


def test_init(monkeypatch):
    monkeypatch.setattr(sys, "platform", "win32")
    import TROPOMIQt
    importlib.reload(TROPOMIQt)
    assert 'QT_QPA_PLATFORM_PLUGIN_PATH' in os.environ
