from unittest.mock import Mock

from pyqtgraph import LineSegmentROI, PolyLineROI

from TROPOMIQt import LockingPolyLineROI


def test_line_segment_setParentHover(qApp):
    line_seg = LockingPolyLineROI._LockingPolyLineSegment((42, 42))
    assert line_seg._parentHovering == False
    line_seg.setParentHover(True)
    assert line_seg._parentHovering == True


def test_line_segment_hoverEvent(monkeypatch):
    parent_hover = Mock()
    monkeypatch.setattr(LineSegmentROI, "hoverEvent", parent_hover)
    line_seg = LockingPolyLineROI._LockingPolyLineSegment((42, 42))
    event = Mock()
    line_seg.hoverEvent(event)
    parent_hover.assert_not_called()


def test_line_segment_mouseClickEvent(monkeypatch, qApp):
    parent_click = Mock()
    monkeypatch.setattr(LineSegmentROI, "mouseClickEvent", parent_click)
    line_seg = LockingPolyLineROI._LockingPolyLineSegment((42, 42))
    click_event = Mock()
    line_seg.mouseClickEvent(click_event)
    click_event.ignore.assert_called()
    parent_click.assert_not_called()


def test_line_segment_makePen(qApp):
    line_seg = LockingPolyLineROI._LockingPolyLineSegment((42, 42))
    hover_pen = Mock()
    normal_pen = Mock()
    line_seg.hoverPen = hover_pen
    line_seg.pen = normal_pen
    ret = line_seg._makePen()
    assert ret == normal_pen
    line_seg.mouseHovering = True
    ret = line_seg._makePen()
    assert ret == hover_pen


def test_LockingPolyLineROI_setLocked():
    roi = LockingPolyLineROI.LockingPolyLineROI([])
    assert roi._locked == False
    assert roi.translatable == True
    assert roi.rotatable == True
    assert roi.resizable == True
    roi.set_locked(True)
    assert roi._locked == True
    assert roi.translatable == False
    assert roi.rotatable == False
    assert roi.resizable == False


def test_LockingPolyLineROI_segmentClicked(monkeypatch):
    super_click = Mock()
    event = Mock()
    monkeypatch.setattr(PolyLineROI, "segmentClicked", super_click)
    roi = LockingPolyLineROI.LockingPolyLineROI([])
    ret = roi.segmentClicked(Mock(), event)
    assert super_click.called
    assert not event.ignore.called
    super_click.reset_mock()
    event.reset_mock()
    roi.set_locked(True)
    ret = roi.segmentClicked(Mock(), event)
    assert ret
    assert event.ignore.called
    assert not super_click.called


def test_LockingPolyLineROI_hoverEvent(monkeypatch):
    super_hover = Mock()
    event = Mock()
    monkeypatch.setattr(PolyLineROI, "hoverEvent", super_hover)
    roi = LockingPolyLineROI.LockingPolyLineROI([])
    ret = roi.hoverEvent(event)
    assert super_hover.called

    super_hover.reset_mock()
    event.reset_mock()
    roi._locked = True
    ret = roi.hoverEvent(event)
    assert ret is True
    assert not super_hover.called


def test_LockingPolyLineROI_mouseClickEvent(monkeypatch):
    super_click = Mock()
    event = Mock()
    monkeypatch.setattr(PolyLineROI, "mouseClickEvent", super_click)
    roi = LockingPolyLineROI.LockingPolyLineROI([])
    ret = roi.mouseClickEvent(event)
    assert super_click.called

    super_click.reset_mock()
    event.reset_mock()
    roi._locked = True
    ret = roi.mouseClickEvent(event)
    assert ret is True
    assert not super_click.called
    assert event.ignore.called


def test_LockingPolyLineROI_setMouseHover(monkeypatch, qApp):
    super_set_hover = Mock()

    monkeypatch.setattr(PolyLineROI, "setMouseHover", super_set_hover)
    roi = LockingPolyLineROI.LockingPolyLineROI([])
    roi.setMouseHover(True)
    super_set_hover.assert_called_with(True)
    roi._locked = True
    super_set_hover.reset_mock()
    roi.setMouseHover(True)
    super_set_hover.assert_called_with(False)


def test_LockingPolyLineROI_addSegment(monkeypatch, qApp):
    seg = Mock()
    seg.handles = [{'item': Mock(), }, {'item': Mock(), }]
    lpl = Mock(return_value = seg)
    monkeypatch.setattr(LockingPolyLineROI, "_LockingPolyLineSegment", lpl)
    roi = LockingPolyLineROI.LockingPolyLineROI([])
    roi.addSegment(1, 2)
    roi.addSegment(1, 2)
    assert len(roi.segments) == 2
    new_seg = Mock()
    new_seg.handles = [{'item': Mock(), }, {'item': Mock(), }]
    lpl.return_value = new_seg
    roi.addSegment(1, 2, 1)
    assert len(roi.segments) == 3
    assert roi.segments[1] is new_seg


def test_lineSegment_setParentHover_nochange():
    fake_self = Mock()
    fake_self._parentHovering = True
    func = LockingPolyLineROI._LockingPolyLineSegment.setParentHover
    func(fake_self, True) #Called with the same value _parentHovering currently has
    assert fake_self._parentHovering is True
    assert not fake_self._updateHoverColor.called