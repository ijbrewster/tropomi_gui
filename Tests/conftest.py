import multiprocessing
import os
import sys

from functools import partial
from threading import Lock, Event
import threading
from unittest.mock import Mock, patch

import numpy
import pandas
import pytest
import xarray
import zipfile

from PySide6.QtCore import (QCoreApplication,
                            Qt,
                            QDataStream,
                            QByteArray,
                            QObject,
                            QSettings)
from PySide6.QtGui import QPainterPath
from PySide6.QtWidgets import QApplication

from pyqtgraph import Point, PolyLineROI

TOP_PATH = os.path.join(os.path.dirname(__file__), '..')
sys.path.append(TOP_PATH)

from TROPOMIQt.MainWindow import mainWindow
from TROPOMIQt import h5pyimport, MainWindow
from TROPOMIQt.data_utils import flatten_data
from TROPOMIQt import utils


BASE_TEST_PATH = os.path.dirname(__file__)
TEST_FILE_PATH = os.path.join(BASE_TEST_PATH, 'TestFiles')
QCoreApplication.setAttribute(Qt.AA_ShareOpenGLContexts)


@pytest.fixture(autouse = True)
def qApp():
    """Create and return a QApplication object for testing.
    When testing is complete, quit the application cleanly."""
    QCoreApplication.setAttribute(Qt.AA_ShareOpenGLContexts)
    try:
        app = QApplication(sys.argv + ['-platform', 'offscreen'])
    except RuntimeError:
        # App already created. All good.
        app = None

    return app


@pytest.fixture(autouse = True)
def messenger_setup():
    utils.MESSAGE_QUEUE = multiprocessing.Queue()
    h5pyimport._TERM_FLAG = multiprocessing.Event()
    
@pytest.fixture
def window_obj():
    with patch.object(MainWindow, 'Thread', return_value = Mock()):
        mw = MainWindow.mainWindow()
        mw._quit_event.set()
    return mw


@pytest.fixture
def test_file_path():
    return TEST_FILE_PATH


@pytest.fixture
def tropomi_data_file(test_file_path):
    file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)
    return file


@pytest.fixture
def raw_data(test_file_path):
    file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)
    data = h5pyimport.import_product(file, options = "")
    data = flatten_data(data)
    return data


@pytest.fixture
def loaded_data(test_file_path):
    data = xarray.open_dataset(os.path.join(test_file_path, 'loaded_data1.nc'))
    return data


@pytest.fixture
def binned_data(test_file_path):
    data = xarray.open_dataset(os.path.join(test_file_path, 'binned_data.nc'))
    return data


@pytest.fixture
def pixel_paths(test_file_path):
    file_path = os.path.join(test_file_path, "PlotPaths.qtserial.zip")
    with zipfile.ZipFile(file_path, 'r') as zf:
        data_stream = QDataStream(QByteArray(zf.read(zf.namelist()[0])))
        size = data_stream.readInt64()
        p_paths = numpy.full((size, ), QPainterPath())

        for idx in range(size):
            path = QPainterPath()
            data_stream >> path
            p_paths[idx] = path

    return p_paths


@pytest.fixture
def generic_test_object():
    class GenericTestObject(QObject):
        """A MainWindow-ish object that doesn't actually instantiate the GUI"""

        def __init__(self):
            super().__init__()

            self.settings = QSettings()
            self._prev_filetype = 'TROPOMI'
            self._noise_coverages = []
            self._noise_area = numpy.nan
            self._calc_results_noise_areas = 0
            self._selected_volcs = set()
            self._save_plot = Mock()

            self.so2data = xarray.open_dataset(os.path.join(TEST_FILE_PATH,
                                                            'loaded_data1.nc'))

            self.plotScale = 1000  # Keep it fast here, we don't need a pretty picture

            # The selection we are using for our tests
            # sel = [(-7241726.932640292, 2198087.3140097405),
            #        (-7329835.251967947, 2195706.008081966),
            #        (-7320310.028256848, 2133792.0539598297),
            #        (-7234583.014856968, 2159986.419165349)]

            noise_roi_coords = [
                [Point(-7146474.695529, 2198087.314010),
                 Point(-7234583.014857, 2195706.008082),
                 Point(-7225057.791146, 2133792.053960),
                 Point(-7139330.777746, 2159986.419165)],
                [Point(-7336979.169751, 2198087.314010),
                 Point(-7425087.489079, 2195706.008082),
                 Point(-7415562.265368, 2133792.053960),
                 Point(-7329835.251968, 2159986.419165)],
                [Point(-7241726.932640, 2262382.574060),
                 Point(-7329835.251968, 2260001.268132),
                 Point(-7320310.028257, 2198087.314010),
                 Point(-7234583.014857, 2224281.679215)],
                [Point(-7241726.932640, 2133792.053960),
                 Point(-7329835.251968, 2131410.748032),
                 Point(-7320310.028257, 2069496.793910),
                 Point(-7234583.014857, 2095691.159115)]
            ]

            # Set some required variables on our test object
            noise_rois = [PolyLineROI(x, True) for x in noise_roi_coords]

            calc_results = pandas.read_pickle(os.path.join(TEST_FILE_PATH,
                                                           'calc_results.pickle'))

            xy_coords = xarray.open_dataarray(os.path.join(TEST_FILE_PATH,
                                                           'xy_coords.nc'))

            self._calc_results = calc_results
            self._noise_rois = noise_rois
            self.xy_coords = xy_coords
            self._calcProcs = 0
            self._stats_lock = Lock()
            self._stats_event = Event()
            self.so2mass = self.so2data['mass']
            self._percentile_levels = (90, 95, 97, 99, 100)
            self._percentile_widgets = [Mock()] * 5
            self._percentile_labels = [Mock()] * 5
            self._selected_area = 3972
            self._scale_min = 0
            self._scale_max = 20
            self.files = ['/Users/israel/Development/tropomi_gui/Tests/TestFiles/S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc']

            # Pull in the functions we need from mainwindow, and tie them to this object
            calc_mass = partial(mainWindow._calculate_area_mass, self)
            get_density = partial(mainWindow._get_interpolated_density,
                                  self)
            calc_filtered = partial(mainWindow._calculate_filtered_data,
                                    self)
            quick_stats = partial(mainWindow._calc_quick_stats,
                                  self)
            quick_stats_run = partial(mainWindow._run_calc_quick_stats,
                                      self)
            get_dates = partial(mainWindow._get_data_dates,
                                self)

            trigger_unlock = Mock()
            trigger_unlock.emit = partial(mainWindow._trigger_unlock_timer,
                                          self)

            unlock_stats = partial(mainWindow._unlock_stats,
                                   self)

            self._trigger_unlock = trigger_unlock
            self._calculate_area_mass = calc_mass
            self._get_interpolated_density = get_density
            self._calculate_filtered_data = calc_filtered
            self._calc_quick_stats = quick_stats
            self._run_calc_quick_stats = quick_stats_run
            self._unlock_stats = unlock_stats
            self._get_data_dates = get_dates
            self._view_corners = None

            # just mock this one, since we only call emit() on it, and that's a Qt
            # function, not one of ours.
            self._need_results_refresh = Mock()
            self._setText = Mock()

            # Mock up the GUI objects needed
            self._lSelectedArea = Mock()
            self._vb = Mock()

            # Some random view range. We may tweak this later.
            self._vb.viewRange = Mock(return_value = ([0, 10000],
                                                      [0, 10000]))

            self._ui = Mock()
            self._ui.cbAltitude.currentData = Mock(return_value = 7)
            self._ui.cbStack.isChecked = Mock(return_value = False)
            self._ui.hsFileSelector.value = Mock(return_value = 0)

    return GenericTestObject()
