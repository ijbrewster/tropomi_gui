from unittest.mock import Mock, patch, call

import pytest

from TROPOMIQt import ScaleWidget

from PySide6.QtCore import QRect

def test_init_scale_widget():
    sw = ScaleWidget.ScaleWidget(line_width = 42)
    assert sw._line_width == 42
    
def test_update_scale():
    sw = ScaleWidget.ScaleWidget()
    # Example values taken from real-world testing
    view_range = [[-11519325.496095132, -8498119.502262322],
                  [6122662.383835866, 7691915.327677879]]
    view_width = 1370.39375
    scale_width = 134.9 # rounded
    km_value = 180
    sw.update_scale(view_range, view_width)
    
    assert round(sw._pxwidth, 1) == scale_width
    assert sw._max == km_value
    #Make sure the width got set to the integer value of the desired width
    assert sw.width() == int(scale_width)
    
def test_update_under_10():
    sw = ScaleWidget.ScaleWidget()
    # Example values taken from real-world testing
    view_range = [
        [-9756856.512124788, -9612294.57065443],
        [6729155.21948881, 6804242.536501603]
    ]
    
    view_width = 1370.39375
    km_value = 9
    sw.update_scale(view_range, view_width)
    assert sw._max == km_value
    
    
def test_paintEvent():
    sw = ScaleWidget.ScaleWidget()
    sw._max = 180
    sw._pxwidth = 134
    sw.setFixedWidth(134)
    sw.setFixedHeight(27)
    
    painter = Mock()
    # values from real-world testing
    painter.boundingRect = Mock(return_value = QRect(0, 0, 41, 16))
    line_top = 18
    bottom = 27
    right = sw._pxwidth # Rounded
    with patch.object(ScaleWidget, 'QPainter', return_value = painter):
        sw.paintEvent(Mock())
        
    assert painter.end.called
    painter.drawLine.assert_has_calls([
        call(1, line_top, 1, bottom - 1),
        call(1, bottom - 1, right - 1, bottom - 1),
        call(right - 1, bottom - 1, right - 1, line_top)
    ])
    
    painter.drawText.assert_called()