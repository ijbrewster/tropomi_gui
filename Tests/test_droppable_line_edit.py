from unittest.mock import Mock

from TROPOMIQt.droppableLineEdit import droppableLineEdit


def test_drag_enter_event():
    event = Mock()
    event.mimeData().hasUrls.return_value = True
    le = droppableLineEdit(None)
    le.dragEnterEvent(event)
    assert event.acceptProposedAction.called
    assert not event.reject.called

    event.acceptProposedAction.reset_mock()
    event.mimeData().hasUrls.return_value = False
    le.dragEnterEvent(event)
    assert not event.acceptProposedAction.called
    assert event.reject.called


def test_dropEvent():
    event = Mock()
    event.mimeData().hasUrls.return_value = False
    le = droppableLineEdit(None)
    le.dropEvent(event)
    assert not event.acceptProposedAction.called

    setText = Mock()
    le.setText = setText
    event.mimeData().hasUrls.return_value = True
    event.mimeData().urls.return_value = [Mock()]
    le.dropEvent(event)

    assert event.acceptProposedAction.called
    assert setText.called
