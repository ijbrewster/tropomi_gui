from unittest.mock import Mock

import pytest

from TROPOMIQt import GradientScale


def test_init_gradient_scale():
    gs = GradientScale.GradientWidget()
    # Check that the last statement was run properly
    assert gs._orientation == 'Vertical'


def test_paint_event():
    labels = {0: 'start',
              1: 'stop', }
    gs = GradientScale.GradientWidget()
    gs.setLabels(labels)

    event = Mock()
    gs.paintEvent(event)

    # Test horizontal paintEvent
    gs.setOrientation('Horizontal')
    gs.paintEvent(event)


def test_set_gradient():
    gs = GradientScale.GradientWidget()
    gradient = Mock()
    gs.setGradient(gradient)
    assert gs.gradient == gradient


def test_set_orientation_bad():
    gs = GradientScale.GradientWidget()
    with pytest.raises(TypeError):
        gs.setOrientation("BadOrientation")

