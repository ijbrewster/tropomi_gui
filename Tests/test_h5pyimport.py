import logging
import os
import queue
import sys
import time

from copy import deepcopy
from datetime import datetime, timezone
from importlib import reload
import multiprocessing
from unittest.mock import Mock, patch

import h5py
import numpy
import xarray
import pytest

from TROPOMIQt import h5pyimport, utils, messenger
from TROPOMIQt.data_utils import parse_file_range


def test_point_date(test_file_path):
    file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)
    data = h5pyimport.import_product(file)

    dfrom, dto = parse_file_range(file_name)
    dfrom = dfrom.replace(tzinfo = timezone.utc)
    dto = dto.replace(tzinfo = timezone.utc)
    load_min = datetime.fromtimestamp(data['datetime_start'].min().item(),
                                      timezone.utc)
    load_max = datetime.fromtimestamp(data['datetime_start'].max().item(),
                                      timezone.utc)
    assert load_min >= dfrom
    assert load_max <= dto


def test_filters_on_import(test_file_path):
    file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    # test valid filter
    filter_ = "valid(SO2_column_number_density)"
    data = h5pyimport.import_product(file, filter_)
    assert not numpy.isnan(data['SO2_column_number_density']).any()

    filter_ = "longitude_range(-156,-154.7)"

    # ensure that we have data outside this range before filtering
    assert (data['longitude'] > -154.7).any() and (data['longitude'] < -156).any()
    original_size = data['longitude'].size

    data = h5pyimport.import_product(file, filter_)
    # And that some, at least, are filtered
    assert data['longitude'].size < original_size
    assert not (data['longitude'] > -154.7).any() and not (data['longitude'] < -156).any()

    # And with the values reversed, we should be *outside* the range
    filter_ = "longitude_range(-154.7,-156)"
    data = h5pyimport.import_product(file, filter_)
    assert not ((data['longitude'] <= -154.7) & (data['longitude'] >= -156)).any()

    # Bad filter field
    filter_ = "bobHajduk=42"
    with pytest.raises(ValueError):
        data = h5pyimport.import_product(file, filter_)


def test_term_flag_on_import(test_file_path):
    file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)

    term_flag = Mock()
    term_flag.is_set = Mock(return_value = True)
    progress_queue = Mock()
    sig_queue = Mock()
    sig_queue.get_nowait = Mock(return_value = True)

    h5pyimport._init_file_load_process(progress_queue, term_flag)
    res = h5pyimport.import_product(file)
    assert res == {}

    reload(h5pyimport)


def test_term_flag_on_filter(test_file_path):
    file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)

    term_flag = Mock()
    term_flag.is_set = Mock(side_effect = [False] * 28 + [True] * 10)
    progress_queue = Mock()

    h5pyimport._init_file_load_process(progress_queue, term_flag)
    res = h5pyimport.import_product(file, "latitude>-160")
    assert term_flag.is_set.call_count >= 28
    assert res == {}

    reload(h5pyimport)


def test_term_flag_at_end(test_file_path):
    file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)

    term_flag = Mock()
    term_flag.is_set = Mock(side_effect = [False] * 29 + [True] * 10)
    progress_queue = Mock()

    h5pyimport._init_file_load_process(progress_queue, term_flag)
    res = h5pyimport.import_product(file, "latitude>-160")
    assert term_flag.is_set.call_count >= 30
    assert res == {}

    reload(h5pyimport)


def test_def_with_from_name(test_file_path, caplog):
    file_name = "V2021362205348.SO2AI_JPSS-1.h5"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)
    h5pyimport._TERM_FLAG = multiprocessing.Event()

    from TROPOMIQt.file_formats import viirs
    res = h5pyimport._load_file_data(viirs.DEF, file)
    assert res.dims['y'] == 6448
    assert res.dims['x'] == 3200
    assert res.dims['corners'] == 4


def test_no_latitude_in_def(test_file_path, caplog):
    file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)
    from TROPOMIQt.file_formats import tropomi
    with patch.dict(tropomi.DEF['GROUPS'][0]['FIELDS'][0], {'NAME': 'bob', }):
        # We no longer have a 'latitude' field in our file def
        res = h5pyimport._load_file_data(tropomi.DEF, file)
        assert res == {}
        assert 'Latitude not found in file def' in caplog.text


def test_term_flag_on_load(test_file_path, caplog):
    file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)
    from TROPOMIQt.file_formats import tropomi

    term_flag = Mock()
    term_flag.is_set = Mock(return_value = True)
    progress_queue = Mock()

    # Test term init
    h5pyimport._init_file_load_process(progress_queue, term_flag)
    assert h5pyimport._TERM_FLAG == term_flag

    # Test term in load file data
    res = h5pyimport._load_file_data(tropomi.DEF, file)
    assert res == {}

    reload(h5pyimport)


def test_load_with_key_error(monkeypatch, test_file_path):
    file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    from TROPOMIQt.file_formats import tropomi

    mock_file = Mock()
    get_item = Mock(side_effect = KeyError)
    mock_file().__getitem__ = get_item

    monkeypatch.setattr(h5py, 'File', mock_file)
    res = h5pyimport._load_file_data(tropomi.DEF, file)
    assert get_item.called  # So we should get and exception
    assert res == {}  # but No run error


def test_signaler_quit(caplog):
    caplog.set_level(logging.DEBUG)
    signaler = messenger.Messenger()
    progress_queue = Mock()
    signaler._queue = progress_queue

    progress_queue.get.return_value = "QUIT"
    signaler.run()
    assert "due to QUIT" in caplog.text


def test_signaler_progress(caplog):
    caplog.set_level(logging.DEBUG)
    signaler = messenger.Messenger()
    mock_prog = Mock()
    mock_queue = Mock()

    signaler._queue = mock_queue
    signaler.progress = mock_prog
    signaler._final = 100

    mock_queue.get.side_effect = ["PROGRESS", "PROGRESS", "QUIT"]
    signaler.run()
    assert mock_prog.emit.call_count == 2
    assert "due to QUIT command" in caplog.text


def test_signaler_status(caplog):
    caplog.set_level(logging.DEBUG)
    signaler = messenger.Messenger()
    mock_status = Mock()
    progress_queue = Mock()

    signaler.status = mock_status
    signaler._queue = progress_queue

    progress_queue.get.side_effect = [['PROGRESS', 15], ['PROGRESS', 75], "QUIT"]
    signaler.run()
    assert mock_status.emit.call_count == 2
    assert "due to QUIT" in caplog.text
    signaler.deleteLater()


def test_monitor_thread_exit(caplog, monkeypatch):
    caplog.set_level(logging.DEBUG)

    stat_signal = Mock()
    progress_queue = Mock()
    progress_queue.get = Mock(side_effect = [('PROGRESS', 'testing'), 'QUIT'])

    mesg = messenger.Messenger()
    mesg.status = stat_signal
    mesg._queue = progress_queue

    mesg.run()
    stat_signal.emit.assert_called_with("testing", -1, 0)
    assert "Exiting messenger" in caplog.text


def test_parse_filters():
    filters = ['latitude<=42']
    result = h5pyimport._parse_filters(filters)
    assert result == [('latitude', '<=', 42)]


def test_bad_options(tropomi_data_file):
    with pytest.raises(FileNotFoundError):
        h5pyimport.import_product("some_file", "",
                                  {'some': 'not_a_string', })

    with pytest.raises(TypeError):
        h5pyimport.import_product(tropomi_data_file,
                                  filters = {'some': 'not_a_string', })


def test_good_options(test_file_path):
    file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    data = h5pyimport.import_product(file, options = "so2_column=15km;")
    numpy.testing.assert_equal(data['SO2_column_number_density'].data,
                               data['SO2_number_density_15km'].data)


def test_no_multiprocessing_pool_fallback(test_file_path, monkeypatch):
    pool = Mock(side_effect = AssertionError)
    monkeypatch.setattr(multiprocessing, "Pool", pool)

    file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    # Need multiple "files" so we try to make a pool.
    with pytest.raises(ValueError):
        data = h5pyimport.import_product([file, file])
    # If we tried to create a pool, and didn't get any errors, then it worked
    assert pool.called


def test_get_pointtimes_bad_size(test_file_path):
    """
    Corner Case:
    Test the scenerio where the datetime array loaded from the file
    doesn't match the size/shape of the latitude array.
    """
    from TROPOMIQt.file_formats import viirs

    file_name = "V2021362205348.SO2AI_JPSS-1.h5"
    file = os.path.join(test_file_path, file_name)
    file_def = viirs.DEF
    file_xa = xarray.Dataset()
    h5_file = h5py.File(file, 'r')
    data_size = 41267200 #2x actual size
    data_shape = (6448, 6400) #2x width to create 2x size

    reload(h5pyimport)

    h5pyimport._get_pointtimes(file_def, file_xa, h5_file, file, data_size,
                               data_shape)

    h5_file.close()

    assert 'datetime_start' in file_xa.coords
    assert file_xa.coords['datetime_start'].size == data_size
    assert file_xa.coords['datetime_start'].shape == data_shape


def test_kill_false(test_file_path):
    file_name = "V2021362205348.SO2AI_JPSS-1.h5"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)
    from TROPOMIQt.file_formats import viirs
    term_flag = Mock()
    term_flag.is_set.return_value = False
    with patch.object(h5pyimport, '_TERM_FLAG', new = term_flag):
        res = h5pyimport._load_file_data(viirs.DEF, file)

    assert not term_flag.set.called
    assert res.dims['y'] == 6448
    assert res.dims['x'] == 3200
    assert res.dims['corners'] == 4


def test_invalid_format(test_file_path, caplog):
    file_name = "V2021362205348.SO2AI_JPSS-1.h5"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)
    from TROPOMIQt.file_formats import viirs
    with (patch.dict(viirs.DEF['GROUPS'][0]['FIELDS'][4], {'NAME': None}),
          pytest.raises(TypeError)):
        h5pyimport.get_filetype(file)

    assert "Unable to identify file type" in caplog.text


def test_field_matching(test_file_path, caplog):
    caplog.set_level(logging.INFO)
    file_name = file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)
    from TROPOMIQt.file_formats import tropomi
    with patch.dict(tropomi.DEF['INFO']['ident_attr'], {'NAME': None}):
        ftype, fdef = h5pyimport.get_filetype(file)

        assert "Guessing file type" in caplog.text
        assert ftype == 'TROPOMI'
        assert fdef == tropomi.DEF


def test_init_netcdffile_no_files():
    reload(h5pyimport)

    configure = Mock()
    with patch.object(h5pyimport.NetCDFFile, 'configure', new = configure):
        test = h5pyimport.NetCDFFile()
        assert not configure.called


def test_import_data_replaces_existing_data(test_file_path):
    """We can test that the old data is replaced by new data, but not
    that the old data is deleted prior to importing the new. Just have to
    trust the python garbage collection system for that."""
    file_name = file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)
    h5pyimport._TERM_FLAG = multiprocessing.Event()

    netcdf_file = h5pyimport.NetCDFFile([file])
    old_data = Mock()
    netcdf_file._file_data = old_data
    netcdf_file.import_data()

    assert netcdf_file._file_data is not old_data


def test_import_data_bad_options(test_file_path):
    file_name = file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)
    h5pyimport._TERM_FLAG = multiprocessing.Event()

    netcdf_file = h5pyimport.NetCDFFile([file])
    with pytest.raises(TypeError, match = 'Options must be a string, not.*'):
        netcdf_file.import_data(options = {'level': '5km', })


def test_import_data_with_so2_operation(test_file_path):
    file_name = file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)
    h5pyimport._TERM_FLAG = multiprocessing.Event()

    netcdf_file = h5pyimport.NetCDFFile([file])
    func = Mock(side_effect = lambda x: x)
    with patch.dict(netcdf_file._file_def['INFO']['so2_template'], {'operation': func, }):
        netcdf_file.import_data()

    so2_field_defs = [x for x in netcdf_file._file_def['GROUPS'][0]['FIELDS']
                      if x.get('DEST') == 'SO2_column_number_density']

    assert len(so2_field_defs) == 1
    assert so2_field_defs[0]['operation'] is func
    assert func.called


def test_import_data_bad_so2_group(test_file_path):
    file_name = file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)
    h5pyimport._TERM_FLAG = multiprocessing.Event()

    netcdf_file = h5pyimport.NetCDFFile([file])
    with (patch.dict(netcdf_file._file_def['INFO']['so2_template'], {'DEFAULT_GROUP': 'BOB', }),
          pytest.raises(TypeError)):
        netcdf_file.import_data()


def test_import_data_kill_during_filter(test_file_path):
    file_name = file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)
    term_flag = Mock()
    term_flag.is_set.side_effect = [False] * 28 + [True]
    h5pyimport._TERM_FLAG = term_flag

    netcdf_file = h5pyimport.NetCDFFile([file])

    # Yes, it is the same filter twice. Should work for testing though :)
    result = netcdf_file.import_data(filters = "SO2_column_number_density_validity>0;SO2_column_number_density_validity>0;",
                                     options = "so2_column=15km;")

    assert utils.MESSAGE_QUEUE.get_nowait() == 'RESET'
    assert result == {}
    reload(h5pyimport)


def test_import_data_exception_raised(test_file_path, caplog):
    file_name = file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)

    mock_term = Mock()

    class TestException(Exception):
        pass

    mock_term.is_set.side_effect = [TestException]

    netcdf_file = h5pyimport.NetCDFFile([file])
    with (pytest.raises(TestException),
          patch.object(h5pyimport, '_TERM_FLAG', new = mock_term)):
        netcdf_file.import_data()

    assert "Error when loading file:" in caplog.text
    assert mock_term.set.called


def test_apply_filter_no_list():
    func = h5pyimport.NetCDFFile._apply_filter

    class FakeData:
        where = Mock()

        def __getitem__(self, key):
            return 42

    fake_data = FakeData()
    fake_self = Mock()
    filter_return = numpy.array([True, False])
    fake_filter = Mock(return_value = filter_return)
    fake_ops = {'bob': fake_filter, }
    fake_self._filter_ops = fake_ops
    ret = func(fake_self, 'bob', 'bob', 42, fake_data)
    fake_data.where.assert_called_with(filter_return, drop = True)
    assert isinstance(ret, list)
    assert len(ret) == 1
    assert ret[0] == fake_data.where()


def test_valid_all():
    func = h5pyimport.NetCDFFile._valid

    class FakeData:
        where = Mock()

        def __getitem__(self, key):
            return numpy.array([42, 42, 42]) # All valid

    fake_self = Mock()
    del fake_self._file_data

    fake_data = FakeData()

    ret = func(fake_self, 'bob', [fake_data])

    assert not hasattr(fake_self, '_file_data')
    assert ret is None


def test_valid_custom_data():
    func = h5pyimport.NetCDFFile._valid

    class FakeData:
        where = Mock()

        def __getitem__(self, key):
            return numpy.array([42, 42, numpy.nan]) # not All valid

    fake_self = Mock()
    del fake_self._file_data

    fake_data = FakeData()
    data = [fake_data]

    ret = func(fake_self, 'bob', data)

    #This should not exist since we are passing in data
    assert not hasattr(fake_self, '_file_data')

    assert isinstance(ret, list)
    assert len(ret) == 1

    # This is overly complex, but I can't directly ensure that the function was called with a
    # numpy array unless I have access to the _actual_ array object it was called with.
    where_args = fake_data.where.call_args
    assert len(where_args) == 2
    assert len(where_args[0]) == 1
    assert len(where_args[1]) == 1
    assert (where_args[0][0] == numpy.array([True, True, False])).all()
    assert isinstance(where_args[1], dict)
    assert 'drop' in where_args[1]
    assert where_args[1]['drop'] == True

    assert ret[0] is fake_data.where()


def test_longitude_range_corner_cases():
    func = h5pyimport.NetCDFFile._longitude_range

    class FakeData:
        where = Mock()

        def __getitem__(self, key):
            return numpy.array([-176, -177, -178, -179, 180, 179, 178, 177, 176])

    fake_self = Mock()
    del fake_self._file_data

    fake_data = FakeData()

    ret = func(fake_self, -185, 185, [fake_data])

    assert not hasattr(fake_self, '_file_data')
    assert ret == [fake_data]


def test_bin_spatial(test_file_path, caplog):
    caplog.set_level(logging.DEBUG)
    file_names = [
        "S5P_NRTI_L2__SO2____20230725T222902_20230725T223402_29957_03_020500_20230726T003548.nc",
        "S5P_NRTI_L2__SO2____20230726T000902_20230726T001402_29958_03_020500_20230726T010218.nc"
    ]

    file = [os.path.join(test_file_path, file_name) for file_name in file_names]

    reload(h5pyimport)
    h5pyimport._TERM_FLAG = multiprocessing.Event()

    netcdf_file = h5pyimport.NetCDFFile(file)
    # This function should raise an error because we aren't telling it to bin the data (yet)
    # However, the data will still be loaded and ready for binning.
    with pytest.raises(ValueError):
        netcdf_file.import_data(options = "so2_column=15km;")
    # Make sure *something* is loaded
    raw_data = netcdf_file._file_data
    assert raw_data
    assert sorted(raw_data[0].dims.keys()) == ['corners', 'x', 'y']

    netcdf_file._bin_spatial(10, 18, .5, 16, -162, .5)
    assert "Completed binning" in caplog.text
    assert dict(netcdf_file._file_data.dims.items()) == {'y': 9, 'x': 15, 'corners': 4, 'file': 2, }


def test_bin_spatial_multi_file_projected(test_file_path, caplog):
    file_name = file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)
    h5pyimport._TERM_FLAG = multiprocessing.Event()

    netcdf_file = h5pyimport.NetCDFFile([file, file])
    proj = '80049533000000000000007d94288c0470726f6a948c046d657263948c056c6f6e5f30948c032d3930948c0e70726573657276655f756e6974739489752e'
    num_lon = 134
    num_lat = 89
    filter_ = f'bin_spatial({num_lat},2024351.4256699837, 6660,{num_lon},-8015003.337115697,6660,{proj})'
    raw_data = netcdf_file.import_data(filters = filter_, options = "so2_column=15km;")
    # Make sure *something* is loaded
    assert raw_data
    assert sorted(raw_data.dims.keys()) == ['corners', 'file', 'x', 'y']
    assert raw_data.dims['x'] == num_lon - 1
    assert raw_data.dims['y'] == num_lat - 1


def test_bin_spatial_empty_file(test_file_path, caplog):
    caplog.set_level(logging.DEBUG)
    file_name = file_name = "S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc"
    file = os.path.join(test_file_path, file_name)

    reload(h5pyimport)
    h5pyimport._TERM_FLAG = multiprocessing.Event()

    netcdf_file = h5pyimport.NetCDFFile([file])
    netcdf_file.import_data(options = "so2_column=15km;")

    empty_file = xarray.Dataset(data_vars = {'data': (['x', 'y'], [[]]), })
    netcdf_file._file_data = [empty_file]

    netcdf_file._bin_spatial(10, 18, .5, 16, -162, .5)
    assert "Completed binning" in caplog.text
    assert netcdf_file._file_data is None


def test_bin_spatial_extra_keys_no_validity(test_file_path, caplog):
    caplog.set_level(logging.DEBUG)
    file_names = [
        "S5P_NRTI_L2__SO2____20230725T222902_20230725T223402_29957_03_020500_20230726T003548.nc",
        "S5P_NRTI_L2__SO2____20230726T000902_20230726T001402_29958_03_020500_20230726T010218.nc"
    ]

    file = [os.path.join(test_file_path, file_name) for file_name in file_names]

    reload(h5pyimport)
    h5pyimport._TERM_FLAG = multiprocessing.Event()

    netcdf_file = h5pyimport.NetCDFFile(file)
    # This function should raise an error because we aren't telling it to bin the data (yet)
    # However, the data will still be loaded and ready for binning.
    with pytest.raises(ValueError):
        netcdf_file.import_data(options = "so2_column=15km;")
    # Make sure *something* is loaded
    raw_data = netcdf_file._file_data
    assert raw_data
    assert sorted(raw_data[0].dims.keys()) == ['corners', 'x', 'y']

    # Add an extra key to the "to_bin" list to make sure it doesn't cause issues
    netcdf_file._to_bin.append('bob_is_your_uncle')

    # Also remove the SO2_column_number_density_validty variable
    #del netcdf_file._file_data['SO2_column_number_density_validity']

    netcdf_file._bin_spatial(10, 18, .5, 16, -162, .5)
    assert "Completed binning" in caplog.text
    assert dict(netcdf_file._file_data.dims.items()) == {'y': 9, 'x': 15, 'corners': 4, 'file': 2, }
