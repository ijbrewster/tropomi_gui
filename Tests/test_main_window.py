"""
test_main_window.py - Tests for various functions of the main window object
"""
import os
import pickle
import threading

from datetime import datetime
from time import sleep
from threading import Lock, Thread, Event
from queue import Queue
from unittest.mock import Mock, patch
from functools import partial

import numpy
import pandas
import requests
import PySide6
import pytest
import pyqtgraph

from PySide6.QtCore import Qt, QPoint, QRect, QSizeF, QSize, QSettings
from PySide6.QtWidgets import (
    QFileDialog,
    QWidget,
    QDialog,
    QGraphicsPixmapItem,
    QSpinBox
)
from PySide6.QtGui import QPixmap, QImageReader
from PySide6 import QtWidgets
from pyqtgraph import Point

from TROPOMIQt import MainWindow, utils
from TROPOMIQt.MainWindow import mainWindow, PlotGraphicsView, Mode

def test_save_record_no_default_path(monkeypatch, generic_test_object, tmpdir_factory):
    tmp_dir = tmpdir_factory.mktemp("output")

    window_obj = generic_test_object

    save_record = partial(mainWindow.on_bSaveRecord_clicked, window_obj)
    window_obj.on_bSaveRecord_clicked = save_record

    select_path = partial(mainWindow.select_save_path, window_obj)
    window_obj.select_save_path = select_path

    monkeypatch.setattr(threading, "Event", Mock)
    window_obj._bkg_render_request = Mock()
    window_obj._quit_event = Mock()
    window_obj._setStyleSheet = Mock()
    window_obj._ui.cbAltitude.currentText = Mock(return_value = "7km")

    window_obj.files = ['/Users/israel/Development/tropomi_gui/Tests/TestFiles/S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc']

    window_obj.settings.beginGroup("preferences")
    old_val = window_obj.settings.value('path')
    window_obj.settings.remove('path')
    window_obj.settings.endGroup()

    get_dir = Mock(return_value = (str(tmp_dir), None))
    monkeypatch.setattr(QFileDialog, "getExistingDirectory", get_dir)

    with (patch.object(MainWindow, "QDialog", return_value = Mock()),
          patch.object(MainWindow, "QTimer", return_value = Mock())):
        window_obj.on_bSaveRecord_clicked()

    assert window_obj.settings.value('preferences/path') == str(tmp_dir)

    if old_val:
        window_obj.settings.beginGroup("preferences")
        window_obj.settings.setValue('path', old_val)
        window_obj.settings.endGroup()


def test_save_record_no_data(monkeypatch, generic_test_object, tmpdir_factory):
    tmp_dir = tmpdir_factory.mktemp("output")

    window_obj = generic_test_object
    window_obj.so2data = None

    save_record = partial(mainWindow.on_bSaveRecord_clicked, window_obj)
    window_obj.on_bSaveRecord_clicked = save_record

    window_obj.settings.beginGroup("preferences")
    old_val = window_obj.settings.value('path')
    window_obj.settings.setValue('path', str(tmp_dir))
    window_obj.settings.endGroup()

    monkeypatch.setattr(threading, "Event", Mock)
    window_obj._bkg_render_request = Mock()
    window_obj._quit_event = Mock()

    with (patch.object(MainWindow, "QDialog", return_value = Mock()),
          patch.object(MainWindow, "QTimer", return_value = Mock())):
        window_obj.on_bSaveRecord_clicked()

    assert os.path.isfile(str(tmp_dir.join('AVO_Operational_Satellite_SO2_analysis.csv')))

    if old_val:
        window_obj.settings.beginGroup("preferences")
        window_obj.settings.setValue('path', old_val)
        window_obj.settings.endGroup()


def test_volcano_clicked(generic_test_object, monkeypatch):
    volc_clicked = partial(mainWindow._volcano_clicked, generic_test_object)
    highlight_volc = partial(mainWindow._highlight_volcano, generic_test_object)
    generic_test_object._volcano_clicked = volc_clicked
    generic_test_object._highlight_volcano = highlight_volc
    generic_test_object._volc_labels = []
    generic_test_object._selected_volcs = set()
    generic_test_object._plot_item = Mock()
    generic_test_object._zoom_level = 0

    filled_brush = pyqtgraph.mkBrush('k')

    class testPoint:
        _brush = pyqtgraph.mkBrush('w')

        def __init__(self, name):
            self._name = name

        def setBrush(self, brush):
            self._brush = brush

        def pos(self):
            return QPoint(42, 42)

        def brush(self):
            return self._brush

        def resetBrush(self):
            self._brush = pyqtgraph.mkBrush('w')

        def data(self):
            return {
                'name': self._name,
            }

    point = testPoint('Test Point 1')
    generic_test_object._volcano_clicked(None, [point, ])
    assert point._brush == filled_brush
    assert generic_test_object._plot_item.addItem.called
    assert not generic_test_object._plot_item.removeItem.called
    assert 'Test Point 1' in generic_test_object._selected_volcs

    # Now selected, so try again with deselection
    generic_test_object._plot_item.addItem.reset_mock()
    generic_test_object._volcano_clicked(None, [point, ])
    assert point._brush != filled_brush
    assert not generic_test_object._plot_item.addItem.called
    assert generic_test_object._plot_item.removeItem.called
    assert 'Test Point 1' not in generic_test_object._selected_volcs

    # Test error handling
    generic_test_object._plot_item.removeItem.reset_mock()
    point.setBrush(filled_brush)
    generic_test_object._volcano_clicked(None, [point, ])
    assert point._brush != filled_brush
    assert not generic_test_object._plot_item.addItem.called
    assert generic_test_object._plot_item.removeItem.called
    assert 'Test Point 1' not in generic_test_object._selected_volcs

    # Multiple options
    point2 = testPoint("Pt2")
    menu = Mock()

    monkeypatch.setattr(MainWindow, "QMenu", menu)

    # Some random error in menu - can't find selected volcano
    menu().exec_().text.return_value = 'bob'

    generic_test_object._plot_item.addItem.reset_mock()
    generic_test_object._plot_item.removeItem.reset_mock()

    generic_test_object._volcano_clicked(None, [point, point2])

    assert point2._brush != filled_brush
    assert point._brush != filled_brush
    assert not generic_test_object._plot_item.addItem.called
    assert not generic_test_object._plot_item.removeItem.called
    assert generic_test_object._selected_volcs == set()

    menu().exec_().text.return_value = "Pt2"
    generic_test_object._plot_item.addItem.reset_mock()
    generic_test_object._plot_item.removeItem.reset_mock()
    generic_test_object._volcano_clicked(None, [point, point2])
    assert point2._brush == filled_brush
    assert generic_test_object._plot_item.addItem.called
    assert not generic_test_object._plot_item.removeItem.called
    assert 'Pt2' in generic_test_object._selected_volcs

    # User closes menu without selecting anything
    menu().exec_.return_value = None

    generic_test_object._plot_item.addItem.reset_mock()
    generic_test_object._plot_item.removeItem.reset_mock()
    point.resetBrush()
    point2.resetBrush()
    generic_test_object._selected_volcs = set()

    generic_test_object._volcano_clicked(None, [point, point2])

    assert point2._brush != filled_brush
    assert point._brush != filled_brush
    assert not generic_test_object._plot_item.addItem.called
    assert not generic_test_object._plot_item.removeItem.called
    assert generic_test_object._selected_volcs == set()


def test_zoom_data(generic_test_object):
    zoom_data = partial(mainWindow._zoom_data, generic_test_object)
    generic_test_object._zoom_data = zoom_data
    generic_test_object._ui.sbLngFrom.value.return_value = -180
    generic_test_object._ui.sbLngTo.value.return_value = -160
    generic_test_object._ui.sbLatFrom.value.return_value = 50
    generic_test_object._ui.sbLatTo.value.return_value = 60

    generic_test_object._zoom_data()

    assert generic_test_object._vb.setRange.called


def test_call_render_background():
    """Another trivial test to get closer to 100% coverage"""
    class testObject:
        _bkg_render_request = Event()
        _bkg_render_timer = Mock()

    test_obj = testObject()

    mainWindow._render_background(test_obj)
    assert test_obj._bkg_render_timer.start.called

    mainWindow._run_render_background(test_obj)
    assert test_obj._bkg_render_request.is_set()


def test_background_render_thread(generic_test_object):
    generic_test_object._bkg_render_request = Event()
    generic_test_object._quit_event = Event()
    generic_test_object._bkg_render_trigger = utils.OrEvent(generic_test_object._bkg_render_request,
                                                            generic_test_object._quit_event)
    generic_test_object._render_lock = Lock()
    generic_test_object._render_ready = Mock()
    generic_test_object._vb.viewRect.return_value = QRect(0, 0, 42, 42)
    generic_test_object._vb.size.return_value = QSizeF(42, 42)
    generic_test_object._mapScene = Mock()
    generic_test_object._finished_bkg_render = None

    generic_test_object._bkg_render_request.set()
    generic_test_object._quit_event.set()

    render = partial(mainWindow._background_render_thread, generic_test_object)
    generic_test_object._background_render_thread = render
    generic_test_object._background_render_thread()

    assert not generic_test_object._render_ready.emit.called
    generic_test_object._quit_event.clear()
    generic_test_object._bkg_render_request.clear()

    thread = Thread(target = generic_test_object._background_render_thread)
    thread.start()
    assert thread.is_alive()
    generic_test_object._bkg_render_request.set()
    sleep(2)  # allow the render thread to run

    assert generic_test_object._mapScene.render.called
    assert generic_test_object._render_ready.emit.called
    assert generic_test_object._render_lock.locked()
    assert not generic_test_object._bkg_render_request.is_set()

    generic_test_object._render_ready.emit.reset_mock()
    generic_test_object._render_lock.release()
    generic_test_object._mapScene.render.reset_mock()

    # Test something waiting for a render
    generic_test_object._finished_bkg_render = Event()
    generic_test_object._bkg_render_request.set()
    sleep(2)
    assert generic_test_object._finished_bkg_render.is_set()
    assert not generic_test_object._render_lock.locked()
    assert not generic_test_object._render_ready.emit.called

    generic_test_object._quit_event.set()
    sleep(1)  # allow the render thread to run
    assert not thread.is_alive()


def test_add_map(generic_test_object):
    generic_test_object._cur_map = None
    generic_test_object._render_lock = Lock()
    generic_test_object.volc_plot = None
    generic_test_object._volcano_clicked = Mock()

    first_map = Mock()

    generic_test_object._map_item = first_map
    generic_test_object._render_lock.acquire()
    add = partial(mainWindow._add_map, generic_test_object)
    volc_plot = Mock()
    volc_labels = Mock()

    with patch.object(MainWindow, 'plot_volcanoes', return_value = (volc_plot, volc_labels)):
        add()

        assert not generic_test_object._render_lock.locked()
        assert generic_test_object._cur_map == generic_test_object._map_item

        generic_test_object._plot_item = Mock()
        generic_test_object._render_lock.acquire()
        add()
        assert generic_test_object.volc_plot is volc_plot

    generic_test_object._plot_item.removeItem.assert_called_with(first_map)
    generic_test_object._plot_item.addItem.assert_called_with(first_map,
                                                              ignoreBounds = True)

    assert not generic_test_object._render_lock.locked()


def test_zoom(generic_test_object):
    generic_test_object._zoom_level = 0
    generic_test_object._plot_item = Mock()
    generic_test_object._scale_widget = None
    volc_labels = [Mock(), Mock()]
    volc_labels[0].property.return_value = False
    generic_test_object._volc_labels = volc_labels
    generic_test_object._vb.viewRange.return_value = ((1000, 5000),
                                                      (5000, 10000))
    generic_test_object._vb.pos.return_value = QPoint(15, 15)
    generic_test_object._vb.height.return_value = 42
    react = partial(mainWindow._react_to_zoom, generic_test_object)
    zoom_in = partial(mainWindow._zoom_in, generic_test_object)
    zoom_out = partial(mainWindow._zoom_out, generic_test_object)

    mock_scale = Mock()
    mock_scale.height.return_value = 10

    with patch.object(MainWindow, 'ScaleWidget', return_value = mock_scale):
        react()
        assert generic_test_object._zoom_level == 1
        for label in volc_labels:
            generic_test_object._plot_item.addItem.assert_any_call(label, ignoreBounds = True)

        generic_test_object._plot_item.removeItem.reset_mock()
        generic_test_object._vb.viewRange.return_value = ((1000, 2000000),
                                                          (5000, 10000))

        react()
        assert generic_test_object._zoom_level == 0
        generic_test_object._plot_item.removeItem.assert_called_with(volc_labels[0])
        with pytest.raises(AssertionError):
            generic_test_object._plot_item.removeItem.assert_called_with(volc_labels[1])

        zoom_in()
        generic_test_object._vb.scaleBy.assert_called_with((.75, .75))

        zoom_out()
        generic_test_object._vb.scaleBy.assert_called_with((1.333, 1.333))


def test_mouse_event():
    class fakeObj:
        _tip = Mock()

    obj = fakeObj()
    obj._tip.isVisible.return_value = True
    mainWindow.mouseMoveEvent(obj, None)
    assert obj._tip.hide.called


def test_get_data_point(generic_test_object):
    expected_point = generic_test_object.xy_coords[76552]
    get_point = partial(mainWindow._get_data_point, generic_test_object)
    # Somewhere around the middle of the data set
    test_point = QPoint(-6610860, 1966812)
    filt, idx = get_point(test_point)

    assert numpy.count_nonzero(filt) == 18
    assert idx == 7
    assert (generic_test_object.xy_coords[filt][idx] == expected_point).all()

    # Test out-of-range
    test_point = QPoint(-5022754, 3200000)
    filt, idx = get_point(test_point)
    assert filt is None
    assert idx is None

    # Single possibility
    test_point = QPoint(-5023000, 1155864)
    filt, idx = get_point(test_point)
    assert numpy.count_nonzero(filt) == 1
    assert idx == 0


def test_point_near_wind():
    test_wind_plot_data = [
        [(1000, 2000, 3000), (1000, 2000, 3000)],
        [(4000, 5000, 6000), (4000, 5000, 6000)]
    ]
    test_wind_plots = [Mock(), Mock()]
    for i in range(len(test_wind_plots)):
        test_wind_plots[i].getData.return_value = test_wind_plot_data[i]

    class fakeObj:
        _wind_plots = test_wind_plots

    test_obj = fakeObj()
    near_wind = partial(mainWindow._point_near_wind, test_obj)

    # search radius is +/- 2000 x and y
    test_point = QPoint(0, 0)  # Should match to the first wind plot
    assert near_wind(test_point)

    test_point = QPoint(8000, 8000)  # Should match to the second wind plot
    assert near_wind(test_point)

    test_point = QPoint(8001, 7000)  # should be *just* too far away
    assert not near_wind(test_point)


def test_mouseHover(generic_test_object):
    hover = partial(mainWindow.mouseHover, generic_test_object)
    test_point = QPoint(-6610860, 1966812)
    generic_test_object._tip = Mock()
    near_wind = Mock(return_value = False)

    generic_test_object._ui.PlotWidget.mapToGlobal.return_value = QPoint(42, 42)
    generic_test_object._ui.cbTrace.currentIndex.return_value = 0
    generic_test_object._point_near_wind = near_wind
    generic_test_object._get_data_point = partial(mainWindow._get_data_point,
                                                  generic_test_object)
    generic_test_object.HOVER_FORMAT_STRS = mainWindow.HOVER_FORMAT_STRS

    hover(test_point)
    assert generic_test_object._tip.show.called

    # Point near wind
    generic_test_object._tip.reset_mock()
    near_wind.return_value = True
    hover(test_point)
    assert generic_test_object._tip.show.called
    generic_test_object._tip.setText.assert_called_with("Click to select vector<br>Shift-Click to Select second vector")

    # point not near anything
    test_point = QPoint(-5022754, 3200000)
    near_wind.return_value = False
    generic_test_object._tip.reset_mock()
    hover(test_point)
    assert not generic_test_object._tip.show.called
    assert generic_test_object._tip.hide.called

    # No SO2 data
    generic_test_object._tip.reset_mock()
    generic_test_object.so2data = None
    hover(test_point)
    assert not generic_test_object._tip.show.called
    assert not generic_test_object._tip.hide.called


def test_clear_winds(generic_test_object):
    clear = partial(mainWindow._clear_winds, generic_test_object)
    clear_interp = partial(mainWindow._clear_interpolated_results, generic_test_object)
    generic_test_object._clear_interpolated_results = clear_interp
    plot_item = Mock()

    wind_plots = [Mock()] * 5
    generic_test_object._wind_plots = wind_plots
    generic_test_object._plot_item = plot_item
    generic_test_object._ui.cbAltitude.findText.return_value = 3
    generic_test_object._ui.cbAltitude.currentText.side_effect = ["INT: Test", "", "15km", "15km", "", "15km"]
    generic_test_object._ui.cbAltitude.currentData.return_value = 15
    generic_test_object._recalc_values = Mock()
    generic_test_object._results_table_model = Mock()

    clear()
    assert generic_test_object._ui.windkeyContainer.hide.called
    assert plot_item.removeItem.call_count == 5
    generic_test_object._ui.cbAltitude.removeItem.assert_called_with(3)

    generic_test_object._calc_results = None
    generic_test_object._ui.cbAltitude.currentText.reset_mock()
    clear()


def test_show_dialog(monkeypatch):
    dlg = Mock()

    class fakeObj:
        pass

    test_obj = fakeObj()
    monkeypatch.setattr(MainWindow, "QDialog", dlg)
    show_dlg = partial(mainWindow._show_dialog, test_obj)
    ret = show_dlg("Test Message")
    assert ret == dlg()
    assert ret.open.called


def test_get_winds(monkeypatch, test_file_path, generic_test_object):
    class superDataMock(Mock):
        def setData(self, *args, **kwargs):
            pass

    class dataMock(superDataMock):
        pass

    get_forward = partial(mainWindow._get_forward_winds, generic_test_object)
    get_wind = partial(mainWindow._get_winds, generic_test_object)
    run_wind = partial(mainWindow._run_get_winds, generic_test_object)
    clear_wind = partial(mainWindow._clear_winds, generic_test_object)
    get_data = partial(mainWindow._get_data_point, generic_test_object)

    monkeypatch.setattr(pyqtgraph, "PlotDataItem", dataMock)
    keyboard_mod = Mock(return_value = Qt.AltModifier)
    monkeypatch.setattr(MainWindow.QApplication, "queryKeyboardModifiers", keyboard_mod)

    generic_test_object._get_winds = get_wind
    generic_test_object._run_get_winds = run_wind
    generic_test_object._clear_winds = clear_wind
    generic_test_object._get_data_point = get_data
    generic_test_object._show_dialog = Mock()
    generic_test_object._wind_plots = []
    generic_test_object._clear_interpolated_results = Mock()
    generic_test_object._plot_item = dataMock()
    generic_test_object._wind_plot_clicked = Mock()  # Won't actually be called, so might as well just be a mock.

    mock_get = Mock()
    mock_get().status_code = 200
    monkeypatch.setattr(requests, "get", mock_get)
    with open(os.path.join(test_file_path, "raw_wind.pickle"), 'rb') as f:
        mock_get().json.return_value = pickle.load(f)

    test_point = QPoint(-6610860, 1966812)
    assert len(generic_test_object._wind_plots) == 0
    get_forward(test_point)
    assert len(generic_test_object._wind_plots) != 0  # not empty
    assert generic_test_object._ui.windkeyContainer.show.called

    generic_test_object._wind_plots = []
    # Point off data
    test_point = QPoint(-5022754, 3200000)
    get_forward(test_point)
    assert len(generic_test_object._wind_plots) != 0
    generic_test_object._wind_plots = []

    generic_test_object.files = []  # See if we can force another error while we are at it
    get_forward(test_point)
    assert len(generic_test_object._wind_plots) != 0

    del generic_test_object.xy_coords
    generic_test_object._wind_plots = []
    test_point = QPoint(-6610860, 1966812)
    get_forward(test_point)
    assert len(generic_test_object._wind_plots) != 0
    # errors
    critical = Mock()
    monkeypatch.setattr(QtWidgets.QMessageBox, "critical", critical)

    # Bad network request
    mock_get().status_code = 404
    generic_test_object._wind_plots = []
    get_forward(test_point)
    assert len(generic_test_object._wind_plots) == 0
    assert 'Wind Error' in critical.call_args[0]

    mock_get().status_code = 200  # Not really, but to make sure the error is the one we want to test
    mock_get.side_effect = [requests.exceptions.ConnectionError, requests.exceptions.ConnectionError]
    critical.reset_mock()
    get_forward(test_point)
    assert len(generic_test_object._wind_plots) == 0
    assert 'Wind Error' in critical.call_args[0]


def test_recalc_values(generic_test_object):
    orig_dens = generic_test_object.so2data['density']
    orig_du = generic_test_object.so2data['du']
    orig_mass = generic_test_object.so2data['mass']

    recalc = partial(mainWindow._recalc_values, generic_test_object)
    ready = partial(mainWindow._on_data_ready, generic_test_object)
    gen = partial(mainWindow._generate_brushes, generic_test_object)
    gen_other = partial(mainWindow._gen_other_brushes, generic_test_object)
    idx_changed = partial(mainWindow.on_cbTrace_currentIndexChanged, generic_test_object)

    generic_test_object._on_data_ready = ready
    generic_test_object._generate_brushes = gen
    generic_test_object._gen_other_brushes = gen_other
    generic_test_object.progress_dialog = Mock()
    generic_test_object._pyqtgraph_plot = Mock()
    generic_test_object.on_cbTrace_currentIndexChanged = idx_changed
    generic_test_object._keys_top = 23
    generic_test_object._ui.percentContainer.height.return_value = 30
    generic_test_object._du_color_map = pyqtgraph.ColorMap(
        [0, .05, .1, .175, .25, .99, 1],
        [(255, 255, 255, 192),
         (241, 187, 252, 192),
         (53, 248, 244, 192),
         (255, 225, 0, 192),
         (248, 152, 6, 192),
         (255, 19, 0, 192),
         (255, 0, 0, 192)]
    )
    generic_test_object._scaleLabels = [
        {0: "0 DU", .05: "1", .1: "2", .25: "5", .6: "12", 1: ">20 DU", },  # SO2 Concentration
        {0: "0%", 1: "100%", },
        {0: "0", 1: "100", }
    ]

    generic_test_object._gradients = [generic_test_object._du_color_map.getGradient(), None, None]

    generic_test_object._ui.cbTrace.currentIndex.return_value = 0
    generic_test_object.chartready = True

    recalc()

    # since we didn't change the contents of the so2 density,
    # the results should remain unchanged.
    assert (generic_test_object.so2data['density'] == orig_dens).all()
    assert (generic_test_object.so2data['du'] == orig_du).all()
    assert (generic_test_object.so2data['mass'] == orig_mass).all()


def test_wind_plot_clicked(monkeypatch, test_file_path, generic_test_object):
    plot_clicked = partial(mainWindow._wind_plot_clicked, generic_test_object)
    clear_inter = partial(mainWindow._clear_interpolated_results, generic_test_object)

    wind_data_file = os.path.join(test_file_path, 'wind_results.pickle')
    with open(wind_data_file, 'rb') as f:
        wind_data = pickle.load(f)

    generic_test_object._ui.PlotWidget = PlotGraphicsView()
    generic_test_object._plot_item = generic_test_object._ui.PlotWidget.getPlotItem()
    generic_test_object._wind_plot_clicked = plot_clicked
    generic_test_object._clear_interpolated_results = clear_inter
    generic_test_object._selected_points = []
    generic_test_object._results_table_model = Mock()
    generic_test_object._ui.cbAltitude.findText.return_value = -1
    generic_test_object._recalc_values = Mock()  # tested elsewhere
    generic_test_object._recalc_noise_debounce = Mock()  # tested elsewhere
    generic_test_object._on_data_ready = Mock()  # tested elsewhere
    generic_test_object.repaint = Mock()  # display function

    wind_plots = MainWindow.plot_wind(wind_data, generic_test_object._plot_item)
    generic_test_object._wind_plots = wind_plots
    wind_plot = generic_test_object._wind_plots[0]
    point_idx = 5
    point_clicked = wind_plot.scatter.points()[point_idx]
    point_size = point_clicked.size()

    plot_clicked(wind_plot, [point_clicked, ])

    assert generic_test_object._results_table_model.refreshView.called
    assert len(generic_test_object._selected_points) == 1
    assert generic_test_object._selected_points[0] == (wind_plot, point_idx, point_size)
    assert wind_plot.opts['symbolSize'][point_idx] > point_size

    # Click a different point
    generic_test_object._results_table_model.refreshView.reset_mock()

    plot2 = generic_test_object._wind_plots[1]
    point2 = plot2.scatter.points()[point_idx]
    point_size2 = point2.size()
    assert point2 != point_clicked

    plot_clicked(plot2, [point2, ])
    assert generic_test_object._results_table_model.refreshView.called
    assert len(generic_test_object._selected_points) == 1
    assert generic_test_object._selected_points[0] == (plot2, point_idx, point_size2)
    assert plot2.opts['symbolSize'][point_idx] > point_size2
    assert wind_plot.opts['symbolSize'][point_idx] == point_size  # The first should be back to normal size

    # shift click a point
    keyboard_mod = Mock(return_value = Qt.ShiftModifier)
    monkeypatch.setattr(MainWindow.QApplication, "queryKeyboardModifiers", keyboard_mod)
    generic_test_object._results_table_model.refreshView.reset_mock()

    plot_clicked(wind_plot, [point_clicked, ])
    assert generic_test_object._results_table_model.refreshView.called
    assert len(generic_test_object._selected_points) == 2
    assert generic_test_object._selected_points == [(plot2, point_idx, point_size2),
                                                    (wind_plot, point_idx, point_size)]
    assert plot2.opts['symbolSize'][point_idx] > point_size2
    assert wind_plot.opts['symbolSize'][point_idx] > point_size

    # And remove it
    generic_test_object._results_table_model.refreshView.reset_mock()

    plot_clicked(plot2, [point2, ])
    assert len(generic_test_object._selected_points) == 1
    assert generic_test_object._selected_points == [(wind_plot, point_idx, point_size)]
    assert plot2.opts['symbolSize'][point_idx] == point_size2
    assert wind_plot.opts['symbolSize'][point_idx] > point_size


def test_tool_button_clicked(generic_test_object):
    clicked = partial(mainWindow._tool_button_clicked, generic_test_object)
    btns = [Mock(), Mock(), Mock(), Mock()]
    generic_test_object._navButtons = btns
    clicked(btns[2])
    for btn in btns:
        btn.setActive.assert_called_with(btn is btns[2])
        btn.repaint.assert_called


def test_set_noise_locked(generic_test_object):
    set_locked = partial(mainWindow._set_noise_locked, generic_test_object)
    noise_rois = [Mock(), Mock(), Mock(), Mock(), Mock(), Mock()]
    generic_test_object._noise_rois = noise_rois
    set_locked(True)
    for roi in noise_rois:
        roi.set_locked.assert_called_with(True)

    set_locked(False)
    for roi in noise_rois:
        roi.set_locked.assert_called_with(False)


def test_set_modes(generic_test_object):
    rectangle = partial(mainWindow._set_mode_rectangle, generic_test_object)
    pan = partial(mainWindow._set_mode_pan, generic_test_object)
    select = partial(mainWindow._set_mode_select, generic_test_object)
    measure = partial(mainWindow._set_mode_measure, generic_test_object)
    wind = partial(mainWindow._set_mode_wind, generic_test_object)
    windf = partial(mainWindow._set_mode_wind_forward, generic_test_object)

    pw = Mock()
    set_locked = Mock()

    generic_test_object._ui.PlotWidget = pw
    generic_test_object._set_noise_locked = set_locked

    rectangle()
    pw.setMode.assert_called_with(Mode.ZOOM)
    set_locked.assert_not_called()

    pan()
    pw.setMode.assert_called_with(Mode.PAN)
    set_locked.assert_called_with(False)

    set_locked.reset_mock()
    select()
    pw.setMode.assert_called_with(Mode.SELECT)
    set_locked.assert_called_with(False)

    measure()
    pw.setMode.assert_called_with(Mode.MEASURE)
    set_locked.assert_called_with(True)

    set_locked.reset_mock()
    wind()
    pw.setMode.assert_called_with(Mode.WIND)
    set_locked.assert_called_with(True)

    set_locked.reset_mock()
    windf()
    pw.setMode.assert_called_with(Mode.WINDF)
    set_locked.assert_called_with(True)


def test_init_export_options():
    class fakeObj(QWidget):
        def _set_export_size(self):
            pass

    test_obj = fakeObj()
    init = partial(mainWindow._init_export_options, test_obj)
    init()

    assert hasattr(test_obj, "_export_options")


def test_set_export_size():
    class fakeObj(QWidget):
        pass

    test_obj = fakeObj()
    set_size = partial(mainWindow._set_export_size, test_obj)
    init = partial(mainWindow._init_export_options, test_obj)

    test_obj._set_export_size = set_size
    width = Mock()
    height = Mock()

    init()

    test_obj._export_options.sbWidth = width
    test_obj._export_options.sbHeight = height

    set_size(2)
    width.setEnabled.assert_called_with(False)
    height.setEnabled.assert_called_with(False)
    assert width.setValue.called
    assert height.setValue.called

    width.reset_mock()
    height.reset_mock()

    set_size(3)
    width.setEnabled.assert_called_with(True)
    height.setEnabled.assert_called_with(True)
    assert not width.setValue.called
    assert not height.setValue.called


def test_show_results():
    class fakeObj:
        _results_win = Mock()

    test_obj = fakeObj()
    show = partial(mainWindow.showResults, test_obj)
    show()
    assert test_obj._results_win.show.called
    assert test_obj._results_win.raise_.called
    assert test_obj._results_win.activateWindow.called


def test_selectfile(monkeypatch):
    mock_plot = Mock()

    class fakeObj:
        _ui = Mock()
        so2data_cache = Mock()
        settings = Mock()
        on_cbFileType_currentTextChanged = Mock()
        setWindowTitle = Mock()
        _fix_slider_size = Mock()
        on_hsFileSelector_valueChanged = Mock()
        _load_file_filter = "file"
        displayed_plot = mock_plot
        _plot_item = Mock()
        so2data = {'pixel_paths': "DATA!", }

    self = fakeObj()
    self._calc_results = "Something or another"
    self.settings.value.return_value = ""
    self._ui.cbAltitude.currentText.return_value = ""
    self._ui.cbStack.isChecked.return_value = True

    open_names = Mock(return_value = ([], False))
    monkeypatch.setattr(QFileDialog, "getOpenFileNames", open_names)

    parse = Mock(return_value = [datetime(2021, 1, 1, 12, 15, 42, 0),
                                 datetime(2021, 1, 1, 12, 18, 15, 0)])

    monkeypatch.setattr(MainWindow.data_utils, "parse_file_range", parse)
    mainWindow.on_selectFile_clicked(self)
    assert self._calc_results == "Something or another"

    open_names.return_value = (["/some/path/to/file", "/some/other/file"], True)

    mainWindow.on_selectFile_clicked(self)

    self._plot_item.removeItem.assert_called_with(mock_plot)
    assert self.so2data is None
    assert self._calc_results is None
    self.on_hsFileSelector_valueChanged.assert_called_with(0, default_alt = True)
    assert self._nav_dir == 'forward'
    self._ui.hsFileSelector.setEnabled.assert_called_with(False)

    self._ui.cbStack.isChecked.return_value = False
    mainWindow.on_selectFile_clicked(self)
    self._ui.hsFileSelector.setEnabled.assert_called_with(True)



def test_show_and_map():
    class fakeObj():
        window_size = 42
        _menuBar = Mock()
        resize = Mock()
        setMenuBar = Mock()
        raise_ = Mock()
        activateWindow = Mock()
        _bkg_render_request = Event()

    mock_super = Mock()
    MainWindow.super = mock_super

    self = fakeObj()
    show = partial(mainWindow.show, self)
    showMap = partial(mainWindow.showMap, self)
    self.showMap = showMap
    self.show = show

    self.show()

    assert self._bkg_render_request.is_set()

    self.resize.assert_called_with(self.window_size)
    self.setMenuBar.assert_called_with(self._menuBar)

    self.showMap()

    assert self.raise_.called
    assert self.activateWindow.called


def test_actionviewsectors(monkeypatch):
    class fakeObj(QWidget):
        def _setup_views(self):
            return 42

    editor = Mock()
    monkeypatch.setattr(MainWindow, "SectorEditor", editor)
    test_obj = fakeObj()
    view = partial(mainWindow.on_actionViewSectors_triggered, test_obj)
    view()
    editor.assert_called_with(test_obj)
    editor().sectors_changed.connect.assert_called_with(test_obj._setup_views)
    assert editor().exec.called


def test_action_about(monkeypatch):
    class fakeObj:
        pass

    test_obj = fakeObj()
    about = Mock()
    monkeypatch.setattr(MainWindow.QMessageBox, "about", about)

    mainWindow.on_actionAbout_triggered(test_obj)

    assert 'SO2 Explorer' in about.call_args[0]


def test_action_prefs(monkeypatch):
    class fakeObj:
        pass

    prefs = Mock()
    test_obj = fakeObj()
    monkeypatch.setattr(MainWindow, "PrefsWin", prefs)
    mainWindow.on_action_prefs_triggered(test_obj)
    assert hasattr(test_obj, "_prefswin")
    prefs.assert_called_with(test_obj)
    assert test_obj._prefswin.open.called


def test_save_sector(monkeypatch, generic_test_object):
    gettext = Mock()
    gettext.return_value = ("", None)
    critical = Mock()
    save_views = Mock()
    setup_views = Mock()

    generic_test_object._vb.viewRange.return_value = ((-1500, 1500),
                                                      (-4200, 4200))
    generic_test_object._setup_views = setup_views

    monkeypatch.setattr(MainWindow.QMessageBox, "critical", critical)
    monkeypatch.setattr(MainWindow.QInputDialog, "getText", gettext)
    monkeypatch.setattr(MainWindow.utils, "save_user_views", save_views)

    save = partial(mainWindow.on_bSaveSector_clicked, generic_test_object)
    save_sector = partial(mainWindow._save_sector, generic_test_object)
    mock_save_sector = Mock(wraps = save_sector)
    generic_test_object._save_sector = mock_save_sector

    save()
    assert not critical.called
    assert not mock_save_sector.called

    gettext.return_value = ("Alaska Peninsula", None)
    save()
    assert 'Unable to save' in critical.call_args[0]
    assert not mock_save_sector.called

    gettext.return_value = ("Bob Smith", None)
    save()
    mock_save_sector.assert_called_with("Bob Smith")
    save_views.assert_called()
    generic_test_object._ui.cbViews.setCurrentText.assert_called_with("Bob Smith")


def test_clear_view_select():
    class fakeObj:
        _ui = Mock()

    test_obj = fakeObj()
    test_obj._ui.cbViews.currentIndex.return_value = 42
    mainWindow._clear_view_selection(test_obj)
    test_obj._ui.cbViews.setCurrentIndex.assert_called_with(0)


def test_set_slider_buttons():
    class fakeObj:
        _ui = Mock()
        files = []

    test_obj = fakeObj()
    test_obj._ui.cbStack.isChecked.return_value = True

    mainWindow._set_slider_buttons(test_obj, 42)

    test_obj._ui.bPrevFile.setEnabled.assert_called_with(False)
    test_obj._ui.bNextFile.setEnabled.assert_called_with(False)

    test_obj._ui.cbStack.isChecked.return_value = False
    test_obj.files = [None, None, None, None]  # Four files

    mainWindow._set_slider_buttons(test_obj, 42)
    test_obj._ui.bPrevFile.setEnabled.assert_called_with(True)
    test_obj._ui.bNextFile.setEnabled.assert_called_with(False)

    mainWindow._set_slider_buttons(test_obj, 0)
    test_obj._ui.bPrevFile.setEnabled.assert_called_with(False)
    test_obj._ui.bNextFile.setEnabled.assert_called_with(True)

    mainWindow._set_slider_buttons(test_obj, 2)
    test_obj._ui.bPrevFile.setEnabled.assert_called_with(True)
    test_obj._ui.bNextFile.setEnabled.assert_called_with(True)


def test_file_selector_moved():
    class fakeObj:
        files = ["SO2_202001011349_202001011456_other-stuff.nc",
                 "SO2_202101011349_202101011456_other-stuff.nc",
                 "SO2_202201011349_202201011456_other-stuff.nc"]
        centralWidget = Mock()
        _set_slider_buttons = Mock()  # Tested above
        lFileTip = Mock()
        _ui = Mock()

    test_obj = fakeObj()

    test_obj._ui.hsFileSelector.pos().y.return_value = 42
    test_obj._ui.hsFileSelector.pos().x.return_value = 42
    test_obj._ui.hsFileSelector.width.return_value = 242
    test_obj.lFileTip.height.return_value = 42
    test_obj.lFileTip.width.return_value = 142
    test_obj.centralWidget().pos().y.return_value = 0

    mainWindow.on_hsFileSelector_sliderMoved(test_obj, 1)

    test_obj.lFileTip.setText.assert_called_with("01/01/21 13:49:00Z")
    assert test_obj.lFileTip.show.called
    assert test_obj.lFileTip.adjustSize.called

    test_obj.files = ["SO2_202001011349_202001021456_other-stuff.nc"]
    mainWindow.on_hsFileSelector_sliderMoved(test_obj, 1)

    test_obj.lFileTip.setText.assert_called_with("01/02/20 14:56:00Z")
    assert test_obj.lFileTip.show.called
    assert test_obj.lFileTip.adjustSize.called


def test_key_press_event(monkeypatch):
    class superKeypressMock(Mock):
        keyPressEvent = Mock()

    class keypressMock(superKeypressMock):
        _ui = Mock()

    test_object = keypressMock()
    mock_super = Mock()
    MainWindow.super = mock_super
    press = partial(MainWindow.mainWindow.keyPressEvent, test_object)
    event = Mock()
    press(event)

    assert not test_object._ui.loadFile.animateClick.called
    mock_super().keyPressEvent.assert_called_with(event)

    mock_super.reset_mock()

    event.key.return_value = Qt.Key_Enter
    press(event)

    assert test_object._ui.loadFile.animateClick.called
    mock_super.assert_not_called()

    test_object._ui.loadFile.animateClick.reset_mock()

    # For good measure
    event.key.return_value = Qt.Key_Return
    press(event)

    assert test_object._ui.loadFile.animateClick.called
    mock_super.assert_not_called()

    del MainWindow.super


def test_cloud_validity_without_validity(generic_test_object):
    selection_mask = numpy.full_like(generic_test_object.so2data['cloud_fraction'],
                                     False, dtype = bool)

    # Remove the validty data
    del generic_test_object.so2data['SO2_column_number_density_validity']

    # Make sure we get a "nan" string for validity
    assert not hasattr(generic_test_object, "_selected_validity")
    mainWindow._calc_cloud_validity(generic_test_object, selection_mask)
    assert generic_test_object._selected_validity == "nan"


def test_calc_filtered_not_returning(generic_test_object):
    selection_mask = numpy.full_like(generic_test_object.so2data['cloud_fraction'],
                                     False, dtype = bool)
    selection_mask[0:1000] = True

    arg1 = (None, selection_mask)

    calc = partial(mainWindow._calculate_filtered_data, generic_test_object)

    generic_test_object._calc_cloud_validity = Mock()
    generic_test_object._calc_quick_stats = Mock()
    generic_test_object._need_results_refresh = Mock()

    generic_test_object._calcProcs = 2
    res = calc(arg1)
    assert generic_test_object._calcProcs == 1
    assert res is None
    assert not generic_test_object._calc_cloud_validity.called
    assert not generic_test_object._calc_quick_stats.called
    assert not generic_test_object._need_results_refresh.emit.called

    res = calc(arg1)
    assert generic_test_object._calcProcs == 0
    assert res is None
    generic_test_object._calc_cloud_validity.assert_called_with(selection_mask)
    assert generic_test_object._calc_quick_stats.called
    assert generic_test_object._need_results_refresh.emit.called


def test_calc_area_mass_no_data(monkeypatch):
    class fakeObj:
        xy_coords = "cartesian coords"

    test_obj = fakeObj()
    filt = Mock(return_value = (None, numpy.full((5, ), False, dtype = bool)))
    monkeypatch.setattr(MainWindow.data_utils, "filter_data_with_selection", filt)
    res = mainWindow._calculate_area_mass(test_obj, ["bob"])
    assert res == (None, None, None)


def test_refresh_results():
    class fakeObj:
        _results_table_model = Mock()
        _results_tableView = Mock()
        repaint = Mock()

    test_obj = fakeObj()
    mainWindow._refresh_results(test_obj)

    assert test_obj._results_table_model.refreshView.called
    assert test_obj._results_tableView.resizeColumnsToContents.called
    assert test_obj.repaint.called


def test_process_filter_future():
    class fakeObj:
        _calculate_filtered_data = Mock()
        _need_results_refresh = Mock()

    test_obj = fakeObj()
    future = Mock()
    mainWindow._process_filter_future(test_obj, future)
    assert test_obj._need_results_refresh.emit.called
    test_obj._calculate_filtered_data.assert_called_with(future.result())


def test_calc_selection(generic_test_object):
    sel = [(-7241726.932640292, 2198087.3140097405),
           (-7329835.251967947, 2195706.008081966),
           (-7320310.028256848, 2133792.0539598297),
           (-7234583.014856968, 2159986.419165349)]
    offset = QPoint(42, 42)
    calc = partial(mainWindow._calc_selection, generic_test_object)

    generic_test_object._ui.PlotWidget.mode = Mode.MEASURE
    generic_test_object._clear_measure = Mock()
    generic_test_object._plot_item = Mock()
    generic_test_object._process_filter_future = Mock()
    generic_test_object._measure_labels = []
    calc(sel, offset)

    assert len(generic_test_object._measure_labels) == 3
    for label in generic_test_object._measure_labels:
        generic_test_object._plot_item.addItem.assert_any_call(label)

    generic_test_object._ui.PlotWidget.mode = Mode.SELECT
    calc(sel, offset)
    assert generic_test_object._calcProcs == 1
    assert not generic_test_object._need_results_refresh.emit.called

    calc(sel, offset, False)
    assert generic_test_object._calcProcs == 1
    assert generic_test_object._need_results_refresh.emit.called

    sel = [
        (-7241726.932640292, 2198087.3140097405),
        (-7329835.251967947, 2195706.008081966)
    ]

    generic_test_object._calc_quick_stats = Mock()
    generic_test_object._measure_labels = []
    generic_test_object._lSelectedArea.setText.reset_mock()
    calc(sel, offset)
    assert generic_test_object._calc_quick_stats.called
    # Didn't create any labels
    assert generic_test_object._measure_labels == []
    # and didn't set the selected area
    assert not generic_test_object._lSelectedArea.setText.called

    generic_test_object._calc_quick_stats.reset_mock()

    sel = []
    calc(sel, offset)
    # Didn't do anything
    assert not generic_test_object._calc_quick_stats.called
    # Didn't create any labels
    assert generic_test_object._measure_labels == []
    # and didn't set the selected area
    assert not generic_test_object._lSelectedArea.setText.called


def test_area_selected():
    mock_timer = Mock()

    class fakeObj:
        _recalc_timer = mock_timer
        _calc_selection = Mock()

    test_obj = fakeObj()
    mainWindow._area_selected(test_obj, "bob", "smith")
    assert mock_timer.stop.called
    assert test_obj._recalc_timer.isSingleShot()
    assert test_obj._recalc_timer.interval() == 500
    assert test_obj._recalc_timer.isActive()


def test_clear_measure():
    labels = [Mock(), Mock(), Mock()]

    class fakeObj:
        _plot_item = Mock()
        _measure_labels = labels

    test_obj = fakeObj()
    mainWindow._clear_measure(test_obj)
    assert test_obj._measure_labels == []
    for label in labels:
        test_obj._plot_item.removeItem.assert_any_call(label)


def test_remove_measure(generic_test_object):
    mainWindow._remove_measure(generic_test_object)
    assert generic_test_object._ui.PlotWidget.clear_measure.called


def test_get_filters(generic_test_object):
    filt = mainWindow.get_filters(generic_test_object)
    # With everything checked, it should just be a bunch of mocks returned here
    for item in filt.values():
        assert isinstance(item, Mock)

    generic_test_object._ui.gbDensity.isChecked.return_value = False
    generic_test_object._ui.gbValidity.isChecked.return_value = False
    generic_test_object._ui.gbLongitude.isChecked.return_value = False
    generic_test_object._ui.gbLatitude.isChecked.return_value = False
    generic_test_object._ui.gbSensZenith.isChecked.return_value = False
    generic_test_object._ui.gbSolZenith.isChecked.return_value = False
    generic_test_object._ui.gbCloud.isChecked.return_value = False

    filt = mainWindow.get_filters(generic_test_object)
    # And with everything unchecked, all should be None
    for item in filt.values():
        assert item is None


def test_is_stacked(generic_test_object):
    stack = mainWindow.is_stack(generic_test_object)
    assert stack is generic_test_object._ui.cbStack.isChecked()


def test_clear_selection(generic_test_object):
    generic_test_object.on_bClearNoise_clicked = Mock()
    generic_test_object._selected_area = 42
    mainWindow._clear_selection(generic_test_object)
    assert generic_test_object._selected_area is None


def test_clear(generic_test_object):
    generic_test_object._clear_selection = Mock()
    generic_test_object._clear_winds = Mock()
    generic_test_object._remove_measure = Mock()
    generic_test_object._initalize_result_data_frames = Mock()

    mainWindow._clear_all(generic_test_object)

    assert generic_test_object._clear_selection.called
    assert generic_test_object._clear_winds.called
    assert generic_test_object._remove_measure.called
    assert generic_test_object._initalize_result_data_frames.called

    mainWindow._clear_select(generic_test_object)
    assert generic_test_object._ui.PlotWidget.clear_select.called


def test_pyqtgraph_plot(generic_test_object, pixel_paths, test_file_path):
    pyqtgraph.setConfigOptions(background = 'w', foreground = 'k')

    gen_other = partial(mainWindow._gen_other_brushes, generic_test_object)
    draw_plot = partial(mainWindow._draw_plot, generic_test_object)

    generic_test_object.so2data['pixel_paths'] = pixel_paths
    generic_test_object.displayed_plot = None
    generic_test_object._plot_item = pyqtgraph.PlotItem()
    generic_test_object._gen_other_brushes = gen_other
    generic_test_object._draw_plot = draw_plot
    generic_test_object.on_cbTrace_currentIndexChanged = Mock()
    generic_test_object.progress_dialog = Mock()
    generic_test_object.settings = Mock()
    generic_test_object.settings.value.return_value = False
    generic_test_object._du_color_map = pyqtgraph.ColorMap(
        [0, .05, .1, .175, .25, .99, 1],
        [(255, 255, 255, 192),
         (241, 187, 252, 192),
         (53, 248, 244, 192),
         (255, 225, 0, 192),
         (248, 152, 6, 192),
         (255, 19, 0, 192),
         (255, 0, 0, 192)]
    )
    original_plot = QGraphicsPixmapItem()
    generic_test_object.displayed_plot = original_plot

    mainWindow._generate_brushes(generic_test_object)
    brushes = generic_test_object._plot_brushes[0]

    view = pyqtgraph.PlotWidget(plotItem = generic_test_object._plot_item)
    view.setFixedSize(800, 600)

    mainWindow._pyqtgraph_plot(generic_test_object, brushes)

    assert isinstance(generic_test_object.displayed_plot,
                      QGraphicsPixmapItem)
    assert generic_test_object.displayed_plot is not original_plot

    QImageReader.setAllocationLimit(0)
    plot_img = generic_test_object.displayed_plot.pixmap().toImage()
    test_img = QPixmap(os.path.join(test_file_path, 'testPixmap.png')).toImage()
    assert plot_img == test_img

    assert generic_test_object.progress_dialog.close.called


def test_gen_other_brushes_nan_validity(generic_test_object):
    generic_test_object._plot_brushes = []
    generic_test_object.on_cbTrace_currentIndexChanged = Mock()
    generic_test_object.so2data['validity'][42] = numpy.nan
    generic_test_object._calc_quick_stats = Mock()
    mainWindow._gen_other_brushes(generic_test_object)
    assert not generic_test_object._plot_brushes[1].any()


def test_clear_percents(generic_test_object):
    class takeList(list):
        def takeAt(self, idx):
            return self.pop(idx)

    layout = takeList([Mock(), Mock(), None])
    generic_test_object._ui.percentLayout = layout

    mainWindow._clear_percents(generic_test_object)

    assert generic_test_object._ui.percentLayout == []


def test_cleanup_processing_thread():
    class fakeObj:
        pass

    test_obj = fakeObj()

    # Should do nothing, without error
    mainWindow._cleanup_processing_thread(test_obj)

    mock = Mock()
    test_obj._processor_thread = mock
    test_obj.loader_thread = "Bob"

    # Should call stuff on the mock
    mainWindow._cleanup_processing_thread(test_obj)

    assert mock.wait.called
    assert mock.deleteLater.called
    assert not hasattr(test_obj, "_processor_thread")


def test_no_data(monkeypatch, generic_test_object):
    show_error = partial(mainWindow.show_error_box, generic_test_object)
    generic_test_object.show_error_box = show_error
    generic_test_object.progress_dialog = Mock()
    message_box = Mock()
    monkeypatch.setattr(MainWindow, "QMessageBox", message_box)

    generic_test_object._ui.cbStack.isChecked.return_value = True

    mainWindow._no_data(generic_test_object)

    assert generic_test_object.progress_dialog.close.called
    assert message_box().show.called
    message_box().setText.assert_called_with("No data found for current filters/data files")

    generic_test_object.progress_dialog.close.reset_mock()
    generic_test_object._ui.cbStack.isChecked.return_value = False
    generic_test_object._nav_dir = 'backward'
    generic_test_object._ui.hsFileSelector.value.return_value = 0
    generic_test_object._ui.hsFileSelector.maximum.return_value = 3

    mainWindow._no_data(generic_test_object)

    assert generic_test_object._nav_dir == 'forward'
    assert generic_test_object.progress_dialog.close.called
    message_box().setText.assert_called_with("No data found for current filters, and no more files to try.")

    message_box().setText.reset_mock()
    generic_test_object.progress_dialog.close.reset_mock()

    mainWindow._no_data(generic_test_object)

    generic_test_object._ui.hsFileSelector.setValue.assert_called_with(1)
    assert not generic_test_object.progress_dialog.close.called
    assert not message_box().setText.called

    generic_test_object._nav_dir = 'backward'
    generic_test_object._ui.hsFileSelector.value.return_value = 3

    mainWindow._no_data(generic_test_object)

    generic_test_object._ui.hsFileSelector.setValue.assert_called_with(2)
    assert not generic_test_object.progress_dialog.close.called
    assert not message_box().setText.called


def test_after_load_complete(generic_test_object, monkeypatch):
    generic_test_object.progress_dialog = Mock()
    generic_test_object._load_canceled = False
    generic_test_object.show_error_box = Mock()
    generic_test_object.progress_dialog = Mock()
    generic_test_object.lFileTip = Mock()
    generic_test_object._no_data = Mock()  # Tested elsewhere
    generic_test_object.lFileTip.width.return_value = 142
    generic_test_object.lFileTip.height.return_value = 42
    generic_test_object._ui.hsFileSelector.value.return_value = 0
    generic_test_object._ui.hsFileSelector.width.return_value = 242
    generic_test_object._ui.hsFileSelector.height.return_value = 42
    generic_test_object._ui.hsFileSelector.pos.return_value = QPoint(42, 42)
    generic_test_object.centralWidget = Mock(return_value = QWidget())
    generic_test_object.is_stack = Mock(return_value = False)

    generic_test_object.so2data_cache = MainWindow.utils.DataCache(generic_test_object._ui.hsFileSelector)

    start_mock = Mock()
    monkeypatch.setattr(MainWindow.processor, "start", start_mock)

    proc_mok = Mock(wraps = MainWindow.processor)
    monkeypatch.setattr(MainWindow, "processor", proc_mok)

    moved = partial(mainWindow.on_hsFileSelector_sliderMoved, generic_test_object)
    generic_test_object.on_hsFileSelector_sliderMoved = moved

    set_buttons = partial(mainWindow._set_slider_buttons, generic_test_object)
    generic_test_object._set_slider_buttons = set_buttons

    ready = partial(mainWindow._on_data_ready, generic_test_object)
    generic_test_object._on_data_ready = ready

    cleanup = partial(mainWindow._cleanup_processing_thread, generic_test_object)
    generic_test_object._cleanup_processing_thread = cleanup

    filt = Mock(return_value = {
        'lng_from': None,
        'lat_from': None,
        'cloud_fraction': None,
        'density': None,
        'validity': None,
        'cloud_fraction': None,
    })
    generic_test_object.get_filters = filt

    mainWindow.on_after_loadComplete(generic_test_object, "Test Failure")
    generic_test_object.progress_dialog.pbProgress.setValue.assert_called_with(100000)
    assert generic_test_object.progress_dialog.close.called
    assert generic_test_object.show_error_box.called

    generic_test_object._load_canceled = True
    generic_test_object.show_error_box.reset_mock()
    generic_test_object.progress_dialog.close.reset_mock()
    generic_test_object.progress_dialog.pbProgress.setValue.reset_mock()

    mainWindow.on_after_loadComplete(generic_test_object, "Test Failure")
    assert generic_test_object.progress_dialog.close.called
    assert generic_test_object._load_canceled is False
    assert not start_mock.called

    generic_test_object.show_error_box.reset_mock()
    generic_test_object.progress_dialog.close.reset_mock()
    generic_test_object.progress_dialog.pbProgress.setValue.reset_mock()

    mainWindow.on_after_loadComplete(generic_test_object, None)
    generic_test_object.progress_dialog.lCurrentProcess.setText.assert_called_with("Filtering data and calculating pixels...")
    assert proc_mok.called
    assert start_mock.called


def test_show_error_box(monkeypatch):
    message_box = Mock()
    monkeypatch.setattr(MainWindow, "QMessageBox", message_box)

    self = Mock()
    finished = Mock()
    mainWindow.show_error_box(self, None, "text", "inf_text", "det_text", finished)
    assert self.message is message_box()

    message_box.assert_called()
    self.message.setText.assert_called_with("text")
    self.message.setInformativeText.assert_called_with("inf_text")
    self.message.setDetailedText.assert_called_with("det_text")
    self.message.finished.connect.assert_any_call(finished)
    assert self.message.show.called


def test_load_complete(generic_test_object, monkeypatch):
    monkeypatch.setattr(MainWindow.data_utils, "flatten_data",
                        Mock(return_value = generic_test_object.so2data))
    loaded = Mock()
    generic_test_object.file_loaded = loaded

    generic_test_object._prev_dataset = 1
    generic_test_object._ui.cbStack.isChecked.return_value = True

    mainWindow._load_complete(generic_test_object,
                              (generic_test_object.so2data, "Test Exception"))
    loaded.emit.assert_called_with("Test Exception")

    mainWindow._load_complete(generic_test_object,
                              (generic_test_object.so2data, None))
    loaded.emit.assert_called_with(None)
    assert (generic_test_object.so2data['SO2_column_number_density'] == generic_test_object.so2data['SO2_number_density_1km']).all()

    # Test a non-existant dataset
    generic_test_object._prev_dataset = 4
    loaded.emit.reset_mock()

    mainWindow._load_complete(generic_test_object,
                              (generic_test_object.so2data, None))
    loaded.emit.assert_called_with(None)

    # Make sure SO2_column_number_density has not changed
    assert (generic_test_object.so2data['SO2_column_number_density'] == generic_test_object.so2data['SO2_number_density_1km']).all()


def test_load_file_clicked(monkeypatch, test_file_path):
    message_box = Mock()
    message_box.Critical = Mock()
    message_box.critical = Mock()
    thread_start = Mock()

    monkeypatch.setattr(threading.Thread, "start", thread_start)
    monkeypatch.setattr(MainWindow, "QMessageBox", message_box)

    class dummyObj:
        files = []
        _ui = Mock()
        _clear_interpolated_results = Mock()
        _prev_filetype = None
        _prev_filters = {}
        so2data_cache = Mock()
        progress_dialog = Mock()
        _load_canceled = True
        _load_complete = Mock()
        on_cbFileType_currentTextChanged = Mock()

    self = dummyObj()
    self._ui.hsFileSelector.value.return_value = 0
    self._ui.cbStack.isChecked.return_value = True
    self._ui.cbFileType.currentText.return_value = "TROPOMI"
    self._ui.cbAltitude.currentData.return_value = 1
    self._ui.gbLongitude.isChecked.return_value = False
    self._ui.gbLatitude.isChecked.return_value = True

    self._ui.sbLatFrom.value.return_value = 40
    self._ui.sbLatTo.value.return_value = 42
    self._ui.sbLngFrom.value.return_value = 170
    self._ui.sbLngTo.value.return_value = -140

    self._ui.gbValidity.isChecked.return_value = True
    self._ui.sbValidity.value.return_value = 5
    self._ui.gbSensZenith.isChecked.return_value = True
    self._ui.sbSensZenith.value.return_value = 60
    self._ui.gbSolZenith.isChecked.return_value = True
    self._ui.sbSolZenith.value.return_value = 60

    mainWindow.on_loadFile_clicked(self)
    assert self._load_canceled is True
    good_file = os.path.join(test_file_path, 'S5P_NRTI_L2__SO2____20210105T231321_20210105T231821_16749_01_020104_20210106T001431.nc')
    self.files = [good_file, good_file]

    # Bad stack parameters - no longitude filter
    mainWindow.on_loadFile_clicked(self)
    assert self.on_cbFileType_currentTextChanged.called
    assert self._load_canceled is True
    assert not self.progress_dialog.open.called
    assert not self._load_complete.called
    assert self.progress_dialog.close.called
    assert message_box.called
    self.load_error.setText.assert_called_with("Unable to load data")
    assert self.load_error.show.called

    self._ui.gbLongitude.isChecked.return_value = True

    mainWindow.on_loadFile_clicked(self)

    assert self._load_canceled is False
    assert self.progress_dialog.open.called
    assert self._load_complete.called
    assert self._prev_filters != {}
    assert not thread_start.called

    prev_filters = self._prev_filters.copy()
    self.so2data_cache.__bool__ = Mock(return_value = False)

    mainWindow.on_loadFile_clicked(self)
    assert self._prev_filters == prev_filters
    assert thread_start.called

    # Not stacking, bad filename
    self.files = ["bob1", "bob2"]
    self._ui.cbStack.isChecked.return_value = False

    mainWindow.on_loadFile_clicked(self)
    self.load_error.setText.assert_called_with("Unable to load data")
    assert self.load_error.show.called


def test_stack_state_change():
    class fakeObj:
        _ui = Mock()
        files = []
        so2data_cache = Mock()
        on_loadFile_clicked = Mock()

    self = fakeObj()

    mainWindow.on_cbStack_stateChanged(self, Qt.Checked)
    self._ui.gbValidity.setChecked.assert_called_with(False)
    self._ui.gbValidity.setEnabled.assert_called_with(False)
    assert not self._ui.hsFileSelector.setEnabled.called
    assert not self.so2data_cache.clear.called
    assert not self.on_loadFile_clicked.called

    self.files = ['bob', 'smith']

    mainWindow.on_cbStack_stateChanged(self, Qt.Unchecked)

    self._ui.gbValidity.setEnabled.assert_called_with(True)
    self._ui.hsFileSelector.setEnabled.assert_called_with(True)
    assert self.so2data_cache.clear.called
    assert self.on_loadFile_clicked.called


def test_views_index_changed():
    view = {'centerLat': '56.00000',
            'centerLng': '-160.00000',
            'pixelSize': '1.00',
            'latTo': 59.6036036036036,
            'latFrom': 52.3963963963964,
            'ul_lon': -168.87687226304058,
            'ur_lon': -151.12312773695942,
            'll_lon': -167.36078724773765,
            'lr_lon': -152.63921275226235,
            'name': 'Alaska Peninsula'}

    sector_roi = Mock()

    class fakeObj:
        _remove_sector_bounds = mainWindow._remove_sector_bounds
        _show_sector_bounds = mainWindow._show_sector_bounds
        on_loadFile_clicked = Mock()  # Tested elsewhere

        _ui = Mock()
        chartready = False
        _vb = Mock()
        _sector_roi = sector_roi

    fakeObj._ui.cbViews.itemData.return_value = None

    self = fakeObj()

    # Test chart NOT ready
    mainWindow.on_cbViews_currentIndexChanged(self, 2)
    assert not self._vb.removeItem.called
    assert self._sector_roi is sector_roi
    assert not hasattr(self, "_view_corners")

    # Test when chart ready, but no valid view selected
    fakeObj.chartready = True

    mainWindow.on_cbViews_currentIndexChanged(self, 2)

    self._vb.removeItem.assert_called_with(sector_roi)
    assert self._sector_roi is None
    assert self._view_corners is None

    # Test specific view selected
    fakeObj._ui.cbViews.itemData.return_value = view
    fakeObj._vb.size.return_value = QSize(1024, 768)
    self._ui.cbUseSector.isChecked.return_value = True

    mainWindow.on_cbViews_currentIndexChanged(self, 2)

    self._vb.addItem.assert_called_with(self._sector_roi)
    assert hasattr(self, "_view_corners")
    assert self.on_loadFile_clicked.called

    # Test default sector
    mainWindow.on_cbViews_currentIndexChanged(self, 1)
    set_call = self._vb.setRange.call_args[1]
    expected_call = {'xRange': [-9981246, -4981246],
                     'yRange': [6500000, 11500000],
                     'padding': 0}

    assert set_call['yRange'] == expected_call['yRange']
    assert set_call['padding'] == 0

    # We should have expanded the X range here
    assert set_call['xRange'][0] < expected_call['xRange'][0]
    assert set_call['xRange'][1] > expected_call['xRange'][1]


def test_cbtrace_change_non_zero():
    """Most of this function was tested elsewhere, so we can be quick and dirty here"""
    class fakeObj:
        _pyqtgraph_plot = Mock()
        _ui = Mock()
        chartready = True
        _plot_brushes = [Mock(), Mock(), None]
        _gradients = [None, None, None]
        _scaleLabels = [None, None, None]
        _keys_top = 22

    self = fakeObj()
    idx = 1  # anything other than zero
    mainWindow.on_cbTrace_currentIndexChanged(self, idx)  # anything other than zero
    assert self._ui.percentContainer.hide.called
    self._pyqtgraph_plot.assert_called_once_with(self._plot_brushes[idx])
    self._ui.windkeyContainer.move.assert_called()


def test_on_cbhighlight_toggled():
    """This function should simply call _pyqtgraph_plot"""
    class fakeObj:
        _plot_brushes = []
        _ui = Mock()
        _pyqtgraph_plot = Mock()

    idx = 0
    fakeObj._ui.cbTrace.currentIndex.return_value = idx

    self = fakeObj()
    mainWindow.on_cbHighlight_toggled(self, "Bob")
    # No brushes, so shouldn't be called
    assert not self._pyqtgraph_plot.called

    self._plot_brushes = [Mock(), None, None]
    mainWindow.on_cbHighlight_toggled(self, "Bob")
    self._pyqtgraph_plot.assert_called_once_with(self._plot_brushes[idx])


def test_cbfiletype_text_changed():
    class fakeObj:
        _ui = Mock()
        _initalize_result_data_frames = mainWindow._initalize_result_data_frames
        _results_table_model = Mock()
        _results_tableView = Mock()
        _need_results_refresh = Mock()

    self = fakeObj()
    filters = [self._ui.gbValidity, self._ui.gbSensZenith, self._ui.gbSolZenith]
    mainWindow.on_cbFileType_currentTextChanged(self, "TROPOMI")

    assert self._load_file_filter == "TROPOMI Files (*.nc)"
    for filt in filters:
        filt.setEnabled.assert_called_with(True)

    mainWindow.on_cbFileType_currentTextChanged(self, "OMPS")

    assert self._load_file_filter == "OMPS (*.h5)"

    mainWindow.on_cbFileType_currentTextChanged(self, "IASI")

    assert self._load_file_filter == "IASI (*.nc)"
    for filt in filters:
        filt.setEnabled.assert_called_with(False)
        filt.setChecked.assert_called_with(False)


def test_hsfileselector_valuechanged():
    class fakeObj:
        _set_slider_buttons = mainWindow._set_slider_buttons
        setWindowTitle = Mock()
        on_loadFile_clicked = Mock()

        _ui = Mock()
        resizing = False
        files = []

    idx = 1
    self = fakeObj()
    mainWindow.on_hsFileSelector_valueChanged(self, idx)

    self.files = ['/some/file/path/to/somefile.nc']
    mainWindow.on_hsFileSelector_valueChanged(self, idx)
    self.setWindowTitle.assert_called_with(f"SO2 Explorer: {'somefile.nc'[:-3]}")


def test_file_nav_button_click():
    class fakeObj:
        _nav_dir = "bob"
        _ui = Mock()

    self = fakeObj()
    self._ui.hsFileSelector.value.return_value = 42

    mainWindow.on_bPrevFile_clicked(self)

    assert self._nav_dir == 'backward'
    self._ui.hsFileSelector.setValue.assert_called_with(41)

    mainWindow.on_bNextFile_clicked(self)
    assert self._nav_dir == 'forward'
    self._ui.hsFileSelector.setValue.assert_called_with(43)


def test_setupViews_current():
    """Most of this is tested elsewhere"""

    class fakeObj:
        _ui = Mock()

    self = fakeObj()
    self._ui.cbViews.currentText.return_value = "Bob"

    mainWindow._setup_views(self)

    self._ui.cbViews.setCurrentText.assert_called_with("Bob")


def test_fix_slider_size():
    class fakeObj:
        on_hsFileSelector_sliderMoved = Mock()
        _ui = Mock()
        resizing = "Bob"

    selector = QSpinBox()
    selector.setValue(42)
    selector.setMaximum(42)

    fakeObj._ui.hsFileSelector = selector

    self = fakeObj()

    mainWindow._fix_slider_size(self)

    self.on_hsFileSelector_sliderMoved.assert_called_with(42)
    assert selector.value() == 42
    assert self.resizing == False


def test_init():    
    mw = mainWindow(splash = Mock())
    # If this is true, then the entire init function ran without error.
    # Probqbly a bit paranoid- if there was an error, the test would
    # likely just fail anyway, but it feels wrong to not check *something*
    assert mw.chartready
    
def test_calculate_noise_full(generic_test_object, test_file_path):
    assert "noise_mass" not in generic_test_object._calc_results
    generic_test_object._lNoiseAreas = Mock()

    mainWindow._recalc_noise(generic_test_object)

    results = generic_test_object._calc_results
    # Check that the proper entries were created in the results DF
    assert "noise_mass" in results
    assert "noise_mean_du" in results
    assert "noise_max_du" in results
    assert "noise_mean_perc" in results
    assert "noise_max_perc" in results

    # Check that the values were calculated correctly
    expected_noise_mass = [.0154, .0113, .0031, .0019]
    expected_noise_mean_du = pandas.Series({
        1.0: 0.13,
        3.0: 0.09,
        7.0: 0.03,
        15.0: 0.02, }, name = 'noise_mean_du')
    expected_noise_max_du = pandas.Series({
        1.0: 3.83,
        3.0: 2.80,
        7.0: 0.76,
        15.0: 0.57, }, name = 'noise_max_du')
    expected_mean_perc = pandas.Series({
        1.0: 57.9,
        3.0: 56.7,
        7.0: 54.8,
        15.0: 54.3, }, name = 'noise_mean_perc')
    expected_max_perc = pandas.Series({
        1.0: 99.5,
        3.0: 99.5,
        7.0: 99.1,
        15.0: 99.4, }, name = 'noise_max_perc')
    assert (results['noise_mass'] == expected_noise_mass).all()
    assert (results['noise_mean_du'] == expected_noise_mean_du).all()
    assert (results['noise_max_du'] == expected_noise_max_du).all()
    assert (results['noise_mean_perc'] == expected_mean_perc).all()
    assert (results['noise_max_perc'] == expected_max_perc).all()

    assert generic_test_object._need_results_refresh.emit.called


def test_calc_cloud_validity(generic_test_object, test_file_path):
    expected_cloud = 21.2
    expected_validity = 86

    test_obj = generic_test_object
    with open(os.path.join(test_file_path, 'selection_mask.pickle'), 'rb') as file:
        selection_mask = pickle.load(file)

    mainWindow._calc_cloud_validity(test_obj, selection_mask)

    assert test_obj._selected_cloud == expected_cloud
    assert test_obj._selected_validity == expected_validity
    

def test_create_noise_rois(test_file_path, generic_test_object):
    sel = [(-7241726.932640292, 2198087.3140097405),
           (-7329835.251967947, 2195706.008081966),
           (-7320310.028256848, 2133792.0539598297),
           (-7234583.014856968, 2159986.419165349)]
    roi = pyqtgraph.PolyLineROI(sel, True)
    roi_bbox = roi.boundingRect()
    width = roi_bbox.width()
    height = roi_bbox.height()
    generic_test_object._ui = Mock()
    generic_test_object._ui.PlotWidget = Mock()
    generic_test_object._ui.PlotWidget.get_noise_roi = Mock(return_value = (sel,
                                                                            width,
                                                                            height))
    generic_test_object._vb = Mock()

    generic_test_object._recalc_noise = Mock()
    generic_test_object._clear_noise_rois = Mock()
    generic_test_object._noise_rois = []
    mode_pan = Mock()
    generic_test_object._ui.modePan = mode_pan
    generic_test_object._tool_button_clicked = Mock()
    generic_test_object._set_mode_pan = Mock()
    generic_test_object._recalc_noise_debounce = Mock(return_value = generic_test_object._recalc_noise())

    mainWindow.on_bSelectNoise_clicked(generic_test_object)
    expected_rois = [
        [Point(-7146474.695529, 2198087.314010),
         Point(-7234583.014857, 2195706.008082),
         Point(-7225057.791146, 2133792.053960),
         Point(-7139330.777746, 2159986.419165)],
        [Point(-7336979.169751, 2198087.314010),
         Point(-7425087.489079, 2195706.008082),
         Point(-7415562.265368, 2133792.053960),
         Point(-7329835.251968, 2159986.419165)],
        [Point(-7241726.932640, 2262382.574060),
         Point(-7329835.251968, 2260001.268132),
         Point(-7320310.028257, 2198087.314010),
         Point(-7234583.014857, 2224281.679215)],
        [Point(-7241726.932640, 2133792.053960),
         Point(-7329835.251968, 2131410.748032),
         Point(-7320310.028257, 2069496.793910),
         Point(-7234583.014857, 2095691.159115)]
    ]

    assert generic_test_object._recalc_noise.called
    assert len(generic_test_object._noise_rois) == 4
    for idx, roi in enumerate(generic_test_object._noise_rois):
        points = [x['pos'] for x in roi.handles]
        expected_points = expected_rois[idx]
        assert points == expected_points


def test_show_dlg():
    inner = MainWindow.show_dlg("Hi Bob!")  # Should give us the "inner" function
    def func(x): return 42
    decorator = inner(func)  # Returns a "wrapped" func

    class FakeSelf:
        _show_dialog = Mock()

    fake_self = FakeSelf()
    val = decorator(fake_self)
    assert val == 42
    fake_self._show_dialog.assert_called_with("Hi Bob!")
    assert fake_self._show_dialog().close.called
    assert fake_self._show_dialog().deleteLater.called
    

def test_cbusesector_checked(generic_test_object):
    generic_test_object._sector_roi = None
    generic_test_object._view_corners = None

    views = Mock()
    views.currentData.return_value = {
        'centerLat': '56.00000',
        'centerLng': '-160.00000',
        'pixelSize': '1.00',
        'latTo': 59.6036036036036,
        'latFrom': 52.3963963963964,
        'ul_lon': -168.87687226304058,
        'ur_lon': -151.12312773695942,
        'll_lon': -167.36078724773765,
        'lr_lon': -152.63921275226235,
        'name': 'Alaska Peninsula'
    }
    
    state_changed = partial(mainWindow.on_cbUseSector_stateChanged, generic_test_object)
    generic_test_object.on_cbUseSector_stateChanged = state_changed
    
    generic_test_object._show_sector_bounds = partial(mainWindow._show_sector_bounds,
                                                      generic_test_object)
    generic_test_object._remove_sector_bounds = partial(mainWindow._remove_sector_bounds,
                                                        generic_test_object)

    generic_test_object._ui.cbViews = views
        
    generic_test_object.on_cbUseSector_stateChanged(Qt.Checked)
    
    assert generic_test_object._sector_roi is not None
    assert generic_test_object._view_corners is not None
    
    
def test_cbusesector_unchecked(generic_test_object):
    roi_mock = Mock()
    generic_test_object._sector_roi = roi_mock
    generic_test_object._view_corners = Mock()
    
    state_changed = partial(mainWindow.on_cbUseSector_stateChanged, generic_test_object)
    generic_test_object.on_cbUseSector_stateChanged = state_changed
    
    generic_test_object._remove_sector_bounds = partial(mainWindow._remove_sector_bounds,
                                                        generic_test_object)
        
        
    generic_test_object.on_cbUseSector_stateChanged(Qt.Unchecked)
    generic_test_object._vb.removeItem.assert_called_with(roi_mock)
    assert generic_test_object._sector_roi is None
    assert generic_test_object._view_corners is None
    
def test_init_results_df(window_obj):
    assert window_obj._calc_results is None
    need_results = Mock()
    window_obj._need_results_refresh = need_results
    window_obj._initalize_result_data_frames()
    assert isinstance(window_obj._calc_results, pandas.DataFrame)
    assert need_results.emit.called
    
def test_unlock_stats(window_obj):
    lock = Mock()
    lock.release.side_effect = [RuntimeError]
    calc_quick_stats = Mock()

    window_obj._stats_lock = lock
    window_obj._calc_quick_stats = calc_quick_stats
    window_obj._stats_event.set()

    window_obj._unlock_stats()

    assert lock.release.called
    assert calc_quick_stats.called

def test_resultsWinGeometry(window_obj):
    geometry = PySide6.QtCore.QByteArray(b'\x01\xd9\xd0\xcb\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x7f\x00\x00\x01\xdf\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x7f\x00\x00\x01\xdf\x00\x00\x00\x00\x00\x00\x00\x00\x03 \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x7f\x00\x00\x01\xdf')
    window_obj.setResultsWinGeometry(geometry)
    geom, visible = window_obj.getResultsWinGeometry()

    assert visible is False

    # The value we get back is different than the value we set,
    # so just make sure we get *something*
    assert bytes(geom) != b''
    

def test_get_wind_plots(window_obj):
    plots = window_obj.get_wind_plots()
    assert plots == ()

    test_plots = [1, 2, 3]
    window_obj._wind_plots = test_plots
    plots = window_obj.get_wind_plots()
    assert isinstance(plots, tuple)
    assert plots == tuple(test_plots)
    

def test_load_status_received(window_obj):
    assert window_obj.progress_dialog.lCurrentProcess.text() == 'Reading File...'  # default
    window_obj._load_status_received("Test 1", 50, 100)
    assert window_obj.progress_dialog.lCurrentProcess.text() == 'Test 1'
    assert window_obj.progress_dialog.pbProgress.value() == 50
    assert window_obj.progress_dialog.pbProgress.maximum() == 100

    window_obj._load_status_received("Test complete", -1, 100)
    assert window_obj.progress_dialog.lCurrentProcess.text() == 'Test complete'
    assert window_obj.progress_dialog.pbProgress.value() == -1
    assert window_obj.progress_dialog.pbProgress.maximum() == 0
    

def test_load_progress_recieved(window_obj):
    window_obj.progress_dialog.pbProgress.setMaximum(100000)
    assert window_obj.progress_dialog.pbProgress.value() != 100
    window_obj._load_progress_received(420)
    assert window_obj.progress_dialog.pbProgress.value() == 100000
    

def test_recalc_noise_debounce(window_obj):
    """single line function, just sets a timer"""
    timer = Mock()
    window_obj._recalc_noise_timer = timer
    window_obj._recalc_noise_debounce()
    timer.start.assert_called_with(500)

def test_clear_noise(window_obj):
    vb = Mock()
    window_obj._vb = vb
    rois = [Mock(), Mock(), Mock()]
    results_df = pandas.DataFrame(columns = ['noise_mass',
                                             'noise_mean_perc', 'noise_max_du',
                                             'noise_max_perc'])

    window_obj._noise_rois = rois
    window_obj._noise_area = 42
    del window_obj._calc_results
    assert not hasattr(window_obj, "_calc_results")

    window_obj.on_bClearNoise_clicked()

    assert window_obj._noise_rois == []
    vb.removeItem.assert_any_call(rois[1])
    rois[2].deleteLater.assert_called()
    assert numpy.isnan(window_obj._noise_area)

    window_obj._noise_rois = rois
    window_obj._noise_area = 42
    window_obj._calc_results = results_df

    window_obj.on_bClearNoise_clicked()
    assert 'noise_mass' not in window_obj._calc_results
    assert 'noise_max_perc' not in window_obj._calc_results
    

def test_cancel_load(monkeypatch, window_obj):
    assert window_obj._load_canceled is False
    window_obj.on_bCancelLoad_clicked()
    assert window_obj._load_canceled is True
    assert window_obj._TERM_FLAG.is_set()