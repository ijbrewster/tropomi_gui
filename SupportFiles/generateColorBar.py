import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.colors import LinearSegmentedColormap

plt.rcParams.update({'font.size': 6})
fig, ax = plt.subplots(figsize=(5, .20))

colorDict = {'red': [[0.0, 1.0, 1.0],
                     [1.0, 1.0, 1.0]],
             'green': [[0.0, 1.0, 1.0],
                       [1.0, 0.0, 0.0]],
             'blue': [[0.0, 1.0, 1.0],
                      [1.0, 0.0, 0.0]], }


cmap = LinearSegmentedColormap('redMap', segmentdata=colorDict, N=512)
norm = mpl.colors.Normalize(vmin=0, vmax=50)

plt.subplots_adjust(**{'left': .01,
 'right': .97,
 'bottom': .17,
 'top': .90, })

cb1 = mpl.colorbar.ColorbarBase(ax, cmap=cmap,
                                norm=norm,
                                orientation='horizontal')
# cb1.set_ticklabels(['0', '.15', '3', '.45', '>0.6'])
cb1.ax.xaxis.set_tick_params(pad=-9, direction="in")


fig.show()
