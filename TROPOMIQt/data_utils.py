"""
data_utils.py - Utility functions for working with SO2 data
Copyright © 2020 Alaska Volcano Observatory
Distributed under MIT license. See license.txt for more information
"""

import logging
from datetime import datetime
import time

import numpy
import pyproj
import pyqtgraph as pg
import shapely

from pyproj.enums import TransformDirection
from PySide6.QtGui import QBrush, QColor
from scipy import stats
from shapely import linestrings
from shapely.prepared import prep
from shapely.geometry import Point, Polygon
from shapely.strtree import STRtree

from vincenty import vincenty

from . import utils, h5pyimport
from .h5pyimport import import_product

LAYERS = {
    'data': 0,
    'map': 10,
    'markers': 20,
    'sector': 30,
    'selections': 40,
    'labels': 50,
    'wind': 60,
    'volc_labels': 70,
}

# Mapping of filter names to column names when different
# TODO: Consider if we can just use the column names for the filter names in all cases
FILTER_ALIASES = {
    'density': 'SO2_column_number_density',
    'validity': 'SO2_column_number_density_validity',
    'lat_from': 'latitude',
    'lat_to': 'latitude',
    'lng_from': 'lognitude',
    'lng_to': 'longitude',
    'sensor_zenith': 'sensor_zenith_angle',
    'solar_zenith': 'solar_zenith_angle',
}

def load_file(filename, filter_string, options, callback=None, fields = None):
    """Load a data file.

    Parameters
    ----------
    filename : str
        The data file to load
    filter_string : str
        a formatted string of filters, such as field>4
    options : str
        Any options for loading the file, such as the primary altitude product
        to load.
    callback : callable
        a function to run on completion. Will be passed the resulting data.

    Returns:
    data : xarray
        xarray containing data loaded from the file
    """
    filter_string = filter_string or ""
    try:
        so2data = import_product(filename, filter_string, options, fields)
    # This is generally bad practice, but since I don't know exactly what sort
    # of errors I might get, and I want to respond to *any* error in the same way,
    # I'll go with it.
    except Exception as exc:
        logging.exception("Error importing data file")
        exc_str = str(exc) or f"Unknown Exception occured. {type(exc)}"
        result = (None, exc_str)
    else:
        # make sure we got a result
        if so2data and so2data['latitude'].size > 0:
            result = (so2data, None)
        else:
            result = (None, "No data loaded. Check your filters and data files")

    if callback is not None:
        callback(result)

    return result


def flatten_data(data):
    """
        Take a N-dimensional data structure, and "flatten" it to a
        N-1 dimensional structure

        Parameters
        ----------
        data : dictionary
            A data dictonary, containing one or more data structures to be
            flattened

        Returns
        -------
        data : dictionary
            The dictionary of data, with the flattened data structures
    """
    # Flatten the x-y data into a simple time-series type data
    data = data.stack(time = ["x", "y"]).reset_index('time', drop = True)

    # Get the mean of the data across the "file" dimension
    try:
        data = data.mean('file')
    except ValueError:
        pass  # No file dimension - only one file, no need to average anything.

    # make sure the dimensions are in the correct order
    args = ['time', 'corners']
    if 'layer' in data.dims:
        args.append('layer')
    data = data.transpose(*args)

    return data


def calc_selected_area(selection):
    """
        Given a selected area in mercator projection, convert to laea and
        calculate the area of the polygon

        Parameters
        ----------
        selection : list
            List of x,y points in the mercator projection that define the
            verticies of a selection polygon

        Returns
        -------
        area : integer
            The area of the polygon, in square kilometers
    """
    x_merc, y_merc = zip(*selection)
    x_laea, y_laea = utils.laea_transformer.transform(x_merc, y_merc)
    area = utils.poly_area([x_laea, ], [y_laea, ]) / 1e6
    area = round(area[0])
    return area


def parse_file_range(file_name):
    """
        Parse a file name into date from and date to

        Parameters
        ----------
        file_name : str
            The formatted file name to parse to/from dates from

        Returns
        -------
        res: tuple
            Two-element tuple of (date_from, date_to)
    """
    # seperate out the date information from the filename
    file_type = file_name[:4]
    if file_type == "S5P_":
        if 'SO2CBR' in file_name:
            file_date_info = file_name.split('_')[6:8]
        else:
            file_date_info = file_name.split("____")[1].split("_")[:2]

        date_from = datetime.strptime(file_date_info[0], "%Y%m%dT%H%M%S")
        date_to = datetime.strptime(file_date_info[1], "%Y%m%dT%H%M%S")
    elif file_type == "OMPS":
        file_date_info = file_name.split("_")[3]
        date_from = date_to = datetime.strptime(file_date_info, "%Ym%m%dt%H%M%S")
    elif file_type == "SO2_":
        file_date_info = file_name.split("_")
        date_from = datetime.strptime(file_date_info[1], "%Y%m%d%H%M")
        date_to = datetime.strptime(file_date_info[2], "%Y%m%d%H%M")
    elif file_type[0] == "V":
        # Viirs file. Day of year
        file_date_info = file_name[1:14]
        date_from = date_to = datetime.strptime(file_date_info, '%Y%j%H%M%S')

    return (date_from, date_to)


def get_mask(poly_bounds, xy_coords):
    """Given a polygon object and a list of cartesian coordinates, return a
    boolean mask and a filtered list of coordinates that *MIGHT* be within the
    polygon. Filtering is based only on the polygon bounding box, no check is
    performed to determine if the point is actually within the polygon itself"""
    x, y = numpy.hsplit(xy_coords, 2)
    x = numpy.asarray(x).squeeze()
    y = numpy.asarray(y).squeeze()

    x_min = x >= poly_bounds[0]
    y_min = y >= poly_bounds[1]
    min_mask = numpy.logical_and(x_min, y_min)

    x_max = x <= poly_bounds[2]
    y_max = y <= poly_bounds[3]
    max_mask = numpy.logical_and(x_max, y_max)

    data_mask = numpy.logical_and(min_mask, max_mask)
    xy_coords = xy_coords[data_mask]

    return (data_mask, xy_coords)


def filter_data_with_selection(data, selection, xy_coords, quick=False, grid_index = None, grid_geometries = None):
    """
        Given a set of cartesian coordinates, return a filter mask containing only
        the points within a specified area. If data is not None, also return a filtered dataset.

        If the area in question is a square, quick can be specified to skip the point in polygon check.
        This will greatly speed up exectution, however it will only work properly if the
        points in the polygon can be accurately determined by looking at bounding boxes alone.

        Parameters
        ----------
        data : numpy array, list, tuple, or None
            The data to filter down to just the portion contained within the area.
            If specified, must be the same length as xy_coords. If None, only
            the filter is calculated, no filtering is actually performed.
        selection : list
            Coordinates of the area corners, in same projection as data x,y
        xy_coords : list
            The coordinates defining the data locations.

        Returns
        -------
        found_data : numpy array or None
            The data points contained within the specified area, or None if data
            parameter was None.
        selection_mask : list
            An array of booleans that, when used to filter a properly sized
            dataset, will return all points that lie within the specified area
    """

    start = time.time()
    found_data = data
    selection_mask = numpy.full(len(xy_coords), fill_value = False,
                                dtype = bool)

    if grid_geometries is not None:
        merc_poly = Polygon(selection)
        mask = grid_index.query(merc_poly)
        if not quick:
            grid_canidates = grid_geometries[mask]
            intersects = shapely.intersects(merc_poly, grid_canidates)
            found_idxs = mask[intersects]
        else:
            found_idxs = mask

        if data is not None:
            found_data = data[found_idxs]

        selection_mask[found_idxs, ] = True

        logging.info(f"Ran filter in: {time.time() - start}")
        return(found_data, selection_mask)

    ##############################
    # Backup code in case this is called without grid_geometries or 
    # grid_index having been created yet.
    area = Polygon(selection)
    # this will get a rough mask, based just on the bounding box of the area.
    # Doing so will typically eliminate the bulk of the data, making subsiquent
    # comparisons with the actual polygon MUCH faster.
    idxs = numpy.arange(len(xy_coords))

    data_mask, xy_coords = get_mask(area.bounds, xy_coords)
    logging.info("Got mask, coords in %f", time.time() - start)

    # Make sure data is a numpy array so we can filter easily, and go ahead and
    # apply the rough filter.
    if data is not None:
        data = numpy.asarray(data)
        data = data[data_mask]

    found_idxs = idxs[data_mask]

    if not quick:
        area = prep(area)

        def point_in_area(x):
            return area.contains(Point(x))

        found_bool = list(map(point_in_area, xy_coords.data))
        found_idxs = found_idxs[found_bool]

        if data is not None:
            found_data = data[found_bool]
        logging.info("Ran loop in %f", time.time() - start)

    else:
        logging.info("Ran Quick in %f", time.time() - start)

    selection_mask[found_idxs, ] = True
    return (found_data, selection_mask)


def gen_dist_labels(selection, font = None):
    """
        Generate a list of distance labels between points of a selection

        Parameters
        ----------
        selection : list
            list of x,y points, in mercator projection, that make up the
            selected area

        Returns
        -------
        labels : list
            List of pyqtgraph TextItem labels to mark the distances between
            points
    """
    x_merc, y_merc = zip(*selection)

    # We want the acutal lat/lon to make distance calculations with
    # Mercator does not preserve accurate distances
    y_latlon, x_latlon = utils.latlon_merc_transformer.transform(x_merc, y_merc,
                                                                 direction = TransformDirection.INVERSE)
    latlon_selection = list(zip(y_latlon, x_latlon))

    labels = []
    background_brush = QBrush(QColor(255, 255, 255, 128))

    for idx, point in enumerate(latlon_selection[1:]):
        prev_point = latlon_selection[idx]
        dist = round(vincenty(point, prev_point))

        # +1 is safe because the index is offset to the negitive by one, since
        # index starts at 0 but we started at the first point in the selection
        midpoint = (numpy.asarray(selection[idx])
                    - numpy.asarray(selection[idx + 1])) / 2 + \
            numpy.asarray(selection[idx + 1])
        label = pg.TextItem(f"{dist}km", (0, 0, 0), fill=background_brush, anchor=(.5, .5))
        if font is not None:
            label.setFont(font)

        label.setPos(*midpoint)
        label.setZValue(LAYERS['volc_labels'])
        labels.append((label, dist))

    return labels


def calc_alt_mass_du(row, densities, pixel_areas, selection_mask):
    """
        Given a result data row and selection mask, calculate the mass and du
        value for the altitude

        Parameters
        ----------
        row : Pandas DataFrame row
            The calc result pandas DataFrame row for the requested altitude
            product.
        densities : ndarray
            Numpy ndarray containing the densities for the entire dataset at
            the desired altitude
        pixel_areas : ndarray
            numpy ndarray of per-data-point areas. Applies to all altitudes.
        selection_mask : ndarray
            boolean array containing the filter for the selection

        Returns
        -------
        total_mass : float
            The total mass of the selected area at the altitude of interest
        alt_du_val : ndarray
            List of DU values for the selected altitude, filtered for the
            selected area
        all_du : ndarray
            array of all du values for the specified altitude
    """
    if row['interpolated'] == True:
        # If this is an interpolated value, we already have the mass
        # and DU values calculated for the dataset
        all_du = row['int_du']
        alt_du_val = all_du[selection_mask]
        alt_mass_val = row['int_mass'][selection_mask]
    else:
        # We need to pull the density data from the dataset, and
        # calculate DU and mass
        all_densities = densities
        all_du = all_densities * 2241.15
        alt_du_val = all_du[selection_mask]

        # Get the per-pixel mass for this area. Start with the density
        alt_mass_val = all_densities[selection_mask]

        # Multiply the density of each pixel by the area of each pixel
        # to get the mass (in moles) of each pixel, then multiply by 64
        # since SO2 is 64 grams/mole to get the per-pixel mass in grams.
        alt_mass_val = (pixel_areas * alt_mass_val) * 64

    # Convert the mass from grams to kt
    alt_mass_val /= 1e9
    # Get the total mass for this area
    total_mass = round(alt_mass_val.sum().item(), 4)

    # We save the total mass and the list of DU values for the sector to
    # a dictionary to be used by the calling function (if noise),
    # or passed on to set labels with (if selection)
    return (total_mass, alt_du_val, all_du)


def calc_other_values(row, total_mass, all_du, alt_du_val):
    """
        Calculate mean and max du and percentile for the altitude, and, if
        interpolated (which gives us a wind time), the emission rate

        Parameters
        ----------
        row : dataframe row
            The currently calculated values for an altitude product
        total_mass : float
            The total SO2 mass for the selected area
        all_du : ndarray
            The DU values for the entire dataset
        alt_du_val : float
            the calculated DU value for the given altitude

        Returns
        -------
        ret_dict : dictionary
            Dictionary containing mean and max DU value for the selection,
            percentile value for mean and max DU, and, if an interpolated
            altitude, emission rate.
    """
    # Mean and max DU values for this sector, saved to the results DataFrame
    mean_du = numpy.round(numpy.nanmean(alt_du_val), 2)
    max_du = numpy.round(numpy.nanmax(alt_du_val), 2)

    # And the percentile, based on the full DU dataset
    mean_sel_percentile = stats.percentileofscore(all_du,
                                                  mean_du)
    max_sel_percentile = stats.percentileofscore(all_du,
                                                 max_du)

    mean_sel_percentile = round(mean_sel_percentile, 1)
    max_sel_percentile = round(max_sel_percentile, 1)

    ret_dict = {
        'sel_mean_du': mean_du,
        'sel_max_du': max_du,
        'sel_mean_perc': mean_sel_percentile,
        'sel_max_perc': max_sel_percentile
    }

    if row['interpolated'] == True:
        # If we are interpolated, go ahead and calculate the emission rate
        # We may not have any noise calculated, if the user did the wind first
        # (or only). If so, we can just use 0 to get a result without noise adjustment.
        try:
            noise_mass = row['noise_mass']
        except KeyError:
            noise_mass = 0.0

        if numpy.isnan(noise_mass):
            noise_mass = 0.0

        noise_mass *= 1000  # Convert kt to tonnes

        plume_mass = total_mass * 1000  # again, kt to tonnes
        plume_mass -= noise_mass

        # row['wind_time'] == age of plume, based on wind point clicked
        # Formula from Taryn Lopez
        rate = round((plume_mass / row['wind_time']) * 24, 2)
        ret_dict['em_rate'] = rate

    return ret_dict


def build_rtree_index(latitude_bounds, longitude_bounds):
    # Create an Rtree index for the grid cells (polygons formed by the four corners)
    # Convert coordinates to Albers Equal area
    x_bounds, y_bounds = utils.latlon_merc_transformer.transform(
        latitude_bounds.data.reshape(-1),
        longitude_bounds.data.reshape(-1)
    )
    x_bounds = x_bounds.reshape(longitude_bounds.shape)
    y_bounds = y_bounds.reshape(latitude_bounds.shape)

    # Stack latitude and longitude bounds into an array of grid cells (4 corners each)
    merc_bounds = numpy.stack((x_bounds, y_bounds), axis=-1)
    cell_polygons = shapely.polygons(merc_bounds)
    stree_index = STRtree(cell_polygons)

    return stree_index, cell_polygons

def find_data_lines(lines, grid_index = None, cell_polygons = None, latitude_bounds = None, longitude_bounds = None):
    if grid_index is None or cell_polygons is None:
        if latitude_bounds is None or longitude_bounds is None:
            raise ValueError("Must provide either BOTH latitude_bounds AND longitude_bounds OR BOTH grid_index AND cell_bboxes")
        # Build the Rtree index for the grid cells (polygons)
        grid_index, cell_polygons = build_rtree_index(latitude_bounds, longitude_bounds)

    result = []
    
    line_latlons = lines.reshape(-1, 2).T
    line_x, line_y = utils.latlon_merc_transformer.transform(
        line_latlons[0],
        line_latlons[1]
    )
    
    laea_lines = numpy.stack([line_x, line_y], axis=1).reshape(lines.shape)
    
    line_geoms = linestrings(laea_lines)
    canidate_cell_idxs = grid_index.query(line_geoms)
    canidate_lines = line_geoms[canidate_cell_idxs[0]]
    canidate_cells = cell_polygons[canidate_cell_idxs[1]]
    intersections = shapely.intersects(canidate_lines, canidate_cells)

    for line_idx in range(len(lines)):
        line_mask = canidate_cell_idxs[0] == line_idx
        cell_indicies = canidate_cell_idxs[1][line_mask]
        intersecting_cells = cell_indicies[intersections[line_mask]]
        result.append(intersecting_cells)

    return result
