"""
    A polyline ROI implementation that allows the ROI to be "locked" so it
    doesn't respond to (most) mouse events
"""

import pyqtgraph as pg
from pyqtgraph import LineSegmentROI
from PySide6.QtCore import Qt


class _LockingPolyLineSegment(LineSegmentROI):
    """
        PolyLine segment to go with the locking PolyLine ROI. Ignores
        mouse hover and click events.
    """
    # Used internally by PolyLineROI

    def __init__(self, *args, **kwds):
        self._parentHovering = False
        LineSegmentROI.__init__(self, *args, **kwds)

    def setParentHover(self, hover):
        """Update the hover color when the parent hover state changes"""
        # set independently of own hover state
        if self._parentHovering != hover:
            self._parentHovering = hover
            self._updateHoverColor()

    def hoverEvent(self, ev):
        """Consume hover events without doing anything"""
        return

    def mouseClickEvent(self, ev):
        """ignore click events"""
        ev.ignore()

    def _makePen(self):
        """
            Return the proper pen, depending on if we (or are parent)
            are being hovered over.
        """
        if self.mouseHovering or self._parentHovering:
            return self.hoverPen

        return self.pen


class LockingPolyLineROI(pg.PolyLineROI):
    """PolyLine ROI implemention with the ability to ignore mouse events (lock)"""
    _locked = False

    def set_locked(self, lock):
        """Set the locked state of this ROI"""
        self._locked = bool(lock)
        self.translatable = not self._locked
        self.rotatable = not self._locked
        self.resizable = not self._locked

    def segmentClicked(self, segment, ev = None, pos = None):
        """Ignore the click event and do nothing if locked"""
        if self._locked:
            ev.ignore()
            return True

        return super().segmentClicked(segment, ev, pos)

    def hoverEvent(self, ev):
        """Do nothing on hover if locked, otherwise behave normally"""
        if self._locked:
            return True

        return super().hoverEvent(ev)

    def mouseClickEvent(self, ev):
        """Do nothing on mouseClick if locked, otherwise behave normally"""
        if self._locked:
            ev.ignore()
            return True

        return super().mouseClickEvent(ev)

    def setMouseHover(self, hover):
        if self._locked:
            hover = False

        return super().setMouseHover(hover)

    def addSegment(self, h1, h2, index=None):
        seg = _LockingPolyLineSegment(handles=(h1, h2), pen=self.pen, hoverPen=self.hoverPen,
                                      parent=self, movable=False)
        if index is None:
            self.segments.append(seg)
        else:
            self.segments.insert(index, seg)
        seg.sigClicked.connect(self.segmentClicked)
        seg.setAcceptedMouseButtons(Qt.NoButton)
        seg.setZValue(self.zValue() + 1)
        for h in seg.handles:
            h['item'].setDeletable(False)
            h['item'].setAcceptedMouseButtons(Qt.NoButton)
