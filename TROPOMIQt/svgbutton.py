from PySide6.QtSvgWidgets import QSvgWidget
from PySide6.QtCore import (Signal,
                            QByteArray,
                            QFile,
                            QIODevice
                            )

from PySide6.QtWidgets import (QWidget,
                               QFrame)

from PySide6.QtGui import QColor

from PySide6.QtXml import QDomDocument


class QSvgButton(QSvgWidget):
    _fill_color = QColor(0, 0, 0)
    _hover_color = None
    _active_color = None
    _isActive = False

    clicked = Signal(QWidget)

    def __init__(self, file=None, frame = None, parent=None):
        super().__init__(parent)
        self.setContentsMargins(10, 10, 10, 10)

        self._frame = frame
        if file is not None:
            svg = QFile(file)
            svg.open(QIODevice.ReadOnly)
            svg_data = svg.readAll()
            svg.close()
            self.load(svg_data)

    def setFrame(self, frame):
        self._frame = frame

    def setFillColor(self, color):
        self._fill_color = color
        self._load_colored(color)

    def setHoverColor(self, color: QColor, isActive: bool = False):
        self._hover_color = color
        if isActive:
            self._active_color = color

    def setActiveColor(self, color: QColor, isHover: bool = False):
        self._active_color = color
        if isHover:
            self._hover_color = color

    def setActive(self, active):
        self._isActive = active
        if active and self._active_color is not None:
            self._load_colored(self._active_color)
        else:
            self._load_colored(self._fill_color)

        if self._frame is not None:
            if active:
                self._frame.setFrameShadow(QFrame.Sunken)
            else:
                self._frame.setFrameShadow(QFrame.Raised)

    def load(self, contents: QByteArray):
        self._imgData = QDomDocument()
        self._imgData.setContent(contents)
        self._load_colored(self._fill_color)

    def enterEvent(self, event):
        if self._hover_color is not None:
            self._load_colored(self._hover_color)

        super().enterEvent(event)

    def leaveEvent(self, event):
        # If we were able to change it, set it back to normal
        if self._hover_color is not None:
            color = self._active_color if self._isActive and self._active_color is not None \
                else self._fill_color
            self._load_colored(color)

        super().leaveEvent(event)

    def mouseReleaseEvent(self, event):
        # For good measure, probably has no effect here
        super().mouseReleaseEvent(event)
        self.clicked.emit(self)

    def _load_colored(self, color: QColor):
        # Contents is an XML document. Modify and dump to QByteArray
        contents = self._imgData

        color_string = color.name(QColor.HexRgb)
        alpha = color.alphaF()

        # Assume 1 level SVG
        children = contents.documentElement().childNodes()
        for idx in range(children.count()):
            element = children.at(idx)
            if not element.isElement():
                continue  # pragma: nocover
            element = element.toElement()
            if element.tagName() == "path":
                element.setAttribute("fill", color_string)
                element.setAttribute("fill-opacity", str(alpha))

        super().load(contents.toByteArray())

