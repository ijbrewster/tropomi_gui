"""
MainWindow.py - Subclass of QMainWindow
Copyright © 2020 Alaska Volcano Observatory
Distributed under MIT license. See license.txt for more information
"""
import csv
import gc
import gzip
import logging
import math
import multiprocessing
import os
import pickle
import sys
import threading
import time

from collections import defaultdict
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timezone, timedelta
from functools import wraps, partial
from io import BytesIO
from math import sqrt, floor
from threading import Thread

import h5py
import numpy
import pyproj
from pyproj import Geod
from pyproj.enums import TransformDirection

import pandas as pd
import requests
import xarray

from PIL import Image
from scipy import stats
from scipy.interpolate import interp1d
from shapely.geometry import Point

from pyresample import geometry, create_area_def, kd_tree

from PySide6 import QtUiTools
from PySide6.QtPrintSupport import QPrinter
from PySide6.QtGui import (QKeySequence,
                           QColor,
                           QLinearGradient,
                           QFont,
                           QBrush,
                           QPen,
                           QTransform,
                           QPixmap,
                           QImage,
                           QPainter,
                           QCursor,
                           QPalette,
                           QPageSize,
                           QPicture,
                           QFontMetrics,
                           QAction)

from PySide6.QtWidgets import (QMainWindow,
                               QFileDialog,
                               QDialog,
                               QWidget,
                               QLabel,
                               QApplication,
                               QMessageBox,
                               QProgressBar,
                               QMenuBar,
                               QInputDialog,
                               QMenu,
                               QGraphicsScene,
                               QGraphicsPixmapItem,
                               QGroupBox,
                               QVBoxLayout,
                               QHBoxLayout,
                               QCheckBox,
                               QTableView,
                               QAbstractButton,
                               QPushButton,
                               QFrame
                               )

from PySide6.QtCore import (Slot,
                            SLOT,
                            Signal,
                            Qt,
                            QPointF,
                            QPoint,
                            QCoreApplication,
                            QSettings,
                            QMetaObject,
                            QSize,
                            QSizeF,
                            QRectF,
                            QTimer,
                            QMarginsF,
                            QBuffer
                            )

from PySide6.QtSvg import QSvgRenderer
from PySide6.QtSvgWidgets import QGraphicsSvgItem

from matplotlib.backends.backend_qtagg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import pyqtgraph as pg

from . import data_utils, messenger, utils, __version__, transverse_calc
from .data_utils import LAYERS
from .h5pyimport import get_filetype
from .LatLonAxisItem import LatLonAxisItem
from .svgbutton import QSvgButton
from .PlotGraphicsView import PlotGraphicsView, Mode
from .GradientScale import GradientWidget
from .ScaleWidget import ScaleWidget
from .load_thread import ProcessorThread as processor
from .droppableLineEdit import droppableLineEdit
from .SectorEditor import SectorEditor
from .VolcanoEditor import VolcanoEditor
from .viewPresets import SECTORS
from .viewPresets import VOLCANOES as DEFAULT_VOLCS
from .preferences import PrefsWin
from .LockingPolyLineROI import LockingPolyLineROI
from .pandasTableModel import PandasTableModel
from .DataScale import DataScale
from TROPOMIQt import h5pyimport

CUR_DIRECTORY = os.path.dirname(__file__)

if getattr(sys, 'frozen', False):
    # Only when frozen, and testing is frozen, so...
    MAIN_DIR = sys._MEIPASS  # pragma: nocover
else:
    MAIN_DIR = os.path.realpath(os.path.join(CUR_DIRECTORY, '../'))

PROJ_LIB = os.path.realpath(os.path.join(CUR_DIRECTORY, "../share/proj"))
pyproj.datadir.set_data_dir(PROJ_LIB)


def clear_on_exit(f):
    """Wrapper to ensure that the stats_lock is *always* released when
    exiting the calc stats function, and to call calc again if needed.

    Runs in the class context, so is ok to access protected self members.
    """
    @wraps(f)
    def wrapper(self, *args, **kwargs):
        try:
            res = f(self, *args, **kwargs)
        finally:
            # Always release the lock, even if an exception occured
            self._trigger_unlock.emit()

        return res
    return wrapper


def show_dlg(message):
    """
        Wrap a function call in the _show_dialog() function, such that the dialog
        is opened before the function is called, and always closed when the
        function exits (even if with an exception)
    """
    def inner(f):
        @wraps(f)
        def real_decorator(self, *args, **kwargs):
            dlg = self._show_dialog(message)
            try:
                ret = f(self, *args, **kwargs)
            finally:
                dlg.close()
                dlg.deleteLater()

            return ret
        return real_decorator
    return inner


class mainWindow(QMainWindow):
    """The main window class. QMainWindow Subclass

    Creates the main map window, and contains functions for interacting with it"""
    # Various properties set when file is loaded or user selects an area
    data_start = None
    data_end = None
    files = []
    _selected_volcs = set()
    so2data = None
    _prev_filters = {}
    _prev_validity = -1
    _prev_lat_from = 9999
    _prev_lat_to = 9999
    _prev_lon_from = 9999
    _prev_lon_to = 9999
    _prev_dataset = 7
    _prev_filetype = 'TROPOMI'
    _zoom_level = 0  # large area
    _bkg_images = defaultdict(list)
    _visible_bkg_images = defaultdict(list)
    _calcProcs = 0
    _nav_dir = 'forward'
    _cur_map = None
    _load_canceled = False

    file_loaded = Signal(str)
    startup_progress = Signal(str)
    _render_ready = Signal()
    _setText = Signal(object, str)
    _setStyleSheet = Signal(object, str)
    _trigger_unlock = Signal()
    _need_results_refresh = Signal()
    _show_msg = Signal((QMessageBox.Icon, str, str, str))
    _close_progress = Signal()

    # String for formatting hover text: SO2, Cloud, and validity
    HOVER_FORMAT_STRS = [
        """SO<sub>2</sub> Mass: {mass:.4f} g<br>
SO<sub>2</sub> Density: {density:.5f} g/m^2<br>
SO<sub>2</sub> Density (DU): {du:.2f} DU<br>
Validity: {validity}<br>
Data Area: {area} Km^2<br>
Percentile: {percentile}<sup>th</sup>""",
        "{cloud:.1f}%",
        "{validity}"
    ]

    def __init__(self, splash=None, parent=None):
        super().__init__(parent)

        if splash:
            self.startup_progress.connect(splash.showMessage)

        self.settings = QSettings()
        self.chartready = False

        # Scaling factor to use when generating graph images. Lower numbers look better, but use more memory and time to draw.
        self.plotScale = 500

        # Define some attributes we will use later. They can be none/empty for now.
        # note that these are not really neccesary, but they make pylint happy,
        # and remove any ambiguity as to if the attribute exists or not
        self._scale_widget = None
        self._calc_results = None
        self._results_table_model = PandasTableModel()
        self._view_area = None
        self._processor_thread = None
        self._map_item = None
        self._export_options = None
        self._finished_bkg_render = None
        self._prefswin = None
        self.xy_coords = None
        self._wind_plots = []
        self._circle_plots = []
        self._noise_rois = []
        self._measure_labels = []
        self._plot_brushes = []
        self._sector_roi = None
        self._view_corners = None
        self.load_error = None
        self.resizing = False
        self._load_file_filter = None
        self.so2mass = None
        self.message = None
        self._selected_area = None
        self._selected_cloud = None
        self._selected_validity = None
        self._recalc_timer = None
        self._quickstats_timer = QTimer()
        self._scaleChange_timer = QTimer()
        self._calc_results_noise_areas = 0
        self._selected_points = []
        self._results_win = QMainWindow()
        self._recalc_noise_timer = QTimer()
        self._noise_coverages = []
        self._noise_area = numpy.nan
        self.volc_plot = None
        self._volc_labels = []
        self._scale_min = 0
        self._scale_max = 20
        self._loaded_dataset = None
        self._loaded_file = None
        self._loaded_filter = None
        self._dsDialog:DataScale = None
        self._wind_point_meta = {}

        # Event to use when canceling a file load
        self._TERM_FLAG = multiprocessing.Event()
        h5pyimport._TERM_FLAG = self._TERM_FLAG

        self._quickstats_timer.setSingleShot(True)
        self._quickstats_timer.timeout.connect(self._calc_quick_stats)

        self._recalc_noise_timer.setSingleShot(True)
        self._recalc_noise_timer.timeout.connect(self._recalc_noise)

        self._scaleChange_timer.setSingleShot(True)
        self._scaleChange_timer.timeout.connect(self._rescale_image)

        # Append user views, if any, to standard
        self._user_views = utils.load_user_views()

        # Load the user-defined volcano list, or the default
        self._update_volcanoes()

        ui_loader = QtUiTools.QUiLoader()
        ui_loader.registerCustomWidget(droppableLineEdit)
        ui_loader.registerCustomWidget(PlotGraphicsView)
        ui_loader.registerCustomWidget(GradientWidget)

        # Load the main window
        self._ui = ui_loader.load(CUR_DIRECTORY + "/MainWindow.ui", self)
        isinstance(self._ui, QMainWindow)

        #Fix some font issues on windows
        #if sys.platform.startswith("win") or sys.platform == 'cygwin':
            #sb_items = self._ui.gbFilters.findChildren(QSpinBox)
            #dsb_items = self._ui.gbFilters.findChildren(QDoubleSpinBox)
            #items = sb_items + dsb_items
            #for item in items:
                #font = item.font()
                ## This was also set in the designer, but apparently windows overrides
                ## it or something, so we have to set it again here.
                ##font.setFamily('Arial')
                #font.setPointSize(13)
                #item.setFont(font)

        # Save the size of the windowWidget to set this window to that size
        self.window_size = self._ui.size()
        self.setCentralWidget(self._ui.centralWidget())
        self._build_menus()
        self.setWindowTitle("SO2 Explorer")

        # Load the progress dialog
        self.progress_dialog = ui_loader.load(CUR_DIRECTORY + "/ProgressDialog.ui", self)

        # initalize some parameters for progress dialog
        isinstance(self.progress_dialog, QDialog)
        self.progress_dialog.setParent(self)
        self.progress_dialog.setWindowFlags(Qt.Dialog | Qt.FramelessWindowHint | Qt.WindowTitleHint | Qt.Sheet)
        self._close_progress.connect(self.progress_dialog.close)

        # Disable the file selector and navigation buttons initally
        self._ui.hsFileSelector.setEnabled(False)
        self._ui.bPrevFile.setEnabled(False)
        self._ui.bNextFile.setEnabled(False)

        # And hide the title
        self._ui.lTitle.hide()

        # Create a "floating" QLabel to show the current file selected
        self.lFileTip = QLabel("Hi!", self)
        self.lFileTip.setTextInteractionFlags(Qt.TextSelectableByMouse | Qt.TextSelectableByKeyboard)
        self.lFileTip.hide()

        # We want to give the option to limit "global" calculations to only the
        # defined sector area. Easier to add here than in the UI file so it
        # flows with the other dynamically created buttons.
        self._ui.cbUseSector = QCheckBox("Use Sector")
        self._ui.cbHighlight = QCheckBox("Highlight")
        self._ui.cbHighlight.setObjectName("cbHighlight")
        self._ui.cbUseSector.setObjectName("cbUseSector")
        self._ui.hlNavigation.addWidget(self._ui.cbHighlight)
        self._ui.hlNavigation.addWidget(self._ui.cbUseSector)

        self._ui.navLayout = QHBoxLayout()
        self._ui.navLayout.setSpacing(2)
        self._ui.hlNavigation.addLayout(self._ui.navLayout)

        # Make some connections
        QMetaObject.connectSlotsByName(self)
        self.file_loaded.connect(self.on_after_loadComplete)
        self._trigger_unlock.connect(self._trigger_unlock_timer)
        self._need_results_refresh.connect(self._refresh_results)
        self._show_msg.connect(self.show_error_box)

        self._setup_views()

        # Set up trace selector
        self._ui.cbTrace.addItem("SO2", 0)
        self._ui.cbTrace.addItem('Cloud Cover', 1)
        self._ui.cbTrace.addItem('Validity', 2)

        # set up data caches
        self.so2data_cache = utils.DataCache(self._ui.hsFileSelector)

        # Set up the graph item
        self._du_color_map = pg.ColorMap([0, .05, .1, .175, .25, .99, 1],
                                         [(255, 255, 255),
                                          (241, 187, 252),
                                          (53, 248, 244),
                                          (255, 225, 0),
                                          (248, 152, 6),
                                          (255, 19, 0),
                                          (255, 0, 0)])

        pg.setConfigOptions(background = 'w', foreground = 'k')
        self._selection = []

        # Set up the basic plot items

        self._ui.PlotWidget = create_plot_widget(self)
        self._ui.vlPlotArea.addWidget(self._ui.PlotWidget)

        self._ui.PlotWidget.areaSelected.connect(self._area_selected)
        self._ui.PlotWidget.windsSelected.connect(self._get_winds)
        self._ui.PlotWidget.forwardWindsSelected.connect(self._get_forward_winds)
        self._ui.PlotWidget.windsCleared.connect(self._clear_winds)
        self._ui.PlotWidget.selectionCleared.connect(self._clear_all)
        self._ui.PlotWidget.measureCleared.connect(self._clear_measure)

        self._plot_item = self._ui.PlotWidget.getPlotItem()
        self._vb = self._plot_item.getViewBox()

        # Create a placeholder widget for the percentiles
        self._percentile_levels = (90, 95, 97, 99, 100)
        self._percentile_widgets = []
        self._percentile_labels = []

        self._ui.percentContainer = create_percentile_widget(len(self._percentile_levels),
                                                             self._percentile_widgets,
                                                             self._percentile_labels)

        # Position the percentile widget, and hide it
        self._keys_top = self._vb.pos().y() + 3
        self._ui.percentContainer.setParent(self._ui.PlotWidget)
        self._ui.percentContainer.move(50, self._keys_top)
        self._ui.percentContainer.hide()

        # And the wind level "key"
        self._ui.windkeyContainer = create_wind_key()
        self._ui.windkeyContainer.setParent(self._ui.PlotWidget)
        self._ui.windkeyContainer.move(50, self._keys_top)
        self._ui.windkeyContainer.hide()

        # Add Background maps
        imgt = time.time()
        self._svg_item, self._svg_renderer = load_map_svg()

        self._mapScene = QGraphicsScene()

        self._mapScene.setBackgroundBrush(QBrush(Qt.transparent, Qt.SolidPattern))
        self._mapScene.addItem(self._svg_item)

        self.startup_progress.emit("Drawing plot area...")
        QApplication.processEvents()

        # Don't automatically scale to the data
        self._vb.disableAutoRange()
        self._plot_item.hideButtons()

        # Don't try to draw points outside the visible range
        # Apparently has some issues, but would be really nice...
        # self._plot_item.setClipToView(True)

        # Set the default range
        self._vb.setRange(xRange=(-9981246, -4981246), yRange=(6500000, 11500000))

        self.displayed_plot = None

        QApplication.processEvents()

        cloud_gradient = QLinearGradient(0, 0, 1, 1)
        cloud_gradient.setColorAt(0, QColor(0, 0, 0, 0))
        cloud_gradient.setColorAt(1, QColor("black"))

        validity_gradient = QLinearGradient(0, 0, 1, 1)
        validity_gradient.setColorAt(0, QColor("blue"))
        validity_gradient.setColorAt(1, QColor("red"))

        self._gradients = [
            self._du_color_map.getGradient(),
            cloud_gradient,
            validity_gradient
        ]

        self._scaleLabels = [
            {0: "0", .05: "1", .1: "2", .25: "5",.75: "15", 1: ">20", },  # SO2 Concentration
            {0: "0%", 1: "100%", },
            {0: "0", 1: "100", }
        ]

        self._ui.lHScale.setOrientation('Vertical')
        color_scale_font:QFont = self._ui.lHScale.font()
        color_scale_font.setPointSize(12)
        color_scale_font.setStyleStrategy(QFont.PreferQuality)

        color_scale_tick_font = QFont(color_scale_font)
        color_scale_tick_font.setPointSize(10)

        self._ui.lHScale.setFont(color_scale_font, color_scale_tick_font)
        self._ui.lHScale.setGradient(self._du_color_map.getGradient())
        self._ui.lHScale.setLabels(self._scaleLabels[0])
        self._ui.lHScale.setTitle("SO<sub>2</sub> Column Density (DU)")

        self._buildNavigation()
        self._build_results_win()

        self._tip = QLabel(self)
        self._tip.setWindowFlags(Qt.Widget | Qt.ToolTip)
        self._tip.setContentsMargins(4, 2, 4, 2)

        self._ui.cbAltitude.setCurrentText('7km')

        # Event to quit background threads cleanly
        self._quit_event = multiprocessing.Event()

        # Set up/Start the background render thread
        self._bkg_render_request = multiprocessing.Event()

        self._bkg_render_trigger = utils.OrEvent(self._quit_event, self._bkg_render_request)

        # Debounce timer for the background re-render
        self._bkg_render_timer = QTimer()
        self._bkg_render_timer.setSingleShot(True)
        self._bkg_render_timer.timeout.connect(self._run_render_background)

        # Background thread to re-render the map image when idle
        self._render_lock = threading.Lock()
        self._render_thread = Thread(target=self._background_render_thread,
                                     daemon=True)
        self._render_thread.start()

        # initalize the quick stats lock/event
        self._stats_lock = threading.Lock()
        self._stats_event = threading.Event()

        self._calcstats_trigger = utils.OrEvent(self._quit_event, self._stats_event)

        # Background thread to calculate quick stats when idle
        self._quick_calc_thread = Thread(target = self._stat_calc_thread,
                                         daemon = True)
        self._quick_calc_thread.start()

        # Connect some signals
        # self._plot_item.scene().sigMouseClicked.connect(self.plot_clicked)
        self._plot_item.scene().setClickRadius(1)

        self._ui.PlotWidget.mouseHover.connect(self.mouseHover)
        self._ui.PlotWidget.mouseMoved.connect(self.mouseMoveEvent)
        self._ui.PlotWidget.zoomData.connect(self._zoom_data)
        self._ui.PlotWidget.viewResized.connect(self._render_background)

        self._vb.sigRangeChangedManually.connect(self._clear_view_selection)

        self._vb.sigRangeChanged.connect(self._delay_quick_stats)
        self._vb.sigRangeChanged.connect(self._react_to_zoom)
        self._vb.sigRangeChanged.connect(self._render_background)

        self._render_ready.connect(self._add_map)
        self._setText.connect(utils.on_setText_triggered)
        self._setStyleSheet.connect(utils.on_setStyleSheet_triggered)

        # Messaging queue/thread for other threads/processes to
        # communiucate with the main (gui) thread
        self._messenger = messenger.Messenger()
        self._messenger.progress.connect(self._load_progress_received)
        self._messenger.status.connect(self._load_status_received)
        self._messenger_thread = threading.Thread(
            target = self._messenger.run,
            daemon = True
        )
        self._messenger_thread.start()

        utils.MESSAGE_QUEUE = self._messenger.get_queue()

        self.chartready = True
        logging.info(f"Set up graph in: {time.time()-imgt}")

    @Slot()
    def on_bRunTraverse_clicked(self):
        wind_data = tuple(zip(self._wind_point_meta['lon'], self._wind_point_meta['lat']))
        volc = self._wind_point_meta['volc']

        circles, traverse_points = transverse_calc.gen_traverses(
            wind_data,
            volc,
            self.so2data
        )

        circle_plots = transverse_calc.plot_circles(self._plot_item, circles)
        self._circle_plots.extend(circle_plots)

        poly_points = transverse_calc.traverses_to_poly(traverse_points, wind_data[0])
        roi = self._ui.PlotWidget.get_select_roi()
        roi.setPoints(poly_points, True)
        self._ui.PlotWidget.set_select_region(roi.saveState())

        with ThreadPoolExecutor(max_workers = 1) as executor:
            future = executor.submit(
                transverse_calc.calc_so2_mass_traverse,
                traverse_points,
                self.so2data
            )
            future.add_done_callback(self._so2_ts_calculated)


    def _so2_ts_calculated(self, future):
        """
        This function just grabs the result from the future,
        and passes it back to the main thread for display
        """
        self._temp_mass_ts = future.result()
        QMetaObject.invokeMethod(
            self,
            "_process_so2_ts",
            Qt.QueuedConnection
        )

    @Slot()
    def _process_so2_ts(self):
        y_values = self._temp_mass_ts
        self._temp_mass_ts = None
        start_time = self._wind_point_meta['point_time']
        wind_interval = timedelta(minutes = self._wind_point_meta['interval'])
        x_values = [start_time + i * wind_interval for i in range(len(y_values))]

        dialog = QDialog(self)
        def cleanup():
            dialog.deleteLater()

        dialog.finished.connect(cleanup)

        dialog.setWindowTitle("SO₂ Time Series Plot")
        dialog.setLayout(QVBoxLayout())

        # Create Matplotlib figure
        figure, ax = plt.subplots()
        canvas = FigureCanvas(figure)
        dialog.layout().addWidget(canvas)

        # Plot data
        ax.plot(x_values, y_values, marker="o", label="SO2 Levels")
        ax.set_xlabel("Time")
        ax.set_ylabel("SO$_2$ (kt)")
        ax.set_title("SO$_2$ Mass Over Time")
        ax.legend()
        ax.grid(True)

        ax.xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y %H:%M'))
        for label in ax.get_xticklabels():
            label.set_rotation(-25)  # Adjust the angle to your preference
            label.set_ha('left')  # Horizontal alignment to the left
            label.set_va('top')    # Vertical alignment to the top

        figure.tight_layout()

        # Draw the plot
        canvas.draw()

        # Create a horizontal layout for the buttons
        button_layout = QHBoxLayout()

        # Add a "Close" button
        close_button = QPushButton("Close")
        close_button.clicked.connect(dialog.accept)
        button_layout.addWidget(close_button)

        # Add a "Save as PDF" button
        save_button = QPushButton("Save as PDF")
        save_button.clicked.connect(self.save_plot_as_pdf)
        button_layout.addWidget(save_button)

        # Align the buttons to the bottom right
        button_layout.addStretch()
        dialog.layout().addLayout(button_layout)

        # Show the dialog with the plot
        dialog.show()

    def save_plot_as_pdf(self):
        # Open a file dialog to select where to save the PDF
        file_dialog = QFileDialog(self)
        file_dialog.setAcceptMode(QFileDialog.AcceptSave)
        file_dialog.setDefaultSuffix("pdf")
        file_dialog.setNameFilter("PDF Files (*.pdf)")

        # If a file is selected
        if file_dialog.exec() == QFileDialog.Accepted:
            file_path = file_dialog.selectedFiles()[0]
            # Save the plot as a PDF
            plt.savefig(file_path, format='pdf')
            print(f"Plot saved as: {file_path}")


    def _build_results_win(self):
        self._results_win.setWindowTitle("Calculation Results")
        centralWidget = QWidget(self._results_win)
        centralLayout = QVBoxLayout(centralWidget)
        centralWidget.setLayout(centralLayout)
        self._results_win.setCentralWidget(centralWidget)

        global_results = QGroupBox(centralWidget)
        global_results.setTitle("Global")
        global_layout = QVBoxLayout(global_results)
        global_results.setLayout(global_layout)
        centralLayout.addWidget(global_results)

        altitude_results = QGroupBox(centralWidget)
        altitude_results.setTitle("Per Altitude Results")
        centralLayout.addWidget(altitude_results)
        alt_layout = QVBoxLayout(altitude_results)
        altitude_results.setLayout(alt_layout)

        row_layout = QHBoxLayout()
        global_layout.addLayout(row_layout)

        noise_areas_layout = QHBoxLayout()
        noise_areas_layout.addWidget(QLabel("Noise Areas Used:"))
        self._lNoiseAreas = QLabel("0")
        self._lNoiseAreas.setFixedWidth(20)
        noise_areas_layout.addWidget(self._lNoiseAreas)
        row_layout.addLayout(noise_areas_layout)

        selected_layout = QHBoxLayout()
        selected_layout.addWidget(QLabel("Selected Area:"))
        self._lSelectedArea = QLabel("0")
        self._lSelectedArea.setFixedWidth(70)
        selected_layout.addWidget(self._lSelectedArea)
        row_layout.addLayout(selected_layout)

        row_layout.addStretch()

        self._results_tableView = QTableView()
        self._results_tableView.setModel(self._results_table_model)

        btn = self._results_tableView.findChild(QAbstractButton)
        if btn:
            btnLayout = QVBoxLayout(btn)
            btnLayout.setContentsMargins(0, 0, 0, 0)
            label = QLabel("alt")
            label.setContentsMargins(2, 0, 0, 0)
            btnLayout.addWidget(label)

            btn.setText('alt')
            btn.setEnabled(True)
            btn.setVisible(True)

        alt_layout.addWidget(self._results_tableView)

    def getResultsWinGeometry(self):
        return (self._results_win.saveGeometry(), self._results_win.isVisible())

    def setResultsWinGeometry(self, geom):
        self._results_win.restoreGeometry(geom)

    @Slot()
    def _trigger_unlock_timer(self):
        # Wait half a second to unlock for rate limiting/race condition avoidance
        self._unlock_timer = QTimer()
        self._unlock_timer.setSingleShot(True)
        self._unlock_timer.timeout.connect(self._unlock_stats)
        self._unlock_timer.start(500)

    @Slot()
    def _unlock_stats(self):
        # Releaste the stats calc lock and see if we need to run again
        try:
            self._stats_lock.release()
        except RuntimeError:  # indicates it is not locked. Good enough.
            pass

        # Now that the lock is released, see if we need to run again
        if self._stats_event.is_set():
            self._calc_quick_stats()

    def get_wind_plots(self):
        """
            Public function to get the private wind_plots attribute.
            Return as a tuple so as to be immutable.
        """
        return tuple(self._wind_plots)

    def _initalize_result_data_frames(self, altitudes = None):
        """Create new, empty data frames to hold the results of calculations and
        keep track of display widgets.

        Takes a list of altitudes to initalize the dataframes with"""

        if altitudes is None:
            altitudes = [self._ui.cbAltitude.itemData(i)
                         for i in range(self._ui.cbAltitude.count())
                         if self._ui.cbAltitude.itemData(i)]

        altitudes = pd.Series(altitudes, dtype = float)

        # Create the _calc_results dataframe with just the columns that always
        # need to exist, as we query on them potentially before they are populated.
        # Other columns will be added when the value is calculated.

        self._calc_results = pd.DataFrame()
        self._calc_results['alt'] = altitudes
        self._calc_results.set_index('alt', inplace = True)

        # Set the type of the int_mass and int_du columns to object
        self._calc_results['int_mass'] = pd.Series(dtype = object)
        self._calc_results['int_du'] = pd.Series(dtype = object)
        self._calc_results['interpolated'] = pd.Series(dtype = bool)
        self._calc_results['interpolated'] = [False, ] * len(altitudes)

        self._results_table_model.setDataframe(self._calc_results)

        for col in (0, 1):
            self._results_tableView.hideColumn(col)

        self._need_results_refresh.emit()

    @Slot(int)
    def on_cbUseSector_stateChanged(self, state):
        """The "Use Sector" checkbox handler"""
        if state == Qt.Checked:
            view = self._ui.cbViews.currentData()
            if view is not None:
                self._show_sector_bounds(view)
        elif state == Qt.Unchecked:
            self._remove_sector_bounds()

        self._calc_quick_stats()

    @Slot()
    def on_bCancelLoad_clicked(self):
        """Cancel loading a file"""
        self._load_canceled = True
        self.progress_dialog.pbProgress.setRange(0, 0)
        self.progress_dialog.lCurrentProcess.setText('Canceling...')
        self.progress_dialog.lCurrentProcess.repaint()
        QApplication.processEvents()
        self._TERM_FLAG.set()

    @Slot(float)
    def _load_progress_received(self, progress):
        if progress > 100:
            progress = 100

        progress = math.floor(progress * 1000)

        if progress > self.progress_dialog.pbProgress.value():
            self.progress_dialog.pbProgress.setValue(progress)

    @Slot(str, int, int)
    def _load_status_received(self, status, progress, end):
        self.progress_dialog.lCurrentProcess.setText(status)
        if progress == -1:
            self.progress_dialog.pbProgress.setRange(0, 0)
        else:
            self.progress_dialog.pbProgress.setRange(0, end)
            self.progress_dialog.pbProgress.setValue(progress)

    @Slot()
    def on_bSelectNoise_clicked(self):
        """Calc noise handler

        Create four noise regions surrounding the selected area and calculate
        noise parameters for them."""
        cur_roi, width, height = self._ui.PlotWidget.get_noise_roi()

        # Create new ROI's for noise
        self._clear_noise_rois()
        noise_roi_defs = [[(x + width, y) for x, y in cur_roi],
                          [(x - width, y) for x, y in cur_roi],
                          [(x, y + height) for x, y in cur_roi],
                          [(x, y - height) for x, y in cur_roi]]

        for roi_def in noise_roi_defs:
            roi = noise_roi(roi_def)
            roi.sigRegionChanged.connect(self._recalc_noise_debounce)
            self._noise_rois.append(roi)
            self._vb.addItem(roi)

        self._recalc_noise_debounce()

        self._tool_button_clicked(self._ui.modePan)
        self._set_mode_pan()

    def _recalc_noise_debounce(self):
        self._recalc_noise_timer.start(500)

    def _recalc_noise(self):
        """Calculate noise mass, du, percentiles, etc"""
        if not hasattr(self, "so2data"):
            # No data to work with
            return

        mass_results_df = pd.DataFrame()
        du_results_df = pd.DataFrame()
        noise_areas = 0
        # TODO: Determine if we have enough data in area to use
        self._noise_area = None
        self._noise_coverages = []
        for idx, roi in enumerate(self._noise_rois):
            offset = roi.pos()
            points = [(pt.pos().x() + offset.x(),
                       pt.pos().y() + offset.y())
                      for pt in roi.getHandles()]
            if self._noise_area is None:
                x_cords, y_cords = list(zip(*points))
                x_aea, y_aea = utils.laea_transformer.transform(x_cords, y_cords)
                x_cords = numpy.asarray([x_aea])
                y_cords = numpy.asarray([y_aea])
                self._noise_area = utils.poly_area(x_cords, y_cords)
                self._noise_area = self._noise_area[0] * 1e-6  # in square km
                logging.info("Total noise area is: %f", self._noise_area)

            mass, du, filter_result = self._calculate_area_mass(points)
            if filter_result is not None:
                data_area = self.so2data.area[filter_result[1]].sum()
                data_area *= 1e-6
                cov_percent = float(data_area) / self._noise_area
            else:
                cov_percent = 0

            self._noise_coverages.append(round(cov_percent * 100, 1))
            if mass is not None and cov_percent >= .95:
                roi.setPen({'color': "#888", 'width': 5, })
                mass_series = pd.Series(mass, name = noise_areas)
                du_series = pd.Series(du, name = noise_areas)
                if mass_results_df.empty:
                    mass_results_df[0] = mass_series
                    du_results_df[0] = du_series
                else:
                    mass_results_df = mass_results_df.join(mass_series)
                    du_results_df = du_results_df.join(du_series)
                noise_areas += 1
            else:
                roi.setPen({'color': "#F88", 'width': 5, })

        self._lNoiseAreas.setText(str(noise_areas))
        self._calc_results_noise_areas = noise_areas

        if mass_results_df.empty:
            self._calc_results['noise_mass'] = numpy.nan
            self._calc_results['noise_mean_du'] = numpy.nan
            self._calc_results['noise_max_du'] = numpy.nan
            self._calc_results['noise_mean_perc'] = numpy.nan
            self._calc_results['noise_max_perc'] = numpy.nan
            self._need_results_refresh.emit()
            return  # Data loaded, but nothing in the results area

        self._calc_results['noise_mass'] = mass_results_df.median(1).round(4)

        # Get the mean/max of noise by taking the mean/max of each of the noise
        # sectors individually, then taking the median value of the sectors
        self._calc_results['noise_mean_du'] = du_results_df.applymap(numpy.nanmean).median(1).round(2)
        self._calc_results['noise_max_du'] = du_results_df.applymap(numpy.nanmax).median(1).round(2)

        # Calculate the percentile of noise mean and max
        for alt, row in self._calc_results.iterrows():
            mean_du = row['noise_mean_du']
            max_du = row['noise_max_du']

            # Need to compare to True explicitly, because NaN is apparently truthy.
            if row['interpolated'] == True:
                all_du = self._get_interpolated_density(alt)
            else:
                try:
                    all_du = self.so2data[f'SO2_number_density_{int(alt)}km']
                except KeyError:
                    # Try interpolating to this altitude after all
                    all_du = self._get_interpolated_density(alt)

            all_du = all_du * 2241.15
            alt = float(alt)

            mean_percentile = round(stats.percentileofscore(all_du, mean_du), 1)
            max_percentile = round(stats.percentileofscore(all_du, max_du), 1)

            self._calc_results.at[alt, 'noise_mean_perc'] = mean_percentile
            self._calc_results.at[alt, 'noise_max_perc'] = max_percentile

        self._calc_quick_stats()
        self._need_results_refresh.emit()

    @Slot()
    def on_bClearNoise_clicked(self):
        """Clear Noise handler

        Remove noise results and areas."""

        self._clear_noise_rois()

    def _clear_noise_rois(self):
        """Remove all ROI's defining noise areas from the map"""
        for roi in self._noise_rois:
            self._vb.removeItem(roi)
            roi.deleteLater()

        # Clear out the results
        try:
            self._calc_results.drop(columns = ['noise_mass', 'noise_mean_du',
                                               'noise_mean_perc', 'noise_max_du',
                                               'noise_max_perc'],
                                    inplace = True,
                                    errors = "ignore")
            self._results_table_model.setDataframe(self._calc_results)
        except AttributeError:
            pass  # No results frame yet

        self._noise_coverages = []
        self._noise_rois = []
        self._calc_results_noise_areas = 0
        self._lNoiseAreas.setText(str(0))
        self._noise_area = numpy.nan

    @Slot()
    def on_bSaveRecord_clicked(self):
        """Save calculation results and information about the selection,
        along with a map image, to a CSV file.

        The path to which these items are saved is defined in the preferences
        dialog.

        Function is written to produce reasonable results under all circumstances,
        including if the user has not loaded a data file or made a selection.
        """

        self.settings.beginGroup("preferences")
        file_path = self.settings.value('path')
        result_file = self.settings.value('resultFile', 'AVO_Operational_Satellite_SO2_analysis.csv')
        self.settings.endGroup()

        # If the user has not set a default path in preferences,
        # ask them for it now.
        # User selection will be saved as the default for future usage.
        if file_path is None:
            file_path = self.select_save_path()

        # Make sure the desired path exists
        os.makedirs(file_path, exist_ok = True)

        analysis_time = datetime.now(timezone.utc).strftime('%m/%d/%Y %H:%M:%S')

        # find start/end date/time of the loaded data file
        date_from, date_to = self._get_data_dates()
        date_from = date_from.strftime('%m/%d/%Y %H:%M') if date_from is not None else 'nan'
        date_to = date_to.strftime('%m/%d/%Y %H:%M') if date_to is not None else 'nan'

        # Compile a bunch of variables into local variables so we can easily
        # catch errors and re-arrange the output as needed.
        stacked = self._ui.cbStack.isChecked()
        file_index = 0 if stacked else self._ui.hsFileSelector.value()

        # Figure out the file type. Currently only for TROPOMI or OMPS,
        # IASI is still incomplete.
        try:
            file_name = os.path.basename(self.files[file_index])
        except IndexError:
            file_name = None
            file_type = ''
        else:
            if self._prev_filetype == 'TROPOMI':
                # TROPOMI could be either NRTI or Offline, figure out which
                # from the file name.
                file_type = f'TROPOMI_{file_name[4:8]}'
            elif self._prev_filetype == 'OMPS':
                file_type = 'OMPS'
            elif self._prev_filetype == 'VIIRS':
                file_type = 'VIIRS'

        # Translate textual OMPS altitudes to integers,
        # otherwise just get the altitude
        altitude = self._ui.cbAltitude.currentText()

        if altitude == "*N/A*":
            altitude = "NaN"

        if len(self.files) == 0:
            num_files = 0
        else:
            num_files = 1 if not stacked else len(self.files)

        # Take the latitude/longitude directly from the filters
        # TODO: Consider if this is always accurate, or if we need to do something
        # different on occasion (say, when the lat/lon filters are not checked).
        # Possibly use lat/lon from view, rather than filter/file?
        viewx, viewy = self._vb.viewRange()  # X,Y range in mercator
        viewy, viewx = utils.latlon_merc_transformer.transform(viewx, viewy,
                                                               direction = TransformDirection.INVERSE)

        lat_from = round(viewy[0], 4)
        lat_to = round(viewy[1], 4)
        lon_from = round(viewx[0], 4)
        lon_to = round(viewx[1], 4)

        # Split the results up into standard and interpolated datasets for
        # easier handling
        try:
            std_product = self._calc_results.query('interpolated!=True')
            int_product = self._calc_results.query('interpolated==True')
            int_product.sort_index(inplace = True)  # Put in order of altitude
        except AttributeError:
            # No calculation results yet, so use an empty dataframe
            std_product = pd.DataFrame()
            int_product = pd.DataFrame()

        # Need the OR in here because the results dataframe is not initalized
        # before loading a data file.
        num_alts = len(std_product.index) or self._ui.cbAltitude.count()

        if len(self._noise_coverages) < 4:
            noise_coverage = [numpy.nan] * 4
        else:
            noise_coverage = self._noise_coverages
        try:
            # Basic information about the selected area
            sel_mass = std_product['selected_mass'].to_numpy()
            sel_cloud = self._selected_cloud
            sel_validity = self._selected_validity
            # Remove any characters that are not a valid part of a number from
            # the selected area label
            sel_area = self._selected_area
        except (KeyError, AttributeError, ValueError):
            # Nothing selected
            sel_mass = numpy.full((num_alts, ), numpy.nan)
            sel_cloud = 0
            sel_validity = 0
            sel_area = 0

        # Mean and max of the *entire* dataset
        if self.so2data is not None:
            mean_du = round(numpy.nanmean(self.so2data['SO2_column_number_density']) * 2241.15, 2)
            max_du = round(numpy.nanmax(self.so2data['SO2_column_number_density']) * 2241.14, 2)
        else:
            mean_du = 'nan'
            max_du = 'nan'

        noise_areas = self._calc_results_noise_areas
        try:
            noise_mass = std_product['noise_mass'].to_numpy()
        except (KeyError, AttributeError):
            noise_mass = numpy.full(num_alts, 'nan')
            corr_mass = sel_mass
        else:
            corr_mass = numpy.round(sel_mass - noise_mass, 4)

        # For mean and max values, we also want the percentile.
        # The zip combined with the list comprehension interlaces the two values
        # to create a single list we can append to the output.
        try:
            noise_mean_du = [value for pair in zip(std_product.noise_mean_du,
                                                   std_product.noise_mean_perc)
                             for value in pair]
        except (KeyError, AttributeError):
            noise_mean_du = numpy.full(num_alts * 2, 'nan').tolist()

        try:
            noise_max_du = [value for pair in zip(std_product.noise_max_du,
                                                  std_product.noise_max_perc)
                            for value in pair]
        except (KeyError, AttributeError):
            noise_max_du = numpy.full(num_alts * 2, 'nan').tolist()

        try:
            sel_mean_du = [value for pair in zip(std_product.sel_mean_du,
                                                 std_product.sel_mean_perc)
                           for value in pair]
        except (KeyError, AttributeError):
            sel_mean_du = numpy.full(num_alts * 2, 'nan').tolist()

        try:
            sel_max_du = [value for pair in zip(std_product.sel_max_du,
                                                std_product.sel_max_perc)
                          for value in pair]
        except (KeyError, AttributeError):
            sel_max_du = numpy.full(num_alts * 2, 'nan').tolist()

        validity_filter = self._ui.sbValidity.value() if self._ui.gbValidity.isChecked() else 'nan'
        cloud_threshold = self._ui.sbCloudPercent.value() if self._ui.gbCloud.isChecked() else 'nan'
        notes = self._ui.leRecordNotes.text() or 'nan'

        # Re-run the quick stats calculation so we can get the results directly
        view_stats = self._run_calc_quick_stats()
        view_mass, view_area, max_so2, max_lon, max_lat = view_stats or ['nan'] * 5

        #####################################
        # Create a "record" to be saved to the CSV file
        #####################################
        record = [analysis_time, date_from, date_to, file_type, ", ".join(self._selected_volcs),
                  altitude, num_files, lat_from, lat_to, lon_from, lon_to,
                  mean_du, max_du, max_lon, max_lat, view_mass, view_area]

        # List comprehension just to format. Works with NaN values.
        record += [f"{mass:.4f}" for mass in sel_mass]
        record += sel_mean_du
        record += sel_max_du

        record += noise_coverage
        record += [sel_cloud, sel_validity, sel_area, noise_areas]

        record += noise_mass.tolist()

        # These may or may not be numpy arrays already, but we need them to be a
        # list regardless, so convert to numpy arrays so we can always call tolist()
        # Bit of extra processing power vs checking first, but simple and effective.
        record += numpy.asarray(noise_mean_du).tolist()
        record += numpy.asarray(noise_max_du).tolist()
        record += [f"{mass:.4f}" for mass in corr_mass]

        # Add in altitude specific things
        # Len 1 means only a single altitude was selected, use NaN values for min/max constraints.
        if len(int_product.index) == 1:
            record += ['nan'] * 12
            record += [int_product.index[0], int_product.selected_mass.iloc[0],
                       int_product.wind_dist.iloc[0], int_product.wind_time.iloc[0],
                       int_product.wind_speed.iloc[0], int_product.em_rate.iloc[0]]
        elif len(int_product.index) == 3:
            # Two wind vectors were selected so we have all three altitude products
            # Assume the index is sorted, since we sorted it above
            # Go through the altitude products as min, max, and "best"
            for alt in [int_product.index[0], int_product.index[2],
                        int_product.index[1]]:
                row = int_product.loc[alt]
                record += [alt, ]
                record += [row.selected_mass, row.wind_dist, row.wind_time,
                           row.wind_speed, row.em_rate]
        else:
            # Only other option *should* be zero, but this will catch *any* number
            # of results other than 1 or 3, possibly producing unexpected output
            # if, somehow, two or four results were desired.
            record += ['nan'] * 18

        record += [validity_filter, cloud_threshold, notes]

        ####################################
        # End record creation
        ####################################

        save_file = os.path.join(file_path, result_file)
        new_file = not os.path.exists(save_file)

        if new_file:
            #########################
            # Create header record
            #########################
            headers = ['Analysis Date', 'Date From', 'Date To', 'Type', 'Volcanos',
                       'Altitude', '# Files', 'Latitude From',
                       'Latitude To', 'Longitude From', 'Longitude To',
                       'Data Mean (DU)', 'Data Max (DU)', 'Max Lon', 'Max Lat',
                       'View Mass (kt)', 'View Area (sq km)']

            mass_strs = []
            noise_strs = []
            noise_mean_du_strs = []
            noise_max_du_strs = []
            sel_mean_du_strs = []
            sel_max_du_strs = []
            corr_mass_strs = []

            alts = std_product.index
            if alts.size == 0:
                # Get the altitude options from the pull-down menu if no altitudes
                # are present in the result data frame. Use the text rather than data,
                # since data may not be populated yet.
                alts = [self._ui.cbAltitude.itemText(x).replace('km', '') for x in
                        range(self._ui.cbAltitude.count())]

            # Some of these could be pulled out to list comprehensions, but do
            # them all here so we only loop over the alts once.
            for alt in alts:
                mass_strs.append(f"Sel Mass ({alt}km, kt)")
                noise_strs.append(f"Median Noise ({alt}km, kt)")
                noise_mean_du_strs.append(f"Median Noise Mean ({alt}km, DU)")
                noise_mean_du_strs.append(f"Median Noise Mean Percentile ({alt}km)")
                noise_max_du_strs.append(f"Noise Max ({alt}km, DU)")
                noise_max_du_strs.append(f"Noise Max Percentile ({alt}km)")
                sel_mean_du_strs.append(f"Selection Mean ({alt}km, DU)")
                sel_mean_du_strs.append(f"Sel Mean Percentile ({alt}km)")
                sel_max_du_strs.append(f"Selection Max ({alt}km, DU)")
                sel_max_du_strs.append(f"Sel Max Percentile ({alt}km)")
                corr_mass_strs.append(f"Corr. SO2 ({alt}km, kt)")

            headers += mass_strs + sel_mean_du_strs + sel_max_du_strs

            headers += ['Noise 1 Coverage (%)', 'Noise 2 Coverage (%)',
                        'Noise 3 Coverage (%)', 'Noise 4 Coverage (%)']

            headers += ['Sel Cloud (%)', 'Sel Validity', 'Sel Area (sq km)', 'Noise Sectors']
            headers += noise_strs + noise_mean_du_strs + noise_max_du_strs
            headers += corr_mass_strs

            for level in ['Lower', 'Upper', 'Best']:
                headers += [f'Trajectory {level} Bound (km)',
                            f'SO2 Mass {level} Bound (kt)',
                            f'{level} bound Wind Distance (km)',
                            f'{level} bound Plume Age (h)',
                            f'{level} bound Wind Speed (m/s)',
                            f'{level} Bound Emission Rate (t/d)']

            headers += ['Validity Filter', 'Cloud Filter (%)', 'Notes']

        ##################################
        # End header generation
        ##################################

        # Write out the CSV file
        with open(save_file, 'a') as file:
            writer = csv.writer(file)
            if new_file:
                writer.writerow(headers)

            writer.writerow(record)

        # Save the map as well. Get the size from preferences rather than
        # asking the user
        self.settings.beginGroup("preferences")
        width = self.settings.value('imgWidth', utils.SIZE_OPTS[1][0])
        height = self.settings.value('imgHeight', utils.SIZE_OPTS[1][1])
        self.settings.endGroup()

        self._save_plot(save_path = file_path, width = width, height = height)

        # Display a quick drop-down dialog notifying the user that the save has
        # completed.
        ack = QDialog(self)
        ack.setAutoFillBackground(True)
        pal = ack.palette()
        pal.setColor(QPalette.Window, Qt.green)
        ack.setPalette(pal)

        layout = QVBoxLayout()
        ack.setLayout(layout)
        label = QLabel(f"Save Complete to directory: {file_path}")
        layout.addWidget(label)
        ack.finished.connect(ack.deleteLater)
        ack.open()

        # Close the acknowledgement dialog after 3 seconds.
        QTimer.singleShot(3000, ack, SLOT("accept()"))

    def _volcano_clicked(self, plotDataItem, points):
        """Handle clicking on a volcano, when there may potentially be multiple
        options under the cursor.

        """

        if len(points) == 1:
            point = points[0]
        else:
            # Make and show a menu to select from
            volc_menu = QMenu(self)
            for point in points:
                volc_menu.addAction(point.data()['name'])
            res = volc_menu.exec_(QCursor.pos())

            if res is None:  # User closed the menu without selecting anything
                return
            volc = res.text()

            # Get the point of the selected volcano
            try:
                point = next((x for x in points if x.data()['name'] == volc))
            except StopIteration:
                return  # REALLY, REALLY, odd, but oh well.

        self._highlight_volcano(point)

    def _highlight_volcano(self, point):
        """
        Select or deselect a volcano at the specified point

        Selected volacnoes will be displayed as filled triangles, and always
        have their name displayed regardless of the zoom level. Additionally,
        selected volcanoes will be output as part of saving a record.

        Point is a scatter plot point for the volcano to be highlighted"""

        cur_brush = point.brush()
        filled_brush = pg.mkBrush('k')
        volc_name = point.data()['name']

        # See if a label already exists for this volcano
        try:
            label = next((x for x in self._volc_labels
                          if x.textItem.toPlainText() == point.data()['name']))
        except StopIteration:
            # No label found. Make one.
            label = pg.TextItem(volc_name, (0, 0, 0))
            label.setPos(point.pos())
            label.setZValue(LAYERS['volc_labels'])

        label_font = label.textItem.font()

        if cur_brush == filled_brush:  # Currently selected, deselect
            point.resetBrush()

            # This property used when zooming in or out and
            # deciding which volcano labels to show.
            label.setProperty('STICKY', None)
            label_font.setBold(False)
            if self._zoom_level == 0:
                # Since we are deselecting, go ahead an remove this label
                # immediately if we are zoomed out
                self._plot_item.removeItem(label)

            # Shouldn't really need a try block here, but better safe.
            try:
                self._selected_volcs.remove(volc_name)
            except KeyError:
                pass  # not in list. This shouldn't happen...

        else:  # Select/highlight this volcano
            point.setBrush(filled_brush)
            self._plot_item.addItem(label, ignoreBounds=True)
            label.setProperty('STICKY', True)
            label_font.setBold(True)
            self._selected_volcs.add(volc_name)

        label.setFont(label_font)

    def _zoom_data(self):
        if self._ui.gbLongitude.isChecked():
            lon_from = self._ui.sbLngFrom.value()
            lon_to = self._ui.sbLngTo.value()
            from_x, _ = utils.latlon_merc_transformer.transform(0, lon_from)
            to_x, _ = utils.latlon_merc_transformer.transform(0, lon_to)

            x_range = (from_x, to_x)
            self._vb.setRange(xRange=x_range, padding=0)
        if self._ui.gbLatitude.isChecked():
            lat_from = self._ui.sbLatFrom.value()
            lat_to = self._ui.sbLatTo.value()
            _, from_y = utils.latlon_merc_transformer.transform(lat_from, -90)
            _, to_y = utils.latlon_merc_transformer.transform(lat_to, -90)

            y_range = (from_y, to_y)
            self._vb.setRange(yRange=y_range, padding=0)

    def _render_background(self):
        "debounce function to prevent slowing performance with many background render requests"
        self._bkg_render_timer.start(50)

    def _run_render_background(self):
        self._bkg_render_request.set()

    def _background_render_thread(self):
        """re-render the background map image when we scale/move the map
        Runs as a loop in another thread so it doesn't block the GUI or
        slow down user interaction.

        Renders the SVG to a bitmap image for fast display. Much faster than
        simply displaying the SVG directly, and re-rendering on scale ensures
        we don't get a jaggies when we zoom in, or disappearing lines when we
        zoom out."""
        while True:
            # wait until we have an event
            self._bkg_render_trigger.wait()
            if self._quit_event.is_set():
                logging.info("Background render thread got quit event")
                return

            if not self._bkg_render_request.is_set():
                continue

            self._bkg_render_request.clear()

            # We have a render request. Do it. But not until any previous requests have been drawn.
            self._render_lock.acquire()
            try:
                self._map_item = render_basemap(self._vb, self._mapScene)

                # If this attribute is not none, something is waiting for this particular render.
                # Let the awaiter deal with it however they wish, don't emit the _render_ready signal.
                if self._finished_bkg_render is not None:
                    self._finished_bkg_render.set()

                    # Since we don't know what called us, or if it will release
                    # the lock, release now to avoid perpetual lock.
                    try:
                        self._render_lock.release()
                    except RuntimeError:  # pragma: nocover
                        # This is plain paranoia, I honestly can't think of any
                        # possible way for this exception to be triggered here,
                        # since we took the lock previously in the same function.
                        pass  # We aren't locked for some reason

                else:
                    # This will release the lock once the item is added.
                    self._render_ready.emit()
            except:
                # If we crash in here, release the lock.
                # Otherwise, it will be released once the image is drawn.
                try:
                    self._render_lock.release()
                except RuntimeError:
                    # Didn't get the lock in the first place for some reason. Oh well.
                    pass

                raise

    @Slot()
    def _add_map(self):
        if self._cur_map is not None:
            self._plot_item.removeItem(self._cur_map)
            self._cur_map = None  # probably paranoid
        else:
            # First run. Keep track of the map, but don't draw it. The first render to come out is
            # before the window has resized completly, and so is WAY off scale wise.
            if self._render_lock.locked():
                self._render_lock.release()

            self._cur_map = self._map_item
            return

        self._plot_item.addItem(self._map_item, ignoreBounds=True)
        # Save as a seperate reference so when we create a new one, we still have a
        # reference to the one currently displayed.
        self._cur_map = self._map_item

        if self._render_lock.locked():
            self._render_lock.release()

        # Plot Volcanos if needed
        if self.volc_plot is None:
            self.volc_plot, self._volc_labels = plot_volcanoes(self._plot_item, size = 12)
            self.volc_plot.sigPointsClicked.connect(self._volcano_clicked)

    def _react_to_zoom(self):
        """Show/hide volcano labels as needed in response to user zoom in/out"""
        viewx, viewy = self._vb.viewRange()
        degrees_15 = 1669792.3618991054  # number of meters in mercator projection for 15 degrees longitude
        x_diff = abs(viewx[0] - viewx[1])

        if self._scale_widget is None:
            self._scale_widget = ScaleWidget()
            self._scale_widget.setFixedHeight(27)
            self._scale_widget.setParent(self._ui.PlotWidget)
            self._scale_widget.show()

        vb_bottom = self._vb.pos().y() + self._vb.height()
        scale_top = vb_bottom - self._scale_widget.height() - 5
        self._scale_widget.move(60, scale_top)
        self._scale_widget.update_scale([viewx, viewy], self._vb.width())

        # hide/show volcano labels based on zoom level
        if self._zoom_level > 0 and x_diff > degrees_15:
            self._zoom_level = 0
            for label in self._volc_labels:
                if not label.property('STICKY'):
                    self._plot_item.removeItem(label)

        elif self._zoom_level == 0 and x_diff <= degrees_15:
            self._zoom_level = 1
            for label in self._volc_labels:
                self._plot_item.addItem(label, ignoreBounds=True)

    def _zoom_in(self):
        self._vb.scaleBy((.75, .75))

    def _zoom_out(self):
        self._vb.scaleBy((1.333, 1.333))

    def _buildNavigation(self):
        self._navButtons = []
        self._ui.saveImage = self._addNavButton('SupportFiles/save.svg',
                                                self._save_plot, activate=False,
                                                tooltip = "Save map image")
        self._ui.zoomIn = self._addNavButton('SupportFiles/plus-sign.svg',
                                             self._zoom_in, activate = False,
                                             tooltip = "Zoom In")
        self._ui.zoomOut = self._addNavButton('SupportFiles/minus-sign.svg',
                                              self._zoom_out, activate = False,
                                              tooltip = "Zoom Out")
        self._ui.modeZoom = self._addNavButton('SupportFiles/zoom.svg',
                                               self._set_mode_rectangle,
                                               tooltip = "Zoom")
        self._ui.modePan = self._addNavButton('SupportFiles/pan.svg',
                                              self._set_mode_pan,
                                              tooltip = "Pan")
        self._ui.modeSelect = self._addNavButton('SupportFiles/select.svg',
                                                 self._set_mode_select,
                                                 tooltip = "Select Area")
        self._ui.modeMeasure = self._addNavButton('SupportFiles/ruler.svg',
                                                  self._set_mode_measure,
                                                  tooltip = "Measure Length")
        self._ui.modeWindR = self._addNavButton('SupportFiles/wind-reverse.svg',
                                                self._set_mode_wind,
                                                tooltip = "Show Back Winds (option/alt for Forward)")
        self._ui.modeWindF = self._addNavButton('SupportFiles/wind.svg',
                                                self._set_mode_wind_forward,
                                                tooltip = "Show Forward Winds (option/alt for Back)")
        self._ui.modePan.setActive(True)

    def _addNavButton(self, icon, action=None, activate=True, tooltip = None):
        SIZE = QSize(16, 16)
        frame = QFrame()
        frame.setFrameShape(QFrame.Panel)
        frame.setFrameShadow(QFrame.Raised)

        frame_layout = QVBoxLayout()
        frame_layout.setContentsMargins(3, 3, 3, 3)
        frame.setLayout(frame_layout)

        icon_path = os.path.realpath(os.path.join(MAIN_DIR, icon))
        button = QSvgButton(icon_path, frame)
        button.setFixedSize(SIZE)
        button.setHoverColor(QColor(68, 68, 68, 200), activate)
        if activate:
            button.clicked.connect(self._tool_button_clicked)
            button.setFillColor(QColor(68, 68, 68, 76))
        else:
            button.setFillColor(QColor(68, 68, 68, 200))

        if action is not None:
            button.clicked.connect(action)

        if tooltip is not None:
            button.setToolTip(tooltip)

        self._navButtons.append(button)

        frame_layout.addWidget(button)
        self._ui.navLayout.addWidget(frame)

        return button

    def mouseMoveEvent(self, pos):
        if self._tip.isVisible():
            self._tip.hide()

    def _get_data_point(self, pos, box_size = 10000):
        """Take a point in map view coordinates, and return the index
        of the closest data point.
        Return will be a boolean filtering array, and an index in the filtered data."""
        start = (pos.x() - box_size, pos.y() - box_size)
        stop = (pos.x() + box_size, pos.y() + box_size)
        area_filt = numpy.all(((self.xy_coords >= start) & (self.xy_coords <= stop)), axis=1)
        relevant_coords = self.xy_coords[area_filt]
        if relevant_coords.size == 0:
            return (None, None)

        if len(relevant_coords) > 1:
            pythag_sides = relevant_coords - (pos.x(), pos.y())
            dists = numpy.linalg.norm(pythag_sides, axis=1)
            closest_idx = dists.argmin()
        else:
            # Only one option, so that's the closest. Save a bit of processing.
            closest_idx = 0

        return (area_filt, closest_idx)

    def _point_near_wind(self, pos):
        """
        Determine if the point provided is 'near' to a wind point

        Arguments
        ---------
        pos: x,y tuple
            The position to compare to the wind points

        Returns
        -------
        found: bool
            A boolean indicating if there are any wind points "near" the
            provided position
        """
        box_size = 2000
        start = (pos.x() - box_size, pos.y() - box_size)
        stop = (pos.x() + box_size, pos.y() + box_size)
        for plot in self._wind_plots:
            xy_coords = numpy.stack(plot.getData(), 1)
            found = numpy.all(((xy_coords >= start) & (xy_coords <= stop)), axis=1).any()
            if found:
                break
        else:
            found = False

        return found

    @Slot(QPointF)
    def mouseHover(self, pos):
        """Handle mouse hover event

        Generate a pop-up with information about the point being hovered over"""
        if not self.so2data or \
           self.xy_coords is None or \
           not self._ui.PlotWidget.isInteractive():
            return

        # Figure out where on the screen to display the pop-up
        # translate the view coordinates to the map scene
        scene_pos = self._vb.mapViewToScene(pos)

        # From there we can get global coordinates
        global_pos = self._ui.PlotWidget.mapToGlobal(scene_pos.toPoint())

        # and finally, translate those to main window coordinates
        # Leave room for the menu bar at the top
        global_pos.setY(global_pos.y() + 17)

        near_wind = self._point_near_wind(pos)
        if near_wind:
            point_txt = "Click to select vector<br>Shift-Click to Select second vector"
        else:
            # Get the filter and index of the point closest to the mouse cursor
            area_filt, closest_idx = self._get_data_point(pos)

            if area_filt is None:
                # No points fall within our area of interest
                self._tip.hide()
                return

            point_data = self.so2data.isel(time = area_filt).isel(time = closest_idx)
            disp_data = {}

            # Some alterations for display (we don't want to modify the original data)
            disp_data['mass'] = round(point_data['mass'].item(), 4)
            disp_data['density'] = round(point_data['density'].item(), 5)
            disp_data['du'] = round(point_data['du'].item(), 2)
            disp_data['area'] = round(point_data['area'].item() / 1e6, 1)
            disp_data['percentile'] = round(point_data['percentile'].item() * 100)
            disp_data['validity'] = point_data['validity'].item()
            disp_data['cloud'] = point_data['cloud'].item()

            point_txt = self.HOVER_FORMAT_STRS[self._ui.cbTrace.currentIndex()]

            point_txt = point_txt.format_map(disp_data)

        self._tip.setText(point_txt)
        self._tip.move(global_pos)
        self._tip.show()

    @Slot()
    def _clear_winds(self):
        """Clear any wind plots from the map"""
        try:
            del self.wind_df
        except AttributeError:
            pass # Nothing to delete

        for plot in self._wind_plots:
            self._plot_item.removeItem(plot)
        self._wind_plots = []

        for plot in self._circle_plots:
            self._plot_item.removeItem(plot)
        self._circle_plots = []

        QApplication.processEvents()

        self._clear_interpolated_results()
        self._ui.windkeyContainer.hide()

    def _clear_interpolated_results(self, clear_results = True):
        current_alt = self._ui.cbAltitude.currentText()
        interpolated_idx = self._ui.cbAltitude.findText("INT:",
                                                        Qt.MatchStartsWith)
        if interpolated_idx >= 0:
            self._ui.cbAltitude.removeItem(interpolated_idx)

            # If we were currently showing an interpolated altitude, switch to a
            # non-interpolated altitude.
            if self._ui.cbAltitude.currentText() == '':
                self._ui.cbAltitude.setCurrentIndex(1)

        if self._ui.cbAltitude.currentText() != current_alt:
            # If we changed altitudes, we need to redraw at the new altitude
            # Update the dataset for the new altitude
            alt = self._ui.cbAltitude.currentData()
            # Will return non-interpolated if requested
            data = self._get_interpolated_density(alt)
            self.so2data['SO2_column_number_density'] = ('time', data.data)
            self._recalc_values()

        if clear_results:
            # Remove the interpolated results
            try:
                self._calc_results.query('interpolated!=True', inplace = True)
                self._results_table_model.setDataframe(self._calc_results)
            except AttributeError:
                # _calc_results doesn't exist yet, so no need to mess with it
                pass

    def _show_dialog(self, message, progress = True):
        dlg = QDialog(self,
                      Qt.Dialog | Qt.FramelessWindowHint | Qt.WindowTitleHint | Qt.Sheet)
        lay = QVBoxLayout()
        dlg.setLayout(lay)
        msg = QLabel(message)
        msg.setTextInteractionFlags(Qt.TextSelectableByMouse | Qt.TextSelectableByKeyboard)
        lay.addWidget(msg)
        if progress:
            prog = QProgressBar()
            prog.setRange(0, 0)
            # prog.setValue(0)
            lay.addWidget(prog)
            QApplication.processEvents()
        dlg.open()
        QApplication.processEvents()
        return dlg

    @Slot(QPointF)
    def _get_forward_winds(self, pos):
        """Get winds at specified pos, looking forward 24 hours from time of pos"""
        return self._get_winds(pos, forward=True)

    def _recalc_values(self, reprocess = True):
        """Recalculate mass, and DU.
        Assumes new SO2_column_number_density value"""
        start_t = time.time()
        # Density, in moles
        density = self.so2data['SO2_column_number_density']
        areas = self.so2data['area']

        DU = density * 2241.15
        mass = (areas * density) * 64

        self.so2data['density'] = density
        self.so2data['du'] = DU
        self.so2data['mass'] = mass

        logging.info("Recalc'd values in %f seconds", time.time() - start_t)

        if reprocess:
            self._on_data_ready({'data': self.so2data})

    @Slot(object, object)
    def _wind_plot_clicked(self, plot, points):
        """
        Process a click on a wind plot point, and calculate various parameters
        based on the point clicked

        Parameters
        ----------
        plot : pyqtgraph PlotDataItem
            The plot containing the point(s) clicked

        points : list of pyqtgraph SpotItem
            A list contining the point(s) clicked on. Could be multiple
            items if points overlap. We only use the first one.
        """
        # pyqtplot overrides the data function for its own purposes, but is
        # still a QObject. By calling super(), we can access the underlying
        # QObject data function to retrieve own custom data from the object
        # We used this to store the altitude of the wind plot as metadata on the
        # wind plot object itself.
        altitude = super(pg.PlotDataItem, plot).data(Qt.UserRole)  # in feet
        altitude /= 3281  # Convert to km
        altitude = round(altitude, 1)

        # The data for this point is the number of hours, and the index
        hours,idx = points[0].data()


        x, y = plot.getData() # the data points for the wind plot clicked.
        # Coordinates are in mercator projection, translate to lat/lon for distance calcs.
        wind_lat, wind_lon = utils.latlon_merc_transformer.transform(
            x,
            y,
            direction = TransformDirection.INVERSE
        )

        clicked_lat = wind_lat[idx]
        clicked_lon = wind_lon[idx]
        VOLCANOES['dist'] = utils.haversine_np(
            VOLCANOES.longitude,
            VOLCANOES.latitude,
            clicked_lon,
            clicked_lat
        )

        closest_volc = VOLCANOES.loc[VOLCANOES['dist'].idxmin()]
        wind_interval = self._wind_point_meta['interval']
        wind_age = wind_interval * idx
        point_time = self._wind_point_meta['start_time'] - timedelta(minutes = wind_age)
        self._wind_point_meta['volc'] = closest_volc
        self._wind_point_meta['lon'] = wind_lon[:idx+1]
        self._wind_point_meta['lat'] = wind_lat[:idx+1]
        self._wind_point_meta['point_time'] = point_time

        # Calculate the distance between point pairs.
        # Use the longitude/latitude arrays, but offset by one to pair point 1 with point 2, etc
        wind_lon1 = wind_lon[:-1]
        wind_lat1 = wind_lat[:-1]
        wind_lon2 = wind_lon[1:]
        wind_lat2 = wind_lat[1:]

        dist = utils.haversine_np(wind_lon1, wind_lat1, wind_lon2, wind_lat2)
        # dist array will now contain the distance between each point pair.
        # sum the distances up to the point clicked.
        dist_total = dist[:idx].sum()

        # Since we now know the total distance traveled, and the time,
        # we can easily calculate the average speed.
        avg_speed = dist_total / hours

        add_level = True
        interp_levels =\
            self._calc_results.query('interpolated==True').index.tolist()

        if (QApplication.queryKeyboardModifiers() & Qt.ShiftModifier) == Qt.ShiftModifier:
            if altitude in interp_levels:
                # Remove this selection
                self._calc_results.drop(altitude, inplace = True)
                del interp_levels[interp_levels.index(altitude)]

                add_level = False
                for item_plot,idx, orig_size in self._selected_points:
                    if item_plot == plot and points[0].index() == idx:
                        item_plot.opts['symbolSize'][idx] = orig_size
                        item_plot.opts['symbolPen'][idx] = None
                        plot.updateItems()
                        self._selected_points.remove((item_plot,idx, orig_size))
                        break

                if len(interp_levels) == 2:
                    # Remove the "in-between" wind level as well.

                    self._calc_results.drop(interp_levels[1:], inplace = True)
                    interp_levels = interp_levels[:1]

            elif len(interp_levels) >= 2:
                QMessageBox.critical(self, "Interpolation Error",
                                     "Unable to add altitude. Can only interpolate between two altitude products")
                return

        else:
            # Remove all interpolated result lines
            self._calc_results.query('interpolated!=True', inplace = True)

            # Remove all selections
            while len(self._selected_points) > 0:
                item_plot,idx, size = self._selected_points.pop()
                item_plot.opts['symbolPen'][idx] = None
                item_plot.opts['symbolSize'][idx] = size
                item_plot.updateItems()

        if add_level:
            # points[0].oldPen = points[0].pen()
            idx = points[0].index()
            self._selected_points.append((plot, idx, points[0].size()))
            plot.opts['symbolPen'][idx] = pg.mkPen('#86d419', width = 4,
                                                   cosmetic = True)
            plot.opts['symbolSize'][idx] += 4
            plot.updateItems()

            # points[0].setPen('#86d419', width = 4, cosmetic = True)
            # points[0].setSize(points[0].size() + 4)
            self._calc_results.at[altitude, 'interpolated'] = True
            self._calc_results.at[altitude, 'wind_time'] = hours
            self._calc_results.at[altitude, 'wind_dist'] = round(dist_total, 1)
            self._calc_results.at[altitude, 'wind_speed'] = round(avg_speed / 3.6, 1)
            self._results_table_model.refreshView()
            QApplication.processEvents()

        interp_records = self._calc_results.query('interpolated==True')
        interp_levels = interp_records.index
        if len(interp_levels) == 2:
            # We want three altitudes: the upper, the lower, and the interpolated "halfway"
            # Since we only have two entries, halfway is simply the median value
            altitude = round(numpy.median(interp_levels), 1)
            self._calc_results.at[altitude, 'interpolated'] = True

            # Go ahead and interpolate the times as well to keep everything in sync
            interp_hours = interp_records.wind_time.median()
            interp_dist = round(interp_records.wind_dist.median(), 1)
            interp_speed = round(interp_dist / interp_hours, 1)
            self._calc_results.at[altitude, 'wind_time'] = interp_hours
            self._calc_results.at[altitude, 'wind_dist'] = interp_dist
            self._calc_results.at[altitude, 'wind_speed'] = round(interp_speed / 3.6, 1)
        else:
            try:
                altitude = interp_levels[0]
            except IndexError:
                # No interpolated altitudes.
                altitude = self._calc_results.index[0]

        logging.debug("Got a click on a wind plot at elevation: %d, age: %d", altitude,
                      hours)

        # Remove any "int" enteries, but leave any results
        self._clear_interpolated_results(False)

        areas = numpy.asarray(self.so2data['area'])

        # Get a new list of interpolated wind values
        # It may have changed in the previous if
        interp_levels = self._calc_results.query('interpolated==True').index
        for wind_alt in interp_levels:
            try:
                interp_data = self._get_interpolated_density(wind_alt)
            except ValueError:
                return

            self._calc_results.at[wind_alt, 'int_mass'] = (areas * interp_data) * 64
            self._calc_results.at[wind_alt, 'int_du'] = interp_data * 2241.15

            if wind_alt == altitude:
                self.so2data['SO2_column_number_density'] = ('time', interp_data.data)

        # Add this altitude to the pull-down list
        item_text = f"INT: {altitude}km"
        self._ui.cbAltitude.addItem(item_text, altitude)
        self._ui.cbAltitude.setCurrentText(item_text)

        # Recalculate the density, etc values, but don't redraw the data yet
        self._recalc_values(False)

        # Recalculate the selected area (if any)
        self._recalc_noise_debounce()
        self._ui.PlotWidget._region_changed_final()

        QApplication.processEvents()
        self.repaint()
        QApplication.processEvents()

        # NOW redraw
        self._on_data_ready({'data': self.so2data})

    def _get_interpolated_density(self, altitude):
        """Given a destination altitude, altitude, find the two non-interpolated
        altitudes bracketing the target, and interpolate between them to get a
        new dataset at the destination altitude"""

        try:
            # Get a list of "raw", or un-interpolated, altitudes
            alt_opts = self._calc_results.query('interpolated != True')\
                .index.astype(int).tolist()
        except (ValueError, AttributeError) as err:
            raise ValueError("Can not interpolate this dataset") from err

        lower_alt = None
        upper_alt = None
        for alt in alt_opts:
            if alt > altitude:
                upper_alt = alt
                break

            lower_alt = alt

        if upper_alt is None:
            # Above our highest product, use the "lower" bound
            interp_data = self.so2data[f'SO2_number_density_{lower_alt}km']
        elif lower_alt is None:
            # Below our lowest product, use the "upper" bound
            interp_data = self.so2data[f'SO2_number_density_{upper_alt}km']
        else:
            # Interpolate between upper and lower bounds
            lower_dataset = self.so2data[f'SO2_number_density_{lower_alt}km']
            upper_dataset = self.so2data[f'SO2_number_density_{upper_alt}km']
            linfunc = interp1d([lower_alt, upper_alt],
                               numpy.vstack([lower_dataset, upper_dataset]),
                               axis = 0)
            interp_data = linfunc(altitude)

        return interp_data

    @Slot(QPointF)
    @show_dlg("Generating Wind. Please wait...")
    def _get_winds(self, pos, forward = False):
        """Get winds at position pos

        If forward==False (the default) get winds going back in time 24 hours
        from the time of the point at pos, or file time if not over a point.
        If forward==True, get winds going forward 24 hours from point time.
        """

        # Make sure we have the current state of the keyboard
        QApplication.processEvents()

        # If the option key (alt on windows) is held down, reverse the direction
        # of wind retreval
        if (QApplication.queryKeyboardModifiers() & Qt.AltModifier) == Qt.AltModifier:
            forward = not forward

        # Since retreiving winds from the remote server is I/O bound, run in a
        # seperate thread to avoid blocking the main thread.
        with ThreadPoolExecutor() as exectutor:
            future = exectutor.submit(self._run_get_winds, pos, forward)
            # pseudo non-blocking wait. We could just use a callback, but this
            # simplifies the progress bar show/hide, and it's not like the user
            # will be wanting to do something else while waiting anyway...
            while not future.done():
                QApplication.processEvents()
                time.sleep(.05)

            err, result = future.result()
            if not err:
                self.wind_df = result
                self.wind_forward = forward
            else:
                QMessageBox.critical(self, "Wind Error", result)
                return

        self._wind_plots = plot_wind(self.wind_df, self._plot_item,
                                     forward, self._wind_plot_clicked)

        self._ui.windkeyContainer.show()
        self._ui.windkeyContainer.raise_()


    def _run_get_winds(self, pos, forward = False):
        self._clear_winds()

        try:
            point_filter, point_idx = self._get_data_point(pos)
            point_date = self.so2data['datetime_start'][point_filter][point_idx]

            # The above is a timestamp, so convert to a real date to make math easy.
            # Of course, timestamp math is *also* easy, just in a different way, so
            # six of one half dozen of the other. If we didn't convert this, we'd
            # have to convert the except clause.
            point_date = datetime.fromtimestamp(point_date, tz = timezone.utc)
        except (IndexError, KeyError, TypeError):
            # If we don't have a point close to the mouse, or the filter is wonky,
            # just use the data start date. Close enough.
            point_date = self._get_data_dates()[0]
            if point_date is None:
                # No data loaded. Use current time
                point_date = datetime.utcnow()
                point_date = point_date.replace(tzinfo = timezone.utc)
            else:
                point_date = point_date.replace(tzinfo = timezone.utc)
        except AttributeError:
            # No data loaded. Use current time
            point_date = datetime.utcnow()
            point_date = point_date.replace(tzinfo = timezone.utc)

        self._wind_point_meta['start_time'] = point_date
        timestamp = point_date.timestamp()

        direction = "F" if forward else "B"

        lat, lon = utils.latlon_merc_transformer.transform(pos.x(), pos.y(),
                                                           direction = TransformDirection.INVERSE)
        url = f"https://avo-vsc-ash.wr.usgs.gov/ash3d-api/ncMetApi/ncMetTraj/{direction}/{lat}/{lon}/{timestamp}"
        url2 = f"https://vsc-ash.wr.usgs.gov/ash3d-api/ncMetApi/ncMetTraj/{direction}/{lat}/{lon}/{timestamp}"
        df = request_winds(url)
        if df.size == 0:
            logging.warning("Request to primary wind URL failed, trying backup")
            df = request_winds(url2)
            if df.size == 0:
                return (True, "No data was returned for the selected parameters")

        df.sort_values(by = ['elevF', 'hour'], inplace = True)
        df['lat'] = df['lat'].astype(float)
        df['lng'] = df['lng'].astype(float)
        x, y = utils.latlon_merc_transformer.transform(df['lat'].to_list(), df['lng'].to_list())
        df['x'] = x
        df['y'] = y

        # interpolate the half hour values
        df.index = pd.to_timedelta(df.hour, 'hours')
        interval = 15 #  In minutes
        self._wind_point_meta['interval'] = interval
        resample_interval = f"{interval}min"

        # Group by elevation, and resample to half hour intervals
        df_interp = df.groupby('elevF').resample(resample_interval).mean()

        # Determine the minute value from the index (which will be in 15-minute intervals)
        df_interp['minute'] = (df_interp.index.get_level_values(1).seconds // 60) % 60

        # Assign symbols based on the minute values
        df_interp['symbol'] = 't'  # Default to triangles for 15-minute marks
        df_interp.loc[df_interp.minute == 0, 'symbol'] = 'o'  # Hourly marks use circles
        df_interp.loc[df_interp.minute == 30, 'symbol'] = 's'  # Half-hour marks use squares

        # Assign symbol sizes based on the minute values
        df_interp['symbol_size'] = 6  # Default to size 4 for triangles
        df_interp.loc[df_interp.minute == 0, 'symbol_size'] = 8  # Hourly marks use size 8
        df_interp.loc[df_interp.minute == 30, 'symbol_size'] = 6  # Half-hour marks use size 6

        # Interpolate the values in the various columns
        for col in ['elevF', 'hour', 'lat', 'lng', 'x', 'y']:
            df_interp[col] = df_interp[col].interpolate()

        return (False, df_interp)

    @Slot(QWidget)
    def _tool_button_clicked(self, btn_clicked):
        for btn in self._navButtons:
            if btn is btn_clicked:
                btn.setActive(True)
            else:
                btn.setActive(False)
            btn.repaint()

    def _set_noise_locked(self, lock):
        """Lock or unlock the noise ROI's to prevent accidentally moving or
        altering them while trying to perform other operations"""
        for roi in self._noise_rois:
            roi.set_locked(lock)

    @Slot()
    def _set_mode_rectangle(self):
        self._ui.PlotWidget.setMode(Mode.ZOOM)
        self._vb.setMouseMode(self._vb.RectMode)

    @Slot()
    def _set_mode_pan(self):
        self._ui.PlotWidget.setMode(Mode.PAN)
        self._vb.setMouseMode(self._vb.PanMode)
        self._set_noise_locked(False)

    @Slot()
    def _set_mode_select(self):
        self._ui.PlotWidget.setMode(Mode.SELECT)
        self._set_noise_locked(False)

    @Slot()
    def _set_mode_measure(self):
        self._ui.PlotWidget.setMode(Mode.MEASURE)
        self._set_noise_locked(True)

    @Slot()
    def _set_mode_wind(self):
        self._ui.PlotWidget.setMode(Mode.WIND)
        self._vb.setMouseMode(self._vb.PanMode)
        self._set_noise_locked(True)

    @Slot()
    def _set_mode_wind_forward(self):
        self._ui.PlotWidget.setMode(Mode.WINDF)
        self._vb.setMouseMode(self._vb.PanMode)
        self._set_noise_locked(True)

    def _init_export_options(self):
        ui_loader = QtUiTools.QUiLoader()
        self._export_options = ui_loader.load(CUR_DIRECTORY + "/ExportOptions.ui", self)
        self._export_options.cbSize.setCurrentIndex(1)
        self._export_options.cbSize.currentIndexChanged[int].connect(self._set_export_size)

    @Slot(int)
    def _set_export_size(self, index):
        self._export_options.sbWidth.setEnabled(index == 3)
        self._export_options.sbHeight.setEnabled(index == 3)

        if index != 3:
            width, height = utils.SIZE_OPTS[index]
            self._export_options.sbWidth.setValue(width)
            self._export_options.sbHeight.setValue(height)

    def _get_data_dates(self, str_format=None):
        file_index = 0 if self._ui.cbStack.isChecked() else self._ui.hsFileSelector.value()
        try:
            first_file = self.files[file_index].split("/")[-1]
            last_file = self.files[-1].split("/")[-1] if self._ui.cbStack.isChecked() else first_file
        except IndexError:
            date_from = None
            date_to = None
        else:
            if first_file is last_file:
                date_from, date_to = data_utils.parse_file_range(first_file)
            else:
                date_from, _ = data_utils.parse_file_range(first_file)
                _, date_to = data_utils.parse_file_range(last_file)

        if str_format is None:
            return (date_from, date_to)

        # else:
        if date_from is None:
            return ("None", "None")

        date_from_str = date_from.strftime(str_format)
        if date_to.date() == date_from.date():
            date_to_str = date_to.strftime('%H:%M')
        else:
            date_to_str = date_to.strftime(str_format)
        return (date_from_str, date_to_str)

    @Slot(QWidget)
    def _save_plot(self, btn = None, save_path = None, width = None, height = None):
        """
            Save an png/pdf of the current map image

            Parameters
            ----------
            btn : QButton
                The button that was clicked to trigger this function, if any.
                Unused, but needed to consume the argument if it exists.
            save_path : str
                The directory in which to save the image with a default name.
                If None, open a file selector dialog for the user to select
            width : int
                The width of the output image.
                If None, open a dialog for the user to select image size
            height : int
                The height of the output image
                If None, open a dialog for the user to select image size
        """
        last_dir = self.settings.value("saveLocation", None)

        # Build up a default file name
        sector_name = self._ui.cbViews.currentText()
        if sector_name == "--":
            if self._selected_volcs:
                sector_name = list(self._selected_volcs)[0]
            else:
                sector_name = "custom"

        alt = self._ui.cbAltitude.currentText()
        if "*N/A*" in alt:
            alt = "UNK"

        # TROPOMI, OMPS, VIIRS...
        sensor = self._ui.cbFileType.currentText()
        date_from, _ = self._get_data_dates()
        start_time = date_from.strftime('%m%d%Y_%H%M') if date_from else ''
        if self._ui.cbStack.isChecked():
            start_time = "Stack"

        default_name = f"{sector_name}_{sensor}_{start_time}_{alt}"
        if last_dir is not None:
            last_dir = os.path.join(last_dir, default_name)
        else:
            last_dir = default_name

        if save_path is None:
            path, fmt = QFileDialog.getSaveFileName(self, "Save Image",
                                                  last_dir, "PNG (*.png);;PDF (*.pdf);;NetCDF (*.nc)",
                                                  )
            if not path:
                return
            # Save the path for future use
            file_path = os.path.dirname(path)
            self.settings.setValue("saveLocation", file_path)
        else:
            fmt = "PNG (*.png)"
            default_name = default_name.split('.')[0]
            save_time = datetime.now().strftime('%Y%m%d_%H%M%S')
            default_name = f"{default_name}_{save_time}.png"
            path = os.path.join(save_path, default_name)

        # This is totally paranoid, and almost certainly can be removed
        if not path:
            return  # pragma: nocover

        extension = fmt[fmt.find('*.')+1:-1]
        if not path.endswith(extension):
            path += extension

        if fmt == 'NetCDF (*.nc)':
            save_thread = threading.Thread(target = self._save_netcdf, args = (path, ))
            save_thread.start()
            self.progress_dialog.pbProgress.setValue(0)
            self.progress_dialog.pbProgress.setRange(0, 0)
            self.progress_dialog.lCurrentProcess.setText("Saving NetCDF File...")
            self.progress_dialog.open()
            return


        if width is None or height is None:
            if self._export_options is None:
                self._init_export_options()

            result = self._export_options.exec_()
            if result == QDialog.Rejected:
                return

            entered_width = self._export_options.sbWidth.value()
            entered_height = self._export_options.sbHeight.value()
        else:
            entered_width = width
            entered_height = height

        self.export_image(entered_width, entered_height, path, extension, self._export_options)
        return

    def _grid_netcdf_sector(self, input_data):
        gridded_data = xarray.Dataset()
        # Figure out gridding parameters
        proj = {'proj': 'aea', 'preserve_units': False, 'lon_0': -154, 'lat_1': 55, 'lat_2': 65,}
        aea_proj = pyproj.Proj(proj, preserve_units = False)
        aea_transformer = pyproj.Transformer.from_proj(utils.lat_lon_proj, aea_proj)

        sector = self._ui.cbViews.itemData(self._ui.cbViews.currentIndex())
        lat_from = sector['latFrom']
        lat_to = sector['latTo']
        lon_from =sector['ul_lon']
        lon_to = sector['ur_lon']

        # Convert lon/lat from/to to projection units
        # Because AEA curves, we don't actually know which of the bounds
        # will be highest/lowest in the projection.
        lats = (lat_from, lat_to, lat_from, lat_to)
        lons = (lon_from, lon_to, lon_to, lon_from)

        aea_lons, aea_lats = aea_transformer.transform(lats, lons)
        x_from = min(aea_lons)
        x_to = max(aea_lons)
        y_from = min(aea_lats)
        y_to = max(aea_lats)


        grid_size_x = self._filedef['INFO'].get('grid_x_resolution', 3500)  # in meters
        grid_size_y = self._filedef['INFO'].get('grid_y_resolution', 3500)

        num_lat = round((y_to - y_from) / grid_size_y)
        num_lon = round((x_to - x_from) / grid_size_x)

        area_extent = (x_from, y_from,
                       x_to, y_to)

        target_def = create_area_def("Binned", proj, area_extent=area_extent,
                                     shape=(num_lat, num_lon))

        lat_data = input_data.latitude.data
        lon_data = input_data.longitude.data
        source_def = geometry.SwathDefinition(lons=lon_data,
                                              lats=lat_data)

        target_lons, target_lats = target_def.get_lonlats()

        radius = self._filedef.get('INFO', {}).get('binRadius', 5e4)

        bin_fields = {
            fld.get('DEST', fld['NAME'])
            for fld_lst in self._filedef['GROUPS']
            for fld in fld_lst['FIELDS']
            if fld.get('bin', True)
        }

        #For this, we are binning the validity. Not sure if that makes sense
        bin_fields.add('SO2_column_number_density_validity')
        total = len(bin_fields)
        progress = 0
        for col in bin_fields:
            progress += 1
            utils.MESSAGE_QUEUE.put(('PROGRESS', "Binning Data...", progress, total))

            try:
                data = input_data[col].data
            except KeyError:
                continue

            g_data = kd_tree.resample_nearest(source_def, data, target_def,
                                              radius_of_influence = radius,
                                              fill_value = numpy.nan)
            dims = ['y', 'x']
            if len(g_data.shape) > 2:
                dims.append('layer')
            gridded_data[col] = (dims, g_data)

        # TODO: crop to the desired sector. Perhaps? Perhaps not?
        # # Figure out how to crop the data to the desired area
        # row_start = 0
        # row_stop = gridded_so2.shape[0]
        # col_start = 0
        # col_stop = gridded_so2.shape[1]
        # for row, data in enumerate(gridded_so2):
            # if numpy.isnan(data).all():
                # if row_start == 0:
                    # continue
                # else:
                    # row_stop = row
                    # break
            # if row_start == 0:
                # row_start = row


        # for col, data in enumerate(gridded_so2.T):
            # if numpy.isnan(data).all():
                # if col_start == 0:
                    # continue
                # else:
                    # col_stop = col
                    # break
            # if col_start == 0:
                # col_start = col

        # gridded_so2 = gridded_so2[row_start:row_stop, col_start:col_stop]
        # gridded_cloud = gridded_cloud[row_start:row_stop, col_start:col_stop]
        # target_lats = target_lats[row_start:row_stop, col_start:col_stop]
        # target_lons = target_lons[row_start:row_stop, col_start:col_stop]

        for field in ['tm5_constant_a', 'tm5_constant_b']:
            gridded_data[field] = input_data[field]

        gridded_data.coords['latitude'] = (['y', 'x'], target_lats)
        gridded_data.coords['longitude'] = (['y', 'x'], target_lons)

        gridded_data.attrs['grid_size_x'] = grid_size_x
        gridded_data.attrs['grid_size_y'] = grid_size_y
        return gridded_data



    def _save_netcdf(self, path):
        """
            Save the currently displayed data in NetCDF format along with selection information
        """

        #Load a fresh copy of the data
        utils.MESSAGE_QUEUE.put(("PROGRESS", "Loading Data..."))
        data = data_utils.load_file(self._loaded_file,
                                    "valid(SO2_column_number_density)",
                                    self._loaded_dataset,
                                    fields = 'ALL')
        data,error = data
        if data is None and error:
            self._close_progress.emit()
            self._show_msg.emit(QMessageBox.Critical,
                                "Unable to save NetCDF File: Unable to load data",
                                error,
                                None)
            return

        # Fix some variables that come in with the wrong dimensions
        for name in ['tm5_constant_a', 'tm5_constant_b']:
            data[name] = data[name][:, 0, 0]

        if self._ui.cbUseSector.isChecked() and self._ui.cbViews.currentIndex() != 0:
            # If we are gridding, just use the already loaded data
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Gridding Data..."))
            data = self._grid_netcdf_sector(data)
            grid_units = 'meters'
        else:
            # Otherwise, load a fresh copy
            grid_units = None

        cloud_data = data['cloud_fraction'].data
        target_lons = data['longitude'].data
        target_lats = data['latitude'].data


        selection = self._ui.PlotWidget.get_select_roi()
        selection_points = selection.getState()['points']
        selection_points = [(item.x(), item.y()) for item in selection_points]
        if selection_points:
            sel_lons,sel_lats = zip(*selection_points)
            sel_lats, sel_lons = utils.latlon_merc_transformer.transform(sel_lons, sel_lats,
                                                                     direction = TransformDirection.INVERSE)
            selection_points = tuple(zip(sel_lats, sel_lons))

        # make target_lons contiguous (no dateline crossing)
        # Needed so the min and max are west and east boundries, respectively
        # In theory, these longitudes should be the same as the lon_from and lon_to
        # Specified above, but I don't like to make assumptions.
        target_lons[target_lons>0] -= 360
        target_lon_from = target_lons.min()
        # Now that we have the west boundry, convert back to normal longitude, if needed.
        if target_lon_from < -180:
            target_lon_from += 360

        # Same for the lon_to
        target_lon_to = target_lons.max()
        if target_lon_to < -180:
            target_lon_to += 360

        # And convert the dataset back to normal longitudes.
        target_lons[target_lons < -180] += 360

        progress = 0
        end = 26
        utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))
        with h5py.File(path, mode = 'w') as file:
            if grid_units is not None:
                file.attrs['grid_size_x'] = data.attrs['grid_size_x']
                file.attrs['grid_size_y'] = data.attrs['grid_size_y']
                file.attrs['grid_units'] = grid_units

            file.attrs['latitude_range'] = [target_lats.min(), target_lats.max()]
            file.attrs['longitude_range'] = [target_lon_from, target_lon_to]

            product_grp = file.create_group("OUTPUT_PRODUCTS")
            product_grp.attrs['description'] = "Output Science products"

            input_data_grp = file.create_group("INPUT_DATA")
            input_data_grp.attrs['description'] = "Input Data"

            geolocation_grp = file.create_group("GEOLOCATION")
            geolocation_grp.attrs['description'] = "Geolocation Information"

            selection_grp = file.create_group("SELECTION")
            selection_grp.attrs['description'] = 'Information about the selected area'

            # metadata_grp = file.create_group("METADATA")
            # metadata_grp.attrs['description'] = "Data about the data"

            # #####################
            # ## METADATA group
            # #####################
            # volclat, volclon, volc_name = list(zip(*VOLCANOES))
            # volc_loc = numpy.asarray(list(zip(volclat, volclon)))

            # volc_locations = metadata_grp.create_dataset('volcano_locations', data = volc_loc, compression = "gzip")
            # volc_locations.attrs['comment'] = "volcano locations"
            # volc_locations.attrs['units'] = 'degree'

            # volc_names = metadata_grp.create_dataset('volcano_names', data = volc_name, compression = "gzip")
            # volc_names.attrs['comment'] = "Volcano names"

            # it would be nice to be able to loop these (DRY), but the description/units/etc
            # are all different.

            #####################
            ## SELECTION group
            #####################
            verticies_ds = selection_grp.create_dataset('vertices', data = selection_points)
            verticies_ds.attrs['long_name'] = "Vertices of the selected area"
            verticies_ds.attrs['units'] = 'degrees_north, degrees_east'
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            #####################
            ## OUTPUT_PRODUCT group
            #####################
            for alt in ['1km', '7km', '15km']:
                name = f"sulfurdioxide_total_vertical_column_{alt}"
                so2_dataset = product_grp.create_dataset(name,
                                                         data = data[f"SO2_number_density_{alt}"],
                                                         compression = "gzip")
                so2_dataset.attrs['units'] = "mol m-2"
                so2_dataset.attrs['standard_name'] = 'atmosphere_mole_content_of_sulfur_dioxide'
                so2_dataset.attrs['long_name'] = f'total vertical column density of sulfur dioxide for a sulfur dioxide plume at {alt} altitude w.r.t. the sea level'
                so2_dataset.attrs['altitude'] = alt
                so2_dataset.attrs['multiplication_factor_to_convert_to_DU'] = 2241.15
                progress += 1
                utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            name = f"SO2_column_number_density_validity"
            dataset = product_grp.create_dataset('qa_value',
                                                 data = data[name],
                                                 compression = "gzip")
            dataset.attrs['units'] = 1
            dataset.attrs['long_name'] = 'data quality value'
            dataset.attrs['comment'] = 'A continuous quality descriptor, varying between 0 (no data) and 1 (full quality data). Recommend to ignore data with qa_value < 0.5'
            dataset.attrs['scale_factor'] = 0.01
            dataset.attrs['_FillValue'] = 255
            dataset.attrs['valid_min'] = 0
            dataset.attrs['valid_max'] = 100
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            name = f"sulfurdioxide_total_vertical_column"
            dataset = product_grp.create_dataset(name,
                                                     data = data[name],
                                                     compression = "gzip")
            dataset.attrs['units'] = "mol m-2"
            dataset.attrs['standard_name'] = 'atmosphere_mole_content_of_sulfur_dioxide'
            dataset.attrs['long_name'] = 'total vertical column of sulfur dioxide for the polluted scenario derived from the total slant column'
            dataset.attrs['multiplication_factor_to_convert_to_DU'] = 2241.15
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            name = f"sulfurdioxide_slant_column_corrected"
            dataset = product_grp.create_dataset(name,
                                                     data = data[name],
                                                     compression = "gzip")
            dataset.attrs['units'] = "mol m-2"
            dataset.attrs['long_name'] = 'background corrected sulfur dioxide slant column density for final selected fitting window'
            dataset.attrs['multiplication_factor_to_convert_to_DU'] = 2241.15
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            name = f"averaging_kernel"
            try:
                dataset = product_grp.create_dataset(name,
                                                     data = data[name],
                                                     compression = "gzip")
                dataset.attrs['units'] = 1
                dataset.attrs['long_name'] = 'Averaging Kernel'
            except KeyError:
                logging.warning("Unable to save averaging_kernel, as it is not present in source file")

            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            name = f"sulfurdioxide_profile_apriori"
            dataset = product_grp.create_dataset(name,
                                                 data = data[name],
                                                 compression = "gzip")
            dataset.attrs['units'] = 1
            dataset.attrs['long_name'] = 'volume mixing ratio profile of sulfur dioxide'
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            name = f"cloud_fraction_intensity_weighted"
            dataset = product_grp.create_dataset(name,
                                                     data = data[name],
                                                     compression = "gzip")
            dataset.attrs['units'] = 1
            dataset.attrs['long_name'] = 'cloud fraction intensity weighted'
            dataset.attrs['comment'] = 'VCD clear sky vs. cloudy weighting factor.'
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            name = f"selected_fitting_window_flag"
            dataset = product_grp.create_dataset(name,
                                                     data = data[name],
                                                     compression = "gzip")
            dataset.attrs['units'] = 1
            dataset.attrs['long_name'] = 'flag describing the selected fitting window for sulfur dioxide retrieval'
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))


            ###################
            ## INPUT DATA Group
            ###################
            cloud_dataset = input_data_grp.create_dataset("cloud_fraction_crb",
                                                          data = cloud_data,
                                                          compression = "gzip")
            cloud_dataset.attrs['units'] = 'fraction'
            cloud_dataset.attrs['long_name'] = 'effective radiometric cloud fraction from the CRB model'
            cloud_dataset.attrs['range'] = [0, 1]
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            try:
                nwind_dataset = input_data_grp.create_dataset(
                    'northward_wind',
                    data = data['northward_wind'].data,
                    compression = "gzip"
                )
                nwind_dataset.attrs['units'] = 'm s-1'
                nwind_dataset.attrs['long_name'] = 'Northward wind from ECMWF at 10 meter height level'
            except KeyError:
                pass

            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            try:
                ewind_dataset = input_data_grp.create_dataset(
                    'eastward_wind',
                    data = data['eastward_wind'].data,
                    compression = "gzip"
                )
                ewind_dataset.attrs['units'] = 'm s-1'
                ewind_dataset.attrs['long_name'] = 'Eastward wind from ECMWF at 10 meter height level'
            except KeyError:
                pass

            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            name = 'cloud_height_crb'
            dataset = input_data_grp.create_dataset(name,
                                                    data = data[name],
                                                    compression = "gzip")
            dataset.attrs['units'] = 'm'
            dataset.attrs['long_name'] = 'cloud radiometric optical centroid height from the CRB model'
            dataset.attrs['comment'] = 'Coregistered height at the level of cloud w.r.t. the geoid/MSL using the OCRA/ROCINN CRB model.'
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            name = 'cloud_albedo_crb'
            dataset = input_data_grp.create_dataset(name,
                                                    data = data[name],
                                                    compression = "gzip")
            dataset.attrs['units'] = 'm'
            dataset.attrs['long_name'] = 'cloud albedo from the CRB model'
            dataset.attrs['comment'] = 'Coregistered cloud albedo based on the OCRA/ROCINN CRB model.'
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            name = 'surface_albedo_328nm'
            dataset = input_data_grp.create_dataset(name,
                                                    data = data[name],
                                                    compression = "gzip")
            dataset.attrs['units'] = '1'
            dataset.attrs['long_name'] = 'surface albdeo at 328nm'
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            name = 'surface_pressure'
            dataset = input_data_grp.create_dataset(name,
                                                    data = data[name],
                                                    compression = "gzip")
            dataset.attrs['units'] = 'Pa'
            dataset.attrs['long_name'] = 'Surface Air Pressure'
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            name = 'tm5_constant_a'
            dataset = input_data_grp.create_dataset(name,
                                                    data = data[name],
                                                    compression = "gzip")
            dataset.attrs['units'] = 'Pa'
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            name = 'tm5_constant_b'
            dataset = input_data_grp.create_dataset(name,
                                                    data = data[name],
                                                    compression = "gzip")
            dataset.attrs['units'] = 1
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            ######################
            ## GEOLOCATION group
            ######################
            lat_dataset = geolocation_grp.create_dataset('latitude',data = target_lats, compression = "gzip")
            lat_dataset.attrs['units'] = 'degrees_north'
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            lon_dataset = geolocation_grp.create_dataset('longitude', data = target_lons, compression = "gzip")
            lon_dataset.attrs['units'] = 'degrees_east'
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            solar_zen = geolocation_grp.create_dataset(
                'solar_zenith_angle',
                data = data['solar_zenith_angle'].data,
                compression = "gzip"
            )
            solar_zen.attrs['comment'] = 'Solar zenith angle at the ground pixel location on the reference ellipsoid. Angle is measured away from the vertical'
            solar_zen.attrs['units'] = 'degree'
            solar_zen.attrs['valid_min'] = numpy.array([0], dtype = float)
            solar_zen.attrs['valid_max'] = numpy.array([180], dtype = float)
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            solar_azimuth = geolocation_grp.create_dataset(
                'solar_azimuth_angle',
                data = data['solar_azimuth_angle'].data,
                compression = "gzip"
            )
            solar_azimuth.attrs['comment'] = 'Solar azimuth angle at the ground pixel location on the reference ellipsoid. Angle is measured clockwise from the North (East = 90, South = 180, West = 270)'
            solar_azimuth.attrs['units'] = 'degree'
            solar_azimuth.attrs['valid_min'] = numpy.array([-180], dtype = float)
            solar_azimuth.attrs['valid_max'] = numpy.array([180], dtype = float)
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            sensor_zen = geolocation_grp.create_dataset(
                'viewing_zenith_angle',
                data = data['sensor_zenith_angle'].data,
                compression = "gzip"
            )
            sensor_zen.attrs['comment'] = 'Zenith angle of the satellite at the ground pixel location on the reference ellipsoid. Angle is measured away from the vertical'
            sensor_zen.attrs['units'] = 'degree'
            sensor_zen.attrs['valid_min'] = numpy.array([0], dtype = float)
            sensor_zen.attrs['valid_max'] = numpy.array([180], dtype = float)
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

            sensor_azimuth = geolocation_grp.create_dataset(
                'viewing_azimuth_angle',
                data = data['sensor_azimuth_angle'].data,
                compression = "gzip"
            )
            sensor_azimuth.attrs['comment'] = 'Satellite azimuth angle at the ground pixel location on the reference ellipsoid. Angle is measured clockwise from the North (East = 90, South = 180, West = 270)'
            sensor_azimuth.attrs['units'] = 'degree'
            sensor_azimuth.attrs['valid_min'] = numpy.array([-180], dtype = float)
            sensor_azimuth.attrs['valid_max'] = numpy.array([180], dtype = float)
            progress += 1
            utils.MESSAGE_QUEUE.put(("PROGRESS", "Saving variables", progress, end))

        self._close_progress.emit()



    def _build_menus(self):
        """Create the menu bar.

        It's easier to make the connections here than to design the menu in the
        .ui file
        """
        self._menuBar = QMenuBar()
        self.setMenuBar(self._menuBar)

        self._filemenu = self._menuBar.addMenu("File")
        self._quit_action = self._filemenu.addAction("Quit", QApplication.instance().quit)
        self._quit_action.setShortcut(QKeySequence("Ctrl+Q"))

        self._graph_selection = []

        self._viewmenu = self._menuBar.addMenu("View")
        self._ui.actionClearSelect = self._viewmenu.addAction("Clear All",
                                                              self._clear_select)
        self._ui.actionClearRulers = self._viewmenu.addAction("Clear Rulers",
                                                              self._remove_measure)
        self._ui.actionClearWind = self._viewmenu.addAction("Clear Wind",
                                                            self._clear_winds)
        self._ui.actionClearNoise = self._viewmenu.addAction("Clear Noise",
                                                             self.on_bClearNoise_clicked)
        self._ui._viewsep = self._viewmenu.addSeparator()
        self._ui._viewscale = self._viewmenu.addAction("Set Scale...",
                                                       self._scale_dialog)
        self._ui._viewsep = self._viewmenu.addSeparator()

        self._windowmenu = self._menuBar.addMenu("Window")
        resultsAction = self._windowmenu.addAction("Results Window",
                                                   self.showResults)
        resultsAction.setShortcut(QKeySequence("Ctrl+R"))
        mapAction = self._windowmenu.addAction("Map Window",
                                               self.showMap)
        mapAction.setShortcut(QKeySequence("Ctrl+M"))
        self._windowmenu.addSeparator()

        self._viewsectors = self._windowmenu.addAction("Manage Sectors...",
                                                       self.on_actionViewSectors_triggered)

        self.actionEditVolcanoes = self._windowmenu.addAction(
            "Manage Volcanoes...",
            self.on_actionEditVolcanoes_triggered
        )

        self._ui.action_prefs = self._windowmenu.addAction("Preferences...",
                                                           self.on_action_prefs_triggered)

        self._helpmenu = self._menuBar.addMenu("Help")
        self._helpmenu.addAction("About SO2 Explorer...", self.on_actionAbout_triggered)
        about_qt = QAction("About Qt...")
        about_qt.triggered.connect(QApplication.aboutQt)
        about_qt.setMenuRole(QAction.AboutQtRole)
        self._helpmenu.addAction(about_qt)
        self._helpmenu.addAction("Check for updates...", utils.check_for_update)

    def _scale_dialog(self):
        if self._dsDialog is None:
            self._dsDialog = DataScale(self._scale_min, self._scale_max)
            self._dsDialog.finished.connect(self._cleanScaleDialog)
            self._dsDialog.rangeChanged.connect(self._set_du_scale)
        self._dsDialog.open()

    def _cleanScaleDialog(self):
        if self._dsDialog is not None:
            self._dsDialog.close()
            self._dsDialog.deleteLater()
            self._dsDialog = None

    def _set_du_scale(self, min_val: int, max_val: int):
        self._scale_min = min_val
        self._scale_max = max_val
        self._scaleChange_timer.start(1000)

    def _rescale_image(self):
        if not self.so2data:
            return

        self._generate_brushes(du_only = True)
        self.on_cbTrace_currentIndexChanged(self._ui.cbTrace.currentIndex())
        self._calc_quick_stats()

    def showResults(self):
        self._results_win.show()
        self._results_win.raise_()
        self._results_win.activateWindow()

    def showMap(self):
        super().show()
        self.raise_()
        self.activateWindow()

    def show(self):
        self.resize(self.window_size)
        super().show()
        self.setMenuBar(self._menuBar)
        self._bkg_render_request.set()

    def on_actionEditVolcanoes_triggered(self):
        """Create and open the volcano editor dialog"""
        editor = VolcanoEditor(self)
        editor.volcsChanged.connect(self.update_volc_plot)
        editor.exec()

    def _update_volcanoes(self):
        global VOLCANOES
        volcs = self.settings.value("Volcanoes", DEFAULT_VOLCS)
        volcanoes_df = pd.DataFrame(volcs, columns=['latitude', 'longitude', 'name'])
        volcanoes_df.apply(lambda row: Point(row['longitude'], row['latitude']), axis=1)
        VOLCANOES = volcanoes_df

    def update_volc_plot(self):
        if self.volc_plot is not None:
            self._plot_item.removeItem(self.volc_plot)

            for label in self._volc_labels:
                self._plot_item.removeItem(label)

        # Load the new Volcanoes list
        self._update_volcanoes()
        self.volc_plot, self._volc_labels = plot_volcanoes(
            self._plot_item, size = 12
        )
        self.volc_plot.sigPointsClicked.connect(self._volcano_clicked)

    def on_actionViewSectors_triggered(self):
        """Hander for the view sectors menu item.

        Create and open the sector editor dialog"""
        editor = SectorEditor(self)
        editor.sectors_changed.connect(self._setup_views)
        editor.exec()

    def on_actionAbout_triggered(self):
        """Handler for the about menu item. Display a basic about box."""
        QMessageBox.about(self, "SO2 Explorer",
                          f"Version {__version__}\n\n"
                          "Written by Israel Brewster, ijbrewster@alaska.edu")

    @Slot()
    def on_action_prefs_triggered(self):
        """Handler for the preferences menu item.
        Create and open the preferences window"""
        self._prefswin = PrefsWin(self)
        self._prefswin.open()

    def _delay_quick_stats(self):
        self._quickstats_timer.start(100)

    @Slot()
    def _calc_quick_stats(self):
        """Launch a background thread to calculate some stats about the view"""
        self._stats_event.set()

    def _stat_calc_thread(self):
        while True:
            self._calcstats_trigger.wait()
            if self._quit_event.is_set():
                logging.info("Calc got quit event. Exiting now.")
                return

            if not self._stats_event.is_set():
                continue

            self._stats_event.clear()
            locked = self._stats_lock.acquire(timeout = 2)
            if not locked:
                # We had a request, but calcs are already being calculated, and we
                # couldn't get a lock after waiting a reasonable time, so give up.
                continue

            stats = self._run_calc_quick_stats()  # Will release the lock on return
            if stats is not None:
                self._set_stats_header(*stats)

    @clear_on_exit
    def _run_calc_quick_stats(self, view_range: list | tuple = None,
                              use_selection: bool = True) -> tuple[float, float,
                                                                   float, float, float] | None:
        """
            Calculate some basic stats about the view area, or selection (if any)

            Parameters
            ----------
            view_range: list|tuple (optional)
                two element iterable containing X and Y limits for the
                area to calculate stats for. Defaults to the current
                viewbox view range.
            use_selection:bool (optional, default True):
                Boolean value indicating if we should limit stats calculations
                to the current selection, if any

            Returns
            -------
            mass_kt: float
               The total mass shown in the visible region, or selected area if
               use_selection is True
            sel_area_km: float
                The area of the visible area, or selected area if use_selection
                is True
            max_so2: float
                The maximum so2 concentration, in DU, within the visible area,
                or selected are if use_selection is True
            max_lon: float
                The longitude of the maximum SO2 value returned above
            max_lat: float
                The latitude of the maximum SO2 value returned above

        """
        if self.so2mass is None or self.so2data is None or \
           self.so2data['SO2_column_number_density'].size <= 0:
            return None

        viewx, viewy = view_range or self._vb.viewRange()

        vcorners = ((viewx[0], viewy[0]), (viewx[1], viewy[0]),
                    (viewx[1], viewy[1]), (viewx[0], viewy[1]))

        if self._view_corners is not None:
            corners = self._view_corners
            quick = False
        else:
            quick = True
            corners = vcorners

        sel_area_km = data_utils.calc_selected_area(corners)

        if corners is not vcorners:
            self._view_area = data_utils.calc_selected_area(vcorners)
        else:
            self._view_area = sel_area_km

        so2_data, selection_mask = data_utils.filter_data_with_selection(
            self.so2mass,
            corners,
            self.xy_coords,
            quick,
            self.so2data.attrs.get('grid_index'),
            self.so2data.attrs.get('grid_geometries')
        )

        try:
            so2_conc = self.so2data['SO2_column_number_density'][selection_mask]
        except IndexError:
            return None

        # Calculate percentiles of the displayed data
        if so2_conc.size > 0:
            percentiles = numpy.nanpercentile(so2_conc * 2241.15, self._percentile_levels)
            percentiles[percentiles < self._scale_min] = self._scale_min
            percentiles[percentiles > self._scale_max] = self._scale_max

            percent_colors = self._du_color_map.map(percentiles * (1 / 20), mode = 'qcolor')
        else:
            percentiles = [numpy.nan] * len(self._percentile_levels)
            percent_colors = [QColor(255, 255, 255)] * len(self._percentile_levels)

        for idx, color in enumerate(percent_colors):
            val = percentiles[idx]

            try:
                disp_val = str(round(val, 2))
            except ValueError:
                disp_val = val

            label = self._percentile_labels[idx]
            self._setText.emit(label,
                               f"{self._percentile_levels[idx]}<sup>th</sup><br>{disp_val}")

            ss = f'border-radius:0px;background-color:{color.name()};border:1px solid black;'
            if idx != 0:
                ss += "border-left:None;"

            widg = self._percentile_widgets[idx]
            # Shuffle this off to the main thread to be set - don't try to do it here.
            self._setStyleSheet.emit(widg, ss)

        mass_kt = round(numpy.nansum(so2_data) / 1e9, 2)  # in kt

        if use_selection and self._calc_results.get('selected_mass',
                                                    pd.Series(dtype = float)).any():
            alt = self._ui.cbAltitude.currentData()
            try:
                selected_mass = self._calc_results.at[float(alt),
                                                      'selected_mass']
                if numpy.isnan(selected_mass):
                    # Probably will never hit this, but added as an extra
                    # safeguard, just in case *one* value is NaN
                    raise ValueError("Selected mass is NaN")

                mass_kt = round(selected_mass, 4)

                # Round is probably not needed, as this should be an int already,
                # but doesn't hurt just in case it's a floating point.
                # Don't recall :-)
                sel_area_km = round(self._selected_area)
                try:
                    noise_mass = self._calc_results.at[float(alt), 'noise_mass']
                    if not numpy.isnan(noise_mass):
                        mass_kt -= noise_mass
                        mass_kt = round(mass_kt, 4)
                except (AttributeError, IndexError, KeyError):
                    pass
            except (IndexError, KeyError, TypeError, ValueError):
                pass

        if so2_conc.size == 0:
            max_so2 = 0
            max_lat = 'n/a'
            max_lon = 'n/a'
        else:
            max_so2 = round(numpy.nanmax(so2_conc) * 2241.15, 2)
            max_so2_idx = numpy.nanargmax(so2_conc)
            try:
                max_lat = float(self.so2data['latitude'][selection_mask][max_so2_idx])
                max_lon = float(self.so2data['longitude'][selection_mask][max_so2_idx])
            except IndexError:
                max_lat = -999
                max_lon = -999
            max_lat = str(round(max_lat, 2))
            max_lon = str(round(max_lon, 2))

        return (mass_kt, sel_area_km, max_so2, max_lon, max_lat)

    def _set_stats_header(self, mass_kt, sel_area_km, max_so2, max_lon, max_lat):
        alt = self._ui.cbAltitude.currentText()
        if alt.endswith('km'):
            alt = alt[:-2] + " km"

        quick_stats = (
            f"Alt: {alt}; SO<sub>2</sub> Mass: {mass_kt} kt; Area: {sel_area_km:,} km<sup>2</sup>;"
            f" SO<sub>2</sub> max: {max_so2} DU at lon: {max_lon} lat: {max_lat}"
        )

        self._setText.emit(self._ui.lStats, quick_stats)

    @Slot()
    def on_bSaveSector_clicked(self):
        """Save sector button handler. Save the current viewport as a sector
        definition"""
        # Ask the user to name the new sector
        view_name, _ = QInputDialog.getText(self, "Sector Name",
                                            "Please enter a name for this sector:")
        if not view_name:
            return

        # Make sure this is a valid name
        used = [x['name'] for x in SECTORS + utils.load_user_views()]
        if view_name in used:
            QMessageBox.critical(self, "Unable to save",
                                 "A sector with that name already exists!\n\n"
                                 "Please try again with a different name")
            return

        # Get the current view range from the graph
        self._save_sector(view_name)

    def _save_sector(self, view_name):
        viewx, viewy = self._vb.viewRange()

        (lat_from,
         lon_from) = utils.latlon_merc_transformer.transform(viewx[0], viewy[0],
                                                             direction = TransformDirection.INVERSE)

        lat_to, lon_to = utils.latlon_merc_transformer.transform(viewx[1], viewy[1],
                                                                 direction = TransformDirection.INVERSE)

        result = {'name': view_name,
                  'latFrom': round(lat_from, 3),
                  'latTo': round(lat_to, 3),
                  'longFrom': round(lon_from, 3),
                  'longTo': round(lon_to, 3)
                  }

        result = utils.calc_sector_bounds(result)

        user_views = utils.load_user_views()
        user_views.append(result)
        utils.save_user_views(user_views)

        self._setup_views()
        self._ui.cbViews.setCurrentText(view_name)

    def resizeEvent(self, event):
        # Ugly, ugly workaround for slider not resizing when resizing window
        # Apparently it will only resize when its value changes, so we have to force an actual value change
        # Setting it back immediately doesn't work, there needs to be an event loop process.
        self._fix_slider_size()

    def _fix_slider_size(self):
        """For some reason, when resizing the window the file slider does not
        expand/contract properly. This function handles that so things look good."""
        self.resizing = True
        value = self._ui.hsFileSelector.value()
        max_val = self._ui.hsFileSelector.maximum()
        if value == max_val:
            self._ui.hsFileSelector.setValue(value - 1)
        else:
            self._ui.hsFileSelector.setValue(value + 1)

        # Process the setValue call so it actually has an effect
        QCoreApplication.processEvents()

        # And now set it back to what it was so it doesn't change
        self._ui.hsFileSelector.setValue(value)
        self.on_hsFileSelector_sliderMoved(value)
        self.resizing = False

    def _setup_views(self):
        """Initalize the sector view selector pull-down box"""
        current_item = self._ui.cbViews.currentText()
        self._ui.cbViews.clear()
        self._ui.cbViews.addItem("--", None)
        self._ui.cbViews.addItem("Default", None)
        for view in sorted(SECTORS + utils.load_user_views(),
                           key=utils.sector_sort_key):
            self._ui.cbViews.addItem(view['name'], view)

        if current_item:
            self._ui.cbViews.setCurrentText(current_item)

    @Slot()
    def _clear_view_selection(self):
        """Set the view sector selector to undefined when moving the map view
        manually."""
        if self._ui.cbViews.currentIndex() > 0:
            self._ui.cbViews.setCurrentIndex(0)

    def _set_slider_buttons(self, value):
        """Enable/Disable the file selector as appropriate when
        selecting/deselecting the stacked check box."""
        if self._ui.cbStack.isChecked() or len(self.files) == 1:
            self._ui.bPrevFile.setEnabled(False)
            self._ui.bNextFile.setEnabled(False)
        else:
            self._ui.bPrevFile.setEnabled((value != 0))
            self._ui.bNextFile.setEnabled((value < (len(self.files) - 1)))

    @Slot(int)
    def on_hsFileSelector_sliderMoved(self, value):
        """File Selector moved handler

        Handle creating and showing a "tip" floater that shows the date/time of
        the file at the current slider position"""
        # Update the label with the new value and show it
        if not self.files:
            return  # no files, don't do anything

        idx = value
        if len(self.files) == 1:
            idx = 0

        self._set_slider_buttons(value)

        file_start, file_stop = data_utils.parse_file_range(self.files[idx].split('/')[-1])

        if len(self.files) > 1 or value == 0:
            self.lFileTip.setText(file_start.strftime('%m/%d/%y %H:%M:%SZ'))
        else:
            self.lFileTip.setText(file_stop.strftime('%m/%d/%y %H:%M:%SZ'))

        self.lFileTip.show()
        self.lFileTip.adjustSize()
        label_pos_x = ((self._ui.hsFileSelector.pos().x() +
                        (self._ui.hsFileSelector.width() / 2)) -
                       (self.lFileTip.width() / 2))
        # one pixel margin between bottom of label and top of selector
        label_pos_y = self._ui.hsFileSelector.pos().y() - self.lFileTip.height() - 1 + 10 + self.centralWidget().pos().y()
        self.lFileTip.move(label_pos_x, label_pos_y)

    @Slot()
    def on_bPrevFile_clicked(self):
        """Click handler for file selector previous file button."""
        self._nav_dir = 'backward'
        self._ui.PlotWidget.clear_select()
        self._ui.hsFileSelector.setValue(self._ui.hsFileSelector.value() - 1)

    @Slot()
    def on_bNextFile_clicked(self):
        """Click handler for file selector next file button"""
        self._nav_dir = 'forward'
        self._ui.PlotWidget.clear_select()
        self._ui.hsFileSelector.setValue(self._ui.hsFileSelector.value() + 1)

    @Slot(int)
    def on_hsFileSelector_valueChanged(self, idx:int, default_alt:bool = False):
        """Value changed handler for File Selector

        Configure the selector buttons properly for the file selected, and load
        the appropriate file (if any)"""
        if self.resizing:
            return

        self._set_slider_buttons(idx)
        if not self.files:
            return  # No files selected

        if len(self.files) == 1:
            idx = 0

        fileName = self.files[idx]

        bare_name = fileName.split("/")[-1]
        self._ui.fileName.setText(bare_name[:-3])
        self.setWindowTitle(f"SO2 Explorer: {bare_name[:-3]}")
        self.on_loadFile_clicked(default_alt = default_alt)

    @Slot()
    def select_save_path(self):
        """Select the path that output files are saved to.

        Will only be called if not specified in preferences when attempting to
        save records."""
        self.settings.beginGroup("preferences")
        default_path = self.settings.value('path', '')

        save_file_path, _ = QFileDialog.getExistingDirectory(self, default_path,
                                                             caption="Select directory to save records to...",
                                                             options=QFileDialog.DontConfirmOverwrite)
        if save_file_path:
            self.settings.setValue('path', save_file_path)

        self.settings.endGroup()
        return save_file_path

    @Slot(str)
    def on_cbFileType_currentTextChanged(self, item):
        """File type change handler

        Reconfigure UI for new file type - re-populate altitude select menu,
        reset results, and set available filters"""
        self._ui.cbAltitude.clear()
        # Clear calculated mass

        # Start with all filters enabled
        filters = [self._ui.gbValidity, self._ui.gbSensZenith, self._ui.gbSolZenith]
        for filt in filters:
            filt.setEnabled(True)

        if item == "TROPOMI":
            self._load_file_filter = "TROPOMI Files (*.nc)"
            altitudes = [1, 7, 15]
        elif item == "OMPS":
            self._load_file_filter = "OMPS (*.h5)"
            altitudes = [1, 3, 8, 13, 20]
        elif item == 'IASI':
            self._load_file_filter = "IASI (*.nc)"
            altitudes = [5, 7, 10, 13, 16, 19, 25, 30]
            # Disable filters for this file Type
            for filt in filters:
                filt.setChecked(False)
                filt.setEnabled(False)
        elif item == 'VIIRS':
            self._load_file_filter = "VIIRS (*.h5)"
            # TODO: investigate if this could be made non-numerical
            altitudes = [1]

        self._initalize_result_data_frames(altitudes)

        if item == "IASI":
            altitudes = ['INT'] + altitudes
        elif item == 'OMPS':
            # Generate a lookup table for altitude to name
            names = {value: key for key, value in utils.OMPS_LOOKUP.items()}

        for altitude in altitudes:
            if item == 'OMPS':
                txt = names[altitude]
            if item == "VIIRS":
                txt = "SO2"  # Only one, but we have to give it a numerical value in the altitudes list
            elif altitude != 'INT':
                txt = f"{altitude}km"
            else:
                txt = altitude

            self._ui.cbAltitude.addItem(txt, altitude)

    @Slot(bool)
    def on_cbHighlight_toggled(self, checked):
        if len(self._plot_brushes) == 0:
            return

        index = self._ui.cbTrace.currentIndex()
        self._pyqtgraph_plot(self._plot_brushes[index])

    @Slot(int)
    def on_cbTrace_currentIndexChanged(self, index):
        """
            Handler for the "trace" pull-down menu.
            Change which data product is displayed in the graph.

            Parameters
            ----------
            index: int
                   Selected index in the trace pull-down menu

            Returns
            -------
            None
        """
        if not self.chartready:
            return

        # QApplication.processEvents()
        try:
            self._pyqtgraph_plot(self._plot_brushes[index])
        except IndexError:
            pass # No brushes as of yet, so nothing to plot


        # And bring the percentile scale to the front (if it should be shown)
        wind_top = self._keys_top
        if index == 0:
            self._scaleLabels[0] = calc_du_levels(self._scale_min, self._scale_max)
            self._ui.percentContainer.show()
            self._ui.percentContainer.raise_()
            # Add a 5 pixel margin between containers
            wind_top += self._ui.percentContainer.height() + 5
        else:
            self._ui.percentContainer.hide()

        # Draw the new color bar
        self._ui.lHScale.setGradient(self._gradients[index])
        self._ui.lHScale.setLabels(self._scaleLabels[index])

        wind_pos = self._ui.windkeyContainer.pos()
        wind_pos.setY(wind_top)
        self._ui.windkeyContainer.move(wind_pos)

    def _remove_sector_bounds(self):
        """Remove the sector bounds lines from the map area"""
        if self._sector_roi is not None:
            self._vb.removeItem(self._sector_roi)

        self._sector_roi = None
        self._view_corners = None

    def _show_sector_bounds(self, view):
        """
            Draw sector bounds on the map

            Parameters
            __________
            view: Dict
                  Dictonary contining the parameters of the sector to draw
                  on the map, in latitude/longitude format

            Returns
            _______
            None
        """
        self._remove_sector_bounds()

        # transform the coordinates to mercator
        ll_x, ll_y = utils.latlon_merc_transformer.transform(view['latFrom'], view['ll_lon'])
        lr_x, lr_y = utils.latlon_merc_transformer.transform(view['latFrom'], view['lr_lon'])
        ur_x, ur_y = utils.latlon_merc_transformer.transform(view['latTo'], view['ur_lon'])
        ul_x, ul_y = utils.latlon_merc_transformer.transform(view['latTo'], view['ul_lon'])

        # Create a region of interest for this sector
        points = [(ll_x, ll_y), (lr_x, lr_y), (ur_x, ur_y), (ul_x, ul_y)]
        self._sector_roi = sector_roi(points)

        self._vb.addItem(self._sector_roi)

        # Save this view for use in calculations
        self._view_corners = points

    @Slot(int)
    def on_cbViews_currentIndexChanged(self, index):
        """
            Set the map view to the selected sector

            Parameters
            ----------
            index: int
                   The menu index of the currently selected sector
        """
        # Don't try to do anything if we don't have a chart to work on, or there is no view selected
        if not self.chartready or index == 0:
            return

        # Clear any existing sector bounds
        self._remove_sector_bounds()

        view = self._ui.cbViews.itemData(index)
        if view is None and index > 1:
            return

        # Figure out the view size to get the proper ratio for displaying this sector
        view_size = self._vb.size()
        view_ratio = view_size.width() / view_size.height()
        if index > 1:
            # transform the upper lat/long to mercator (upper long is widest)
            ul_x, ll_y = utils.latlon_merc_transformer.transform(view['latFrom'], view['ul_lon'])
            ur_x, ur_y = utils.latlon_merc_transformer.transform(view['latTo'], view['ur_lon'])

            x_range = [ul_x, ur_x]
            y_range = [ll_y, ur_y]

            if self._ui.cbUseSector.isChecked():
                self._show_sector_bounds(view)
        else:
            # Random default that shows most of Alaska
            x_range = [-9981246, -4981246]
            y_range = [6500000, 11500000]

        # Probably don't need the abs, but it seems safer this way
        x_dist = abs(x_range[1] - x_range[0])
        y_dist = abs(y_range[1] - y_range[0])

        sector_ratio = x_dist / y_dist

        if sector_ratio > view_ratio:
            # Need to add y to fit
            y_dist_new = x_dist / view_ratio
            delta_y = y_dist_new - y_dist  # Will be positive, if I did the math right
            y_range[0] -= (delta_y / 2)
            y_range[1] += (delta_y / 2)
        elif sector_ratio < view_ratio:
            # Need to add x to fit
            x_dist_new = y_dist * view_ratio
            delta_x = x_dist_new - x_dist  # Will be positive, if I did the math right
            x_range[0] -= (delta_x / 2)
            x_range[1] += (delta_x / 2)

        self._vb.setRange(xRange=x_range, yRange=y_range, padding=0)
        if index > 1:
            # Set the latitude/longitude filters to match the *final* view range,
            # with 1 degree padding on every side
            lon_from, lat_from = utils.mercator_proj(x_range[0], y_range[0], inverse=True)
            lon_to, lat_to = utils.mercator_proj(x_range[1], y_range[1], inverse=True)

            self._ui.sbLngFrom.setValue(lon_from - 1)
            self._ui.sbLngTo.setValue(lon_to + 1)
            self._ui.sbLatFrom.setValue(lat_from - 1)
            self._ui.sbLatTo.setValue(lat_to + 1)

            # Don't reload the data if we are not filtering on latitude or longitude
            if self._ui.gbLatitude.isChecked() or self._ui.gbLongitude.isChecked():
                self.on_loadFile_clicked()

    @Slot(int)
    def on_cbStack_stateChanged(self, state):
        """
            Stack or unstack the data, depending on the state of the checkbox,
            and reload the data

            Parameters
            ----------
            state: bool
                   Whether the data files selected should be stacked together
                   or not

            Returns
            _______
            None
        """
        # Disable the validty filter if stacking
        if state == Qt.Checked:
            self._ui.gbValidity.setChecked(False)
            self._ui.gbValidity.setEnabled(False)
        else:
            self._ui.gbValidity.setEnabled(True)

        if len(self.files) == 0:
            return

        enable = (state == Qt.Unchecked)
        self._ui.hsFileSelector.setEnabled(enable)
        self._ui.bPrevFile.setEnabled(enable)
        self._ui.bNextFile.setEnabled(enable)

        # Clear the caches. We always need to do a full reload if going from stacked to
        # unstacked or visa versa.
        self.so2data_cache.clear()
        self.on_loadFile_clicked()

    @Slot()
    def on_selectFile_clicked(self):
        """
            Initalizes the file selector with one or more user selected files to
            be loaded, and triggers loading the first one.
        """

        last_dir = self.settings.value("dataLocation", None)
        opt_down = QApplication.queryKeyboardModifiers() & Qt.AltModifier == Qt.AltModifier

        filt = "All Compatible (*.nc *.h5);;TROPOMI (*.nc);;OMPS (*.h5);;IASI (*.nc);;VIIRS (*.h5)"

        fileNames, _ = QFileDialog.getOpenFileNames(parent=self,
                                                    caption="Select data file(s)",
                                                    filter=filt,
                                                    dir=last_dir)
        if not fileNames:
            return

        # Clear out old data
        del self._calc_results
        self._calc_results = None
        if self.displayed_plot:
            self._plot_item.removeItem(self.displayed_plot)
            self.displayed_plot = None
            QApplication.processEvents()

        if self.so2data:
            if 'pixel_paths' in self.so2data:
                del self.so2data['pixel_paths']

            self.so2data = None

        # save the selected directory for next time. All files selected will be from the same directory
        file_path = "/".join(fileNames[0].split("/")[:-1])
        self.settings.setValue("dataLocation", file_path)

        # New set of files, so clear caches
        self.so2data_cache.clear_all()
        gc.collect()

        self.files = sorted(fileNames)  # due to file names, ensures date/time order
        first_filename = self.files[0].split("/")[-1]
        self._ui.fileName.setText(first_filename)
        self.setWindowTitle(f"SO2 Explorer: {first_filename}")

        # These may be the same file, if only one was selected
        date_from, _ = data_utils.parse_file_range(first_filename)
        _, date_to = data_utils.parse_file_range(self.files[-1].split("/")[-1])

        self._ui.lFileFrom.setText(date_from.strftime('%m/%d/%y\n%H:%M:%S'))
        self._ui.lFileTo.setText(date_to.strftime('%m/%d/%y\n%H:%M:%S'))

        self._ui.hsFileSelector.setMaximum(len(self.files) - 1)
        self._ui.hsFileSelector.setValue(0)
        self._fix_slider_size()

        # If more than one file, enable the slider and next/previous buttons.
        if len(self.files) > 1 and not self._ui.cbStack.isChecked():
            self._ui.hsFileSelector.setEnabled(True)
            self._ui.hsFileSelector.setValue(0)
            self._ui.bPrevFile.setEnabled(False)  # Already on the first file
            self._ui.bNextFile.setEnabled(True)
        else:
            self._ui.hsFileSelector.setEnabled(False)
            self._ui.bPrevFile.setEnabled(False)
            self._ui.bNextFile.setEnabled(False)

        # Default navigation direction forward
        self._nav_dir = 'forward'

        use_default_alt = not opt_down
        self.on_hsFileSelector_valueChanged(0, default_alt = use_default_alt)

    def _get_filter_string(self):
        filters = self.get_filters()


    @Slot()
    def on_loadFile_clicked(self, default_alt = False):
        """
            Load a data file.
            File loaded is based on the file selected in the File Selector.
        """
        self._loaded_file = None
        self._loaded_dataset = None
        self._loaded_filter = None

        if not self.files:
            return

        self._ui.PlotWidget.clear_select()

        file_index = self._ui.hsFileSelector.value()
        if len(self.files) == 1 or self._ui.cbStack.isChecked():
            # If stacking, just pass all the selected files at once
            filename = self.files
            check_file = filename[0]
        else:
            filename = self.files[file_index]
            check_file = filename

        if not filename:
            QMessageBox.critical(self, "Load Error", "No File Selected.")
            return

        # Remove any interpolated result sets
        self._clear_interpolated_results()

        try:
            filetype, self._filedef = get_filetype(check_file)
        except FileNotFoundError:
            self.progress_dialog.close()
            self._ui.cbStack.setChecked(False)
            self.load_error = QMessageBox()
            self.load_error.setText("Unable to load data")
            self.load_error.setInformativeText("Unable to read file(s)")
            self.load_error.setIcon(QMessageBox.Critical)
            self.load_error.finished.connect(self.load_error.deleteLater)
            self.load_error.show()
            return

        current_alt = self._ui.cbAltitude.currentIndex()
        self._ui.cbFileType.setCurrentText(filetype)

        # This will update the altitude pull-down
        self.on_cbFileType_currentTextChanged(self._ui.cbFileType.currentText())

        if filetype != self._prev_filetype:
            default_alt = True # Always use the default alitutde when changing file types.
            self.so2data_cache.clear()

        if default_alt:
            self._ui.cbAltitude.setCurrentText(utils.DEFAULT_ALTS[filetype])
            if self._ui.cbAltitude.currentText() == '':
                self._ui.cbAltitude.setCurrentIndex(0)
        else:
            self._ui.cbAltitude.setCurrentIndex(current_alt)

        dataset = self._ui.cbAltitude.currentData()
        self._prev_dataset = dataset
        self._prev_filetype = filetype

        filters = []
        if self._ui.cbStack.isChecked():
            # see if we need to re-import
            # Figure out binning. We need latitude and longitude bounds to bin data.
            if not self._ui.gbLongitude.isChecked() or not self._ui.gbLatitude.isChecked():
                self.progress_dialog.close()
                self._ui.cbStack.setChecked(False)
                self.load_error = QMessageBox()
                self.load_error.setText("Unable to load data")
                self.load_error.setInformativeText("Can not stack data without both latitude and longitude bounds set")
                self.load_error.setIcon(QMessageBox.Critical)
                self.load_error.finished.connect(self.load_error.deleteLater)
                self.load_error.show()
                return

            # get some filter values
            lat_from = self._ui.sbLatFrom.value()
            lat_to = self._ui.sbLatTo.value()
            lon_from = self._ui.sbLngFrom.value()
            lon_to = self._ui.sbLngTo.value()
            validity = self._ui.sbValidity.value() if self._ui.gbValidity.isChecked() else None
            sensor_zenith = self._ui.sbSensZenith.value() if self._ui.gbSensZenith.isChecked() else None
            solar_zenith = self._ui.sbSolZenith.value() if self._ui.gbSolZenith.isChecked() else None

            # Adjust lonFrom to work across dateline via "more negitive than -180" values
            if lon_from > 0 and lon_from > lon_to:
                lon_from -= 360

            # Since we are binning, go ahead and chop off un-needed data *before* the bin to speed
            # ingest operations. Add 5 degrees padding to make sure we don't chop off any
            # desired data.
            filters.append(f"latitude>={lat_from-5}")
            filters.append(f"latitude<={lat_to+5}")
            filters.append(f"longitude_range({lon_from-5},{lon_to+5})")

            change_keys = ('validity', 'lat_from', 'lat_to', 'lon_from', 'lon_to', 'sensor_zenith', 'solar_zenith')
            for key in change_keys:
                if self._prev_filters.get(key) != locals().get(key):
                    self.so2data_cache.clear()  # Reload the data from the file
                    logging.info("Reloading data due to pre-load filter change")
                    # Update the dictionary with current values
                    for udkey in change_keys:
                        self._prev_filters[udkey] = locals().get(udkey)

                    break
            else:
                logging.debug("No filters changed. All quiet.")

            if validity is not None:
                filters.append(f'SO2_column_number_density_validity>={validity}')
            if sensor_zenith is not None:
                filters.append(f'sensor_zenith_angle<{sensor_zenith}')
            if solar_zenith is not None:
                filters.append(f'solar_zenith_angle<{solar_zenith}')

            x_from, y_from = utils.latlon_merc_transformer.transform(lat_from, lon_from)
            x_to, y_to = utils.latlon_merc_transformer.transform(lat_to, lon_to)

            grid_size_x = self._filedef['INFO'].get('grid_x_resolution', 6660)  # in meters
            grid_size_y = self._filedef['INFO'].get('grid_y_resolution', 6660)
            num_lat = round((y_to - y_from) / grid_size_y)
            num_lon = round((x_to - x_from) / grid_size_x)

            proj = {'proj': 'merc', 'lon_0': '-90',
                    'preserve_units': False}

            proj = pickle.dumps(proj).hex()

            filters.append(f"bin_spatial({num_lat},{y_from}, {grid_size_y},{num_lon},{x_from},{grid_size_x},{proj})")

        filter_string = ";".join(filters)

        # Open Progress dialog
        self.progress_dialog.pbProgress.setValue(0)

        # Scale to 100000 so we can show finer grained progress
        self.progress_dialog.pbProgress.setRange(0, 100000)
        self._load_canceled = False
        self.progress_dialog.bCancelLoad.setEnabled(True)
        self.progress_dialog.lCurrentProcess.setText("Loading File(s)...")

        self.progress_dialog.open()
        QApplication.processEvents()

        # Most of the time, dataset will be an integer, so convert to str in
        # check so we can always call startswith, therby catching the possibility
        # of being the string "*" or a variant therof
        if filetype == 'OMPS':
            dataset = next((key for key, value in utils.OMPS_LOOKUP.items()
                            if value == dataset))
        elif filetype == 'VIIRS':
            dataset = '*'
        else:
            dataset = f"{dataset}km"

        dataset_option = f'so2_column={dataset};' if not str(dataset).startswith("*") else ''

        self._loaded_file = filename
        self._loaded_dataset = dataset_option
        self._loaded_filter = filter_string
        if not self.so2data_cache:
            #SIGNALLER.ensure_signaller()
            load_thread = Thread(target=data_utils.load_file,
                                 args=(filename, filter_string,
                                       dataset_option,
                                       self._load_complete),
                                 daemon=True)
            load_thread.start()

        else:
            # Don't flatten if pulling from cache, since it is already flattened
            self._load_complete((self.so2data_cache.data(), None),
                                flatten=False)


    def _load_complete(self, result, flatten=True):
        """
            Data loading complete handler.

            Parameters
            ----------
            result : dict
                Dictionary of raw data as loaded from the file(s)

            IMPORTANT NOTE: This function will run in another thread.
            DO NOT try to modify GUI from it
        """
        # If we have good data, "flatten" the data
        so2data, exception = result
        if exception is not None:
            if exception == '':
                exception = "Unknown Exception"
            self.file_loaded.emit(exception)
            return

        if flatten:
            so2data = data_utils.flatten_data(so2data)

        self.so2data = so2data

        altitude_product = f'SO2_number_density_{self._prev_dataset}km'
        try:
            self.so2data['SO2_column_number_density'] = self.so2data[altitude_product]
        except KeyError:
            pass  # This altitude product doesn't exist, just leave the old one


        self.file_loaded.emit(exception)  # this should get us back to the main thread.

    def calc_rtree_idx(self):
        print("Starting rtree index generation")
        t1 = time.time()
        self.so2data.attrs['grid_index'] = None
        self.so2data.attrs['grid_geometries'] = None
        gi, cb = data_utils.build_rtree_index(
            self.so2data['latitude_bounds'],
            self.so2data['longitude_bounds']
        )
        self.so2data.attrs['grid_index'] = gi
        self.so2data.attrs['grid_geometries'] = cb
        print(f"Built r-tree index in: {time.time() - t1}")


    def show_error_box(self, icon, text, inf_text=None, det_text=None,
                       finished=None):
        """
            Display a dialog box with a custom icon and text

            Parameters
            ----------
            icon : icon
                QMessageBox.icon object for the icon to set on the message box
            text : str
                String of the main text to show in the message
            inf_text : str
                Informative text to display in the message box
            det_text : str
                Detail text to display in the message box
            finished : callable
                Callback function to attach to the finished signal of the
                message box

            See also
            --------
            Qt QMessageBox Documentation (https://doc.qt.io/qt-5/qmessagebox.html)
        """
        self.message = QMessageBox(self)
        self.message.setIcon(icon)
        self.message.setText(text)

        if inf_text is not None:
            self.message.setInformativeText(inf_text)

        if det_text is not None:
            self.message.setDetailedText(det_text)

        if finished is not None:
            self.message.finished.connect(finished)

        self.message.finished.connect(self.message.deleteLater)
        self.message.show()
        self.message.activateWindow();

    def on_after_loadComplete(self, error):
        """
            Initiates processing/filtering of loaded data

            Parameters
            ----------
            error : None or str
                The error that occured, if any, while loading the data
        """
        self.progress_dialog.pbProgress.setValue(100000)
        QApplication.processEvents()
        if error:  # not none or empty string
            logging.error("****Exited with error: %s", str(error))
            self.progress_dialog.close()

            if not self._load_canceled:
                self.show_error_box(QMessageBox.Critical,
                                    "An error occured while loading the file.",
                                    error)
            else:
                self._load_canceled = False

            return

        # Show indicator of current file selected
        self.on_hsFileSelector_sliderMoved(self._ui.hsFileSelector.value())

        self.progress_dialog.lCurrentProcess.setText("Filtering data and calculating pixels...")
        self.progress_dialog.pbProgress.setRange(0, 0)
        self.progress_dialog.bCancelLoad.setEnabled(False)

        if not self.so2data_cache:
            self.so2data_cache.set_data(self.so2data)

        # Make sure we have all data needed for filters
        user_filters = self.get_filters()
        filter_names = [data_utils.FILTER_ALIASES.get(key,key) for key, value in
                        user_filters.items() if value is not None and data_utils.FILTER_ALIASES.get(key,key) not
                        in self.so2data]
        if filter_names:
            # Pull in any missing fields
            new_data,error = data_utils.load_file(self._loaded_file, self._loaded_filter,
                                            self._loaded_dataset, fields = filter_names)
            if new_data:
                new_data = data_utils.flatten_data(new_data)
                for key in filter_names:
                    logging.info("%s Not found in dataset. Importing.", key)
                    try:
                        self.so2data[key] = new_data[key]
                    except KeyError:
                        logging.error("Unaale to add product")

        self._processor_thread = processor(self.so2data, self)
        self._processor_thread.data_ready.connect(self._on_data_ready)
        self._processor_thread.show_msg.connect(self.show_error_box)
        self._processor_thread.finished.connect(self._cleanup_processing_thread)
        self._processor_thread.no_data.connect(self._no_data)
        self._processor_thread.start()

    @Slot()
    def _no_data(self):
        if self._ui.cbStack.isChecked():
            self.progress_dialog.close()
            self.show_error_box(QMessageBox.Critical,
                                "No data found for current filters/data files")
            return

        value = self._ui.hsFileSelector.value()
        max_val = self._ui.hsFileSelector.maximum()
        if (self._nav_dir == 'forward' and value >= max_val) or (self._nav_dir == 'backward' and value <= 0):
            self.progress_dialog.close()
            self.show_error_box(QMessageBox.Critical,
                                "No data found for current filters, and no more files to try.")

            self._nav_dir = 'forward'  # reset to going forward for the next attempt.
            return

        if self._nav_dir == 'forward':
            value += 1
        elif self._nav_dir == 'backward':
            value -= 1

        self._ui.hsFileSelector.setValue(value)

    def _cleanup_processing_thread(self):
        """
            Make sure that the data processing thread is really, truely,
            completly cleaned up and deleted

            Sometimes Qt objects can hang around longer than they should.
        """
        # Make sure the thread is *really* done
        if not hasattr(self, '_processor_thread'):
            return  # Nothing to clean up.

        self._processor_thread.wait()
        # Make sure the so2data object is deleted
        del self._processor_thread.so2data

        # Run the C/Qt cleanup routines
        self._processor_thread.deleteLater()
        # And the python
        del self._processor_thread


    def _on_data_ready(self, result):
        """
            Draw the inital data plots on the map.
            Called when data has been fully processed.

            Parameters
            ----------
            data : dict
                Dictionary of fully-proccesed SO2 data
        """
        self.progress_dialog.lCurrentProcess.setText("Drawing plots...")
        QCoreApplication.processEvents()
        self.so2data = result['data']

        #  save for future calculations
        if 'xy_coords' in result['data']:
            # Doesn't change except when we load a file
            self.xy_coords = result['data']['xy_coords']
            del result['data']['xy_coords']
            self.progress_dialog.pbProgress.setRange(0, len(self.xy_coords))
            # and launch the rtree generation thread in the background.
            Thread(target = self.calc_rtree_idx, daemon = True).start()            

        self.so2mass = result['data']['mass']
        self._generate_brushes()

    def _generate_brushes(self, du_only = False):
        if not du_only:
            self._plot_brushes = [
                [] # Empty entry to be replaced by the DU brushes
            ]

        # Generate SO2 brushes
        normalized_du = (self.so2data['du'].data - self._scale_min) / (self._scale_max - self._scale_min)
        normalized_du[normalized_du > 1] = 1
        normalized_du[normalized_du < 0] = 0

        # When rounded to 5 digits, the color results are identical (to not rounding). Rounding to
        # 4 might introduce a few extreamly slight (off-by-one) color errors, but significantly
        # reduces the number of unique values, thus noticiably improving performance.
        normalized_du = numpy.round(normalized_du, 4)

        lookup_table = {x: pg.mkBrush(self._du_color_map.map(x))
                        for x in numpy.unique(normalized_du)}
        so2brushes = [lookup_table[x] for x in normalized_du]
        self._plot_brushes[0] = so2brushes

        pen100 = QPen(Qt.green)
        pen100.setWidth(1)
        pen100.setCosmetic(True)
        pen99 = QPen(Qt.yellow)
        pen99.setWidth(1)
        pen99.setCosmetic(True)
        pen97 = QPen('#ffa500')
        pen97.setWidth(1)
        pen97.setCosmetic(True)
        pen0 = QPen(Qt.NoPen)
        pens = numpy.array([pen0, pen97, pen99, pen100])
        bins = [0, .97, .99, 1]

        indexes = numpy.digitize(self.so2data['percentile'],
                                 bins)
        self._highlight_pens = pens[indexes - 1]

        if not du_only:
            self._gen_other_brushes()

    def _clear_percents(self):
        item = self._ui.percentLayout.takeAt(0)
        while item is not None:
            item_widget = item.widget()
            del item_widget
            del item
            item = self._ui.percentLayout.takeAt(0)

    def _gen_other_brushes(self):
        """
            Generate the Qt brushes used to draw cloud and validity graphs, and
            append them to the _plot_brushes class member.

            Triggers the graph drawing when done.
        """
        # Generate cloud brushes
        QApplication.processEvents()

        # kinda convoluted, but makes for fast generation of the list
        # Get the transparency value - translate
        cloud_trans_value = self.so2data['cloud'] * 255
        cloud_trans_value = cloud_trans_value.where(~numpy.isnan(cloud_trans_value),
                                                    drop = True).round()
        cloudbrushes = numpy.full(self.so2data['cloud'].shape, QColor(0, 0, 0, 0))

        valid_cloud = list(map(utils.cloud_color_lookup.get, cloud_trans_value.to_numpy()))

        cloudbrushes[~numpy.isnan(self.so2data['cloud'])] = valid_cloud

        # Update the progress bar
        QApplication.processEvents()

        self._plot_brushes.append(cloudbrushes)

        validity_cmp = pg.ColorMap([0, 1], [(0, 0, 255), (255, 0, 0)])

        if numpy.isnan(self.so2data['validity']).any():
            validitybrushes = numpy.zeros_like(self.so2data['validity'])
        else:
            normalized_validty = self.so2data['validity'] / 100
            lookup_table = {x: pg.mkBrush(validity_cmp.map(x))
                            for x in numpy.unique(normalized_validty)}
            validitybrushes = numpy.vectorize(lookup_table.get)(normalized_validty)

        self._plot_brushes.append(validitybrushes)

        # Go ahead and plot the current item
        self.on_cbTrace_currentIndexChanged(self._ui.cbTrace.currentIndex())

        # And run the quick calcs
        self._calc_quick_stats()

    def _draw_plot(self, brushes):
        pens = self._highlight_pens if self._ui.cbHighlight.isChecked() else [Qt.NoPen] * len(brushes)
        x_coords = self.so2data['lon'].data
        y_coords = self.so2data['lat'].data
        paths = self.so2data['pixel_paths'].data
        scales = self.so2data['scale_sizes'].data
        # We can't draw full-size for memory and perfomance reasons,
        # so we draw at a fraction full size and scale up on display.

        # Calculate image ratio
        x_span = x_coords.max() - x_coords.min()
        y_span = y_coords.max() - y_coords.min()
        ratio = x_span / y_span  # y_span*ratio=x_span

        # Figure out max image size to not exceed 15GB
        target_size = 10  # in GB
        if x_coords.size > 500000:
            target_size = 5  # If we have a lot of data, make the image smaller to resepect memory

        max_bits = target_size * 1024 * 1024 * 1024 * 8
        max_pixels = max_bits / 32  # 32 bits/pixel, = x*y = y*ratio*y
        max_y = sqrt(max_pixels / ratio)

        target_scale = floor(y_span / max_y)

        # No need to make the image *too* large
        if target_scale < 500:
            target_scale = 500
        self.plotScale = target_scale

        master_scale = self.plotScale
        logging.debug(f"Using scale factor of {master_scale}")

        x_size = x_span / master_scale
        y_size = y_span / master_scale
        offset_x = -1 * x_coords.min()
        offset_y = -1 * y_coords.min()
        from math import ceil

        pixmap = QPixmap(QSize(ceil(x_size), ceil(y_size)))
        pixmap.fill(Qt.transparent)
        painter = QPainter(pixmap)

        print("Drawing image", end = "")
        for idx in range(x_coords.size):
            x = (x_coords[idx] + offset_x) / master_scale
            y = (y_coords[idx] + offset_y) / master_scale

            brush = brushes[idx]
            path = paths[idx]
            if path is None:
                continue

            size = scales[idx] / master_scale
            pen = pens[idx]

            painter.save()
            painter.translate(x, y)
            painter.scale(size, size)
            painter.setPen(pen)
            painter.setBrush(brush)
            painter.drawPath(path)
            painter.restore()
            if idx % 100000 == 0:
                print(".", end = "")
                self.progress_dialog.pbProgress.setValue(idx)
                QApplication.processEvents()

        painter.end()
        print("")
        gc.collect()
        print("Image draw complete")

        transform = QTransform()
        top = y_coords.min()
        left = x_coords.min()
        transform.translate(left, top)
        transform.scale(master_scale,
                        master_scale)

        plot_img = QGraphicsPixmapItem(pixmap)
        plot_img.setTransform(transform)
        plot_img.setZValue(LAYERS['data'])
        return plot_img

    def _pyqtgraph_plot(self, brushes):
        """
            Generate a plot using the supplied brushes for color and shape,
            then close the progress dialog

            Parameters
            ----------
            brushes : list or tuple
                A list of Qt brushes to use when drawing the data. Must be the
                same length as the latitude and longitude lists previously
                stored in the graphdata attribute
        """
        if self.displayed_plot is not None:
            self._plot_item.removeItem(self.displayed_plot)

        high_quality = self.settings.value('preferences/UseHighQuality', False)
        if high_quality:
            pens = self._highlight_pens if self._ui.cbHighlight.isChecked() else None

            self.displayed_plot = self._plot_item.plot(
                self.so2data['lon'].data.tolist(),
                self.so2data['lat'].data.tolist(),
                pen=None, name="so2",
                symbol=self.so2data['pixel_paths'].data.tolist(),
                symbolBrush=brushes,
                symbolPen=pens,
                pxMode=False,
                symbolSize=self.so2data['scale_sizes'].data.tolist(),
                dynamicRangeLimit = None
            )

        else:
            self.displayed_plot = self._draw_plot(brushes)
            self._plot_item.addItem(self.displayed_plot, ignoreBounds = True)

            # if doing low quality, we no longer need the pixel paths,
            # so delete them to save memory
#             if self.so2data and 'pixel_paths' in self.so2data:
#                 del self.so2data['pixel_paths']

        self.displayed_plot.setZValue(LAYERS['data'])

        self.progress_dialog.close()

    @Slot()
    def _clear_all(self):
        self._clear_selection()
        self._clear_winds()
        self._remove_measure()
        self._initalize_result_data_frames()

    @Slot()
    def _clear_select(self):
        self._ui.PlotWidget.clear_select()

    @Slot()
    def _clear_selection(self):
        self.on_bClearNoise_clicked()
        try:
            self._calc_results.drop(columns = ['sel_mean_du', 'sel_max_du',
                                               'sel_mean_perc', 'sel_max_perc',
                                               'selected_mass', 'adj_mass', 'em_rate'],
                                    inplace = True,
                                    errors = "ignore")

            self._results_table_model.setDataframe(self._calc_results)
        except AttributeError:
            pass

        self._selected_area = None

    def is_stack(self):
        return self._ui.cbStack.isChecked()

    def get_filters(self):
        density = self._ui.sbDensity.value() if \
            self._ui.gbDensity.isChecked() else None

        validity = self._ui.sbValidity.value() if \
            self._ui.gbValidity.isChecked() else None

        if self._ui.gbLongitude.isChecked():
            lng_from = self._ui.sbLngFrom.value()
            lng_to = self._ui.sbLngTo.value()
        else:
            lng_from = None
            lng_to = None

        if self._ui.gbLatitude.isChecked():
            lat_from = self._ui.sbLatFrom.value()
            lat_to = self._ui.sbLatTo.value()
        else:
            lat_from = None
            lat_to = None

        sensor_zenith = self._ui.sbSensZenith.value() if \
            self._ui.gbSensZenith.isChecked() else None

        solar_zenith = self._ui.sbSolZenith.value() if \
            self._ui.gbSolZenith.isChecked() else None

        cloud = self._ui.sbCloudPercent.value() if \
            self._ui.gbCloud.isChecked() else None

        filters = {
            'density': density,
            'validity': validity,
            'lat_from': lat_from,
            'lat_to': lat_to,
            'sensor_zenith': sensor_zenith,
            'solar_zenith': solar_zenith,
            'lng_from': lng_from,
            'lng_to': lng_to,
            'cloud_fraction': cloud,
        }
        return filters

    @Slot()
    def _remove_measure(self):
        self._ui.PlotWidget.clear_measure()

    @Slot()
    def _clear_measure(self):
        for label in self._measure_labels:
            self._plot_item.removeItem(label)

        self._measure_labels = []

    @Slot(list)
    def _area_selected(self, selection, offset, is_async=True):
        """When the user makes a selection, this function will calculate mass"""
        func = partial(self._calc_selection, selection, offset, False)

        if self._recalc_timer is not None:
            self._recalc_timer.stop()

        # Delay calculation by a small amount to allow additional points to be
        # added to the selection
        self._recalc_timer = QTimer()
        self._recalc_timer.timeout.connect(func)
        self._recalc_timer.setSingleShot(True)
        self._recalc_timer.start(500)
        return

    @Slot(list, list)
    def _calc_selection(self, selection, offset, is_async = True):
        # Selection isn't long enough to do anything with, or we don't have any
        # data so skip it
        if not self.so2data or len(selection) < 2:
            return

        if len(selection) < 3 and self._ui.PlotWidget.mode != Mode.MEASURE:
            self._calc_quick_stats()
            return

        # If there is an offset, apply it to the points
        offset_x = offset.x()
        offset_y = offset.y()
        if offset_x or offset_y:
            selection = [(pnt[0] + offset_x, pnt[1] + offset_y)
                         for pnt in selection]

        if self._ui.PlotWidget.mode == Mode.MEASURE:
            self._clear_measure()
            labels = data_utils.gen_dist_labels(selection)
            for label, dist in labels:
                self._plot_item.addItem(label)
                self._measure_labels.append(label)
            return

        # else:
        # Figure out the selected area
        self._selected_area = data_utils.calc_selected_area(selection)
        self._lSelectedArea.setText(f"{self._selected_area:,}km<sup>2</sup>")
        logging.debug(self._lSelectedArea.sizeHint())

        self._calcProcs += 1

        # Get the filtered data selection, and perform calculations on the data
        if is_async:
            pool = ThreadPoolExecutor()
            future = pool.submit(
                data_utils.filter_data_with_selection,
                None,
                selection,
                self.xy_coords,
                grid_index = self.so2data.attrs.get('grid_index'),
                grid_geometries = self.so2data.attrs.get('grid_geometries')
            )
            future.add_done_callback(self._process_filter_future)
        else:
            calc_result = data_utils.filter_data_with_selection(
                None,
                selection,
                self.xy_coords,
                grid_index = self.so2data.attrs.get('grid_index'),
                grid_geometries = self.so2data.attrs.get('grid_geometries')
            )

            self._calculate_filtered_data(calc_result)
            self._need_results_refresh.emit()

    def _process_filter_future(self, future):
        result = future.result()
        self._calculate_filtered_data(result)
        self._need_results_refresh.emit()

    @Slot()
    def _refresh_results(self):
        self._results_table_model.refreshView()
        self._results_tableView.resizeColumnsToContents()
        self.repaint()
        QApplication.processEvents()

    def _calculate_area_mass(self, selection):
        """Calculate the mass of SO2 within a given area and return a tuple
        containing the total mass and the raw DU values within that area"""
        filter_result = data_utils.filter_data_with_selection(
            None,
            selection,
            self.xy_coords,
            grid_index = self.so2data.attrs.get('grid_index'),
            grid_geometries = self.so2data.attrs.get('grid_geometries')
        )

        if not filter_result[1].any():
            return (None, None, None)

        alt_mass, alt_du = self._calculate_filtered_data(filter_result, True)
        return (alt_mass, alt_du, filter_result)

    def _calculate_filtered_data(self, result, ret_result = False):
        """
            Given the result of the _filter_data_with_area function, perform a
            number of calculations to determine mass and DU at all altitudes for
            the filtered data set.

            Results are either returned immediately, or further used to
            calculate mean, max, mean percentile and (for altitudes with
            wind data) emission rate
        """

        self._calcProcs -= 1
        if self._calcProcs > 0:
            return None

        _, selection_mask = result

        # Get mass data for all altitude products
        pixel_areas = numpy.asarray(self.so2data['area'])[selection_mask]
        alt_mass = {}
        alt_du = {}

        # Loop through the altitude products
        for alt, row in self._calc_results.iterrows():
            isinstance(alt, float)
            if alt.is_integer():
                alt = int(alt)

            interpolated_altitude = (row['interpolated'] == True)
            densities = self.so2data[f'SO2_number_density_{alt}km'] \
                if not interpolated_altitude else None

            # Just for the code analyzer. alt will always be a float at this point.
            (total_mass,
             alt_du_val,
             all_du) = data_utils.calc_alt_mass_du(row,
                                                   densities,
                                                   pixel_areas,
                                                   selection_mask)
            alt_mass[alt] = total_mass
            alt_du[alt] = alt_du_val

            # If we aren't returning the result, then we can use the results of
            # this calculation directly. We only return results when we want to
            # do something different with them.
            if not ret_result:
                calc_results = data_utils.calc_other_values(row,
                                                            total_mass,
                                                            all_du,
                                                            alt_du_val)

                for col, val in calc_results.items():
                    self._calc_results.at[alt, col] = val

        # Noise just returns the results. Since it is calculating the individual
        # noise sectors, it needs to combine the results from each of the four sectors
        if ret_result:
            return (alt_mass, alt_du)

        # else: # Effectively, but since we returned actually having this code
        # indented is redundant.
        self._calc_results['selected_mass'] = pd.Series(alt_mass)
        self._calc_results['adj_mass'] = pd.Series(dtype = numpy.float64)
        self._need_results_refresh.emit()
        self._calc_quick_stats()
        self._calc_cloud_validity(selection_mask)
        return None

    def _calc_cloud_validity(self, selection_mask):
        cloud_data = self.so2data['cloud_fraction'][selection_mask]
        cloud_percent = 0
        if len(cloud_data) > 0:  # Prevent divide by zero errors
            cloud_percent = round(numpy.average(cloud_data) * 100, 1)

        try:
            validity_data = self.so2data['SO2_column_number_density_validity'][selection_mask]
        except KeyError:
            validity_val = "nan"
        else:
            validity_val = 0
            if validity_data is not None and validity_data.size:
                validity_val = int(numpy.average(validity_data))

        self._selected_cloud = cloud_percent
        self._selected_validity = validity_val

    def keyPressEvent(self, event):
        if event.key() in (Qt.Key_Enter, Qt.Key_Return):
            self._ui.loadFile.animateClick()
            return True

        # else:
        return super().keyPressEvent(event)

    @show_dlg("Image Generating. Please Wait...")
    def export_image(self, width, height, path, fmt, options = None):
        ##################################
        # Image generation parameters
        ##################################
        map_pos = 1
        side_margin = 5
        bottom_margin = 5
        percent_height = 0  # Define as zero, so if we have wind but no percentile widget,
                            # we know where to put the wind widget. Will be changed if percentile
                            # widget is shown.
        inset_top = self._keys_top # Absolute top of all inset "widgets" (percentile, wind)
        ###################################

        # Extract export options
        if options:
            show_select = options.cbExportSelection.isChecked()
            show_noise = options.cbExportNoise.isChecked()
            show_sector = options.cbExportSector.isChecked()
            show_percent = options.cbExportPercentiles.isChecked()
            show_scale = options.cbExportColorbar.isChecked()
            show_stats = options.cbExportStats.isChecked()
            show_wind = options.cbExportWind.isChecked()
            show_levels = options.cbExportWindKey.isChecked()
            show_volcanoes = options.cbExportVolcs.isChecked()
            show_map_scale = options.cbExportScale.isChecked()
            show_measures = options.cbExportMeasures.isChecked()
            show_title = options.cbExportTitle.isChecked()
            show_graticule = options.cbShowGraticule.isChecked()
        else:
            show_select = True
            show_noise = True
            show_sector = True
            show_percent = True
            show_scale = True
            show_stats = True
            show_wind = True
            show_levels = True
            show_volcanoes = True
            show_map_scale = True
            show_measures = True
            show_title = True
            show_graticule = True


        self._output_font_items = {
            'header': [],
            'subhead': [],
            'label': [],
            'key': [],
            'volc': [],
            'key_head': [],
        }

        date_from_str, date_to_str = self._get_data_dates(str_format='%d %b %Y %H:%M')
        dpi = int(QApplication.primaryScreen().logicalDotsPerInch())
        pixel_size = QSize(round(width *dpi), round(height *dpi))
        line_scale = .00116 * pixel_size.width()

        # Create a top-level display widget
        display_widget = QDialog()
        display_widget.setAutoFillBackground(True)
        display_palette = display_widget.palette()
        display_palette.setColor(QPalette.Window, QColor(255, 255, 255))
        display_widget.setPalette(display_palette)
        display_widget.setFixedSize(pixel_size)

        # And the layout for this widget
        display_layout = QVBoxLayout()
        display_layout.setContentsMargins(side_margin, 2, side_margin, bottom_margin)
        display_layout.setSpacing(5)
        display_widget.setLayout(display_layout)

        title = f"{self._prev_filetype} {date_from_str}-{date_to_str} UTC"
        stats_text = self._ui.lStats.text()

        # Start adding widgets
        map_pos = 0
        # Add a title widget
        if show_title:
            map_pos += 1
            title_widget = QLabel(title)
            self._output_font_items['header'].append(title_widget.setFont)
            #title_widget.setFont(header_font)
            title_widget.setAlignment(Qt.AlignCenter)
            display_layout.addWidget(title_widget)


        # Add statistics widget
        if show_stats:
            map_pos += 1
            stats_widget = QLabel()
            stats_widget.setAlignment(Qt.AlignCenter)
            self._output_font_items['subhead'].append(stats_widget.setFont)
            #stats_widget.setFont(subhead_font)
            display_layout.addWidget(stats_widget)

        ##################################
        # Basemap
        ##################################
        # Create the map/data plot
        plot_layout = QHBoxLayout()
        plot_widget = create_plot_widget(None, show_graticule)
        plot_widget.setStyleSheet("border-radius:5px;background-color:rgb(255,255,255);")

        plot_layout.addWidget(plot_widget)

        display_layout.addLayout(plot_layout)
        display_layout.setStretch(map_pos, 1)

        plot_item = plot_widget.getPlotItem()
        viewbox = plot_item.getViewBox()
        viewbox.disableAutoRange()
        viewbox.setRange(self._vb.targetRect())

        viewbox.setAutoFillBackground(True)

        graph_palette = viewbox.palette()
        # 227 gray
        graph_palette.setColor(QPalette.Window, QColor(0, 0, 0, 28))

        viewbox.setPalette(graph_palette)

        left_axis = plot_item.getAxis('left')
        bottom_axis = plot_item.getAxis('bottom')
        right_axis = plot_item.getAxis('right')
        top_axis = plot_item.getAxis('top')

        right_axis.setZValue(LAYERS['map'])
        top_axis.setZValue(1)
        left_axis.setZValue(1)
        bottom_axis.setZValue(1)

        if fmt == '.pdf':
            right_axis.setPen(QPen(Qt.NoPen))
            top_axis.setPen(QPen(Qt.NoPen))

        self._output_font_items['label'].append(left_axis.label.setFont)
        self._output_font_items['label'].append(bottom_axis.label.setFont)
        self._output_font_items['key'].append(lambda x: left_axis.setStyle(tickFont = x))
        self._output_font_items['key'].append(lambda x: bottom_axis.setStyle(tickFont = x))

        # Load the SVG map background image
        map_item, svg_renderer = load_map_svg(True, fmt == ".pdf")
        plot_item.addItem(map_item, ignoreBounds = True)
        ###############################
        # End basemap
        ###############################

        ###############################
        # Stats Text
        ###############################
        if show_stats:
            stats = self._run_calc_quick_stats(viewbox.viewRange())
            if stats is not None:
                mass, area, maxdu, maxlon, maxlat = stats
                alt = self._ui.cbAltitude.currentText()
                if alt.endswith('km'):
                    alt = alt[:-2] + " km"

                quick_stats = (
                    f"Alt: {alt}; SO<sub>2</sub> Mass: {mass} kt;"
                    f" SO<sub>2</sub> max: {maxdu} DU at lon: {maxlon} lat: {maxlat}"
                )

                stats_widget.setText(quick_stats)
            else:
                stats_widget.hide()

        ###############################
        # Data plot
        ###############################
        # TODO: use the current plot (so2, cloud, validity) rather than assuming SO2
        # Plot the data
        if self.so2data and 'lon' in self.so2data:
            pens = self._highlight_pens if self._ui.cbHighlight.isChecked() else None
            plot_item.plot(
                self.so2data['lon'].data.tolist(),
                self.so2data['lat'].data.tolist(),
                pen=None, name="so2",
                symbol=self.so2data['pixel_paths'].data.tolist(),
                symbolBrush=self._plot_brushes[0],
                symbolPen=pens,
                pxMode=False,
                symbolSize=self.so2data['scale_sizes'].data.tolist(),
                dynamicRangeLimit = None
            )
            plot_item.setZValue(LAYERS['data'])
        ###############################
        # End data plot
        ###############################


        ##############################
        # Volcanoes
        ##############################
        if show_volcanoes:
            volc_size = int(round(pixel_size.width() / 66))  # pixel size
            volc_line_width = 1.25 if fmt == ".pdf" else 2
            volc_plot, labels = plot_volcanoes(plot_item, size = volc_size, width = volc_line_width)
            volc_plot.scatter.setExportMode(True)
            volc_points = volc_plot.scatter.points()
            filled_brush = pg.mkBrush('k')
            selected_volcs = self._selected_volcs
            for idx, label in enumerate(labels):
                label_text = label.textItem.toPlainText()
                always_show = label_text in selected_volcs

                self._output_font_items['volc'].append(volc_setFont(always_show, label))

                if self._zoom_level > 0 or always_show:
                    plot_item.addItem(label)

                if always_show:
                    volc_points[idx].setBrush(filled_brush)


        ##############################
        # End Volcanoes
        ##############################

        ##############################
        # ROI's
        ##############################
        # Add sector roi, if any
        line_mult = 2 if fmt == ".pdf" else 8
        penWidth = max(line_mult * line_scale, .5)
        if self._sector_roi is not None and show_sector:
            state = self._sector_roi.saveState()
            roi = sector_roi(state['points'])
            plot_item.addItem(roi)
            roi.setState(state)


        # Selection ROI
        if show_select:
            select_roi = self._ui.PlotWidget.get_select_roi()
            state = select_roi.getState()
            if state['points']:
                roi = LockingPolyLineROI([])
                roi.setZValue(LAYERS['selections'])
                pen:QPen = QPen(select_roi.currentPen)
                pen.setWidthF(penWidth)
                roi.pen = pen
                roi.currentPen = pen
                plot_item.addItem(roi)
                roi.setState(state)

        # Noise ROIs
        if show_noise:
            for noise_roi in self._noise_rois:
                state = noise_roi.getState()
                roi = LockingPolyLineROI(state['points'])
                roi.setZValue(LAYERS['selections'])
                pen = QPen(noise_roi.currentPen)
                pen.setWidthF(penWidth)
                roi.pen = pen
                roi.currentPen = pen

                plot_item.addItem(roi)
                roi.setState(state)


        ##############################
        # End ROI's
        ##############################

        # Add percentile widget
        ##############################
        # Percentile widget
        ##############################
        key_width = int(round(pixel_size.width() / 4))
        percent_height = int(round(key_width / 3.6))

        if self._ui.percentContainer.isVisible() and show_percent:
            p_labels = []
            p_widgets = []
            percent_container = create_percentile_widget(len(self._percentile_levels),
                                                         p_widgets,
                                                         p_labels)

            percent_container.setParent(plot_widget)
            percent_container.setFixedSize(QSize(key_width, percent_height))

            percent_title = percent_container.findChild(QLabel, "Percentiles Title")
            self._output_font_items['key'].append(percent_title.setFont)

            # copy labels/widgets from existing widget
            for idx, (label, widget) in enumerate(zip(self._percentile_labels, self._percentile_widgets)):
                p_labels[idx].setText(label.text())
                ss = widget.styleSheet()
                p_widgets[idx].setStyleSheet(ss)
                self._output_font_items['key'].append(p_labels[idx].setFont)

        ############################
        # End Percentile Widget
        ############################

        ############################
        # Wind Plots
        ############################
        # Add wind plots (if any)
        if hasattr(self, "wind_df") and self.get_wind_plots():
            if show_wind:
                wind_plots = plot_wind(self.wind_df, plot_item, forward = self.wind_forward,
                                       scale = line_scale)
                for plot in wind_plots:
                    plot.scatter.setExportMode(True)

                # Higlight any selected points
                outline_width = 4 if fmt == ".pdf" else 10
                size_increase = outline_width if fmt == '.pdf' else 1
                highlight_pen = pg.mkPen('#86d419', width = outline_width, cosmetic = True)

                for orig_plot,idx, _ in self._selected_points:
                    elevation = super(pg.PlotDataItem, orig_plot).data(Qt.UserRole)
                    # Figure out which of *our* plots this point is in
                    for plot in wind_plots:
                        plot_e = super(pg.PlotDataItem, plot).data(Qt.UserRole)
                        if plot_e == elevation:
                            break
                    else:
                        # Couldn't find a plot for this point
                        continue

                    plot.opts['symbolPen'][idx] = highlight_pen
                    plot.opts['symbolSize'][idx] += size_increase

                    plot.updateItems()

            if show_levels:
                wind_key = create_wind_key()
                wind_key.setFixedWidth(key_width)

                wind_title = wind_key.findChild(QLabel, "Winds Title")
                self._output_font_items['key'].append(wind_title.setFont)

                label_layout = wind_key.findChild(QHBoxLayout, "Wind Levels")
                for idx in range(label_layout.count()):
                    label = label_layout.itemAt(idx).widget()
                    self._output_font_items['key'].append(label.setFont)

                wind_key.setParent(plot_widget)
        ##############################
        # End wind plots
        ##############################

        ##############################
        # Measures
        ##############################
        if show_measures:
            measure_roi = self._ui.PlotWidget.get_measure_roi()
            state = measure_roi.getState()
            if state['points']:
                pen = QPen(measure_roi.currentPen)
                pen.setWidthF(penWidth)
                roi = LockingPolyLineROI(
                    [], False,
                    pen=pen,
                    hoverPen = {'color': (200, 200, 0, 0), 'width': 5},
                    handlePen = {'color': (0, 0, 170), 'width': 3}
                )
                roi.setZValue(LAYERS['selections'])
                plot_item.addItem(roi)
                roi.setState(state)

                labels = data_utils.gen_dist_labels(state['points'])
                for label, dist in labels:
                    self._output_font_items['label'].append(label.setFont)
                    plot_item.addItem(label)

        #############################
        # End Measures
        #############################

        ##############################
        # Scale Bar
        ##############################
        if show_map_scale:
            map_scale_widget = ScaleWidget(line_width = 1.5 * line_scale )
            self._output_font_items['key'].append(map_scale_widget.setFont)
            scale_height = int(round(pixel_size.width() / 26))
            map_scale_widget.setFixedHeight(scale_height)
            map_scale_widget.setParent(plot_widget)
            map_scale_widget.show()

            # map_scale_widget.update_scale(viewbox.viewRange(), viewbox.width())

            # vb_bottom = viewbox.pos().y() + viewbox.height()
            # map_scale_widget.move(60, vb_bottom)

        ##############################
        # End Scale Bar
        ##############################


        ##############################
        # Color Scale Bar
        ##############################
        # Add Color Scale bar
        if show_scale:
            gradient = self._du_color_map.getGradient()
            scale_widget = GradientWidget()
            scale_widget._barThickness /= 1.5
            self._output_font_items['label'].append(lambda x: scale_widget.setFont(label_font = x))
            self._output_font_items['key'].append(lambda x: scale_widget.setFont(tick_font = x))

            scale_widget.setOrientation('Vertical')
            scale_widget.setTitle("SO<sub>2</sub> Column Density (DU)")
            scale_widget.setLabels(self._scaleLabels[0])
            scale_widget.setGradient(gradient)

            plot_layout.addWidget(scale_widget)
            plot_layout.setStretch(0, 1)

            # Create gradient widget. Thanks to the way Qt produces PDF's, gradients dont work
            # in Apple programs, so we have to go through a different process that rasterizes
            # the gradient in order to get it to show up - we can't just use the scale widget
            # created above.
            gradient_label = QLabel()
            gradient_ss = "background:qlineargradient(x1:0 y1:1, x2:0 y2:0,"
            gradient_stops = []
            for pos, color in gradient.stops():
                rgba = color.getRgb()
                stop = f"stop:{pos} rgba{str(rgba)}"
                gradient_stops.append(stop)

            gradient_ss += ", ".join(gradient_stops)
            gradient_ss += ");"
            gradient_label.setStyleSheet(gradient_ss)
            gradient_label.setFixedWidth(scale_widget._barThickness)

        ########################
        # End color scale
        ########################



        #################
        #  Figure out fonts
        #
        target_width = pixel_size.width()

        base_font = QFont(display_widget.font())
        base_font.setFamily('Helvetica')

        header_font = QFont(base_font)
        subhead_font = QFont(base_font)
        label_font = QFont(base_font)
        key_font = QFont(base_font)
        key_head_font = QFont(base_font)

        key_font_size = int(round(pixel_size.width() / 52))  # pixel size
        if key_font_size < 8:
            key_font_size = 8

        key_font.setPixelSize(key_font_size)

        # Do a bit more work to figure out the header font size, so we know it fits.
        # This is due to the header text being much more variable than the key text.
        header_font_size = int(round(pixel_size.width() / 45)) # Select a possibly reasonable starting size
        header_fit = False
        subhead_fit = False
        while not header_fit or not subhead_fit:
            header_font_size -= 1
            if not header_fit:
                header_font.setPixelSize(header_font_size)
                metrics = QFontMetrics(header_font)
                title_width = metrics.boundingRect(title).width()
                header_fit = title_width <= target_width
            if not subhead_fit:
                subhead_font.setPixelSize(header_font_size)
                metrics = QFontMetrics(subhead_font)
                subhead_width = metrics.boundingRect(stats_text).width()
                subhead_fit = subhead_width <= target_width

        # Font metrics boundingRect is quite conservative, so bump up the header font size by a bit
        grow_factor = 3
        header_font.setPixelSize(header_font.pixelSize() + grow_factor)
        subhead_font.setPixelSize(subhead_font.pixelSize() + grow_factor)

        # Set the label font (axis tickmarks, colorscale, etc) to be half way between the key fonts
        # and the header fonts.
        label_font_size = round(1.5 * key_font_size) #int(round(key_font_size + .4 * (header_font_size + grow_factor - key_font_size)))
        label_font.setPixelSize(label_font_size)

        key_head_size = round(1.25 *key_font_size)
        key_head_font.setPixelSize(key_head_size)

        volc_label_font = QFont(key_font)

        self._output_fonts = {
            'header': header_font,
            'subhead': subhead_font,
            'label': label_font,
            'key': key_font,
            'key_head': key_head_font,
            'volc': volc_label_font,
        }

        logging.debug("header size: %f", header_font.pixelSize())
        logging.debug("Subheader size: %f", subhead_font.pixelSize())
        logging.debug("label size: %f", label_font.pixelSize())
        logging.debug("key size: %f", key_font.pixelSize())
        #
        ####################


        self._set_output_fonts()


        # Now that all the fonts are set, reposition everything
        display_widget.show()
        QApplication.processEvents()
        display_widget.hide()
        for i in range(5):
            QApplication.processEvents()

        inset_top = viewbox.pos().y() + 3

        # If we are showing the map scale, we have to re-position it after everything is
        # added *and* we briefly display the plot so everything is drawn and positioned correctly.
        if show_map_scale:
            map_scale_widget.update_scale(viewbox.viewRange(), viewbox.width())
            vb_bottom = viewbox.pos().y() + viewbox.height()
            ms_top = vb_bottom - map_scale_widget.height() - 3
            map_scale_widget.move(viewbox.pos().x() + 3, ms_top)
            QApplication.processEvents()


        if self._ui.percentContainer.isVisible() and show_percent:
            percent_container.move(viewbox.pos().x() + 3, inset_top)
            QApplication.processEvents()
            percent_height = percent_container.height()

            inset_top += percent_height

        if hasattr(self, "wind_df") and self.get_wind_plots() and show_levels:
            wind_key.move(viewbox.pos().x() + 3, inset_top + 5)
            QApplication.processEvents()


        for i in range(5):
            QApplication.processEvents()

        ##################################
        # Output widget created. Begin PDF/PNG generation
        ##################################
        # Render widget to a QPicture so it remains a vector when printed to PDF
        # Workaround found from 2014 thread - apparently this has been an issue forever.
        # Also have to do it twice, because the first time moves things around.
        qpic = QPicture()
        pic_painter = QPainter(qpic)
        display_widget.render(pic_painter, QPoint())
        pic_painter.end()


        if fmt == ".pdf":
            # Set up the PDF printer
            page_size = QPageSize(QSizeF(width, height), QPageSize.Inch,
                                  matchPolicy = QPageSize.ExactMatch)
            printer = QPrinter()

            printer.setOutputFormat(QPrinter.PdfFormat)
            printer.setPageSize(page_size)
            printer.setResolution(dpi)
            printer.setOutputFileName(path)
            printer.setPageMargins(QMarginsF(0, 0, 0, 0))

            painter = QPainter(printer)
            painter.drawPicture(QPointF(0, 0), qpic)

            ##############################
            # Special drawing code for the scale bar work around issues with Apple PDF display
            ##############################
            if show_scale:
                # Figure out the proper height for the gradient scale.
                # Must be done after the base scale is drawn to get
                # the margins correct.
                gradient_rect:QRectF = scale_widget.get_gradient_rect()

                scale_rect_pos = scale_widget.pos()
                scale_rect_pos.setY(scale_rect_pos.y() + scale_widget.margin_top)
                gradient_rect.moveTo(scale_rect_pos)

                gradient_top = gradient_rect.top()
                gradient_left = gradient_rect.left()

                gradient_height = gradient_rect.height()
                gradient_label.setFixedHeight(gradient_height)
                gradient_label.render(painter, QPoint(gradient_left, gradient_top))

                # Draw the scale lines
                line_step = gradient_height / 20
                line_y = gradient_rect.top() + 1

                line_left = gradient_left + (2 / 3) * gradient_rect.width()
                while line_y <= gradient_rect.bottom():
                    painter.drawLine(line_left, line_y, gradient_rect.right(), line_y)
                    line_y += line_step
                painter.drawLine(line_left, line_y, gradient_rect.right(), line_y)
            ##############################

            painter.end()
        else:


            image = QImage(pixel_size.width() *4,
                           pixel_size.height() *4,
                           QImage.Format_ARGB32)
            # Paint at a higher resolution
            #  72dpix4=288dpi, ~300
            image.setDevicePixelRatio(4);
            painter = QPainter(image)

            painter.drawPicture(QPoint(0, 0), qpic)
            painter.end()
            # Convert QImage to PIL image so we can save with DPI information
            buffer = QBuffer()
            buffer.open(QBuffer.ReadWrite)
            image.save(buffer, 'PNG')
            pil_img = Image.open(BytesIO(buffer.data()))
            pil_img.save(path, dpi = (288, 288))

    def _set_output_fonts(self):
        for key, font in self._output_fonts.items():
            for font_func in self._output_font_items[key]:
                font_func(font)


def volc_setFont(bold, item):
    def set_label_font(font):
        font = QFont(font) # Make a copy so we can set bold independantly.
        font.setBold(bold)
        item.setFont(font)
    return set_label_font

def render_basemap(viewbox, map_scene):
    """
    Render the background map into a QGraphicsPixmapItem, scaled and translated to the correct location.

    PARAMETERS
    ----------
    viewbox: The target viewbox for which the map will be rendered
    map_scene (QGraphicsScene): An existing map scene containing the map which will be re-rendered into the new scene.

    RETURNS
    -------
    map_item (QGraphicsPixmapItem): The basemap scene re-rendered, scaled, and transformed to fit the specified viewbox
    """
    scene = viewbox.viewRect()  # scene coordinates.

    # Render to the viewbox size so the final image will be, essentially, unscaled
    vb_size = viewbox.size().toSize()

    source_rect = QRectF(scene)
    view_width = source_rect.width()
    view_height = source_rect.height()
    source_rect.setWidth(view_width * 2)
    source_rect.setHeight(view_height * 2)
    source_rect.moveLeft(source_rect.left() - view_width / 2)
    source_rect.moveBottom(source_rect.bottom() - view_height / 2)

    render_size = vb_size * 2

    mapImage = QPixmap(render_size)
    mapImage.fill(Qt.transparent)

    # Create a painter so we can render into the image
    painter = QPainter(mapImage)

    map_scene.render(painter, QRectF(), source_rect, Qt.IgnoreAspectRatio)

    # Done with the painter, get rid of it.
    painter.end()
    # del painter

    map_item = QGraphicsPixmapItem(mapImage)
    # mapImage = None  # release this referance to the image. Now only the QGraphisPixmapItem has it.

    # Get the proper positioning/size for the image
    transform = QTransform()
    transform.translate(scene.left() - view_width / 2,
                        scene.top() - view_height / 2)

    try:
        transform.scale(scene.width() / vb_size.width(),
                        scene.height() / vb_size.height())
    except ZeroDivisionError:  # pragma: nocover
        # Since we aren't doing anything here, there is nothing to test
        pass  # Don't scale if the viewbox size is zero

    map_item.setTransform(transform)

    # Set the layering
    map_item.setZValue(LAYERS['map'])
    return map_item


def create_plot_widget(parent, graticule: bool = True) -> QWidget:
    # Set up the basic plot items
    lon_spacing = [(1113194.907932737, 0),
                   (222638.98158654757, 0),
                   (111319.49079327285, 0)
                   ]

    lat_spacing = [(10, 0),
                   (2, 0),
                   (1, 0)
                   ]

    tick_pen = QPen(Qt.DashLine)
    axis_pen = QPen("black")
    axis_pen.setWidthF(0.25)

    # Set up the custom axis items that translate x,y mercator coordinates to lat lon.
    right_axis = LatLonAxisItem("right", pen = axis_pen, tickPen = tick_pen)
    right_axis.setTickSpacing(levels = lat_spacing)

    if graticule:
        right_axis.setGrid(75)

    right_axis.setStyle(showValues = False, tickAlpha = 75)
    right_axis.setZValue(LAYERS['map'])

    # Set up the custom axis items that translate x,y mercator coordinates to lat lon.
    top_axis = LatLonAxisItem("top", pen = axis_pen, tickPen = tick_pen)
    top_axis.setTickSpacing(levels = lon_spacing)

    if graticule:
        top_axis.setGrid(75)

    top_axis.setStyle(showValues = False, tickAlpha = 75)
    top_axis.setZValue(LAYERS['map'])


    left_axis = LatLonAxisItem("left")
    left_axis.setTickSpacing(levels = lat_spacing)
    left_axis.setLabel('º Latitude')
    left_axis.enableAutoSIPrefix(enable=False)
    bottom_axis = LatLonAxisItem("bottom")
    bottom_axis.setTickSpacing(levels = lon_spacing)
    bottom_axis.setLabel("º Longitude")
    bottom_axis.enableAutoSIPrefix(enable=False)

    # Custom view box to give a solid border, lock the aspect ratio to reduce distortion,
    # and set limits on panning/zooming to keep within map boundries.
    view_box = pg.ViewBox(border = {'color': "black", 'width': 1}, lockAspect = 1)
    view_box.setLimits(**{
        'xMin': -20037508.341676053,
        'xMax': 20037508.341676053,
        'yMax': 19971868.880408563,
        'yMin': -19971868.880408563,
    })

    plot_widget = PlotGraphicsView(parent,
                                   axisItems={"right": right_axis,
                                              'top': top_axis,
                                              'left': left_axis,
                                              'bottom': bottom_axis,
                                              },
                                   viewBox=view_box)

    plot_item = plot_widget.getPlotItem()
    view_box.setParent(plot_item)
    # plot_item.addItem(lat_grid)
    plot_widget.showAxis("right")
    plot_widget.showAxis("top")

    plot_widget.setBackgroundBrush(QBrush(Qt.transparent, Qt.NoBrush))
    plot_widget.setStyleSheet("border-radius:5px;background-color:rgb(227,227,227);")

    return plot_widget


def load_map_svg(ak_only = False, thin = True):
    svg_item = QGraphicsSvgItem()

    if ak_only:
        file = "AlaskaMap"
        if thin:
            file += "Thin"

        svg_file_path = os.path.join(MAIN_DIR, f'SupportFiles/{file}.svg')
        svg_renderer = QSvgRenderer(svg_file_path)
    else:
        # svg_file_path = os.path.join(MAIN_DIR, 'SupportFiles/WorldMap.svg')
        # svg_renderer = QSvgRenderer(svg_file_path)
        svg_file_path = os.path.join(MAIN_DIR, 'SupportFiles/WorldMap.gz')
        with gzip.open(svg_file_path) as wmap:
            svg_renderer = QSvgRenderer(wmap.read())

    svg_item.setSharedRenderer(svg_renderer)

    # These values are the limits for the mercator projection, +/-180 degree longitude.
    # No magic here:-)
    x_start = -20037508.34278924
    x_stop = 20037508.342789244
    x_size = x_stop - x_start

    # 89 degrees north
    y_stop = 19971868.880408563

    transform = QTransform()

    # This, however, IS a magic number, just to get things to line up properly N-S
    # I believe this is necessary due to the SVG clipping at the top of the data,
    # rather than "89º North", such that this offset *should* be the difference between 89º North
    # and whatever latitude the furthest north shoreline is at. This has not been verified.
    transform.translate(x_start, y_stop - 1587600)

    svg_width = svg_item.boundingRect().width()
    SCALE_FACTOR = x_size / svg_width

    transform.scale(SCALE_FACTOR, -1 * SCALE_FACTOR)
    svg_item.resetTransform()
    svg_item.setTransform(transform)

    svg_item.setZValue(LAYERS['map'])
    return (svg_item, svg_renderer)


def create_percentile_widget(num_levels, widget_store = None, label_store = None):
    percent_container = QWidget()
    percent_height = 60 if sys.platform.startswith('win') else 45
    percent_container.setFixedSize(QSize(300, percent_height))
    percent_container.setObjectName("Percent Container")
    percent_container.setAutoFillBackground(False)
    percent_container.setStyleSheet('background-color:transparent')

    percent_container.raise_()

    main_layout = QVBoxLayout(percent_container)
    main_layout.setObjectName("Main Layout")
    main_layout.setContentsMargins(0, 0, 0, 0)
    main_layout.setSpacing(0)

    title = QLabel(percent_container)
    title.setTextInteractionFlags(Qt.TextSelectableByMouse | Qt.TextSelectableByKeyboard)
    title.setText("Percentiles (DU):")
    title.setAlignment(Qt.AlignLeft)
    title.setObjectName("Percentiles Title")
    title_font = title.font()
    title_font.setPointSize(13)
    title.setFont(title_font)
    main_layout.addWidget(title)

    percent_layout = QHBoxLayout()
    percent_layout.setObjectName("Percent Bar Layout")
    percent_layout.setContentsMargins(0, 0, 0, 0)
    percent_layout.setSpacing(0)
    main_layout.addLayout(percent_layout)

    main_layout.setStretchFactor(title, 0)
    main_layout.setStretchFactor(percent_layout, 1)

    # Create the correct number of widgets for the number of levels we have
    for _ in range(num_levels):
        widg = QWidget()
        lay = QVBoxLayout()
        lay.setContentsMargins(0, 0, 0, 0)
        widg.setLayout(lay)
        label = QLabel()
        label.setTextInteractionFlags(Qt.TextSelectableByMouse | Qt.TextSelectableByKeyboard)
        labelFont = label.font()
        labelFont.setPointSize(11)
        label.setFont(labelFont)
        label.setAlignment(Qt.AlignCenter)
        lay.addWidget(label)
        percent_layout.addWidget(widg)
        if hasattr(widget_store, 'append'):
            widget_store.append(widg)

        if hasattr(label_store, 'append'):
            label_store.append(label)

    return percent_container


def plot_wind(df, plot_item, forward = False, click_callback = None, scale = 1):
    elevations = df.elevF.unique()
    plots = []
    for elevation in elevations:
        data = df[df.elevF == elevation].copy()
        x = data.x.tolist()
        y = data.y.tolist()
        color = utils.WIND_COLORS[elevation]
        pen_width = 1.5 * scale
        pen = {'color': color,
               'width': pen_width,
               }


        # Back winds should be a dashed line
        if not forward:
            pen['dash'] = [14, 7]

        symbol_pen = [None, ]*len(x)
        symbol_size = data.symbol_size.copy()
        if scale != 1:
            symbol_size *= scale

        plot = plot_item.plot(x, y,
                              pen = pen,
                              symbol = data.symbol.tolist(),
                              symbolBrush = color,
                              symbolPen = symbol_pen,
                              symbolSize = symbol_size.tolist(),
                              pxMode = True,
                              data = list(zip(data['hour'], range(len(data)))))
        plot.setZValue(LAYERS['wind'])

        if click_callback is not None:
            plot.sigPointsClicked.connect(click_callback)

        # Have to use super here because pg chose to override the setData
        # function with something completly different.
        super(pg.PlotDataItem, plot).setData(Qt.UserRole, elevation)
        super(pg.PlotDataItem, plot).setData(Qt.UserRole+1, symbol_pen)
        super(pg.PlotDataItem, plot).setData(Qt.UserRole+2, symbol_size)

        plots.append(plot)

    return plots

def create_wind_key():
    wind_key = QWidget()
    wind_key.setObjectName("Wind Key Container")
    wind_key.setAutoFillBackground(False)
    wind_key.setStyleSheet('background-color:transparent')
    wind_key.setFixedWidth(200)

    main_layout = QVBoxLayout(wind_key)
    main_layout.setObjectName("Wind Vertical Layout")
    main_layout.setSpacing(0)
    main_layout.setContentsMargins(0, 0, 0, 0)

    title = QLabel(wind_key)
    title.setText("Wind Levels (1000ft):")
    title.setTextInteractionFlags(Qt.TextSelectableByMouse | Qt.TextSelectableByKeyboard)
    title.setAlignment(Qt.AlignLeft)
    title.setObjectName("Winds Title")
    title_font = title.font()
    title_font.setPointSize(13)
    title.setFont(title_font)
    main_layout.addWidget(title)

    key_layout = QHBoxLayout()
    key_layout.setObjectName("Wind Levels")
    main_layout.addLayout(key_layout)
    key_layout.setSpacing(0)
    key_layout.setContentsMargins(0, 0, 0, 0)
    first = True
    for level, color in utils.WIND_COLORS.items():
        label = QLabel(wind_key)
        label.setText(f"{int(level / 1000)}")

        text_color = 'black' if color in ('#0FF', '#add8e6') else 'white'
        ss = f'padding-top:3px;padding-bottom:3px;border-radius:0px;background-color:{color};color:{text_color};border:1px solid black;'
        if not first:
            ss += "border-left:None;"
        else:
            first = False

        label.setStyleSheet(ss)
        label.setTextInteractionFlags(Qt.TextSelectableByMouse | Qt.TextSelectableByKeyboard)
        label.setAlignment(Qt.AlignCenter)
        label_font = label.font()
        label_font.setPointSize(11)
        label.setFont(label_font)

        key_layout.addWidget(label)

    return wind_key

#############################
# ROI definition functions
#############################
def sector_roi(points):
    roi = pg.PolyLineROI(points, closed = True,
                          pen = {'color': "#F00",
                                 "width": 2,
                                 'style': Qt.DashLine, },
                          movable = False,
                          rotatable = False,
                          resizable = False,
                          removable = False,
                          handlePen = pg.mkPen(None),
                          hoverPen = {'color': "#F00",
                                      "width": 2,
                                      'style': Qt.DashLine, },
                          handleHoverPen = pg.mkPen(None))
    roi.setZValue(LAYERS['sector'])
    return roi

def noise_roi(points):
    roi = LockingPolyLineROI(points, True,
                              pen = {'color': "#888", 'width': 5, },
                              hoverPen = {'color': (200, 200, 0), 'width': 5},
                              handlePen = {'color': "#DDD", 'width': 3}
                              )
    roi.setZValue(LAYERS['selections'])
    return roi

def plot_volcanoes(dest:pg.PlotItem, font:QFont|None = None,
                   size:float = 10, width = 1):
    volclat = VOLCANOES['latitude'].tolist()
    volclon = VOLCANOES['longitude'].tolist()
    volc_name = VOLCANOES['name'].tolist()
    volcx, volcy = utils.latlon_merc_transformer.transform(volclat, volclon)
    volc_labels = []

    for idx, label in enumerate(volc_name):
        volc_label = pg.TextItem(label, (0, 0, 0))
        if font is not None:
            volc_label.setFont(font)
        volc_label.setPos(volcx[idx], volcy[idx])
        volc_label.setZValue(LAYERS['volc_labels'])
        volc_labels.append(volc_label)


    volc_plot = dest.plot(volcx, volcy,
                          pen=None,
                          symbolPen={
                              'color': '#000000',
                              'width': width
                          },
                          symbol='t1',
                          data=[{'name': x} for x in volc_name],
                          symbolBrush=None,
                          ignoreBounds=True,
                          symbolSize = size)

    volc_plot.setZValue(LAYERS['markers'])

    return (volc_plot, volc_labels)

def create_scale_widget(view_range, width):
    scale = ScaleWidget()
    scale.setFixedHeight(27)

    return scale


def calc_du_levels(du_min, du_max):
    levels = {
        0: 0,
        .05: 1,
        .1: 2,
        .25: 5,
        .75: 15,
        1: 20,
    }

    diff = du_max - du_min
    for level in levels:
        val = round(du_min + (level * diff), 1)
        try:
            if val.is_integer():
                val = int(val)
        except AttributeError:
            pass # Already an integer

        levels[level] = str(val)

    levels[level] = ">" + levels[level]
    return levels

def request_winds(url):
    try:
        res = requests.get(url)
    except requests.exceptions.ConnectionError as e:
        logging.error("Unable to connect to wind server: %s", str(e))
        return pd.DataFrame()

    if res.status_code != 200:
        logging.error("Unable to get result from wind server. Server returned code: %d %s", res.status_code, res.text)
        return pd.DataFrame()

    winds = res.json()
    df = pd.DataFrame.from_dict(winds['dA'])

    return df
