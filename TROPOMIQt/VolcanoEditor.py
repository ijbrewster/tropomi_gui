# Volcano Editor Dialog
import os
from copy import deepcopy 

from PySide6 import QtUiTools

from PySide6.QtWidgets import (QDialog,
                               )

from PySide6.QtCore import (
    QFile,
    QModelIndex, 
    QAbstractTableModel, 
    QSettings,
    Signal,
    Qt
)

from .viewPresets import VOLCANOES

CUR_DIRECTORY = os.path.dirname(__file__)

class VolcanoEditor(QDialog):
    volcsChanged = Signal()
    
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        
        self.settings = QSettings()
        
        ui_loader = QtUiTools.QUiLoader()
        editor_ui_file = QFile(os.path.join(CUR_DIRECTORY, 'VolcanoEditor.ui'))
        editor_ui_file.open(QFile.ReadOnly)
        
        self._ui = ui_loader.load(editor_ui_file, self)
        editor_ui_file.close()
        
        self.setLayout(self._ui.layout())
        self.layout().setContentsMargins(12, 12, 12, 12)
        
        self.setWindowTitle("Volcanoes")
        self.resize(554, 537)
        
        self.init_volcano_view()
        
        self._ui.buttonBox.accepted.connect(self.saveChanges)
        self._ui.buttonBox.rejected.connect(self.cancelChanges)
        self._ui.bAddRow.clicked.connect(self.addRow)
        self._ui.bRemoveRow.clicked.connect(self.removeRow)
        
        self._ui.tvVolcs.horizontalHeader().sortIndicatorChanged.connect(self._ui.tvVolcs.sortByColumn)
        self._ui.tvVolcs.sortByColumn(1, Qt.AscendingOrder)
        
    def saveChanges(self):
        self.save_volcs()
        return self.accept()
        
    def cancelChanges(self):
        self.volcModel = VolcanoTableModel(self._original)
        self.save_volcs()
        return self.reject()
    
    def init_volcano_view(self):
        volcs = self.settings.value('Volcanoes', VOLCANOES)
        volcs = [list(x) for x in volcs] # Make sure items are mutable
        self._original = deepcopy(volcs)
        self.volcModel = VolcanoTableModel(volcs)
        self.volcModel.dataChanged.connect(self.save_volcs)
        self._ui.tvVolcs.setModel(self.volcModel)
        
    def save_volcs(self):
        volcs = self.volcModel.getVolcs()
        self.settings.setValue('Volcanoes', volcs)
        self.volcsChanged.emit()
        
    def addRow(self):
        self.volcModel.insertRow(-1)
        
    def removeRow(self):
        selection = self._ui.tvVolcs.selectedIndexes()
        for idx in selection:
            row = idx.row()
            self.volcModel.removeRow(row)
            
        self.save_volcs()
                    
        
class VolcanoTableModel(QAbstractTableModel):
    COLUMNS = (
        'Lat',
        'Lng',
        'Name'
    )
    
    COL_TYPES = (
        float,
        float,
        str
    )
    
    
    def __init__(self, volcs, parent=None):
        super().__init__(parent)
        self._data = volcs
        
    def getVolcs(self):
        return self._data
    
    def rowCount(self, parent=QModelIndex()):
        return len(self._data)
    
    def columnCount(self, parent=QModelIndex()):
        return len(self.COLUMNS)
    
    def data(self, index: QModelIndex, role=Qt.DisplayRole):
        if role in (Qt.DisplayRole, Qt.EditRole, Qt.ToolTipRole):
            row = index.row()
            col = index.column()
            data = self._data[row][col]
            return str(data)
        
        return None
        
    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role != Qt.DisplayRole:
            return None

        if orientation == Qt.Horizontal:
            return self.COLUMNS[section]
        else:
            return section
    
    def setData(self, index: QModelIndex, value, role=Qt.EditRole):
        row = index.row()
        col = index.column()
        data_type = self.COL_TYPES[col]
        value = data_type(value)
        self._data[row][col] = value
        self.dataChanged.emit(index, index, [role, ])
        return True
    
    def flags(self, index):
        return Qt.ItemIsSelectable | Qt.ItemIsEditable | Qt. ItemIsEnabled
    
    def insertRows(self, row, count, parent=QModelIndex()):
        self.beginInsertRows(QModelIndex(), row + 1, row + count )
        if row == -1:
            row = len(self._data)
        idx = row
        while idx < row + count:
            self._data.insert(idx, [0, 0, 'NEW'])
            idx += 1
        self.endInsertRows()
        return True
        
    def removeRows(self, row, count, parent=QModelIndex()):
        self.layoutAboutToBeChanged.emit()
        self.beginRemoveRows(QModelIndex(), row, row + count)
        for i in range(count):
            del self._data[row]
        self.endRemoveRows()
        self.layoutChanged.emit()
        return True
        
    def longitudeSort(self, row):
        lng_col = self.COLUMNS.index('Lng')
        value = row[lng_col]
        if value > 0:
            return value - 360
        else:
            return value
        
    def sort(self, column, order=Qt.AscendingOrder):
        self.layoutAboutToBeChanged.emit()
        reverse = False if order == Qt.AscendingOrder else True
        lng_col = self.COLUMNS.index('Lng')
        if column == lng_col:
            key = self.longitudeSort
        else:
            key = lambda x: x[column]
            
        self._data.sort(key = key, reverse = reverse)
        self.layoutChanged.emit()