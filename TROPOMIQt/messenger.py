"""messenger.py
A simple QObject subclass to facilitate passing status messages between the main thread
and child threads/processes.
"""
import logging
import multiprocessing

from PySide6.QtCore import (
    QObject,
    Signal
)

class Messenger(QObject):
    progress = Signal(float)
    status = Signal(str, int, int)
    
    def __init__(self):
        super().__init__()
        self._progress = -1
        self._final = 0
        self._queue = multiprocessing.Queue()
        
    def get_queue(self):
        return self._queue
    
    def run(self):
        while True:
            msg = self._queue.get()
            if msg == 'QUIT':
                logging.info("Exiting messenger due to QUIT command")
                break
            elif msg == 'RESET':
                self._progress = -1
                self._final = 0
            elif isinstance(msg, (list, tuple)) and msg[0] == 'FINAL':
                self._final = int(msg[1])
            elif msg == 'PROGRESS':
                self._progress += 1
                if self._final == 0:
                    self._final = 1 # Avoid divide by zero
                    
                self.progress.emit((self._progress / self._final) * 100)
            elif isinstance(msg, (list, tuple)) and msg[0] == 'PROGRESS':
                if len(msg) == 4: # fixed progress indicator
                    prog = msg[2]
                    total = msg[3]
                elif len(msg) == 2: # Indefinite progress indicator
                    prog = -1
                    total = 0
                    
                self.status.emit(msg[1], prog, total)
            else:
                logging.warning("Received unexpected message: %s", str(msg))