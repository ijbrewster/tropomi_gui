"""
__init__.py
Copyright © 2020 Alaska Volcano Observatory
Distributed under MIT license. See license.txt for more information
"""
import sys

__version__ = "2.7.4"
APP_NAME = 'SO2 Explorer'
ORGANIZATION = 'AVO'
DOMAIN = 'uaf.edu'

from . import utils
utils.configure_logging()


# This only comes into play when running on my build windows machine, so the absolute paths are fine.
if sys.platform == 'win32' and not getattr(sys, 'frozen', False):
    import PySide6
    import os
    os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = os.path.dirname(PySide6.__file__) + '/plugins/platforms'
