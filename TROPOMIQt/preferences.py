import os

from PySide6.QtWidgets import (QDialog,
                               QFileDialog)
from PySide6 import QtUiTools
from PySide6.QtCore import (QSettings,
                            Slot)

from .utils import SIZE_OPTS, configure_logging

CUR_DIRECTORY = os.path.dirname(__file__)


class PrefsWin(QDialog):
    def __init__(self, parent = None):

        super().__init__(parent)

        self.settings = QSettings()
        ui_loader = QtUiTools.QUiLoader()
        self._ui = ui_loader.load(os.path.join(CUR_DIRECTORY, "preferences.ui"),
                                  self)

        self._ui.buttonBox.accepted.connect(self._savePreferences)
        self._ui.buttonBox.rejected.connect(self.reject)
        self._ui.bBrowseSave.clicked.connect(self._get_save_path)
        self._ui.cbImageSize.currentIndexChanged.connect(self._imageSizeChanged)
        self._ui.cbSaveDebug.clicked.connect(self._setDebugLog)
        self._ui.pbBrowseLogLocation.clicked.connect(self._getDebugLogLocation)

        self.finished.connect(self.deleteLater)
        self.finished.connect(self._ui.deleteLater)

        self.setLayout(self._ui.layout())

        self.setGeometry(self._ui.geometry())
        self.setWindowTitle(self._ui.windowTitle())

        self.settings.beginGroup("preferences")
        save_path = self.settings.value('path', '')
        save_name = self.settings.value('resultFile', 'AVO_Operational_Satellite_SO2_analysis.csv')
        img_width = self.settings.value('imgWidth', SIZE_OPTS[0][0])
        img_height = self.settings.value('imgHeight', SIZE_OPTS[0][1])
        img_size = self.settings.value('imgSize', 0)
        high_quality = self.settings.value('UseHighQuality', False)
        check_for_updates = self.settings.value('checkForUpdates', True)
        debug_log = self.settings.value('logFile', '')
        save_debug = self.settings.value('saveDebug', False)
        self.settings.endGroup()

        # Apparently on windows, this sometimes comes through as a string rather than a boolean :(
        if isinstance(high_quality, str):
            high_quality = True if high_quality.lower() == 'true' else False

        if isinstance(save_debug, str):
            save_debug = True if save_debug.lower() == 'true' else False

        if isinstance(check_for_updates, str):
            check_for_updates = True if check_for_updates.lower() == 'true' else False

        self._ui.cbCheckForUpdates.setChecked(check_for_updates)
        self._ui.sbUpdateInterval.setValue(self.settings.value('UDCheckDays', 7))
        self._ui.leSavePath.setText(save_path)
        self._ui.leFileName.setText(save_name)
        self._ui.sbWidth.setValue(img_width)
        self._ui.sbHeight.setValue(img_height)
        self._ui.cbImageSize.setCurrentIndex(img_size)
        self._ui.cbUseHighQuality.setChecked(high_quality)
        entry_enabled = (img_size >= len(SIZE_OPTS))
        self._ui.sbWidth.setEnabled(entry_enabled)
        self._ui.sbHeight.setEnabled(entry_enabled)

        self._ui.leDebugLogLocation.setText(debug_log)
        self._ui.leDebugLogLocation.setEnabled(save_debug and bool(debug_log))
        self._ui.cbSaveDebug.setChecked(save_debug and bool(debug_log))

    @Slot()
    def _savePreferences(self):
        save_location = self._ui.leSavePath.text()
        save_name = self._ui.leFileName.text()
        img_width = self._ui.sbWidth.value()
        img_height = self._ui.sbHeight.value()
        size_idx = self._ui.cbImageSize.currentIndex()
        udcheck_enabled = self._ui.cbCheckForUpdates.isChecked()
        ud_interval = self._ui.sbUpdateInterval.value()
        self.accept()

        self.settings.beginGroup("preferences")
        self.settings.setValue('path', save_location)
        self.settings.setValue('resultFile', save_name)
        self.settings.setValue('imgWidth', img_width)
        self.settings.setValue('imgHeight', img_height)
        self.settings.setValue('imgSize', size_idx)
        self.settings.setValue('UseHighQuality', self._ui.cbUseHighQuality.isChecked())
        self.settings.setValue('logFile', self._ui.leDebugLogLocation.text())
        self.settings.setValue('saveDebug', self._ui.cbSaveDebug.isChecked())
        self.settings.endGroup()

        self.settings.setValue('checkForUpdates', udcheck_enabled)
        self.settings.setValue('UDCheckDays', ud_interval)

        configure_logging()

    @Slot()
    def _get_save_path(self):
        path = QFileDialog.getExistingDirectory(self, "Save Directory")
        if not path:
            return
        self._ui.leSavePath.setText(path)

    @Slot(int)
    def _imageSizeChanged(self, index):
        try:
            img_width, img_height = SIZE_OPTS[index]
        except IndexError:
            self._ui.sbWidth.setEnabled(True)
            self._ui.sbHeight.setEnabled(True)
        else:
            self._ui.sbWidth.setEnabled(False)
            self._ui.sbHeight.setEnabled(False)
            self._ui.sbHeight.setValue(img_height)
            self._ui.sbWidth.setValue(img_width)

    @Slot()
    def _getDebugLogLocation(self) -> bool:
        path, _ = QFileDialog.getSaveFileName(self, "Debug Log:",
                                              'SO2Explorer Debug.log', "Text Logs (*.log)",
                                              "Text Logs (*.log)",
                                              options=QFileDialog.DontConfirmOverwrite)
        if not path:
            return False
        self._ui.leDebugLogLocation.setText(path)

        # We selected a path, so assume we actually want a log file
        self._ui.leDebugLogLocation.setEnabled(True)
        self._ui.cbSaveDebug.setChecked(True)
        return True

    @Slot(bool)
    def _setDebugLog(self, checked: bool) -> None:
        if checked and not self._ui.leDebugLogLocation.text():
            path_set = self._getDebugLogLocation()
            if not path_set:
                self._ui.cbSaveDebug.setChecked(False)
                checked = False

        self._ui.leDebugLogLocation.setEnabled(checked)
