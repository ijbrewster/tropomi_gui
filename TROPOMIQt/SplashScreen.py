from PySide6.QtWidgets import QSplashScreen
from PySide6.QtCore import Qt
from PySide6.QtGui import QFont


class SO2SpashScreen(QSplashScreen):
    def drawContents(self, painter):
        painter.setPen(Qt.white)
        painter.setBrush(Qt.black)

        msg_font = QFont("Arial", 20)
        msg_font.setBold(True)
        painter.setFont(msg_font)

        msg_rect = self.rect()
        msg_rect.setY(msg_rect.y() + 100)

        painter.drawText(msg_rect, Qt.AlignHCenter | Qt.AlignVCenter, self.message())
