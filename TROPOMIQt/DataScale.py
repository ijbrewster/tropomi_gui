from PySide6.QtWidgets import (
    QDialog,
    QSpinBox,
    QWidget,
    QHBoxLayout,
    QVBoxLayout,
    QDialogButtonBox
)

from PySide6.QtCore import Signal, Qt
from superqt import QRangeSlider


class DataScale(QDialog):
    """Displays a dialog box for use in setting the scale range of SO2 data"""

    rangeChanged = Signal(int, int)
    maxChanged = Signal(int)
    minChanged = Signal(int)

    def __init__(self, min_val: int = 0, max_val: int = 20, parent: QWidget = None) -> None:
        super().__init__(parent)
        self._min_val = min_val
        self._max_val = max_val
        self._setupUI()

    def _setupUI(self):
        self.setWindowTitle("Set DU Scale range")
        self.top_layout = QVBoxLayout(self)

        self.slider_layout = QHBoxLayout()
        self.top_layout.addLayout(self.slider_layout)

        self._minDisp = QSpinBox()
        self._minDisp.setMinimum(-200)
        self._minDisp.setMaximum(99)
        self._minDisp.setValue(self._min_val)
        self._minDisp.valueChanged.connect(self._minChanged)
        self.slider_layout.addWidget(self._minDisp)

        self._valSlider = QRangeSlider(Qt.Horizontal)
        self._valSlider.setFixedWidth(300)
        self._valSlider.setMinimum(-201)
        self._valSlider.setMaximum(100)
        self._valSlider.setValue([self._min_val, self._max_val])
        self._valSlider.valueChanged.connect(self._valueChanged)
        self._valSlider.barMovesAllHandles = False
        self._valSlider.setTickPosition(QRangeSlider.TicksBelow)
        self._valSlider.setSingleStep(5)
        self._valSlider.setTickInterval(10)
        self.slider_layout.addWidget(self._valSlider)

        self._maxDisp = QSpinBox()
        self._maxDisp.setMinimum(-199)
        self._maxDisp.setMaximum(100)
        self._maxDisp.setValue(self._max_val)
        self._maxDisp.valueChanged.connect(self._maxChanged)
        self.slider_layout.addWidget(self._maxDisp)

        self.buttons = QDialogButtonBox(QDialogButtonBox.Ok)
        self.buttons.accepted.connect(self.accept)
        self.top_layout.addWidget(self.buttons)

    def _valueChanged(self, value):
        min_val, max_val = value
        if min_val != self._min_val:
            self._min_val = min_val
            self._minDisp.setValue(min_val)
            self.minChanged.emit(min_val)

        if max_val != self._max_val:
            self._max_val = max_val
            self._maxDisp.setValue(max_val)
            self.maxChanged.emit(max_val)

        self.rangeChanged.emit(min_val, max_val)

    def _minChanged(self, min_val):
        max_val = self._max_val
        val = (min_val, max_val)
        self._valSlider.setValue(val)
        self._valueChanged(val)

    def _maxChanged(self, max_val):
        min_val = self._min_val
        val = (min_val, max_val)
        self._valSlider.setValue(val)
        self._valueChanged(val)
