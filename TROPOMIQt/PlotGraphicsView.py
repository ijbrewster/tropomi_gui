"""Custom PyQtGraph PlotWidget to allow overriding functions for custom behavior"""

from enum import Enum, auto

import pyqtgraph as pg
from PySide6.QtGui import (QCursor, )
from PySide6.QtCore import (Signal,
                            QTimer,
                            QPointF,
                            Qt)

from PySide6.QtWidgets import QApplication

from .LockingPolyLineROI import LockingPolyLineROI
from .data_utils import LAYERS


class Mode(Enum):
    """ENUM of the various modes we could possibly be in"""
    ZOOM = auto()
    PAN = auto()
    SELECT = auto()
    MEASURE = auto()
    WIND = auto()
    WINDF = auto()


class PlotGraphicsView(pg.PlotWidget):
    """Custom PlotWidget subclass for SO2 Explorer specific functionality"""
    viewResized = Signal()
    selectionCleared = Signal()
    measureCleared = Signal()
    areaSelected = Signal(list, object)
    windsSelected = Signal(QPointF)
    forwardWindsSelected = Signal(QPointF)
    windsCleared = Signal()
    zoomData = Signal()
    mouseHover = Signal(QPointF)
    mouseMoved = Signal(QPointF)

    def __init__(self, parent=None, background='default', **kargs):
        super().__init__(parent, background, **kargs)
        self.down = False
        self.changeTimer = QTimer()
        self.doubleClickTimer = QTimer()
        self.clickTimer = QTimer()
        self.hoverTimer = QTimer()
        self.programChanged = False
        self.clickPoint = None
        self._selection = []
        self._measure_sel = []
        self.mouseIn = False
        self.mode = Mode.PAN

        # The point last clicked, if any
        self._clickPoint = None

        self._roi = LockingPolyLineROI(
            [], False,
            pen={'color': "#0A0", 'width': 5},
            hoverPen = {'color': (200, 200, 0), 'width': 5},
            handlePen = {'color': (75, 128, 128), 'width': 3}
        )
        self._roi.sigRegionChanged.connect(self.select_region_changed)
        self._roi.setZValue(LAYERS['selections'])

        self._measure_roi = LockingPolyLineROI(
            [], False,
            pen={'color': "#00A", 'width': 5,
                 'style': Qt.DashLine, },
            hoverPen = {'color': (200, 200, 0, 0), 'width': 5},
            handlePen = {'color': (0, 0, 170), 'width': 3}
        )
        self._measure_roi.sigRegionChanged.connect(self.select_region_changed)

        self.getPlotItem().getViewBox().addItem(self._roi)
        self.getPlotItem().getViewBox().addItem(self._measure_roi)

        self.doubleClickTimer.setSingleShot(True)
        self.clickTimer.setSingleShot(True)
        self.clickTimer.timeout.connect(self.mouseClickEvent)
        self.changeTimer.setSingleShot(True)
        self.changeTimer.timeout.connect(self._region_changed_final)
        self.hoverTimer.setSingleShot(True)
        self.hoverTimer.timeout.connect(self.mouseHoverEvent)

    def setMode(self, mode):
        """
            Set the mode of the viewport

            Parameters
            ----------
            mode: enum Mode
                The mode to set the view to
        """
        self.mode = mode
        self._roi.set_locked(mode not in (Mode.SELECT, Mode.PAN))
        self.setInteractive(mode in (Mode.ZOOM, Mode.PAN,
                                     Mode.WIND, Mode.WINDF))

    def resizeEvent(self, ev):
        super().resizeEvent(ev)

        # Probably better places to handle this, but this is easy and works for our purposes.
        # Don't emit this signal - and, therby, try drawing the map background - unless the map
        # region has a reasonable height.
        if self.size().height() >= 200:
            self.viewResized.emit()

    def enterEvent(self, ev):
        """Mark the mouse as being in the view area"""
        self.mouseIn = True

    def leaveEvent(self, ev):
        """Mark the mouse as not being in the view area"""
        self.mouseIn = False

    def mouseClickEvent(self, point=None):
        """
            Figure out what we want to do in response to a mouse Single Click,
            depending on what mode we are in.

            Parameters
            ----------
            point : tuple
                The location of the mouse click
        """
        # What we actually want to happen
        if point is None:
            point = self._clickPoint

        if self.mode is Mode.WIND:
            self.windsSelected.emit(point)
            return

        if self.mode is Mode.WINDF:
            self.forwardWindsSelected.emit(point)
            return

        if self.mode is Mode.MEASURE:
            close = False
            roi = self._measure_roi
            sel = self._measure_sel
        else:
            # 2 because this point isn't in self._selection yet, but still counts
            close = len(self._selection) >= 2
            roi = self._roi
            sel = self._selection

        # Adjust point for any offset
        offset = roi.pos()
        offset_x = offset.x()
        offset_y = offset.y()
        point.setX(point.x() - offset_x)
        point.setY(point.y() - offset_y)

        sel.append((point.x(), point.y()))

        self.programChanged = True
        roi.setPoints(sel, close)

    def get_select_region(self):
        """
            Return the currently selected area (if any)

            Returns
            -------
            state : str
                String describing the currently selected region
        """
        if self.mode is Mode.MEASURE:
            return self._measure_roi.saveState()

        return self._roi.saveState()

    def set_select_region(self, state):
        """
            Set the currently selected area

            Parameters
            ----------
            state : str
                A string describing the selected region, sutable for the
                PyQtGraph ROI setState function.
        """
        if self.mode is Mode.MEASURE:
            self._measure_roi.setState(state)

        self._roi.setState(state)

    def select_region_changed(self):
        """Set a timer to "gather" changes"""
        if self.programChanged:
            self.programChanged = False
        else:
            self.changeTimer.start(200)

    def _region_changed_final(self):
        """Get the selected area into a nicer format and emit a signal
        notifying of the changed area"""

        roi = self._measure_roi if self.mode is Mode.MEASURE else self._roi

        offset = roi.pos()
        new_sel = [(x.pos().x(), x.pos().y()) for x in roi.getHandles()]
        if self.mode is Mode.MEASURE:
            self._measure_sel = new_sel
        else:
            self._selection = new_sel

        self.areaSelected.emit(new_sel, offset)

    def get_noise_roi(self):
        """
            Returns parameters needed to figure out noise regions
        """
        offset = self._roi.pos()
        source_roi = [(x + offset.x(), y + offset.y()) for x, y in self._selection]
        bounds = self._roi.boundingRect()
        return (source_roi, bounds.width(), bounds.height())

    def _on_mouseDoubleClickEvent(self, event):
        """
            Handle double-click events
        """
        if self.mode in (Mode.WIND, Mode.WINDF):
            self.windsCleared.emit()
        elif self.mode is Mode.MEASURE:
            self.clear_measure()
        else:
            self.clear_select()

        if self.isInteractive():
            if self.mode is Mode.ZOOM:
                self.zoomData.emit()

            super().mouseDoubleClickEvent(event)
            if self.mode not in (Mode.MEASURE, Mode.WIND, Mode.WINDF):
                return

        event.accept()

    def mouseReleaseEvent(self, ev):
        """
            Handle mouse-up events. Could be a single click or a double click.
        """
        if self.isInteractive():
            super().mouseReleaseEvent(ev)

            # See if we are clicking on a wind graph
            if self.mode not in (Mode.MEASURE, Mode.ZOOM, Mode.SELECT):
                items_under_mouse = self.items(ev.pos())
                # May be empty if we don't have any wind plots yet.
                for plot in self.window().get_wind_plots():
                    if plot.curve in items_under_mouse:
                        return

            if self.mode not in (Mode.MEASURE, Mode.WIND, Mode.WINDF):
                return

        click_point = self.getPlotItem().getViewBox().mapSceneToView(ev.localPos())
        if self.clickTimer.isActive():
            # This is the second mouse-up event within a doubleClickInterval
            # Which makes it a double click, not a regular click. There is also
            # a mouseDoubleClickEvent we could capture, but since this one fires
            # for *both*, that gets complicated. Simply checking here allows us
            # to properly differentiate between single and double clicks.
            self.clickTimer.stop()

            if self._clickPoint:
                dist = abs(((click_point.x() - self._clickPoint.x()) ** 2
                            + (click_point.y() - self._clickPoint.y()) ** 2) ** .5)
            else:
                dist = -1  # assume no change in mouse position

            if dist < 100:
                # If the mouse has moved significantly between clicks, then this is
                # not a double-click, but a seperate single-click
                self._on_mouseDoubleClickEvent(ev)
                return
            else:
                # Make sure we capture the previous click
                self.mouseClickEvent(self._clickPoint)
                # Then move on to handling this click

        self._clickPoint = click_point
        self.clickTimer.start(QApplication.doubleClickInterval())

    def mouseMoveEvent(self, ev):
        """
            Set hover timer to fire when the mouse stops moving, and emit a signal.
            Otherwise, behave normally
        """
        self.hoverTimer.start(300)
        ev.ignore()
        super().mouseMoveEvent(ev)
        self.mouseMoved.emit(ev.pos())

    def mouseHoverEvent(self):
        """Emit an event with the mouse position when hovering"""
        if self.mouseIn:
            pos = self.mapFromGlobal(QCursor.pos())
            viewPos = self.getPlotItem().getViewBox().mapSceneToView(pos)
            self.mouseHover.emit(viewPos)

    def mousePressEvent(self, ev):
        """
            Mouse down. If we are interactive, behave normally.
            If not, do nothing.
        """
        if self.isInteractive():
            return super().mousePressEvent(ev)

        ev.accept()
        return True

    def clear_select(self, emit = True):
        """Clear the selected area (if any)"""
        self._roi.clearPoints()
        self._roi.setPos(0, 0)

        self._selection = []
        if emit:
            self.selectionCleared.emit()

    def clear_measure(self):
        self._measure_roi.clearPoints()
        self._measure_roi.setPos(0, 0)
        self._measure_sel = []
        self.measureCleared.emit()

    def get_select_roi(self):
        return self._roi

    def get_measure_roi(self):
        return self._measure_roi
