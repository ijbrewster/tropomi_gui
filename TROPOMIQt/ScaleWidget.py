import math

from pyproj.enums import TransformDirection

from PySide6.QtWidgets import QLabel
from PySide6.QtGui import QPainter, QPen
from PySide6.QtCore import Qt

from . import utils


class ScaleWidget(QLabel):
    def __init__(self, text = '', line_width = 2, parent = None):
        super().__init__(text, parent)
        self._max = 0
        self._pxwidth = 0
        self._line_width = line_width
        self._target_scale = 1 / 10
        self.setStyleSheet('background:rgba(255,255,255,0);')

    def update_scale(self, view_range, view_width):
        """
        The surprisingly complex method needed to *accurately* calculate scale size when
        working with projections that do not accurately translate distances (such as mercator)
        """
        xmin, xmax = view_range[0]

        # this is the latitude to do calcs at.
        y_mid = view_range[1][0] + .5 * (view_range[1][1] - view_range[1][0])

        view_x = abs(xmax - xmin)
        target_meters = view_x * self._target_scale

        # Convert to lat/lon to find actual points at this distance
        ((lat1, lat2),
         (lon1, lon2)) = utils.latlon_merc_transformer.transform([xmin, xmin + target_meters],
                                                                 [y_mid, y_mid],
                                                                 direction = TransformDirection.INVERSE)

        actual_km = utils.haversine_np(lon1, lat1, lon2, lat2)
        # Round to a nice number
        if actual_km > 10:
            target_km = round(actual_km / 10) * 10
        else:
            target_km = round(actual_km)

        # Calculate the lat/lon target_km from the starting point
        R = 6378.1  # Radius of the Earth
        brng = 1.57  # Bearing is 90 degrees converted to radians.
        lat1 = math.radians(lat1)
        lon1 = math.radians(lon1)

        lat2 = math.asin(math.sin(lat1) * math.cos(target_km / R) + math.cos(lat1) *
                         math.sin(target_km / R) * math.cos(brng))

        lon2 = lon1 + math.atan2(math.sin(brng) * math.sin(target_km / R) * math.cos(lat1),
                                 math.cos(target_km / R) - math.sin(lat1) * math.sin(lat2))

        lat2 = math.degrees(lat2)
        lon2 = math.degrees(lon2)

        # Convert lat2/lon2 back to scene coordinates so we can find the proper matching pixels
        x2, y2 = utils.latlon_merc_transformer.transform(lat2, lon2)

        new_target_meters = abs(x2 - xmin)

        effective_scale = new_target_meters / view_x
        px_width = view_width * effective_scale

        self._max = target_km
        self._pxwidth = px_width
        self.setFixedWidth(px_width)
        self.update()

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setPen(QPen(Qt.black, self._line_width))
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setRenderHint(QPainter.LosslessImageRendering)

        max_val = str(self._max) + "km"

        max_rect = painter.boundingRect(self.rect(), 0, max_val)
        text_height = max_rect.height()
        line_top = text_height + 2
        self.setFixedWidth(self._pxwidth + max_rect.width() / 2 + 2)

        bottom = self.rect().height()
        right = self._pxwidth

        painter.drawLine(1, line_top, 1, bottom - 1)
        painter.drawLine(1, bottom - 1, right - 1, bottom - 1)
        painter.drawLine(right - 1, bottom - 1, right - 1, line_top)

        painter.drawText((right - max_rect.width() / 2) - 1, text_height, max_val)
        painter.end()
