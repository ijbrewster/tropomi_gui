"""Utility functions for use at various locations"""

import logging
import math
import multiprocessing
import os
import pickle
import sys
import threading
import webbrowser

from functools import partial
from logging import config as logConfig

import numpy
import pyproj
import requests


from PySide6.QtCore import (QStandardPaths,
                            Slot,
                            QSettings)
from PySide6.QtWidgets import (QVBoxLayout,
                               QHBoxLayout,
                               QLabel,
                               QDialog,
                               QMessageBox,
                               QTextBrowser,
                               QSizePolicy,
                               QPushButton
                               )
from PySide6.QtGui import QColor

from . import __version__, APP_NAME, DOMAIN, ORGANIZATION

# Some commonly used transformations/projections


lat_lon_proj = pyproj.Proj('epsg:4326', preserve_units=False)

# NOTE: These values are for Alaska. If using this software elsewhere 
# in the world, you may need to adjust.
mercator_proj = pyproj.Proj(proj = 'merc', lon_0 = -90, preserve_units=False)
aea_string = "+proj=aea +lat_1=55 +lat_2=65 +lat_0=50 +lon_0=-154 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs"

latlon_merc_transformer = pyproj.Transformer.from_proj(lat_lon_proj,
                                                       mercator_proj)
laea_transformer = pyproj.Transformer.from_proj(mercator_proj, 'proj=laea')

OMPS_LOOKUP = {
    'PBL': 1,
    'TRL': 3,
    'TRM': 8,
    'TRU': 13,
    'STL': 20,
}

WIND_COLORS = {
    5000: '#F00',
    10000: '#00F',
    20000: '#008001',
    30000: '#0FF',
    40000: '#f0f',
    50000: '#add8e6',
}

DEFAULT_ALTS = {'TROPOMI': '7km',
                'OMPS': '8km',
                'IASI': '7km',
                'VIIRS': '1km', }

SIZE_OPTS = ((6, 4), (9, 6), (12, 8))

_update_dlg = None

MESSAGE_QUEUE: multiprocessing.Queue = None # Will be initialized by MainWindow.py


def poly_area(x_coords, y_coords):
    """
        Use the "shoelace" formula (https://en.wikipedia.org/wiki/Shoelace_formula)
        to calculate the area of multiple polygons
        based on their x and y verticies. Units are in terms of source
        coordinates. Polygons can have any number of verticies.

        Parameters
        ----------
        x_coords : list
            A list of lists of polygon x-coordinate verticies
        y_coords : list
            A list of lists of polygon y-coordinate verticies

        Returns
        -------
        area : ndarray
            A numpy array of areas for each polygon
    """
    sum1 = numpy.sum(x_coords * numpy.roll(y_coords, -1, 1), 1)
    sum2 = numpy.sum(y_coords * numpy.roll(x_coords, -1, 1), 1)

    area = .5 * numpy.absolute(sum1 - sum2)
    return area


def check_for_update(interactive = True):
    """
    Check the server for any updates, and notify the user if available

    Parameters
    ----------
    interactive : bool
        Whether or not to show dialogs on error or no update available
    """
    global _update_dlg

    settings = QSettings()

    url = f'https://apps.avo.alaska.edu/so2web/check_for_new/{__version__}'
    if sys.platform.startswith("win") or sys.platform == 'cygwin':
        url += '/WIN'

    try:
        res = requests.get(url)
    except Exception:
        return

    if not res.status_code == 200:
        if interactive:
            QMessageBox.critical(None, "Unable to check",
                                 f"An error occured while checking for updates\n{res.text}")
        return

    res = res.json()
    version = res['version']
    skip_version = settings.value('skipVersion', None)
    link = res.get('link')

    if not res['update'] or version == skip_version or not link:
        if interactive:
            QMessageBox.information(None, "No updates available",
                                    f"You have the most recent version ({__version__})")
        return

    _update_dlg = QDialog()
    _update_dlg.finished.connect(_update_dlg.deleteLater)
    title_html = ("<p><b>A new version of SO<sub>2</sub> Explorer is available!<b></p>"
                  f"<p>SO<sub>2</sub> Explorer {version} is now available -- you have {__version__}. "
                  "Would you like to download it now?</p>"
                  "<p><b>Release Notes:</b></p>")
    title = QLabel()
    title.setText(title_html)

    release_notes = QTextBrowser()
    sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.MinimumExpanding)
    release_notes.setMaximumHeight(300)
    release_notes.setMinimumHeight(0)
    release_notes.setSizePolicy(sizePolicy)
    release_notes.setHtml(res['notes'])

    button_layout = QHBoxLayout()
    skip_btn = QPushButton("Skip This Version")
    skip_btn.setAutoDefault(False)
    skip_btn.setDefault(False)
    skip_btn.clicked.connect(_update_dlg.reject)
    skip_fcn = partial(settings.setValue, 'skipVersion', version)
    skip_btn.clicked.connect(skip_fcn)
    button_layout.addWidget(skip_btn)

    button_layout.addStretch(1)

    later_btn = QPushButton("Remind Me Later")
    later_btn.setAutoDefault(False)
    later_btn.setAutoDefault(False)
    later_btn.clicked.connect(_update_dlg.reject)

    install_btn = QPushButton("Download Update")
    install_btn.setAutoDefault(True)
    install_btn.setDefault(True)
    download_func = partial(webbrowser.open, link)
    install_btn.clicked.connect(download_func)
    install_btn.clicked.connect(_update_dlg.accept)

    button_layout.addWidget(later_btn)
    button_layout.addWidget(install_btn)

    dlg_layout = QVBoxLayout(_update_dlg)
    dlg_layout.addWidget(title)
    dlg_layout.addWidget(release_notes)
    dlg_layout.addLayout(button_layout)

    _update_dlg.open()


# All valid cloud colors. Pregenerate so we can just grab the ones we need quickly.
cloud_color_lookup = {x: QColor(0, 0, 0, x) for x in range(256)}


@Slot(object, str)
def on_setText_triggered(item, value):
    """Vary basic set text handler.

    Using this, we can emit a generic signal containing a widget, and run the
    setText slot in that widget. In C++ Qt, we could probably just call the slot
    using a queued connection, but in PySide that isn't as easy, so we go this way.

    Calling this as a signal/slot means it gets run on the main thread.
    """
    item.setText(value)


@Slot(object, str)
def on_setStyleSheet_triggered(item, ss):
    """Vary basic set StyleSheet handler.

    Using this, we can emit a generic signal containing a widget, and run the
    setStyleSheet slot for that widget. In C++ Qt, we could probably just call the slot
    using a queued connection, but in PySide that isn't as easy, so we go this way.

    Calling this as a signal/slot means it gets run on the main thread.
    """
    item.setStyleSheet(ss)


def load_user_views():
    """
    load_user_views

    Find the standard location for Application Data files, make sure it exists
    (create directory if not), and load the data file containing the user-defined
    sectors (if any). Perform any needed conversion on the data in the process.

    Returns
    -------
    user_views : list
        List of dictionaries of any user-defined views
    """
    app_data_location = QStandardPaths.writableLocation(QStandardPaths.AppDataLocation)
    os.makedirs(app_data_location, exist_ok=True)

    user_view_file = os.path.realpath(os.path.join(app_data_location,
                                                   "UserViews.db"))
    try:
        user_views = pickle.loads(open(user_view_file, 'rb').read())
        if user_views and isinstance(user_views[0], (list, tuple)):
            user_views = list(map(_convert_view_format, user_views))
            save_user_views(user_views)
        user_views = [x if 'ul_lon' in x else calc_sector_bounds(x)
                      for x in user_views]

    except (FileNotFoundError, ) as err:
        user_views = []
        logging.warning("Unable to load user sectors. %s", str(err))

    return user_views


def calc_sector_bounds(sector):
    """
    Figure out the ul,ur etc corners of a sector given lat/lon from/to and
    assuming a volcview sector ratio of 1000wx800h pixels

    Parameters
    ----------
    sector : dict
        A dictionary containing sector parameters, at least longFrom, longTo,
        latFrom and latTo

    Returns
    -------
    sector : dict
        The same dictionary as provided in parameters, with the ul_lon,
        ur_lon,ll_lon, and lr_lon keys added.
    """
    center_lon = numpy.median([sector['longFrom'], sector['longTo']])
    # constant, approximate. The number of km per degree latitude.
    # Just to avoid having magic numbers floating around.
    lat_km_per_degree = 111.321
    half_pixel_width = 500  # Half the "volcview" sector pixel width of 1000
    half_pixel_height = 400  # Half the "volcview" sector height of 800

    # Due to the curvature of the earth, one km will cover more longitude at
    # higher latitudes than lower latitudes, so calculate the number of degrees
    # longitude covered by 1km at both the top and bottom latitude of the sector.
    upper_lon_km_per_degree = math.cos(math.radians(sector['latTo'])) * lat_km_per_degree  # km_per_degree
    lower_lon_km_per_degree = math.cos(math.radians(sector['latFrom'])) * lat_km_per_degree

    # Half the user-defined sector height, in degrees
    half_degree_height = (sector['latTo'] - sector['latFrom']) / 2

    # Half the user-defined sector height, converted from degrees to km
    half_km_height = half_degree_height * lat_km_per_degree

    # Figure out the km/pixel for the sector, based on the height
    pixel_size = half_km_height / half_pixel_height

    # Figure out half the user-defined sector size, in km, given the volcview
    # sector width of 1000 pixels and the above calculated pixel size (km/pixel)
    half_km_width = half_pixel_width * pixel_size

    # Figure out the number of degrees longitude covered by the specified
    # half-width in km at both the top and bottom of the sector
    # (as they will differ slightly)
    upper_deg_width = half_km_width / upper_lon_km_per_degree
    lower_deg_width = half_km_width / lower_lon_km_per_degree

    # Calculate the volcview sector longitude corners based on the center
    # longitude of the user-defined sector.
    sector['ul_lon'] = center_lon - upper_deg_width
    if sector['ul_lon'] < -180:
        sector['ul_lon'] += 360

    sector['ur_lon'] = center_lon + upper_deg_width
    if sector['ur_lon'] < -180:
        sector['ur_lon'] += 360

    sector['ll_lon'] = center_lon - lower_deg_width
    if sector['ll_lon'] < -180:
        sector['ll_lon'] += 360

    sector['lr_lon'] = center_lon + lower_deg_width
    if sector['lr_lon'] < -180:
        sector['lr_lon'] += 360

    return sector


def sector_sort_key(sector):
    """Sort key for sector definitions

    Sorts sectors by longitude, east-to-west, with large sectors having an
    offset applied to force them first.
    """
    try:
        sort_x = sector['ul_lon'] if sector['ul_lon'] < 0 else sector['ul_lon'] - 360
    except KeyError:
        # Run the calculations add the missing keys to the sector, then try again.
        sector = calc_sector_bounds(sector)
        sort_x = sector['ul_lon'] if sector['ul_lon'] < 0 else sector['ul_lon'] - 360

    large_sectors = {'Full Arc': 3000,
                     'Western AK': 2000,
                     'Eastern AK': 1000}

    sort_offset = large_sectors.get(sector['name'], 0)
    sort_x -= sort_offset
    return sort_x


def _convert_view_format(view):
    """Convert any saved user views from old style (volcview) format to new-style (dict)"""
    center_x, center_y = latlon_merc_transformer.transform(view[0], view[1])
    # These numbers are just to stay consistant with vulcView sectors
    pixel_width = 1000
    pixel_height = 800
    pixel_size = view[2]
    xmeters = pixel_size * 1000 * pixel_width
    ymeters = pixel_size * 1000 * pixel_height

    x_range = (center_x - xmeters, center_x + xmeters)
    y_range = (center_y - ymeters, center_y + ymeters)

    # convert ranges back to lat/lon
    inverse_transformer = pyproj.Transformer.from_proj(mercator_proj, lat_lon_proj)
    lat_from, lon_from = inverse_transformer.transform(x_range[0], y_range[0])
    lat_to, lon_to = inverse_transformer.transform(x_range[1], y_range[1])
    sector_dict = {'name': view[-1],
                   'latFrom': round(lat_from, 3),
                   'latTo': round(lat_to, 3),
                   'longFrom': round(lon_from, 3),
                   'longTo': round(lon_to, 3), }

    logging.debug("%s", str(sector_dict))
    return sector_dict


def save_user_views(user_views):
    """
    Save user-defined sectors to the application data file

    Parameters
    ----------
    user_views : list
        A list containing any user defined views (as dictionaries)
    """
    save_file = os.path.realpath(os.path.join(QStandardPaths.writableLocation(QStandardPaths.AppDataLocation),
                                              "UserViews.db"))
    with open(save_file, 'wb') as viewfile:
        viewfile.write(pickle.dumps(user_views))


class DataCache():
    """
    A specialized data storage class for caching SO2 data. Takes a key with a
    value attribute, and when setting/receiving data, it stores the data under
    the *current* value.

    Allows for the storage of multiple data files, for faster switching between
    them.
    """

    def __init__(self, key):
        """Key must be an object with a .value() method. it will be used to
        return the proper piece of data when this object is called."""
        if not hasattr(key, 'value'):
            raise ValueError("Key must have value attribute!")
        self.key = key
        self._my_data = {}

    def set_data(self, data):
        """
        If the key for this particular cache object exists in the internal
        dictionary, delete it, then set the data at the key value to this data.
        """
        key_val = self.key.value()
        if key_val in self._my_data:
            del self._my_data[key_val]
        self._my_data[key_val] = data

    def data(self):
        """Return the data cached in this object"""
        key_val = self.key.value()
        return self._my_data[key_val]

    def clear(self):
        """Clears the CURRENT item from the cache"""
        key_val = self.key.value()
        if key_val in self._my_data:
            del self._my_data[key_val]

    def clear_all(self):
        """Clears ALL content from the cache"""
        self._my_data = {}

    def __bool__(self):
        key_val = self.key.value()
        return key_val in self._my_data

    def __getattr__(self, name):
        key_val = self.key.value()
        if not key_val in self._my_data:
            raise AttributeError

        return getattr(self._my_data[key_val], name)

    def __contains__(self, item):
        return item in self._my_data


def haversine_np(lon1, lat1, lon2, lat2) -> int | numpy.ndarray:
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)

    All args must be of equal length.

    Less precise than vincenty, but fine for short distances,
    and works on vector math

    """
    lon1, lat1, lon2, lat2 = map(numpy.radians, [lon1, lat1, lon2, lat2])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = numpy.sin(dlat / 2.0)**2 + numpy.cos(lat1) * numpy.cos(lat2) * numpy.sin(dlon / 2.0)**2

    c = 2 * numpy.arcsin(numpy.sqrt(a))
    km = 6367 * c
    return km


# Define versions of set and clear that trigger a "change" callback
def or_set(self):
    self._set()
    for or_event, triggers in self.links:
        event_changed(triggers, or_event)


def or_clear(self):
    self._clear()
    for or_event, triggers in self.links:
        event_changed(triggers, or_event)


def event_changed(events, or_event):
    bools = [event.is_set() for event in events]
    if any(bools):
        or_event.set()
    else:
        or_event.clear()


def orify(event, link):
    # Make sets and clears trigger a "changed" callback that notifies an OrEvent
    # of a change. Copy original "set" and "clear" so they can still be called.
    if not hasattr(event, "_set"):
        event.links = []
        event._set = event.set
        event._clear = event.clear
        event.set = lambda: or_set(event)
        event.clear = lambda: or_clear(event)

    event.links.append(link)


# Create a unified OrEvent from a list of regular events
def OrEvent(*events):
    or_event = threading.Event()
    # any time a constituent event is set or cleared, update this one

    for event in events:
        orify(event, (or_event, events))

    # initialize
    event_changed(events, or_event)
    return or_event


def configure_logging():
    # Initialize logging
    if sys.platform == 'darwin': # THANK YOU Qt for this platform discrepancy
        org = DOMAIN
    else:
        org = ORGANIZATION

    settings = QSettings(org, APP_NAME)
    settings.beginGroup("preferences")
    save_debug = settings.value('saveDebug', False)

    # Windows hack to make sure we have a boolean so the logic is correct
    if type(save_debug) == str:
        save_debug = True if save_debug.lower() == 'true' else False

    log_file = settings.value('logFile', '')
    settings.endGroup()

    log_format = '%(asctime)s %(module)s %(levelname)s: %(message)s'

    config = {
        'version': 1,
        'root': {
            'handlers': ['console'],
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'level': 'INFO',
                'formatter': 'standard',
                'stream': 'ext://sys.stdout',
            },
        },
        'formatters': {
            'standard': {
                'format': log_format,
            }
        },
    }

    if save_debug and log_file:
        level = logging.DEBUG
        config['handlers']['debugLog'] = {
            'class': 'logging.FileHandler',
            'level': 'DEBUG',
            'formatter': 'standard',
            'filename': log_file,
        }
        config['root']['handlers'].append('debugLog')
    else:
        level = logging.INFO

    config['root']['level'] = level

    logging.config.dictConfig(config)
