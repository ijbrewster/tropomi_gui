"""
PandasTableModel.py - Subclass of QAbstractTableModel
Copyright © 2020 Alaska Volcano Observatory
Distributed under MIT license. See license.txt for more information
"""
import pandas
import numpy

from PySide6.QtCore import (QAbstractTableModel, QModelIndex, Qt)

HEADER_MAP = {
    'noise_mass': 'Noise (kt)',
    'noise_mean_du': 'Noise DU (mean)',
    'noise_mean_perc': 'Noise mean Per.',
    'noise_max_du': 'Noise Max (DU)',
    'noise_max_perc': 'Noise Max Per.',
    'selected_mass': 'Sel. Mass (kt)',
    'sel_mean_du': 'Sel. Mean (DU)',
    'sel_max_du': 'Sel. Max (DU)',
    'sel_mean_perc': 'Sel. Mean (per)',
    'sel_max_perc': 'Sel. Max (per)',
    'em_rate': 'EM Rate (t/day)',
    'wind_time': 'Age (h)',
    'wind_dist': 'Length (km)',
    'wind_speed': 'Wind Speed (m/s)',
    'interpolated': 'interp.',
    'adj_mass': 'Adj. Mass (kt)',
}


class PandasTableModel(QAbstractTableModel):
    """A QAbstractTableModel based subclass for displaying a pandas data frame
    in a QTableView"""

    def __init__(self, df = pandas.DataFrame(), parent = None, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self._df = df
        self._last_rows = 0
        self._last_cols = 0

    def rowCount(self, parent = QModelIndex()):
        """Return the size along the 'time' axis by default"""
        if parent.isValid() or self._df is None:
            return 0  # As per documentation

        return len(self._df)

    def columnCount(self, parent = QModelIndex()):
        if parent.isValid() or self._df is None:
            return 0

        return len(self._df.columns)

    def refreshView(self):
        """
        Make sure the results view is fully updated. Most of this shouldn't be
        needed, but I had issues getting it to show new columns/rows.
        """
        if self._df is not None:
            self._df.sort_index(inplace = True)

        rows = self.rowCount()
        cols = self.columnCount()
        self.dataChanged.emit(self.index(0, 0), self.index(rows, cols))
        if cols != self._last_cols:
            self.headerDataChanged.emit(Qt.Horizontal, 0, cols)
            self._last_cols = cols

        if rows != self._last_rows:
            self.headerDataChanged.emit(Qt.Vertical, 0, rows)
            self._last_rows = rows

    def data(self, index, role = Qt.DisplayRole):
        data = None
        if role == Qt.DisplayRole:
            col = self._df.columns[index.column()]
            if col == 'adj_mass':
                try:
                    noise_mass = self._df.iloc[index.row()]['noise_mass']
                except KeyError:
                    noise_mass = numpy.nan

                sel_mass = self._df.iloc[index.row()]['selected_mass']
                if numpy.isnan(noise_mass):
                    data = sel_mass
                else:
                    data = round(sel_mass - noise_mass, 4)
            else:
                data = self._df.iloc[index.row(), index.column()]

            if numpy.isnan(data):
                data = ''

        return str(data) if data is not None else None

    def setDataframe(self, df):
        self.layoutAboutToBeChanged.emit()
        self._df = df
        self.layoutChanged.emit()
        self.refreshView()

    def headerData(self, section, orientation, role = Qt.DisplayRole):
        data = None  # default
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                col_name = str(self._df.columns[section])
                disp_name = HEADER_MAP.get(col_name, col_name)
                data = str(disp_name)

            if orientation == Qt.Vertical:
                try:
                    data = str(self._df.index[section])
                except IndexError:
                    data = None

        return data
