from PySide6 import QtUiTools
from PySide6.QtWidgets import (QTreeWidget,
                               QTreeWidgetItem,
                               QDialog,
                               QInputDialog,
                               QLineEdit)

from PySide6.QtCore import (QFile,
                            Qt,
                            QMetaObject,
                            Slot,
                            Signal,
                            )

from PySide6.QtGui import QColor, QBrush
import os

from .viewPresets import SECTORS
from .utils import (load_user_views,
                    save_user_views)


CUR_DIRECTORY = os.path.dirname(__file__)


class SectorEditor(QDialog):
    sectors_changed = Signal()

    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)

        # Load the UI
        ui_loader = QtUiTools.QUiLoader()
        editor_win_file = QFile(os.path.join(CUR_DIRECTORY, 'SectorEditor.ui'))
        editor_win_file.open(QFile.ReadOnly)

        self._ui = ui_loader.load(editor_win_file, self)
        editor_win_file.close()

        self.setLayout(self._ui.layout())
        self.layout().setContentsMargins(12, 12, 12, 12)  # not sure why this doesn't come across, but whatever.
        self._ui.show()

        # Set the dialog parameters
        self.setWindowTitle("Sectors")
        self.resize(554, 537)

        # Populate the view
        self.init_sector_view()

        QMetaObject.connectSlotsByName(self)

        # for the IDE
        isinstance(self._ui.twSectors, QTreeWidget)

    def init_sector_view(self):
        default_views = self.create_top_item("Default Sectors")
        default_views.addChildren(self.create_tree_items(SECTORS, default_views))
        self._ui.twSectors.addTopLevelItem(default_views)

        user_views = self.create_top_item("User Sectors")
        user_views.addChildren(self.create_tree_items(load_user_views(), user_views, True))
        self._ui.twSectors.addTopLevelItem(user_views)

        for column in range(4):
            self._ui.twSectors.resizeColumnToContents(column)

    def create_top_item(self, text):
        header_brush = QBrush(Qt.lightGray)
        top_item = QTreeWidgetItem(self._ui.twSectors)
        top_item.setBackground(0, header_brush)
        top_item.setExpanded(True)
        top_item.setData(0, Qt.UserRole, False)
        top_item.setFlags(top_item.flags() & ~Qt.ItemIsSelectable)
        top_item.setText(0, text)
        top_item.setFirstColumnSpanned(True)
        return top_item

    def create_tree_items(self, items, parent, editable=False):
        view_items = []
        for view in sorted(items, key=lambda x: x['name']):
            view_item = QTreeWidgetItem(parent)
            view_item.setData(0, Qt.UserRole, editable)
            view_item.setText(0, view['name'])
            view_item.setText(1, str(view['latFrom']))
            view_item.setText(2, str(view['latTo']))
            view_item.setText(3, str(view['ul_lon']))
            view_item.setText(4, str(view['ur_lon']))
            view_items.append(view_item)
        return view_items

    @Slot(QTreeWidgetItem, QTreeWidgetItem)
    def on_twSectors_currentItemChanged(self, current, previous):
        editable = current.data(0, Qt.UserRole)
        self._ui.bRemoveSector.setEnabled(editable)
        self._ui.bEdit.setEnabled(editable)

    @Slot()
    def on_bRemoveSector_clicked(self):
        current_item = self._ui.twSectors.currentItem()
        if not current_item.data(0, Qt.UserRole):
            return  # Item is not deletable, don't do anything. Should never occur, as this button should be disabled.

        user_views = load_user_views()
        # Make a new list containing everything but the currently selected item.
        user_views = [x for x in user_views if x['name'] != current_item.text(0)]
        save_user_views(user_views)
        current_item.parent().removeChild(current_item)
        del current_item
        self.sectors_changed.emit()

    @Slot()
    def on_bEdit_clicked(self):
        current_item = self._ui.twSectors.currentItem()
        if not current_item.data(0, Qt.UserRole):
            return  # Item is not editable, don't do anything. Should never occur, as this button should be disabled.

        current_name = current_item.text(0)
        new_name, accepted = QInputDialog.getText(self, "Edit Sector", "Enter a new name for this sector:", QLineEdit.Normal, current_name)

        if not accepted or not new_name:
            return

        user_sectors = load_user_views()
        for sector in user_sectors:
            if sector['name'] == current_name:
                sector['name'] = new_name
                break

        save_user_views(user_sectors)
        current_item.setText(0, new_name)
        self.sectors_changed.emit()

    @Slot()
    def on_bDone_clicked(self):
        self.accept()


