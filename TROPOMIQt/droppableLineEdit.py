from PySide6 import QtWidgets


class droppableLineEdit(QtWidgets.QLineEdit):

    def __init__(self, parent):
        super(droppableLineEdit, self).__init__(parent)
        self.mainWindow = parent

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.acceptProposedAction()
        else:
            event.reject()

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            file = event.mimeData().urls()[0]
            file = file.toLocalFile()
            self.setText(file)
            event.acceptProposedAction()


