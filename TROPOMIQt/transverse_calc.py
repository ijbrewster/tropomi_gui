import math
import time

import numpy
import pandas
import pyqtgraph as pg
import scipy

from cachetools import cached, LRUCache
from pyproj import Geod
from PySide6.QtWidgets import QDialog, QVBoxLayout
from scipy.ndimage import gaussian_filter1d

import matplotlib.pyplot as plt
from matplotlib.backends.backend_qtagg import FigureCanvasQTAgg as FigureCanvas

from . import utils
from .data_utils import LAYERS
from TROPOMIQt import data_utils

def gen_circles(wind_data, volc):
    wind = pandas.DataFrame(wind_data, columns = ['longitude', 'latitude'])
    radii = utils.haversine_np(wind.longitude, wind.latitude, volc.longitude, volc.latitude)

    # Convert radii from a pandas series to a numpy array
    radii = radii.to_numpy()

    lat_circle = []
    lon_circle = []
    geod = Geod(ellps="WGS84")
    theta = numpy.linspace(-90, 90, 50)  # In degrees
    theta_full = numpy.linspace(-180, 180, 50)

    # Calculate the circle points
    # this is a small loop - less an 48. No need to optimize.
    for i, radius in enumerate(radii):
        # Shift the theta array to start at the desired point.
        _, bearing, _ = geod.inv(
            wind.longitude[i],
            wind.latitude[i],
            volc.longitude,
            volc.latitude
        )

        if i == len(radii) - 1:
            # Use a full circle for the final transect
            theta = theta_full

        theta_shifted = (theta + bearing) % 360

        lons, lats, _ = geod.fwd(
            numpy.full(theta_shifted.shape, volc.longitude),  # Center longitude
            numpy.full(theta_shifted.shape, volc.latitude),  # Center latitude
            theta_shifted,  # Azimuth angles
            numpy.full_like(theta_shifted, radius * 1000)  # Radius in meters
        )

        lon_circle.append(lons)
        lat_circle.append(lats)

    lat_circle = numpy.array(lat_circle)
    lon_circle = numpy.array(lon_circle)

    circles = numpy.stack((lat_circle, lon_circle), axis=1)

    return circles

def plot_circles(plot_item, circles):
    # Convert latitude/longitude to mercator
    plots = []
    for circ_lat, circ_lon in circles:
        x, y = utils.latlon_merc_transformer.transform(circ_lat, circ_lon)
        plot = plot_item.plot(x, y, pen = pg.mkPen(color = 'b'))
        plot.setZValue(LAYERS['wind'])
        plots.append(plot)

    return plots

def show_so2_traverse_plots(x_index, y_values):
    """
    Displays a new window with plots of x_index vs y_values for each circle.

    :param x_index: 1D NumPy array of x values (shared across all circles).
    :param y_values: 2D NumPy array where each row corresponds to y values for a circle.
    """
    plt.ioff()
    plt.rcParams.update({'font.size': 8})

    peak_edges = find_peak_start_stop_threshold(y_values)

    # Create a new QDialog for the new window
    dialog = QDialog()
    dialog.setWindowTitle("SO₂ Traverse Plots")

    # Set up the layout for the new window
    layout = QVBoxLayout(dialog)

    # Create a figure and axes for the plots
    n_circles = y_values.shape[0]  # Number of circles is the number of rows in y_values
    n_cols = 2
    n_rows = math.ceil(n_circles/n_cols)

    fig, axes = plt.subplots(n_rows, n_cols, figsize=(12, 15))
    fig.subplots_adjust(left=0.15, right=0.95, top=0.95, bottom=0.05, wspace=0.5, hspace=0.7)

    def cleanup():
        dialog.deleteLater()

    dialog.finished.connect(cleanup)

    # Ensure axes is always iterable, even if there's only one subplot
    if n_circles == 1:
        axes = [axes]

    axes = axes.ravel()
    # Plot each circle's data
    for i in range(len(axes)):
        if i >= n_circles:
            axes[i].set_visible(False)  # Hide unused axes if n_circles < n_rows * n_cols
            continue

        peak_start, peak_stop = peak_edges[i]
        peak_x1 = x_index[peak_start]
        peak_x2 = x_index[peak_stop]
        ax = axes[i]
        ax.plot(x_index, y_values[i])
        # Plot vertical lines at the peak start and stop positions
        ax.axvline(x=peak_x1, color='g', linestyle='--', label=f"Peak Start: {peak_x1}")
        ax.axvline(x=peak_x2, color='r', linestyle='--', label=f"Peak Stop: {peak_x2}")
        ax.grid(True)

    # Create a canvas to embed the figure into the Qt dialog
    canvas = FigureCanvas(fig)
    canvas.setParent(dialog)
    layout.addWidget(canvas)

    # Show the new window with the plot
    dialog.resize(900, 1024)  # Adjust the size as needed
    dialog.show()

def gen_circle_density_plots(data, circles):
    """
    Generate density values for data points along concentric circles.

    This function computes the density values of data points that lie along concentric circles.
    For each point on the circle, the nearest data point is found. A plot of density vs circle
    index for each circle is then returned.

    Args:
        data (tuple): A tuple containing three arrays:
            - density_data (numpy.ndarray): A 1D array of density values corresponding to data points.
            - data_lats (numpy.ndarray): A 1D array of latitude values for the data points.
            - data_lons (numpy.ndarray): A 1D array of longitude values for the data points.
        circles (numpy.ndarray): A 3D array of shape (num_circles, 2, num_points), where each circle is
                                  represented by latitude and longitude points.

    Returns:
        tuple: A tuple containing:
            - x_index (numpy.ndarray): A 1D array of indices relative to the circle points' midpoint.
            - y_values (numpy.ndarray): A 1D array of density values corresponding to the nearest
              circle points for each data point.
    """
    density_data,data_lats,data_lons = data

    n_points = circles.shape[2]  # Get the number of points per circle from the array shape
    midpoint = n_points // 2

    # Generate indices with 0 at the midpoint
    x_index = numpy.arange(-midpoint, midpoint)

    lat_diff = data_lats - circles[:, 0, :, None]
    lon_diff = data_lons - circles[:, 1, :, None]
    distances = lat_diff**2 + lon_diff**2
    idxs = numpy.argmin(distances, axis=2)

    y_values = density_data[idxs]

    return (x_index, y_values)

def find_plume(y_values):
    peak_indexs = []

    for plot in y_values:
        main_peak_idx = numpy.argmax(plot) # Should be close to zero
        _, left_bases, right_bases = scipy.signal.peak_prominences(plot, [main_peak_idx])
        peak_indexs.append((left_bases[0], right_bases[0]))

    return peak_indexs


def cache_key(*args, **kwargs):
    """Cache keys need to be hashable, and numpy arrays are not,
    so convert it into a hashable format to use as a key."""
    # Convert the numpy array argument to a hashable type
    args = tuple(tuple(arg.flatten()) if isinstance(arg, numpy.ndarray) else arg for arg in args)
    return args

@cached(LRUCache(1), key=cache_key)
def find_peak_start_stop_threshold(y_values: numpy.ndarray, smoothing_sigma: float=2, std_factor: float=1.25) -> tuple:
    """
    Find peak boundaries using a simple threshold method.
    :param y_values: Array of y-values (list of plots).
    :param smoothing_sigma: Standard deviation for Gaussian smoothing (adjust to reduce noise).
    :param threshold_factor: Fraction of the peak value (e.g., 0.5 means the peak drops to 50%).
    :return: List of tuples with (start, stop) indices for each plot.
    """
    peak_start_stop = []

    for plot in y_values:
        # Step 1: Apply Gaussian smoothing to reduce noise
        smoothed_plot = gaussian_filter1d(plot, sigma=smoothing_sigma)

        # Step 2: Find the peak value and its index (the peak)
        peak_value = numpy.max(smoothed_plot)
        peak_idx = numpy.argmax(smoothed_plot)

        # Step 3: Calculate standard deviation of the smoothed plot
        std_dev = numpy.std(smoothed_plot)

        # Step 4: Set dynamic threshold based on standard deviation
        threshold_value = peak_value - std_factor * std_dev

        # Step 5: Search for the left boundary where the signal drops below the threshold
        left_bound = peak_idx
        while left_bound > 0 and smoothed_plot[left_bound] > threshold_value:
            left_bound -= 1

        # Step 6: Search for the right boundary where the signal drops below the threshold
        right_bound = peak_idx
        while right_bound < len(smoothed_plot) - 1 and smoothed_plot[right_bound] > threshold_value:
            right_bound += 1

        # Step 6: Append the refined start and stop indices
        peak_start_stop.append((left_bound, right_bound))

    return peak_start_stop

def gen_traverses(wind_data, volc, so2data, show_plots=True):
    circles = gen_circles(wind_data, volc)
    if len(circles) == 0:
        return

    density_data = so2data['density'].to_numpy()
    data_lats = so2data['latitude'].to_numpy()
    data_lons = so2data['longitude'].to_numpy()

    #figure out a rough filter to reduce the amount of data we are working with.
    # Extract the largest circle (circle 0)
    largest_circle_lats = circles[0, 0, :]
    largest_circle_lons = circles[0, 1, :]

    # Calculate the bounding box
    lat_min, lat_max = largest_circle_lats.min(), largest_circle_lats.max()
    lon_min, lon_max = largest_circle_lons.min(), largest_circle_lons.max()

    # Filter the data points within the bounding box
    filter_mask = (data_lats >= lat_min) & (data_lats <= lat_max) & \
                  (data_lons >= lon_min) & (data_lons <= lon_max)

    data_lats = data_lats[filter_mask]
    data_lons = data_lons[filter_mask]
    density_data_cropped = density_data[filter_mask]

    x,y = gen_circle_density_plots(
        (density_data_cropped, data_lats, data_lons),
        circles
    )

    if show_plots:
        show_so2_traverse_plots(x, y)

    # find the start/stop index of the peak reading, in circle point indexes.
    peak_edges = numpy.asarray(find_peak_start_stop_threshold(y))

    # Reshape circles to yeild a single list of lat/lon pairs for each circle,
    # rather than seperate latitude/longitude lists.
    circle_latlons = circles.transpose(0, 2, 1)  # Shape: (num_circles, num_points, 2)

    # Due to the way numpy broadcasting works, we need to use np.arange
    # to create an array of indices for the first dimension, but in columunar format.
    num_circles = circles.shape[0]
    broadcast_indexes = numpy.arange(num_circles).reshape(num_circles, 1)

    #  Convert the circle point indexes to lat/lon points.
    traverse_points = circle_latlons[broadcast_indexes, peak_edges]

    return (circles, traverse_points)


def calc_so2_mass_traverse(traverse_points, so2data):

    # Pull the relevant datasets from so2data
    grid_index = so2data.attrs.get('grid_index')
    cell_polygons = so2data.attrs.get('grid_geometries')
    pixel_areas = so2data['area']
    density_data = so2data['density'].to_numpy()

    t1 = time.time()

    traverse_data_idxs = data_utils.find_data_lines(
        traverse_points,
        grid_index,
        cell_polygons
    )
    print(f"Got data indexes in: {time.time() - t1}")

    max_len = max(len(sublist) for sublist in traverse_data_idxs)

    # Create masked arrays for so2 and density
    traverse_so2 = numpy.ma.empty((len(traverse_data_idxs), max_len), dtype=density_data.dtype)
    traverse_so2.mask = True

    traverse_areas = numpy.ma.empty((len(traverse_data_idxs), max_len), dtype=pixel_areas.dtype)
    traverse_areas.mask = True

    for i, idxs in enumerate(traverse_data_idxs):
        traverse_so2[i, :len(idxs)] = numpy.take(density_data, idxs)
        traverse_areas[i, :len(idxs)] = numpy.take(pixel_areas, idxs)

    so2_mass = (traverse_areas * traverse_so2) * 64 #  Still per-data point
    # convert from grams to kt
    so2_mass /= 1e9

    mass_ts = so2_mass.sum(axis=1).round(4).tolist()

    # Reverse the list so it goes from less time to more time.
    mass_ts.reverse()

    return mass_ts


def traverses_to_poly(traverse_points, first_point=None):
    #  Seperate out the first and second point into seperate arrays
    # The second point array needs to be reversed so we can go around
    # the perimiter of the polygon.
    first_points = traverse_points[:, 0]  # First point (lat, lon) from each circle
    second_points = traverse_points[:, 1][::-1]  # Second point (lat, lon) from each circle, in reverse order

    # Concatenate first points and second points. This gives a list of points
    # defining the perimiter of a polygon
    flattened_result = numpy.concatenate([first_points, second_points])

    # Break latitude and longitude out into seperate lists for conversion to mercator.
    select_lats = flattened_result[:, 0]
    select_lons = flattened_result[:, 1]

    if first_point is not None:
        #  Add the "zero" point from the first circle to the list
        zero_lon, zero_lat = first_point
        select_lats = numpy.concatenate(([zero_lat], select_lats))
        select_lons = numpy.concatenate(([zero_lon], select_lons))

    poly_x, poly_y = utils.latlon_merc_transformer.transform(select_lats, select_lons)
    points = numpy.column_stack((poly_x, poly_y))

    return points
