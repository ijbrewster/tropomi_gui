"""load_thread.py

QThread subclass to process the loaded data in a seperate thread so as to not
block the main (GUI) thread
"""

import logging
import time

from multiprocessing import cpu_count
from concurrent.futures import ThreadPoolExecutor

import numpy
import xarray
from scipy import stats

from PySide6.QtGui import QPainterPath
from PySide6.QtWidgets import (QMessageBox,
                               )
from PySide6.QtCore import (Signal,
                            QThread,
                            QPointF,
                            QDataStream,
                            QByteArray,
                            QIODevice)

from .utils import (latlon_merc_transformer,
                    laea_transformer,
                    poly_area)


def _generate_path(coord):
    path = QPainterPath(QPointF(*coord[0]))
    path.lineTo(*coord[1])
    path.lineTo(*coord[2])
    path.lineTo(*coord[3])
    path.closeSubpath()
    return path


def _generate_path_mt(coords, start_idx, dest):
    for idx, coord in enumerate(coords):
        path = QPainterPath(QPointF(*coord[0]))
        path.lineTo(*coord[1])
        path.lineTo(*coord[2])
        path.lineTo(*coord[3])
        path.closeSubpath()
        dest[start_idx + idx] = path
#    return path


def deserialze_array(dest, start_idx, values):
    for idx, spath in enumerate(values):
        QDataStream(QByteArray(spath)) >> dest[idx + start_idx]


def _generate_path_mp(coords):
    res = numpy.empty((coords.shape[0]), dtype = object)
    for idx, coord in enumerate(coords):
        path = QPainterPath(QPointF(*coord[0]))
        path.lineTo(*coord[1])
        path.lineTo(*coord[2])
        path.lineTo(*coord[3])
        path.closeSubpath()

        buff = QByteArray()
        out_stream = QDataStream(buff, QIODevice.WriteOnly)
        out_stream << path
        res[idx] = buff.data()

    return res


def _build_filter(data, def_filt, stacking):
    """
        Create a boolean array of data to keep based on specified filters

        Parameters
        ----------
        data : dict
            Dictionary of SO2 data for which to generate a filter
        filters: dict
            Dictionary of filter definitions

        Returns
        -------
        filter : ndarray
            Boolean array of data to keep based on user-specified filters


    """
    if def_filt['lng_from'] is not None:
        lng_from = [def_filt['lng_from'], ]
        lng_to = [def_filt['lng_to'], ]

        # The system is designed to work with values from -360 to 0 rather than +/-180.
        if lng_from[0] > 0 and lng_from[0] > lng_to[0]:
            lng_from[0] = lng_from[0] - 360

        # Handle "across the dateline" input as numbers more negitive than 180
        if lng_from[0] < -180:
            lng_from.append(lng_from[0] + 360)
            lng_to.append(180)

            # lngFrom is now only -180
            lng_from[0] = -180
    else:
        # Longitude must be between +/- 180
        lng_from = [-180]
        lng_to = [180]

    # Since we know we (most likely) have NaN values, go ahead and ignore warnings about them.
    with numpy.errstate(invalid='ignore'):
        filters = []
        errors = []

        # Valid SO2/density
        # Applying the density filter inherently implies valid SO2, so no need to seperate the two
        # We always filter out NaN values, regardless of other filters applied.
        if def_filt['density'] is not None:
            def_filt['density'] /= 2241.15  # Convert DU filter to density

            filters.append(numpy.logical_and(~numpy.isnan(data['SO2_column_number_density']),
                                             data['SO2_column_number_density'] >= def_filt['density']))
        else:
            filters.append(~numpy.isnan(data['SO2_column_number_density']))

        # latitude
        if def_filt['lat_from'] is not None:
            filters.append(numpy.logical_and(data['latitude'] >= def_filt['lat_from'],
                                             data['latitude'] <= def_filt['lat_to']))
        else:
            # latitude must be in range +/- 90 to be valid
            filt = numpy.logical_and(data['latitude'] >= -90,
                                     data['latitude'] <= 90)
            if not filt.all():
                filters.append(filt)

        # Longitude
        if lng_from is not None:
            # If not none, then it is a list
            lng_filters = []
            for start, stop in zip(lng_from, lng_to):
                lng_filters.append(numpy.logical_and(data['longitude'] >= start,
                                                     data['longitude'] <= stop))
            if len(lng_filters) > 1:
                lng_filter = numpy.logical_or.reduce(lng_filters)
            else:
                lng_filter = lng_filters[0]

            if not lng_filter.all():
                filters.append(lng_filter)

        # cloud
        if def_filt['cloud_fraction'] is not None:
            filters.append(data['cloud_fraction'] < (def_filt['cloud_fraction'] / 100))

        if not stacking:
            # Some filters only get applied here if not stacking, due to
            # being applied to the individual files before stacking when
            # stack is selected

            # Validity
            if def_filt['validity'] is not None:
                try:
                    valid_filter = data['SO2_column_number_density_validity'] >= def_filt['validity']
                    if not valid_filter.all():
                        filters.append(valid_filter)
                except KeyError:
                    errors.append("Validity")

            # Sensor Zenith angle
            if def_filt.get('sensor_zenith') is not None:
                try:
                    filters.append(data['sensor_zenith_angle'] <
                                   def_filt['sensor_zenith'])
                except KeyError:
                    errors.append("Sensor Zenith")

            # Solar Zenith angle
            try:
                if def_filt['solar_zenith']:
                    filters.append(data['solar_zenith_angle'] <
                                   def_filt['solar_zenith'])
            except KeyError:
                # No solar zenith angle data in file, can't filter on it
                errors.append("Solar Zenith")

    err = '\n'.join(errors) if errors else None

    if len(filters) == 1:
        keep_records = filters[0]
    else:
        keep_records = numpy.logical_and.reduce(filters)

    return (keep_records, err)


class ProcessorThread(QThread):
    """QThread subclass to process the data"""
    data_ready = Signal((object, ))
    no_data = Signal()
    show_msg = Signal((QMessageBox.Icon, str, str, str))

    def __init__(self, file_data, parent=None):
        super().__init__(parent)
        self.so2data = file_data

    def run(self):
        start = time.time()

        # get filter values
        user_filters = self.parent().get_filters()

        keep_records, err = _build_filter(self.so2data, user_filters,
                                          self.parent().is_stack())
        if err:
            self.show_msg.emit(QMessageBox.Warning,
                               "WARNING: Unable to apply one or more filters",
                               f"You can avoid this dialog by unchecking the following filters:\n\n{err}",
                               None)

        if not keep_records.any():  # not keeping any data
            self.no_data.emit()
            return

        keep_records = xarray.DataArray(keep_records, dims=['time'])
        self.so2data = self.so2data.where(keep_records, drop = True)

        # Figure out the pixel bounds
        pixel_bounds = numpy.stack((self.so2data['latitude_bounds'],
                                    self.so2data['longitude_bounds']),
                                   axis=-1)
        if pixel_bounds.size == 0:
            self.no_data.emit()
            return

        x_merc_bounds = pixel_bounds[:, :, 1].reshape(pixel_bounds[:, :, 1].size)
        y_merc_bounds = pixel_bounds[:, :, 0].reshape(pixel_bounds[:, :, 0].size)
        if not self.parent().is_stack():
            # If we aren't stacking, then the pixel_bounds are in lat/lon,
            # not mercator, and need to be translated. We need *both* mercator
            # (for plotting) and laea (for calculations)
            x_merc_bounds, y_merc_bounds = latlon_merc_transformer.transform(y_merc_bounds, x_merc_bounds)

        x_laea, y_laea = laea_transformer.transform(x_merc_bounds, y_merc_bounds)

        # Recombine transformed x and y into corner coords
        x_laea = x_laea.reshape(int(x_laea.size / 4), 4)
        y_laea = y_laea.reshape(int(y_laea.size / 4), 4)

        _t1 = time.time()
        laea_areas = poly_area(x_laea, y_laea)
        _t2 = time.time()
        logging.debug("Calculated areas without pool in: %f seconds", _t2 - _t1)

        self.so2data['area'] = ('time', laea_areas)
        self.so2data['sizes'] = ('time', (2 * numpy.sqrt(laea_areas / numpy.pi)))

        x_merc_bounds = x_merc_bounds.reshape(int(x_merc_bounds.size / 4), 4)
        y_merc_bounds = y_merc_bounds.reshape(int(y_merc_bounds.size / 4), 4)
        merc_pixel_bounds = numpy.stack([x_merc_bounds, y_merc_bounds], axis=2)

        # To get the shape of each pixel, shift each one to 0,0 lower left bounding box
        shifted_coords = merc_pixel_bounds - numpy.min(merc_pixel_bounds, axis=1)[:, None, :]
        # We have to do min twice to get the single min value for each group of corner points
        # If we only did it once, X and Y would be scaled seperately, distorting the shape.
        scale_factors = numpy.max(numpy.max(shifted_coords, axis=1), axis=1)
        self.so2data['scale_sizes'] = ('time', scale_factors)

        # Scale each pixel to fit within -0.5 - +0.5
        scaled_coords = (shifted_coords * (1 / scale_factors[:, None, None])) - .5
        # "Center" the scaled coordinates so the paths correctly represent the points
        scaled_coords -= (((numpy.max(scaled_coords, axis=1) - numpy.min(scaled_coords, axis=1)) - 1) / 2)[:, None, :]

        pixel_paths = numpy.empty((scaled_coords.shape[0], ), dtype = object)
        start_idx = 0
        with ThreadPoolExecutor() as executor:
            for chunk in numpy.array_split(scaled_coords, cpu_count()):
                executor.submit(_generate_path_mt, chunk, start_idx, pixel_paths)
                start_idx += chunk.shape[0]
                # threads.append(thread)

            executor.shutdown()

        self.so2data['pixel_paths'] = ('time', pixel_paths)

        calc_x, calc_y = latlon_merc_transformer.transform(numpy.asarray(self.so2data['latitude']),
                                                           numpy.asarray(self.so2data['longitude']))
        # Save the transformed coordinates for later use by the calculation functions
        self.so2data['lon'] = ('time', calc_x)
        self.so2data['lat'] = ('time', calc_y)

        xy_coords = numpy.stack([calc_x, calc_y], axis=1)
        self.so2data['xy_coords'] = (['time', 'xy'], xy_coords)

        # Number of moles in each pixel
        pixel_moles = laea_areas * self.so2data['SO2_column_number_density']
        pixel_mass = (pixel_moles) * 64  # in grams

        self.so2data['mass'] = ('time', pixel_mass.data)

        du_val = self.so2data['SO2_column_number_density'] * 2241.15
        self.so2data['du'] = ('time', du_val.data)

        percentile = stats.rankdata(du_val) / du_val.size
        self.so2data['percentile'] = ('time', percentile)

        # If stacking, we won't have validity. Could be implemented as check for stacking, but this
        # will catch any reason we don't have validity data
        try:
            validity_data = self.so2data['SO2_column_number_density_validity']
        except KeyError:
            validity_data = numpy.full_like(self.so2data['SO2_column_number_density'],
                                            None)

        self.so2data['validity'] = ('time', validity_data.data)

        try:
            cloud_data = self.so2data['cloud_fraction']
        except KeyError:
            cloud_data = numpy.full_like(self.so2data['SO2_column_number_density'],
                                         numpy.nan)

        self.so2data['cloud'] = ('time', cloud_data.data)
        self.so2data['density'] = self.so2data['SO2_column_number_density']

        logging.debug("Processed data in: %f seconds", time.time() - start)
        self.data_ready.emit({'success': True, 'data': self.so2data})
