"""
iasi.py - File import definition file for IASI data files
Copyright © 2020 Alaska Volcano Observatory
Distributed under MIT license. See license.txt for more information

--ALPHA-ALPHA-ALPHA--
Due to a total lack of real-world testing, this file format is extreamly ALPHA and
probably does not work at all.
"""
__TYPE__ = 'IASI'

IASI_ALTS = ['INT', '30km', '25km', '19km', '16km', '13km', '10km', '7km', '5km']


def _process_iasi_data(coda_file, options):
    """
        Some tweaking specific to IASI data files to bring them into full
        compliance with our "expected" format
    """
    if 'so2_column' in options:
        so2_col_idx = IASI_ALTS.index(options['so2_column'])
    else:
        so2_col_idx = 0  # Default, estimated plume height

    # Pull out the requested SO2 Density column
    # The system expects values in moles/m^2, not DU, so divide by 2241.15 to convert.
    coda_file._file_data['SO2_column_number_density'] = (
        coda_file._file_data['SO2_VCD'][:, so2_col_idx]) / 2241.15

    # And delete the "full" data that we don't need
    del coda_file._file_data['SO2_VCD']

    # Add this to the file def so it gets binned properly
    #_file_def['fields']['SO2_column_number_density'] = {'bin': True, }


DEF = {
    'INFO': {
        'nDims': 1,  # Number of expected dimensions in latitude/longitude products
        'binRadius': 1e5,
        'file_time': {},
        'point_time': {'path': 'GEOLOCATIONS/time'},
        'post_processor': _process_iasi_data,
        'pixel_size': 12,  # In km
    },
    'GROUPS': [
        {
            'GROUP': 'GEOLOCATIONS',
            'FIELDS': [
                {
                    'NAME': 'latitude',
                    'bin': False,
                },
                {
                    'NAME': 'longitude',
                    'bin': False,
                },
            ],
        },
        {
            'GROUP': 'SO2',
            'FIELDS': [
                {
                    'NAME': 'SO2_VCD',
                }
            ],
        }
    ]
}
