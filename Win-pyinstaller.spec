# -*- mode: python ; coding: utf-8 -*-

block_cipher = None
from PyInstaller.utils.hooks import copy_metadata

meta=copy_metadata('pyproj')

a = Analysis(['SO2 Explorer.py'],
             pathex=['C:\\Users\\Israel\\Development-Win\\tropomi_gui\\env\\Lib\\site-packages\\shiboken6'],
             binaries=[],
             datas=[
	       ('TROPOMIQt/*.ui','TROPOMIQt/'),
	       ('SupportFiles\\SO2Explorer.icns','SupportFiles/'),
	       ('SupportFiles/SplashScreen.png','SupportFiles/'),
	       ('SupportFiles/*.svg','SupportFiles/'),
	       ('SupportFiles/WorldMap.gz','SupportFiles/'),
	       ('TROPOMIQt/file_formats*','TROPOMIQt/file_formats/'),
	       ('env/lib/site-packages/pyqtgraph','pyqtgraph'),
meta[0]
	     ],
             hiddenimports=[
	         'PySide6.QtPrintSupport','pyproj._datadir','pyproj.datadir',
             'pkg_resources.py2_warn','scipy.special.cython_special','TROPOMIQt.file_formats.tropomi',
             'TROPOMIQt.file_formats.omps','TROPOMIQt.file_formats.iasi','cmath',
             'pyqtgraph.graphicsItems.ViewBox.axisCtrlTemplate_PySide6',
             'pyqtgraph.graphicsItems.PlotItem.plotConfigTemplate_PySide6',
             'pyqtgraph.imageview.ImageViewTemplate_PySide6',
             'numcodecs.blosc','numcodecs.blosc.Blosc','numcodecs.compat_ext','shapely._geos'],
             hookspath=[],
             runtime_hooks=[],
             excludes=['FixTk', 'tcl', 'tk', '_tkinter', 'tkinter', 'Tkinter'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='SO2 Explorer',
          debug=False,
          bootloader_ignore_signals=True,
          strip=True,
          icon="SupportFiles/SO2Explorer.ico",
          upx=True,
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='SO2 Explorer')
