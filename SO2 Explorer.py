"""
SO2 Explorer.py - Main entry point for launching SO2 Explorer
Copyright © 2020 Alaska Volcano Observatory
Distributed under MIT license. See license.txt for more information
"""

import os

#os.environ['QT_MAC_WANTS_LAYER'] = "1"
os.environ['QT_MAC_USE_NSWINDOW'] = "1"

import sys

from datetime import datetime, timedelta

from multiprocessing import freeze_support
freeze_support()

from PySide6.QtWidgets import QApplication
from PySide6.QtGui import QIcon, QPixmap
from PySide6.QtCore import Qt, QCoreApplication

import TROPOMIQt
from TROPOMIQt import (MainWindow,
                       SplashScreen,
                       utils)


if __name__ == "__main__":
    if getattr(sys, 'frozen', False):
        # fix the QTWEBENGINEPROCESS_PATH enviroment variable
        MAIN_DIR = sys._MEIPASS

        os.environ['QT_PLUGIN_PATH'] = os.path.join(MAIN_DIR, "PySide6\\plugins")
    else:
        MAIN_DIR = os.path.dirname(__file__)
        os.environ["CODA_DEFINITION"] = os.path.realpath(os.path.join(MAIN_DIR,
                                                                      "share/coda/definitions"))

    QCoreApplication.setAttribute(Qt.AA_ShareOpenGLContexts)

    qt_app = QApplication(sys.argv)
    qt_app.setApplicationDisplayName(TROPOMIQt.APP_NAME)
    qt_app.setApplicationName(TROPOMIQt.APP_NAME)
    qt_app.setOrganizationName(TROPOMIQt.ORGANIZATION)
    qt_app.setOrganizationDomain(TROPOMIQt.DOMAIN)

    splash_img = QPixmap(os.path.join(MAIN_DIR, "SupportFiles/SplashScreen.png"))
    splash = SplashScreen.SO2SpashScreen(splash_img)
    splash.setWindowFlag(Qt.WindowStaysOnTopHint, True)
    splash.showMessage("Loading map images...")
    splash.show()
    qt_app.processEvents()

    ICON_PATH = os.path.realpath(os.path.join(MAIN_DIR, 'SupportFiles/SO2Explorer.icns'))
    app_icon = QIcon(ICON_PATH)
    qt_app.setWindowIcon(app_icon)

    main_window = MainWindow.mainWindow(splash)
    main_window.setWindowIcon(app_icon)

    settings = main_window.settings

    mapWinGeom = settings.value('mapWin/geometry', None)
    if mapWinGeom is not None:
        main_window.restoreGeometry(mapWinGeom)

    resultsGeom = settings.value('resultsWin/geometry', None)
    if resultsGeom is not None:
        main_window.setResultsWinGeometry(resultsGeom)

    if settings.value('resultsWin/visible', True):
        main_window.showResults()

    main_window.show()
    splash.finish(main_window)

    # Check for updates, if scheduled
    udcheck_enabled = settings.value('checkForUpdates', True)
    if udcheck_enabled:
        last_check = settings.value('lastUDCheck', '1000-01-01')
        check_frequency = int(settings.value('UDCheckDays', 7))
        last_check = datetime.strptime(last_check, '%Y-%m-%d')
        days_since = datetime.today().date() - last_check.date()
        if days_since >= timedelta(days = check_frequency):
            utils.check_for_update(False)
            settings.setValue('lastUDCheck',
                              datetime.today().strftime('%Y-%m-%d'))

    res = qt_app.exec()

    # Cleanly exit the background threads
    utils.MESSAGE_QUEUE.put("QUIT")
    main_window._quit_event.set()

    main_window._render_thread.join()
    main_window._quick_calc_thread.join()
    main_window._messenger_thread.join()

    # Save some window positions
    results_geom, results_visible = main_window.getResultsWinGeometry()
    settings.setValue('resultsWin/geometry', results_geom)
    settings.setValue('resultsWin/visible', results_visible)
    settings.setValue('mapWin/geometry', main_window.saveGeometry())

    sys.exit(res)
