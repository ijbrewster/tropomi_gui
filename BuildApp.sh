#!/bin/bash
cd /Users/israel/Development/tropomi_gui
source bin/activate

/Users/israel/Development/tropomi_gui/env/bin/pyinstaller /Users/israel/Development/tropomi_gui/SO2\ Explorer-Mac.spec

#VERSION=`bin/python -c "from TROPOMIQt import __version__; print(__version__)"`
#echo "Setting bundle version to $VERSION"
#defaults write "/Users/israel/Development/TROPOMI/dist/SO2 Explorer.app/Contents/Info.plist" CFBundleShortVersionString $VERSION
echo "Code Signing application"
codesign --force --deep -s "Developer ID Application" dist/SO2\ Explorer*.app
echo "Build Complete"
deactivate
