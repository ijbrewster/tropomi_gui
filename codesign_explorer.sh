#!/bin/bash

# The codesigning done by pyinstaller doesn't work correctly with the shapely
# package due to where pyinstaller places the files - if you try to move the
# pyinstaller created application to a different machine, you may get a message
# stating that the application is damaged, and the OS will refuse to launch the 
# application.
#
# This script fixes this by moving the shapely directories into the expected location
# and re-signing the modified package.
WORK_DIR=`pwd`
echo "The current working directory is: $PWD"
APP="$1"

echo "Application specified is: '$APP'"

if [ ! -e "$APP" ]; then
    echo "Unable to find specified application: '$APP'"
    exit 1
fi

cd "$APP"/Contents/MacOS
echo "Moving shapely directory to correct location"
mv shapely ../Resources/
echo "linking shapely directory into MacOS directory"
ln -s ../Resources/shapely .
cd ../Resources
echo "linking libgeos into Resources directory"

ln -s ../MacOS/libgeos*.dylib .
#ln -s ../MacOS/libgeos_c.1.17.1.dylib .
echo "Changing directory to: '$WORK_DIR'"
cd "$WORK_DIR"
echo "Codesigning specified application while in directory '`pwd`'"
codesign -s 'Developer ID Application' --force --all-architectures --timestamp --deep "$APP"
