# SO<sub>2</sub> Explorer Readme

## Introduction

SO<sub>2</sub> Explorer is a python based application for the visualization and processing of satelite based SO<sub>2</sub> data from the TROPOMI or OMPS sensors. Features include:

- Data visualization at different atmospheric levels
- Automatic noise calculation and removal
- High-quality PDF map image generation
- Automatic wind trajectory retrevial
- Data interpolation between two wind levels
- Emission rate calculation based on wind trajectories

## Installation

SO<sub>2</sub> Explorer runs as a python script, at time of writing tested with Python 3.11. Package requirements are listed in requirements.txt, and may be installed using pip. It is recommended that this be done in a virtual enviroment.

To install SO<sub>2</sub> Explorer, you clone this repository using git, and then run the following commands from within the repository top-level directory to set up a virtual enviroment with the required packages:

```sh
python3.11 -m venv so2env
source so2env/bin/activate
pip install --upgrade pip wheel # optional
pip install -r requirements.txt
```

> [!NOTE]
> Additional system level libraries may be required to install one or more of the python packages. If you get an error during install, look at the error text for indication of a missing header file, and if found install the relevant system level packages. A more advanced package manager, such as conda, may also be used to install all requirements, including system level packages

If you wish to run tests, the `pytest` module will be additionally required

Once the runtime enviroment is set up, SO<sub>2</sub> Explorer can be run by simply activating the virtual enviroment (if used) and running the `SO2 Explorer.py` script

```sh
source so2env/bin/activate # Assuming setup as above
python "SO2 Explorer.py"
```

If a double-clickable application package is desired, `pyinstaller` build scripts for mac and windows are included in this repository.