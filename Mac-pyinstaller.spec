# -*- mode: python ; coding: utf-8 -*-
import sys
sys.path.append('/Users/israel/Development/tropomi_gui')
sys.path.append('Users/isrel/Development/tropomi_gui/lib/python3.10/site-packages/shiboken6/')
sys.modules['FixTk'] = None

from PyInstaller.utils.hooks import copy_metadata

try:
	from TROPOMIQt import __version__ as ver
except ModuleNotFoundError:
	ver = "2.4.0-beta"
block_cipher = None

meta=copy_metadata('pyproj')
print("*****META IS",meta)
a = Analysis(['SO2 Explorer.py'],
             pathex=['/Users/israel/Development/tropomi_gui/env/lib/python3.10/site-packages/shiboken6/'],
             binaries=[('env/lib/python3.10/site-packages/PySide6/qt/plugins/','.')],
             datas=[('TROPOMIQt/*.ui','TROPOMIQt/'),
             	('SupportFiles/SO2Explorer.icns','SupportFiles/'),
             	('SupportFiles/SplashScreen.png','SupportFiles/'),
             	('SupportFiles/*.svg','SupportFiles/'),
             	('SupportFiles/WorldMap.gz','SupportFiles/'),
             	('TROPOMIQt/file_formats/*','TROPOMIQt/file_formats/'),
             	('env/lib/python3.10/site-packages/pyqtgraph','pyqtgraph'),
		meta[0]],
             hiddenimports=['PySide6.QtPrintSupport','pyproj._datadir','pyproj.datadir',
             'pkg_resources.py2_warn','scipy.special.cython_special','TROPOMIQt.file_formats.tropomi',
             'TROPOMIQt.file_formats.omps','TROPOMIQt.file_formats.iasi','TROPOMIQt.file_formats.viirs','cmath',
             'numcodecs.blosc','numcodecs.blosc.Blosc','numcodecs.compat_ext',
             'pyproj._compat', 'shapely._geos'],
             hookspath=[],
             runtime_hooks=[],
             excludes=['FixTk', 'tcl', 'tk', '_tkinter', 'tkinter', 'Tkinter'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

a.binaries = a.binaries + [('libgeos_c.dylib', '/Users/israel/Development/tropomi_gui/env/lib/python3.10/site-packages/shapely/.dylibs/libgeos_c.1.17.1.dylib','BINARY')]
a.binaries = a.binaries + [('libgeos.dylib', '/Users/israel/Development/tropomi_gui/env/lib/python3.10/site-packages/shapely/.dylibs/libgeos.3.11.1.dylib','BINARY')]

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='SO2 Explorer',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='SO2 Explorer')
app = BUNDLE(coll,
             name=f'SO2 Explorer {ver}.app',
             icon='SupportFiles/SO2Explorer.icns',
             bundle_identifier='edu.alaska.avo.so2explorer',
             info_plist={
             	"NSHighResolutionCapable":True,
             	"NSPrincipleClass":"NSApplication",
             	"CFBundleShortVersionString":ver,
             })
